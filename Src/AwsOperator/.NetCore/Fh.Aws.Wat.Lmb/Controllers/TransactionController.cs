//using System;
//using Fh.Vvn.Okt.Lmb.Data;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using Amazon.Extensions.Configuration.SystemsManager;
//using Microsoft.AspNetCore.Cors;

//namespace Fh.Vvn.Okt.Lmb.Controllers
//{
//    [Route("[controller]")]
//    public class TransactionController : Controller
//    {
//        private IConfiguration _mConfiguration;
//        private ILogger _mLogger;
//        private ITransactionAdapter _mtransactionAdapter;

//        public TransactionController(ITransactionAdapter transactionAdapter, IConfiguration configuration, ILogger<TransactionController> logger)
//        {
//            _mtransactionAdapter = transactionAdapter;
//            _mConfiguration = configuration;
//            _mLogger = logger;

//        }

//        [EnableCors]
//        [HttpGet("List/{koperNummer}/{datumTransacties}/{vestigingCode?}")]
//        public ActionResult<string> ListTransactions(string koperNummer, DateTime datumTransacties, [FromQuery] string[] koperPlaat, int? vestigingCode = null)
//        {
//            _mConfiguration.WaitForSystemsManagerReloadToComplete(TimeSpan.FromSeconds(5));

//            //TODO: Debug logging
//            Response.ContentType = "application/json";

//            //TODO: Pass claim kopernummer && requested kopernummer to security function. new class???????
//            if (!_mtransactionAdapter.IsRequestAllowed(koperNummer, vestigingCode))
//                return BadRequest(string.Format("{0} mag niet is transacties opvragen voor kopernummer {1}", koperNummer, koperNummer));
//            //Microsoft.AspNetCore.Mvc.

//            var result = _mtransactionAdapter.ListTransactionsByDate(koperNummer, datumTransacties, vestigingCode);
//            return Ok(result);
//        }

//        [EnableCors]
//        [HttpGet("Pull/{koperNummer}/{datumTransacties}/{pullFromTime}/{vestigingCode?}")]
//        public ActionResult<string> PullTransactions(string koperNummer, DateTime datumTransacties, string pullFromTime, [FromQuery] string[] koperPlaat, int? vestigingCode = null)
//        {
//            _mConfiguration.WaitForSystemsManagerReloadToComplete(TimeSpan.FromSeconds(5));

//            //TODO: Debug logging
//            Response.ContentType = "application/json";
//            //TODO: Pass claim kopernummer && requested kopernummer to security function. new class???????
//            if (!_mtransactionAdapter.IsRequestAllowed(koperNummer, vestigingCode))
//                return BadRequest(string.Format("{0} mag niet is transacties opvragen voor kopernummer {1}", koperNummer, koperNummer));

//            var result = _mtransactionAdapter.ListTransactionsByDateAndTime(koperNummer, datumTransacties, pullFromTime, vestigingCode);
//            return Ok(result);
//        }
//    }
//}

//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Converters;
//using Newtonsoft.Json.Linq;
//using RfhCore.Configuration;
//using RfhCore.Logging.Abstractions;
//using System;

//namespace RfhCore.Logging
//{
//    public static class StringExtensions
//    {
//        public static bool IsJson(this string input)
//        {
//            input = input.Trim();
//            bool IsWellFormed(string inputString)
//            {
//                try
//                {
//                    JToken.Parse(input);
//                }
//                catch
//                {
//                    return false;
//                }
//                return true;
//            }

//            return ((input.StartsWith("{") && input.EndsWith("}")) || (input.StartsWith("[") && input.EndsWith("]")))
//                && IsWellFormed(input);
//        }
//    }


//    /// <summary>
//    /// Formats a logging line in Json. This can be used for structured logging.
//    /// </summary>
//    public class RfhLogLineJsonFormatter : IRfhLogLineFormatter
//    {
//        protected string mApplicationName;
//        protected string mStage;
//        protected readonly JsonSerializerSettings mSerializerSettings;

//        public RfhLogLineJsonFormatter(IConfiguration configuration)
//        {
//            mApplicationName = configuration.GetApplicationName();
//            mStage = configuration.GetStage();
//            mSerializerSettings = new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
//        }

//        /// <summary>
//        /// Format the log line
//        /// </summary>
//        public virtual string Format(string message, LogLevel level, EventId eventId, string scope, DateTime datetime, Exception exception)
//        {
//            var @event = new RfhLogEvent<string>
//            {
//                App = mApplicationName,
//                DateTime = datetime,
//                Stage = mStage,
//                Level = level,
//                EventID = eventId.Id,
//                Scope = scope,
//                Message = message,
//                Exception = exception?.ToString() // some exceptions can't be serialized because of loops in references (even though the ignore flag is set in the serializer settings) and also don't throw an exception. But memory increases dramatically, which causes a Lambda function to be terminated
//            };

//            try
//            {
//                //if (!(message.TrimStart().StartsWith("{") && message.TrimEnd().EndsWith("}")) &&
//                //    !(message.TrimStart().StartsWith("[") && message.TrimEnd().EndsWith("]")))
//                if (!message.IsJson())
//                    return JsonConvert.SerializeObject(@event, mSerializerSettings);

//                JObject json = JObject.Parse(message);
//                var @eventWithJsonMessage = new RfhLogEvent<JObject>
//                {
//                    App = mApplicationName,
//                    DateTime = datetime,
//                    Stage = mStage,
//                    Level = level,
//                    EventID = eventId.Id,
//                    Scope = scope,
//                    Message = json,
//                    Exception = exception?.ToString()
//                };

//                return JsonConvert.SerializeObject(@eventWithJsonMessage, mSerializerSettings);
//            }
//            catch (Exception)
//            {
//                return JsonConvert.SerializeObject(@event, mSerializerSettings);
//            }
//        }



//        /// <summary>
//        /// Class to represent a log line to be formatted in Json.
//        /// </summary>
//        public class RfhLogEvent<T>
//        {
//            public string App { get; set; }
//            public DateTime DateTime { get; set; }
//            public string Stage { get; set; }
//            [JsonConverter(typeof(StringEnumConverter))]
//            public LogLevel Level { get; set; }
//            public string Scope { get; set; }
//            public int EventID { get; set; }
//            public T Message { get; set; }
//            public string Exception { get; set; }
//        }
//    }
//}