﻿using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using Fh.Aws.Wat.Lmb.Common;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fh.Aws.Wat.Lmb.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AwsServiceController : Controller
    {
        #region SnapShot

        [HttpGet("List/ByTag/{domain}/{stage}/{purposeIdTag}")]
        public async Task<JsonResult> GetEc2ServiceSnapShot(string domain, string stage, string purposeIdTag)
        {
            if (string.IsNullOrEmpty(purposeIdTag.Trim()))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            return Json(await AWSEc2WindowsService.GetServiceSnapshot(
                CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, purposeIdTag));
        }

        [HttpGet("List/ByInstance/{domain}/{stage}/")]
        public async Task<JsonResult> GetEc2ServiceSnapShot(string domain, string stage, [FromQuery] string[] instanceIds)
        {
            if (instanceIds == null || instanceIds.GetLength(0) == 0)
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            return Json(await AWSEc2WindowsService.GetServiceSnapshot(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, instanceIds.ToList()));
        }

        #endregion

        #region Stop service

        [HttpPost("Stop/{domain}/{stage}/{instanceId}")]
        public async Task<JsonResult> StopWindowsServices(string domain, string stage, string instanceId, [FromBody] string[] serviceNames)
        {
            if (string.IsNullOrEmpty(instanceId) || serviceNames.GetLength(0) == 0)
                return Json("Invalid request.");

            if (!instanceId.StartsWith("i-"))
                return Json("Invalid request.");

            if (serviceNames.Any(item => !item.ToLower().EndsWith(".wis")))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            return Json(await AWSEc2WindowsService.StopWindowsServiceOnEC2(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, instanceId, serviceNames.ToList(),
                $"AwsEc2WindowsServiceOperatorLmb ({instanceId}>Stop service(s)) => {AWSCredentialKeeper.AWSOperatorUser}"));
        }

        [HttpPost("Stop/{domain}/{stage}")]
        public async Task<JsonResult> StopWindowsServices(string domain, string stage, [FromQuery] string[] instanceIds, [FromBody] string[] serviceNames)
        {
            return Json("Not implemented.");

            if (instanceIds.GetLength(0) == 0 || serviceNames.GetLength(0) == 0)
                return Json("Invalid request.");

            if (instanceIds.Any(item => !item.ToLower().StartsWith("i-")))
                return Json("Invalid request.");

            if (serviceNames.Any(item => !item.ToLower().EndsWith(".Wis")))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            List<CommandDetail> runCommandsResults = new List<CommandDetail>();
            foreach (string instanceItem in instanceIds)
            {
                runCommandsResults.Add(await AWSEc2WindowsService.StopWindowsServiceOnEC2(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, instanceItem, serviceNames.ToList(),
                $"AwsEc2WindowsServiceOperatorLmb ({instanceItem}>Stop service(s)) => {AWSCredentialKeeper.AWSOperatorUser}"));
            }
            return Json(runCommandsResults);
        }

        #endregion

        #region Start service

        [HttpPost("Start/{domain}/{stage}/{instanceId}")]
        public async Task<JsonResult> StartWindowsServices(string domain, string stage, string instanceId, [FromBody] string[] serviceNames)
        {
            if (string.IsNullOrEmpty(instanceId.Trim()) || serviceNames.GetLength(0) == 0)
                return Json("Invalid request.");

            if (!instanceId.ToLower().StartsWith("i-"))
                return Json("Invalid request.");

            if (serviceNames.Any(item => !item.ToLower().EndsWith(".wis")))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            return Json(await AWSEc2WindowsService.StartWindowsServiceOnEC2(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, instanceId, serviceNames.ToList(),
                $"AwsEc2WindowsServiceOperatorLmb ({instanceId}>Start service(s)) => {AWSCredentialKeeper.AWSOperatorUser}"));
        }

        [HttpPost("Start/{domain}/{stage}")]
        public async Task<JsonResult> StartWindowsServices(string domain, string stage, [FromQuery] string[] instanceIds, [FromBody] string[] serviceNames)
        {
            return Json("Not implemented.");

            if (instanceIds.GetLength(0) == 0 || serviceNames.GetLength(0) == 0)
                return Json("Invalid request.");

            if (instanceIds.Any(item => !item.ToLower().StartsWith("i-")))
                return Json("Invalid request.");

            if (serviceNames.Any(item => !item.ToLower().EndsWith(".Wis")))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            List<CommandDetail> runCommandsResults = new List<CommandDetail>();
            foreach (string instanceItem in instanceIds)
            {
                runCommandsResults.Add(await AWSEc2WindowsService.StartWindowsServiceOnEC2(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, instanceItem, serviceNames.ToList(),
                $"AwsEc2WindowsServiceOperatorLmb ({instanceItem}>Start service(s)) => {AWSCredentialKeeper.AWSOperatorUser}"));
            }
            return Json(runCommandsResults);
        }

        #endregion

        #region Restart service

        [HttpPost("Restart/{domain}/{stage}/{instanceId}")]
        public async Task<JsonResult> RestartWindowsServices(string domain, string stage, string instanceId, [FromBody] string[] serviceNames)
        {


            if (string.IsNullOrEmpty(instanceId.Trim()) || serviceNames.GetLength(0) == 0)
                return Json("Invalid request.");

            if (!instanceId.ToLower().StartsWith("i-"))
                return Json("Invalid request.");

            if (serviceNames.Any(item => !item.ToLower().EndsWith(".wis")))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            return Json(await AWSEc2WindowsService.RestartWindowsServiceOnEC2(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, instanceId, serviceNames.ToList(),
                $"AwsEc2WindowsServiceOperatorLmb ({instanceId}>Restart service(s)) => {AWSCredentialKeeper.AWSOperatorUser}"));
        }

        [HttpPost("Restart/{domain}/{stage}")]
        public async Task<JsonResult> RestartWindowsServices(string domain, string stage, [FromQuery] string[] instanceIds, [FromQuery] string[] serviceNames)
        {
            return Json("Not implemented.");

            if (instanceIds.GetLength(0) == 0 || serviceNames.GetLength(0) == 0)
                return Json("Invalid request.");

            if (instanceIds.Any(item => !item.ToLower().StartsWith("i-")))
                return Json("Invalid request.");

            if (serviceNames.Any(item => !item.ToLower().EndsWith(".Wis")))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            List<CommandDetail> runCommandsResults = new List<CommandDetail>();
            foreach (string instanceItem in instanceIds)
            {
                runCommandsResults.Add(await AWSEc2WindowsService.RestartWindowsServiceOnEC2(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, instanceItem, serviceNames.ToList(),
                $"AwsEc2WindowsServiceOperatorLmb ({instanceItem}>Restart service(s)) => {AWSCredentialKeeper.AWSOperatorUser}"));
            }
            return Json(runCommandsResults);
        }

        #endregion
    }
}