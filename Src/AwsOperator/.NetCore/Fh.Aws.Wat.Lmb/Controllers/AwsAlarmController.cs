﻿using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using Fh.Aws.Wat.Lmb.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fh.Aws.Wat.Lmb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AwsAlarmController : Controller
    {
        private IConfiguration _mConfiguration;
        private ILogger _mLogger;

        #region Constructors

        public AwsAlarmController(IConfiguration configuration, ILogger<AwsAlarmController> logger)
        {
            _mConfiguration = configuration;
            _mLogger = logger;
        }

        #endregion

        #region GetAlarmByAlarmName

        [HttpGet("List/{domain}/{stage}/{appCode}")]
        public async Task<JsonResult> GetAlarmsByApplicationCode(string appCode, string domain, string stage)
        {
            if (string.IsNullOrEmpty(appCode.Trim()))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            //Random rnd = new Random();
            //var x = JsonConvert.DeserializeObject<List<AlarmDetail>>(dummy);
            //foreach (var item in x)
            //{
            //    item.AlarmState = rnd.Next(0, 3) == 0 ? "NOK" : "OK";
            //}
            //return Json(x);
            return Json(await AWSCloudWatchAlarms.GetAlarmsByApplicationCode(appCode, CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum)); ;
        }

        #endregion

        #region GetAlarmsByApplicationCode

        [HttpGet("List/{domain}/{stage}")]
        public async Task<JsonResult> GetAlarmsByApplicationByName(string domain, string stage, [FromQuery]string[] alarmNames)
        {
            if (alarmNames.GetLength(0) == 0)
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

            return Json(await AWSCloudWatchAlarms.GetAlarmByAlarmName(alarmNames.ToList(), CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum));
        }

        #endregion

        #region ResetAlarm

        [HttpGet("Reset/{domain}/{stage}/{alarmName}")]
        public async Task<JsonResult> ResetAlarm(string alarmName, string domain, string stage)
        {
            if (string.IsNullOrEmpty(alarmName.Trim()))
                return Json("Invalid request.");

            if (!CommonOperations.CheckPermissions(User, domain, stage))
                return Json("Not permitted.");

           AlarmDetail returnObject = await AWSCloudWatchAlarms.ResetAlarm(CommonOperations.DomainRoleMapping[domain].DomainEnum, CommonOperations.StageRoleMapping[stage].StageEnum, alarmName);
           return Json(returnObject);
        }

        #endregion
    }
}
