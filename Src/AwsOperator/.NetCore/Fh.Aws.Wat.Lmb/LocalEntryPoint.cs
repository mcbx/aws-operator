using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using RfhCore.Configuration;
using RfhCore.Logging;

namespace Fh.Aws.Wat.Lmb
{
    /// <summary>
    /// The Main function can be used to run the ASP.NET Core application locally using the Kestrel webserver.
    /// </summary>
    public class LocalEntryPoint
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(configBuilder => configBuilder.AddRfhConfiguration())
                .ConfigureLogging((hostingContext, logBuilder) => logBuilder.AddRfhLogging(hostingContext.Configuration))
                .UseStartup<Startup>()
                .Build();
    }
}
