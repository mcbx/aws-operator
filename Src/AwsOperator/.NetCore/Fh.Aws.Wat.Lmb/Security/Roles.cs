﻿using System;
using System.Collections.Generic;

namespace Fh.Aws.Wat.Lmb.Security
{
    public class OperatorRoles
    {
        public const string God = "SuperOperator";
        public const string Operator = "ControlOperator";
        public const string Reader = "InfoReader";
    }

    public static class AwsAccounts
    {
        private static Dictionary<string, string> _accountNumbers = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        static AwsAccounts()
        {
            _accountNumbers.Add("Commerce_Dev", "002779451522");
            _accountNumbers.Add("Commerce_Sys", "504057805512");
            _accountNumbers.Add("Commerce_Acc", "504057805512");
            _accountNumbers.Add("Commerce_Prd", "120298155027");

            _accountNumbers.Add("Finance_Dev", "002779451522");
            _accountNumbers.Add("Finance_Sys", "631114505105");
            _accountNumbers.Add("Finance_Acc", "631114505105");
            _accountNumbers.Add("Finance_Prd", "677985655788");

            _accountNumbers.Add("Logistiek_Dev", "002779451522");
            _accountNumbers.Add("Logistiek_Sys", "060681614320");
            _accountNumbers.Add("Logistiek_Acc", "060681614320");
            _accountNumbers.Add("Logistiek_Prd", "900858092965");

            _accountNumbers.Add("Support_Dev", "002779451522");
            _accountNumbers.Add("Support_Sys", "618470607968");
            _accountNumbers.Add("Support_Acc", "618470607968");
            _accountNumbers.Add("Support_Prd", "419792440550");
        }

        public static Dictionary<string, string> ListRoleArn()
        {
            //REMARK: Change to correct roles later
            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            foreach(KeyValuePair<string, string> item in _accountNumbers)
            {
                //returnValue.Add($"{item.Key.Replace("_", ".")}.Readonly", $"arn:aws:iam::{item.Value}:role/rfh-readonly");
                //returnValue.Add($"{item.Key.Replace("_", ".")}.DevOps", $"arn:aws:iam::{item.Value}:role/rfh-devops");
                returnValue.Add($"{item.Key.Replace("_", ".")}.Readonly", $"arn:aws:iam::{item.Value}:role/service-role/ThijsTestRole");
                returnValue.Add($"{item.Key.Replace("_", ".")}.DevOps", $"arn:aws:iam::{item.Value}:role/service-role/ThijsTestRole");
            }

            return returnValue;
        }

        public static string GetAccountNumber(string domain, string enviroment)
        {
            string key = $"{domain.ToLowerInvariant()}_{enviroment.ToLowerInvariant()}";
            if (!_accountNumbers.ContainsKey(key))
                return string.Empty;

            return _accountNumbers[key];
        }
    }

    public class DomainRoles
    {
        public const string Commerce = "Commerce";
        public const string Finance = "Finance";
        public const string Logistiek = "Operations";
        public const string Support = "Support";
    }

    public class StageRoles
    {
        public const string Dev = "Sandbox";
        public const string Sys = "Staging";
        public const string Acc = "Staging";
        public const string Prd = "Live";
    }
}
