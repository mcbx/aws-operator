using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Wat.Lmb.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace Fh.Aws.Wat.Lmb
{
    public class Startup
    {
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;

            AWSCredentialKeeper.Initialize(AwsAccounts.ListRoleArn(), Environment.UserName);
            AWSCredentialKeeper.GetCredentialsWithToken("RFH-DEV-VEILEN", string.Empty, false);
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                    }
                );
            });

            //services.AddSingleton<ITransactionAdapter, TransactionAdapter>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //services.AddAuthorization(options => options.AddPolicy(Policies.KvvOperaties, policyBuilder =>
            //{
            //    policyBuilder.RequireRole(Roles.KvvUser);
            //}));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
