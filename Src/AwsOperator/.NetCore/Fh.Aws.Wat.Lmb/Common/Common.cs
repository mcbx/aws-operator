﻿using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Wat.Lmb.Security;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Fh.Aws.Wat.Lmb.Common
{
    public class DomainMapItem
    {
        public DomainMapItem(string domainRole, EAWSDomain domainEnum)
        {
            DomainRole = domainRole;
            DomainEnum = domainEnum;
        }

        public string DomainRole { get; set; }

        public EAWSDomain DomainEnum { get; set; }
    }

    public class StageMapItem
    {
        public StageMapItem(string stageRole, EAWSEnvironment stageEnum)
        {
            StageRole = stageRole;
            StageEnum = stageEnum;
        }

        public string StageRole { get; set; }

        public EAWSEnvironment StageEnum { get; set; }
    }
    
    public class CommonOperations
    {
        public static readonly Dictionary<string, DomainMapItem> DomainRoleMapping = new Dictionary<string, DomainMapItem>(
            new[]
            {
                new KeyValuePair<string, DomainMapItem>("commerce", new DomainMapItem(DomainRoles.Commerce, EAWSDomain.Commerce)),
                new KeyValuePair<string, DomainMapItem>("finance", new DomainMapItem(DomainRoles.Finance, EAWSDomain.Finance)),
                new KeyValuePair<string, DomainMapItem>("operations", new DomainMapItem(DomainRoles.Logistiek, EAWSDomain.Operations)),
                new KeyValuePair<string, DomainMapItem>("support", new DomainMapItem(DomainRoles.Support, EAWSDomain.Support))
            }
        );

        public static readonly Dictionary<string, StageMapItem> StageRoleMapping = new Dictionary<string, StageMapItem>(
            new[]
            {
                new KeyValuePair<string, StageMapItem>("dev", new StageMapItem(StageRoles.Dev, EAWSEnvironment.Dev)),
                new KeyValuePair<string, StageMapItem>("sandbox", new StageMapItem(StageRoles.Dev, EAWSEnvironment.Dev)),

                //new KeyValuePair<string, StageMapItem>("sys", new StageMapItem(StageRoles.Sys, EAWSEnvironment.Sys)),
                //new KeyValuePair<string, StageMapItem>("acc", new StageMapItem(StageRoles.Acc, EAWSEnvironment.Acc)),
                //new KeyValuePair<string, StageMapItem>("prd", new StageMapItem(StageRoles.Prd, EAWSEnvironment.Prd))
            }
        );

        public static bool CheckPermissions(ClaimsPrincipal userClaim, string domain, string stage)
        {
#if DEBUG
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, "VJCDev"),
                new Claim(ClaimTypes.NameIdentifier, "666"),
                new Claim("name", "VJC_Dev"),
                new Claim(ClaimTypes.Role, "rfh-sandbox:002779451522:admin")
            };

            userClaim = new ClaimsPrincipal(new ClaimsIdentity(claims, "LocalTest"));
#endif

            if (!StageRoleMapping.ContainsKey(stage.ToLowerInvariant()))
                return false;

            var stageRole = StageRoleMapping[stage].StageRole.ToLowerInvariant();
            var domainRole = DomainRoleMapping[domain].DomainRole.ToLowerInvariant();

            //REMARK: we ignore the admin part for the moment
            var roles = userClaim.Claims.Where(item => item.Type == ClaimTypes.Role && item.Value.StartsWith($"rfh-{stageRole}:{AwsAccounts.GetAccountNumber(domain, stage)}"));
            if (roles.Count() == 0)
                return false;

            return true;
        }
    }
}
