using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Wat.Lmb.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.Extensions.Logging;
using RfhCore.Configuration;
using RfhCore.Logging;
using System;
using System.Linq;
using System.Security.Claims;

namespace Fh.Aws.Wat.Lmb
{
    /// <summary>
    /// This class extends from APIGatewayProxyFunction which contains the method FunctionHandlerAsync which is the 
    /// actual Lambda function entry point. The Lambda handler field should be set to
    /// 
    ///Fh.Aws.Wat.Lmb::Fh.Aws.Wat.Lmb.LambdaEntryPoint::FunctionHandlerAsync
    /// </summary>
    public class LambdaEntryPoint : Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction
    // When using an ELB's Application Load Balancer as the event source change 
    // the base class to Amazon.Lambda.AspNetCoreServer.ApplicationLoadBalancerFunction

    {
        /// <summary>
        /// The builder has configuration, logging and Amazon API Gateway already configured. The startup class
        /// needs to be configured in this method using the UseStartup<>() method.
        /// </summary>
        /// <param name="builder"></param>
        protected override void Init(IWebHostBuilder builder)
        {
            builder
                .ConfigureAppConfiguration(configBuilder => configBuilder.AddRfhConfiguration())
                .ConfigureLogging((hostingContext, logBuilder) => logBuilder.AddRfhLogging(hostingContext.Configuration))
                .UseStartup<Startup>();
        }

        protected override void PostCreateContext(HostingApplication.Context context, APIGatewayProxyRequest apiGatewayRequest, ILambdaContext lambdaContext)
        { 
            if (apiGatewayRequest?.RequestContext?.Authorizer == null)
                return;

            if (apiGatewayRequest.RequestContext.Authorizer.TryGetValue("roleclaims", out object roleClaimsObject))
            {
                if (!string.IsNullOrWhiteSpace(roleClaimsObject.ToString()))
                {
                    var identity = new ClaimsIdentity(roleClaimsObject.ToString().Split(",").Select(r => new Claim(ClaimTypes.Role, r)));
                    context.HttpContext.User = new ClaimsPrincipal(identity);
                }
            }
        }
    }
}
