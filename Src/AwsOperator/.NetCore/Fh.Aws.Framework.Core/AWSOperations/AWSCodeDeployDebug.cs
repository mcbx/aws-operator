﻿using Amazon.CodeDeploy;
using Amazon.CodeDeploy.Model;
using Amazon.Runtime;
using Amazon.SimpleSystemsManagement;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSCodeDeployDebug
    {
        #region CodeDeploy

        #region Lists

        public static Task<List<string>> ListCodeDeployApplications(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                List<string> returnValue = new List<string>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                string appNotation = $"{appCode.ToLower()}-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-";

                using (AmazonCodeDeployClient client = new AmazonCodeDeployClient(myCred))
                {
                    ListApplicationsRequest req = new ListApplicationsRequest();

                    var res = client.ListApplicationsAsync(req).GetAwaiter().GetResult();
                    foreach (string applicationItem in res.Applications)
                    {
                        if (applicationItem.StartsWith(appNotation))
                            returnValue.Add(applicationItem);
                    }

                    while (!string.IsNullOrEmpty(res.NextToken))
                    {
                        req.NextToken = res.NextToken;
                        res = client.ListApplicationsAsync(req).GetAwaiter().GetResult();

                        foreach (string applicationItem in res.Applications)
                        {
                            if (applicationItem.StartsWith(appNotation))
                                returnValue.Add(applicationItem);
                        }
                    }
                }

                return returnValue;
            });
        }

        public static Task<Dictionary<string, string>> ListCodeDeploymentGroups(string codeDeployApplication, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                Dictionary<string, string> returnValue = new Dictionary<string, string>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCodeDeployClient client = new AmazonCodeDeployClient(myCred))
                {
                    ListDeploymentGroupsRequest req = new ListDeploymentGroupsRequest();
                    req.ApplicationName = codeDeployApplication;

                    var res = client.ListDeploymentGroupsAsync(req).GetAwaiter().GetResult();
                    foreach (string applicationItem in res.DeploymentGroups)
                    {
                        returnValue.Add(applicationItem, string.Empty);
                    }

                    while (!string.IsNullOrEmpty(res.NextToken))
                    {
                        req.NextToken = res.NextToken;
                        res = client.ListDeploymentGroupsAsync(req).GetAwaiter().GetResult();

                        foreach (string applicationItem in res.DeploymentGroups)
                        {
                            returnValue.Add(applicationItem, string.Empty);
                        }
                    }

                    var splitList = AWSCommon.SplitList(returnValue.Keys.ToList(), 100);
                    foreach (List<string> splitListItem in splitList)
                    {
                        BatchGetDeploymentGroupsRequest bdgReq = new BatchGetDeploymentGroupsRequest();
                        bdgReq.ApplicationName = codeDeployApplication;
                        bdgReq.DeploymentGroupNames.AddRange(splitListItem);

                        var bdgRes = client.BatchGetDeploymentGroupsAsync(bdgReq).GetAwaiter().GetResult();
                        foreach (DeploymentGroupInfo groupItem in bdgRes.DeploymentGroupsInfo)
                        {
                            if (returnValue.ContainsKey(groupItem.DeploymentGroupName))
                            {
                                returnValue[groupItem.DeploymentGroupName] = groupItem.DeploymentGroupId;
                            }
                        }
                    }
                }

                return returnValue;
            });
        }

        public static Task<Dictionary<string, DeploymentDetail>> ListCodeDeployments(string codeDeployApplication, string codeDeployGroupName, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();
                Dictionary<string, DeploymentDetail> returnValue = new Dictionary<string, DeploymentDetail>();

                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCodeDeployClient client = new AmazonCodeDeployClient(myCred))
                {
                    ListDeploymentsRequest req = new ListDeploymentsRequest();
                    req.ApplicationName = codeDeployApplication;
                    req.DeploymentGroupName = codeDeployGroupName;

                    var res = client.ListDeploymentsAsync(req).GetAwaiter().GetResult();
                    foreach (string applicationItem in res.Deployments)
                    {
                        returnValue.Add(applicationItem, null);
                    }

                    while (!string.IsNullOrEmpty(res.NextToken))
                    {
                        req.NextToken = res.NextToken;
                        res = client.ListDeploymentsAsync(req).GetAwaiter().GetResult();

                        foreach (string applicationItem in res.Deployments)
                        {
                            returnValue.Add(applicationItem, null);
                        }
                    }

                    var splitList = AWSCommon.SplitList(returnValue.Keys.ToList(), 100);
                    foreach (List<string> splitListItem in splitList)
                    {
                        BatchGetDeploymentsRequest dReq = new BatchGetDeploymentsRequest();
                        dReq.DeploymentIds.AddRange(splitListItem);

                        var dRes = client.BatchGetDeploymentsAsync(dReq).GetAwaiter().GetResult();
                        foreach (DeploymentInfo deploymentItem in dRes.DeploymentsInfo)
                        {
                            if (returnValue.ContainsKey(deploymentItem.DeploymentId))
                            {
                                returnValue[deploymentItem.DeploymentId] = new DeploymentDetail()
                                {
                                    DeploymentId = deploymentItem.DeploymentId,
                                    RunDate = deploymentItem.CreateTime,
                                    Status = deploymentItem.Status.ToString(),
                                    DeploymentErrorMessage = deploymentItem.ErrorInformation == null ? string.Empty : deploymentItem.ErrorInformation.Message
                                };
                            }
                        }
                    }
                }
                timer.Stop();
                Debug.WriteLine(timer.Elapsed);
                return returnValue;
            });
        }

        public static Task<List<string>> GetDeploymentInstanceTargets(string deploymentId, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                List<string> returnValue = new List<string>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCodeDeployClient client = new AmazonCodeDeployClient(myCred))
                {
                    ListDeploymentInstancesRequest ldirReq = new ListDeploymentInstancesRequest();
                    ldirReq.DeploymentId = deploymentId;

                    var ldirRes = client.ListDeploymentInstancesAsync(ldirReq).GetAwaiter().GetResult();
                    returnValue.AddRange(ldirRes.InstancesList);
                    while (!string.IsNullOrEmpty(ldirRes.NextToken))
                    {
                        ldirReq.NextToken = ldirRes.NextToken;
                        ldirRes = client.ListDeploymentInstancesAsync(ldirReq).GetAwaiter().GetResult();

                        returnValue.AddRange(ldirRes.InstancesList);
                    }
                }

                return returnValue;
            });
        }

        #endregion

        #region Get Script Log From deployment

        public static Task<CommandDetail> FetchScriptLogFromDeployment(EAWSDomain domain, EAWSEnvironment env, string instanceId, string codeDeploymentId)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return new CommandDetail();

                using (AmazonCodeDeployClient client = new AmazonCodeDeployClient(myCred))
                {
                    GetDeploymentInstanceRequest dReq = new GetDeploymentInstanceRequest();
                    dReq.DeploymentId = codeDeploymentId;
                    dReq.InstanceId = instanceId;

                    var dRes = client.GetDeploymentInstanceAsync(dReq).GetAwaiter().GetResult();
                    foreach (LifecycleEvent summaryItem in dRes.InstanceSummary.LifecycleEvents)
                    {
                        if (summaryItem.Status == LifecycleEventStatus.Failed && summaryItem.Diagnostics != null && (!string.IsNullOrEmpty(summaryItem.Diagnostics.Message) || !string.IsNullOrEmpty(summaryItem.Diagnostics.LogTail)))
                        {
                            return new CommandDetail()
                            {
                                CommandType = CommandDetailType.Other,
                                CommandId = "-",
                                CommandInstanceId = instanceId,
                                CommandResult = $"Event: {summaryItem.LifecycleEventName}{Environment.NewLine}Message: {summaryItem.Diagnostics.Message}{Environment.NewLine}Script: {summaryItem.Diagnostics.ScriptName}{Environment.NewLine}LogTrail:{Environment.NewLine}{summaryItem.Diagnostics.LogTail}",
                                CommandStatus = CommandStatus.Success
                            };
                        }
                    }
                }

                return new CommandDetail()
                {
                    CommandType = CommandDetailType.Other,
                    CommandId = "-",
                    CommandInstanceId = instanceId,
                    CommandResult = "Geen log gevonden in deployment trail.",
                    CommandStatus = CommandStatus.Success
                };
            });
        }

        #endregion

        #region GetScriptLogFromInstance

        public static Task<CommandDetail> FetchScriptLogFromInstance(EAWSDomain domain, EAWSEnvironment env, string instanceId, string codeDeploymentGroupId, string codeDeployment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail();

                string command = string.Format(@"$logFile = 'C:\ProgramData\Amazon\CodeDeploy\{0}\{1}\logs\scripts.log'
if (!(Test-Path $logFile -PathType Leaf))
{{
    Write-host '$logFile does not exist.'
    return
}}

Write-host(Get-content $logFile -Raw)", codeDeploymentGroupId, codeDeployment);

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>() { instanceId }, new List<string>() { command }, string.Empty).GetAwaiter().GetResult()[instanceId];
            });}

        #endregion

        #endregion
    }
}
