﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AwsS3Operations
    {
        private static void ProcessS3List(AWSOperationClassContainers.S3Bucket bucket, List<S3Object> items)
        {
            foreach (S3Object item in items)
            {
                var splits = Path.GetDirectoryName(item.Key).Split(new char[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
                if (splits.Count() == 0 && item.Size > 0)
                {
                    bucket.Items[bucket.BucketName].SubItems.Add(item.Key, new S3BucketItem() { Name = item.Key, Size = item.Size, FilePath = item.Key, LastModified = item.LastModified, Downloadable = item.StorageClass == S3StorageClass.Standard });
                    continue;
                }

                //empty folder
                if (splits.Count() == 1 && item.Size == 0 && string.IsNullOrEmpty(Path.GetFileName(item.Key)))
                {
                    bucket.Items[bucket.BucketName].SubItems.Add(item.Key.TrimEnd('/'), new S3BucketItem() { Name = item.Key.TrimEnd('/'), Size = item.Size, LastModified = item.LastModified });
                    continue;
                }

                //add all other items
                if (splits.Count() > 0 && item.Size > 0)
                {
                    var currentItem = bucket.Items[bucket.BucketName];
                    var pathItems = splits;
                    foreach (var pItem in pathItems)
                    {
                        if (!currentItem.SubItems.ContainsKey(pItem))
                        {
                            currentItem.SubItems.Add(pItem, new S3BucketItem() { Name = pItem, Size = 0, LastModified = currentItem.LastModified });
                        }

                        currentItem = currentItem.SubItems[pItem];
                    }

                    currentItem.SubItems.Add(Path.GetFileName(item.Key), new S3BucketItem() { Name = Path.GetFileName(item.Key), Size = item.Size, FilePath = item.Key, LastModified = item.LastModified, Downloadable = item.StorageClass == S3StorageClass.Standard });
                }
            }
        }

        public static Task<Dictionary<string, AWSOperationClassContainers.S3Bucket>> GetS3Buckets(EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                Dictionary<string, AWSOperationClassContainers.S3Bucket> returnValue = new Dictionary<string, AWSOperationClassContainers.S3Bucket>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonS3Client client = new AmazonS3Client(myCred))
                {
                    ListBucketsRequest req = new ListBucketsRequest();

                    var resp = client.ListBucketsAsync(req).GetAwaiter().GetResult();
                    resp.Buckets.ForEach(itm =>
                    {
                        AWSOperationClassContainers.S3Bucket bucket = new AWSOperationClassContainers.S3Bucket();
                        bucket.BucketName = itm.BucketName;
                        bucket.Items.Add(itm.BucketName, new S3BucketItem() { Name = itm.BucketName, Size = 0 });

                        returnValue.Add(itm.BucketName, bucket);
                    });
                }

                return returnValue;
            });
        }

        public static Task<AWSOperationClassContainers.S3Bucket> ListItemsFromS3Bucket(string bucketName, EAWSDomain domain, EAWSEnvironment env)
        {
            AWSOperationClassContainers.S3Bucket bucket = new AWSOperationClassContainers.S3Bucket();
            bucket.BucketName = bucketName;
            bucket.Items.Add(bucket.BucketName, new S3BucketItem() { Name = bucket.BucketName, Size = 0 });

            return ListItemsFromS3Bucket(bucket, domain, env);
        }

        public static Task<AWSOperationClassContainers.S3Bucket> ListItemsFromS3Bucket(AWSOperationClassContainers.S3Bucket bucket, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return bucket;

                bucket.Items[bucket.BucketName].SubItems.Clear();
                string region = string.Empty;
                using (AmazonS3Client client = new AmazonS3Client(myCred))
                {
                    var locResp = client.GetBucketLocationAsync(new GetBucketLocationRequest() { BucketName = bucket.BucketName }).GetAwaiter().GetResult();
                    region = locResp.Location.Value;

                    //if (locResp.Location.Value != client.Config.RegionEndpoint.SystemName)
                    //{
                    //        bucket.Items[bucket.BucketName].SubItems.Add("RegionError", new S3BucketItem() { Name = "Bucket region different then client.", Size = -1 });
                    //    return bucket;
                    //}
                }

                using (AmazonS3Client client = new AmazonS3Client(myCred, RegionEndpoint.GetBySystemName(region)))
                {
                    var req = new ListObjectsV2Request();
                    req.BucketName = bucket.BucketName;
                    var response = client.ListObjectsV2Async(req).GetAwaiter().GetResult();

                    ProcessS3List(bucket, response.S3Objects);

                    while (response.IsTruncated && !string.IsNullOrEmpty(response.NextContinuationToken))
                    {
                        req.ContinuationToken = response.NextContinuationToken;
                        response = client.ListObjectsV2Async(req).GetAwaiter().GetResult();
                        ProcessS3List(bucket, response.S3Objects);
                    }
                }

                return bucket;
            });
        }

        public static Task<bool> SaveS3ItemsToDisk(AWSOperationClassContainers.S3Bucket s3Bucket, AWSOperationClassContainers.S3BucketItem bucketItem, string saveBasePath, EAWSDomain domain, EAWSEnvironment env)
        {
            return SaveS3ItemsToDisk(s3Bucket, (new[] { bucketItem }).ToList(), saveBasePath, domain, env);
        }

        public static Task<bool> SaveS3ItemsToDisk(AWSOperationClassContainers.S3Bucket s3Bucket, List<AWSOperationClassContainers.S3BucketItem> bucketItems, string saveBasePath, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return false;

                string region = string.Empty;
                using (AmazonS3Client client = new AmazonS3Client(myCred))
                {
                    var locResp = client.GetBucketLocationAsync(new GetBucketLocationRequest() { BucketName = s3Bucket.BucketName }).GetAwaiter().GetResult();
                    region = locResp.Location.Value;
                }

                using (AmazonS3Client client = new AmazonS3Client(myCred, RegionEndpoint.GetBySystemName(region)))
                {
                    foreach (S3BucketItem bucketItem in bucketItems)
                    {
                        GetObjectRequest req = new GetObjectRequest();
                        req.BucketName = s3Bucket.BucketName;
                        req.Key = bucketItem.FilePath;

                        var result = client.GetObjectAsync(req).GetAwaiter().GetResult();

                        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                        CancellationToken token = cancellationTokenSource.Token;
                        result.WriteResponseStreamToFileAsync(Path.Combine(saveBasePath, s3Bucket.BucketName, bucketItem.FilePath), false, token).GetAwaiter().GetResult();
                    }
                }

                return true;
            });
        }
    }
}
