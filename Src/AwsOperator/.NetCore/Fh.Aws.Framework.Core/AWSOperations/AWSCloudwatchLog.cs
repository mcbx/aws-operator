﻿using Amazon.CloudWatchLogs;
using Amazon.CloudWatchLogs.Model;
using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSCloudwatchLog
    {
        #region CloudWatchLog

        #region General

        public static Task<List<string>> FindLogGroups(EAWSDomain domain, EAWSEnvironment env, string logGroupPrefix)
        {
            return Task.Run(() =>
            {
                List<string> returnValue = new List<string>();

                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
                {
                    DescribeLogGroupsRequest req = new DescribeLogGroupsRequest();
                    req.LogGroupNamePrefix = logGroupPrefix;
                    var res = client.DescribeLogGroupsAsync(req).GetAwaiter().GetResult();
                    returnValue.AddRange(res.LogGroups.Select(logGroupItem => logGroupItem.LogGroupName));

                    while (!string.IsNullOrEmpty(res.NextToken))
                    {
                        res.NextToken = res.NextToken;
                        res = client.DescribeLogGroupsAsync(req).GetAwaiter().GetResult();

                        returnValue.AddRange(res.LogGroups.Select(logGroupItem => logGroupItem.LogGroupName));
                    }
                }
                return returnValue;
            });
        }

        public static Task<Dictionary<string, LogStreamItem>> GetLogStreams(EAWSDomain domain, EAWSEnvironment env, string logGroup, string logStreamPrefix, bool includeEmpty)
        {
            return Task.Run(() =>
            {
                Dictionary<string, LogStreamItem> returnValue = new Dictionary<string, LogStreamItem>();

                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
                {
                    DescribeLogStreamsRequest req = new DescribeLogStreamsRequest();
                    req.LogGroupName = logGroup;

                    if (!string.IsNullOrEmpty(logStreamPrefix))
                        req.LogStreamNamePrefix = logStreamPrefix;

                    var res = client.DescribeLogStreamsAsync(req).GetAwaiter().GetResult();
                    foreach (LogStream logStreamItem in res.LogStreams)
                    {
                        if (!includeEmpty && logStreamItem.StoredBytes == 0)
                            continue;

                        if (!returnValue.ContainsKey(logStreamItem.LogStreamName))
                        {
                            LogStreamItem lItem = new LogStreamItem()
                            {
                                LogStreamName = logStreamItem.LogStreamName,
                                LastEventTimestamp = logStreamItem.LastEventTimestamp,
                                StoredBytes = logStreamItem.StoredBytes
                            };
                            returnValue.Add(logStreamItem.LogStreamName, lItem);
                        }
                    }

                    while (!string.IsNullOrEmpty(res.NextToken))
                    {
                        req.NextToken = res.NextToken;
                        res = client.DescribeLogStreamsAsync(req).GetAwaiter().GetResult();

                        foreach (LogStream logStreamItem in res.LogStreams)
                        {
                            if (!includeEmpty && logStreamItem.StoredBytes == 0)
                                continue;

                            if (!returnValue.ContainsKey(logStreamItem.LogStreamName))
                            {
                                LogStreamItem lItem = new LogStreamItem()
                                {
                                    LogStreamName = logStreamItem.LogStreamName,
                                    LastEventTimestamp = logStreamItem.LastEventTimestamp,
                                    StoredBytes = logStreamItem.StoredBytes
                                };
                                returnValue.Add(logStreamItem.LogStreamName, lItem);
                            }
                        }
                    }
                }
                return returnValue;
            });
        }

        public static Task<Dictionary<string, StringBuilder>> GetLogEntries(string appCode, string applicationName, EAWSDomain domain, EAWSEnvironment env,
    DateTime filterDateTimeFrom, DateTime filterDateTimeTo, string logGroup)
        {
            return GetLogEntries(appCode, applicationName, domain, env,
                filterDateTimeFrom, filterDateTimeTo, logGroup, string.Empty);
        }

        public static Task<Dictionary<string, StringBuilder>> GetLogEntries(string appCode, string applicationName, EAWSDomain domain, EAWSEnvironment env,
            DateTime filterDateTimeFrom, DateTime filterDateTimeTo, string logGroup, string filterExpression)
        {
            return Task.Run(() =>
            {
                Dictionary<string, StringBuilder> returnValue = new Dictionary<string, StringBuilder>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
                {
                    var reqEv = new FilterLogEventsRequest();
                    reqEv.LogGroupName = logGroup;
                    if (filterDateTimeFrom != DateTime.MinValue && filterDateTimeTo != DateTime.MinValue)
                    {
                        reqEv.StartTime = new DateTimeOffset(filterDateTimeFrom).ToUnixTimeMilliseconds();
                        reqEv.EndTime = new DateTimeOffset(filterDateTimeTo).ToUnixTimeMilliseconds();
                    }

                    if (!string.IsNullOrEmpty(filterExpression))
                        reqEv.FilterPattern = filterExpression;

                    returnValue.Add(logGroup, new StringBuilder());

                    var resEv = client.FilterLogEventsAsync(reqEv).GetAwaiter().GetResult();
                    StringBuilder resultBuilder = new StringBuilder();

                    foreach (FilteredLogEvent ev in resEv.Events.OrderBy(item => item.Timestamp))
                    {
                        resultBuilder.AppendLine(ev.Message);
                    }

                    while (!string.IsNullOrEmpty(resEv.NextToken))
                    {
                        reqEv.NextToken = resEv.NextToken;
                        resEv = client.FilterLogEventsAsync(reqEv).GetAwaiter().GetResult();

                        foreach (FilteredLogEvent ev in resEv.Events.OrderBy(item => item.Timestamp))
                        {
                            resultBuilder.AppendLine(ev.Message);
                        }
                    }

                    returnValue[logGroup] = resultBuilder;
                }

                return returnValue;
            });
        }

        public static Task<Dictionary<string, StringBuilder>> GetLogEntries(string appCode, string applicationName, EAWSDomain domain, EAWSEnvironment env,
            DateTime filterDateTimeFrom, DateTime filterDateTimeTo, string logGroup, List<string> logStreams)
        {
            return Task.Run(() =>
            {
                Dictionary<string, StringBuilder> returnValue = new Dictionary<string, StringBuilder>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
                {
                    foreach (string item in logStreams)
                    {
                        var reqEv = new GetLogEventsRequest();
                        reqEv.LogGroupName = logGroup;
                        reqEv.StartFromHead = true;

                        if (filterDateTimeFrom != DateTime.MinValue && filterDateTimeTo != DateTime.MinValue)
                        {
                            reqEv.StartTime = filterDateTimeFrom;
                            reqEv.EndTime = filterDateTimeTo;
                        }
                        returnValue.Add(item, new StringBuilder());
                        reqEv.LogStreamName = item;

                        var resEv = client.GetLogEventsAsync(reqEv).GetAwaiter().GetResult();
                        StringBuilder resultBuilder = new StringBuilder();
                        foreach (OutputLogEvent ev in resEv.Events)
                        {
                            resultBuilder.AppendLine(ev.Message);
                        }

                        while (!string.IsNullOrEmpty(resEv.NextForwardToken))
                        {
                            reqEv.NextToken = resEv.NextForwardToken;
                            resEv = client.GetLogEventsAsync(reqEv).GetAwaiter().GetResult();

                            foreach (OutputLogEvent ev in resEv.Events)
                            {
                                resultBuilder.AppendLine(ev.Message);
                            }
                        }

                        returnValue[item] = resultBuilder;
                    }
                }

                return returnValue;
            });
        }

        private static void ProcessSaveLogEntriesToFile(bool includeTimestamp, GetLogEventsResponse resEv, StreamWriter fileWriter)
        {
            foreach (OutputLogEvent ev in resEv.Events)
            {
                if (!includeTimestamp)
                {
                    fileWriter.WriteLine(ev.Message);
                }
                else
                {
                    if (Regex.IsMatch(ev.Message, @"^(?:[1-9]\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:|Z|[+-][01]\d:[0-5]\d)"))
                        fileWriter.WriteLine($"{ev.Message}");
                    else
                        fileWriter.WriteLine($"{ev.Timestamp.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss.ffffff")} => {ev.Message}");
                }
            }
        }

        public static Task<Dictionary<string, string>> SaveLogEntriesToFile(EAWSDomain domain, EAWSEnvironment env,
            DateTime filterDateTimeFrom, DateTime filterDateTimeTo, string logGroup, List<string> logStreams, string destinationFolder, bool includeTimestamp, string ownerOperator)
        {
            return Task.Run(() =>
            {
                Dictionary<string, string> returnValue = new Dictionary<string, string>();

                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
                {
                    foreach (string item in logStreams)
                    {
                        returnValue.Add(item, string.Empty);

                        var reqEv = new GetLogEventsRequest();
                        reqEv.LogGroupName = logGroup;
                        reqEv.LogStreamName = item;
                        reqEv.StartFromHead = true;

                        if (filterDateTimeFrom != DateTime.MinValue && filterDateTimeTo != DateTime.MinValue)
                        {
                            reqEv.StartTime = filterDateTimeFrom;
                            reqEv.EndTime = filterDateTimeTo;
                        }

                        var resEv = client.GetLogEventsAsync(reqEv).GetAwaiter().GetResult();
                        string outputFile = Path.Combine(destinationFolder);
                        outputFile = Path.Combine(outputFile, $"{ownerOperator}-Fetched{DateTime.Now.ToString("ddMMyyyy")}-{logGroup.Trim('/').Replace("/", "_")}-{item.Trim('/').Replace("/", "_")}.log");

                        if (File.Exists(outputFile))
                            File.Delete(outputFile);

                        using (StreamWriter fileWriter = new StreamWriter(outputFile, true, Encoding.UTF8, 5120))
                        {
                            ProcessSaveLogEntriesToFile(includeTimestamp, resEv, fileWriter);

                            while (resEv.Events.Count != 0)
                            {
                                reqEv.NextToken = resEv.NextForwardToken;
                                resEv = client.GetLogEventsAsync(reqEv).GetAwaiter().GetResult();

                                ProcessSaveLogEntriesToFile(includeTimestamp, resEv, fileWriter);
                            }

                            returnValue[item] = outputFile;
                            fileWriter.Flush();
                        }
                    }
                    return returnValue;
                }
            });
        }

        private static void ProcessSaveLogEntriesToFile(bool includeTimestamp, FilterLogEventsResponse resEv, StreamWriter fileWriter)
        {
            foreach (FilteredLogEvent ev in resEv.Events.OrderBy(item => item.Timestamp))
            {
                if (!includeTimestamp)
                {
                    fileWriter.WriteLine($"{ev.Message.TrimEnd('\n').TrimEnd('\r').TrimEnd()}");
                }
                else
                {
                    if (Regex.IsMatch(ev.Message, @"^(?:[1-9]\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:|Z|[+-][01]\d:[0-5]\d)"))
                    {
                        fileWriter.WriteLine($"{ev.Message.TrimEnd('\n').TrimEnd('\r').TrimEnd()}");
                    }
                    else
                    {
                        fileWriter.WriteLine($"{DateTimeOffset.FromUnixTimeMilliseconds(ev.Timestamp).ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss.ffffff")} => {ev.Message.TrimEnd('\n').TrimEnd('\r').TrimEnd()}");
                    }
                }
            }
        }

        private static bool ProcessSearchLogEntries(bool includeTimestamp, FilterLogEventsResponse resEv, Dictionary<string, object> container)
        {
            foreach (FilteredLogEvent ev in resEv.Events.OrderBy(item => item.Timestamp))
            {
                if (!container.ContainsKey(ev.LogStreamName))
                    container.Add(ev.LogStreamName, new StringBuilder());

                StringBuilder logItems = (StringBuilder)container[ev.LogStreamName];

                if (!includeTimestamp)
                {
                    logItems.AppendLine($"{ev.Message.TrimEnd('\n').TrimEnd('\r').TrimEnd()}");
                }
                else
                {
                    if (Regex.IsMatch(ev.Message, @"^(?:[1-9]\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:|Z|[+-][01]\d:[0-5]\d)"))
                    {
                        logItems.AppendLine($"{ev.Message.TrimEnd('\n').TrimEnd('\r').TrimEnd()}");
                    }
                    else
                    {
                        logItems.AppendLine($"{DateTimeOffset.FromUnixTimeMilliseconds(ev.Timestamp).ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss.ffffff")} => {ev.Message.TrimEnd('\n').TrimEnd('\r').TrimEnd()}");
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Search for logentries and return them in a Dictionary. Please note that the more items are found the longer it takes and the more memory the program uses
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="env"></param>
        /// <param name="filterDateTimeFrom"></param>
        /// <param name="filterDateTimeTo"></param>
        /// <param name="logGroup"></param>
        /// <param name="logStreams"></param>
        /// <param name="filterPattern"></param>
        /// <param name="includeTimestamp"></param>
        /// <returns></returns>
        public static Task<Dictionary<string, object>> SearchLogEntries(EAWSDomain domain, EAWSEnvironment env,
    DateTime filterDateTimeFrom, DateTime filterDateTimeTo, string logGroup, List<string> logStreams, string filterPattern, bool includeTimestamp)
        {
            return Task.Run(() =>
            {
                Dictionary<string, object> returnValue = new Dictionary<string, object>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
                {
                    var reqEv = new FilterLogEventsRequest();
                    reqEv.LogGroupName = logGroup;
                    if (logStreams.Count > 0)
                        reqEv.LogStreamNames = logStreams;

                    if (!string.IsNullOrEmpty(filterPattern))
                        reqEv.FilterPattern = filterPattern;

                    if (filterDateTimeFrom != DateTime.MinValue && filterDateTimeTo != DateTime.MinValue)
                    {
                        reqEv.StartTime = new DateTimeOffset(filterDateTimeFrom).ToUnixTimeMilliseconds();
                        reqEv.EndTime = new DateTimeOffset(filterDateTimeTo).ToUnixTimeMilliseconds();
                    }

                    try
                    {
                        var resEv = client.FilterLogEventsAsync(reqEv).GetAwaiter().GetResult();

                        ProcessSearchLogEntries(includeTimestamp, resEv, returnValue);
                        while (!string.IsNullOrEmpty(resEv.NextToken))
                        {
                            reqEv.NextToken = resEv.NextToken;
                            resEv = client.FilterLogEventsAsync(reqEv).GetAwaiter().GetResult();

                            ProcessSearchLogEntries(includeTimestamp, resEv, returnValue);
                        }
                    }
                    catch (ResourceNotFoundException)
                    {

                    }
                }
                return returnValue;
            });
        }

        public static Task<Dictionary<string, string>> SaveLogEntriesToFile(EAWSDomain domain, EAWSEnvironment env,
            DateTime filterDateTimeFrom, DateTime filterDateTimeTo, string logGroup, string applicationName, string destinationFolder, bool includeTimestamp, string ownerOperator)
        {
            return Task.Run(() =>
            {
                Dictionary<string, string> returnValue = new Dictionary<string, string>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
                {
                    var reqEv = new FilterLogEventsRequest();
                    reqEv.LogGroupName = logGroup;
                    if (filterDateTimeFrom != DateTime.MinValue && filterDateTimeTo != DateTime.MinValue)
                    {
                        reqEv.StartTime = new DateTimeOffset(filterDateTimeFrom).ToUnixTimeMilliseconds();
                        reqEv.EndTime = new DateTimeOffset(filterDateTimeTo).ToUnixTimeMilliseconds();
                    }

                    var resEv = client.FilterLogEventsAsync(reqEv).GetAwaiter().GetResult();
                    string outputFile = Path.Combine(destinationFolder);
                    outputFile = Path.Combine(outputFile, $"{ownerOperator}-Fetched{DateTime.Now.ToString("ddMMyyyy")}-{logGroup.Trim('/').Replace("/", "_")}.log");

                    if (File.Exists(outputFile))
                        File.Delete(outputFile);

                    using (StreamWriter fileWriter = new StreamWriter(outputFile, true, Encoding.UTF8, 5120))
                    {
                        ProcessSaveLogEntriesToFile(includeTimestamp, resEv, fileWriter);

                        while (!string.IsNullOrEmpty(resEv.NextToken))
                        {
                            reqEv.NextToken = resEv.NextToken;
                            resEv = client.FilterLogEventsAsync(reqEv).GetAwaiter().GetResult();

                            ProcessSaveLogEntriesToFile(includeTimestamp, resEv, fileWriter);
                        }

                        returnValue.Add(applicationName, outputFile);
                        fileWriter.Flush();
                    }
                }
                return returnValue;
            });
        }

        #endregion

        #region GetLambdaLogGroupsByAppCode

        public static Task<Dictionary<string, string>> GetLambdaLogGroupsByAppCode(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                Dictionary<string, string> returnValue = new Dictionary<string, string>();
                string logGroupPrefix = $"/aws/lambda/{appCode.ToLower()}-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-";
                List<string> foundResults = FindLogGroups(domain, env, logGroupPrefix).GetAwaiter().GetResult();

                foreach (string foundItem in foundResults)
                {
                    var splitItems = foundItem.Trim('/').Split('/');
                    if (splitItems[splitItems.GetLength(0) - 1].StartsWith(@"") && !returnValue.ContainsKey(splitItems[splitItems.GetLength(0) - 1]))
                        returnValue.Add($"fh.{splitItems[splitItems.GetLength(0) - 1].Replace($"{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-", string.Empty).Replace("-", ".")}", string.Empty);

                    returnValue[$"fh.{splitItems[splitItems.GetLength(0) - 1].Replace($"{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-", string.Empty).Replace("-", ".")}"] = foundItem;
                }

                return returnValue;
            });
        }

        #endregion

        #region Get Ec2 instance Logstreams by instanceid 

        public static Task<Dictionary<string, LogStreamItem>> GetWindowsLogStreamsByInstanceId(string instanceId, EAWSDomain domain, EAWSEnvironment env)
        {
            return GetLogStreams(domain, env, "rfh-instance-logs-windows", $"{instanceId.ToLower()}/", true);
        }

        #endregion

        #region GetApplicationLogNamesByAppCode

        public static Task<List<string>> GetApplicationLogNamesByAppCode(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                List<string> returnValue = new List<string>();

                var functionResult = GetLogStreams(domain, env, $"/app/{appCode.ToLower()}", $"{AWSCommon.TranslateStage(env)}", true).GetAwaiter().GetResult();
                foreach (LogStreamItem l in functionResult.Values.ToArray())
                {
                    string[] streamNameItems = l.LogStreamName.Split('/');
                    if (!returnValue.Contains(streamNameItems[1]))
                    {
                        returnValue.Add(streamNameItems[1]);
                    }
                }

                return returnValue;
            });
        }

        #endregion

        #region GetApplicationLogStreamsByApplication

        public static Task<Dictionary<string, LogStreamItem>> GetApplicationLogStreamsByApplication(string appCode, string applicationName, EAWSDomain domain, EAWSEnvironment env, bool includeEmptyLogStreams)
        {
            return GetLogStreams(domain, env, $"/app/{appCode.ToLower()}", $"{AWSCommon.TranslateStage(env)}/{applicationName}/", includeEmptyLogStreams);
        }

        public static Task<Dictionary<string, LogStreamItem>> GetApplicationLogStreamsByApplication(string appCode, string applicationName, EAWSDomain domain, EAWSEnvironment env)
        {
            return GetApplicationLogStreamsByApplication(appCode, applicationName, domain, env, true);
        }

        #endregion

        #region GetLogEntiresFromLogStreams

        public static Task<Dictionary<string, StringBuilder>> GetLogEntriesFromLogStreams(string appCode, string applicationName, EAWSDomain domain, EAWSEnvironment env,
            DateTime filterDateTimeFrom, DateTime filterDateTimeTo, List<string> logStreams)
        {
            return GetLogEntries(appCode, applicationName, domain, env, filterDateTimeFrom, filterDateTimeTo, $"/app/{appCode.ToLower()}", logStreams);
        }

        #endregion

        #region SaveLogEntriesToFileByAppCode

        public static Task<Dictionary<string, string>> SaveLogEntriesToFileByAppCode(string appCode, EAWSDomain domain, EAWSEnvironment env,
            DateTime filterDateTimeFrom, DateTime filterDateTimeTo, List<string> logStreams, string destinationFolder, bool ignoreCase, string ownerOperator)
        {
            return SaveLogEntriesToFile(domain, env, filterDateTimeFrom, filterDateTimeTo, $"/app/{(ignoreCase ? appCode.ToLower() : appCode)}", logStreams, destinationFolder, false, ownerOperator);
        }

        public static Task<Dictionary<string, string>> SaveLogEntriesToFileByLambdaFunction(EAWSDomain domain, EAWSEnvironment env,
    DateTime filterDateTimeFrom, DateTime filterDateTimeTo, string lambdaLogGroup, string lambdaApplicationName, string destinationFolder, string ownerOperator)
        {
            return SaveLogEntriesToFile(domain, env, filterDateTimeFrom, filterDateTimeTo, lambdaLogGroup, lambdaApplicationName, destinationFolder, true, ownerOperator);
        }

        #endregion

        #region SaveLogEntriesToFileByInstanceEventLog

        public static Task<Dictionary<string, string>> SaveLogEntriesToFileByInstanceEventLog(EAWSDomain domain, EAWSEnvironment env,
    DateTime filterDateTimeFrom, DateTime filterDateTimeTo, List<string> logStreams, string destinationFolder, string ownerOperator)
        {
            return SaveLogEntriesToFile(domain, env, filterDateTimeFrom, filterDateTimeTo, "rfh-instance-logs-windows", logStreams, destinationFolder, true, ownerOperator);
        }

        #endregion

        #endregion
    }
}
