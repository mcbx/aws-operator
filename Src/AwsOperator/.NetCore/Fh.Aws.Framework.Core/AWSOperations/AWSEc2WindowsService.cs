﻿using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSEc2WindowsService
    {
        #region Windows Service Operations

        #region Windows Service Details (Snapshot)

        public static Task<Dictionary<string, List<JsonServiceItem>>> GetServiceSnapshot(EAWSDomain domain, EAWSEnvironment env, string purposeIdTag)
        {
            return GetServiceSnapshot(domain, env, AWSEc2.GetEc2InstanceByPurposeId(purposeIdTag, domain, env).GetAwaiter().GetResult());
        }

        public static Task<Dictionary<string, List<JsonServiceItem>>> GetServiceSnapshot(EAWSDomain domain, EAWSEnvironment env, List<string> ec2Targets)
        {
            return Task.Run(() =>
            {
                Dictionary<string, List<JsonServiceItem>> returnValue = new Dictionary<string, List<JsonServiceItem>>();

                Dictionary<string, CommandDetail> results = AWSRunCommand.RunPowershellCommand(domain, env, ec2Targets, new List<string>() { "Get-Service -Name \"*.Wis*\" | select Name, DisplayName, Status, StartType | ConvertTo-Json -Compress" }, string.Empty).GetAwaiter().GetResult();
                foreach (KeyValuePair<string, CommandDetail> item in results)
                {
                    if (!returnValue.ContainsKey(item.Key))
                        returnValue.Add(item.Key, new List<JsonServiceItem>());

                    returnValue[item.Key].AddRange(AWSCommon.DeserializeServiceJson(item.Value.CommandResult));
                }

                return returnValue;
            });
        }

        #endregion

        public static Task<CommandDetail> StartWindowsServiceOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string serviceName, string commandComment)
        {
            return StartWindowsServiceOnEC2(domain, env, instanceId, new List<string>() { serviceName }, commandComment);
        }

        public static Task<CommandDetail> StartWindowsServiceOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> serviceNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> serviceOperations = new List<string>();
                serviceNames.ForEach(serviceItem =>
                {
                    serviceOperations.Add($"Set-Service -Name '{serviceItem}' -StartupType Automatic; Start-Service '{serviceItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                serviceOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        public static Task<CommandDetail> RestartWindowsServiceOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string serviceName, string commandComment)
        {
            return RestartWindowsServiceOnEC2(domain, env, instanceId, new List<string>() { serviceName }, commandComment);
        }

        public static Task<CommandDetail> RestartWindowsServiceOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> serviceNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> serviceOperations = new List<string>();
                serviceNames.ForEach(serviceItem =>
                {
                    serviceOperations.Add($"Restart-Service '{serviceItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                serviceOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        public static Task<CommandDetail> StopWindowsServiceOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string serviceName, string commandComment)
        {
            return StopWindowsServiceOnEC2(domain, env, instanceId, new List<string>() { serviceName }, commandComment);
        }

        public static Task<CommandDetail> StopWindowsServiceOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> serviceNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> serviceOperations = new List<string>();
                serviceNames.ForEach(serviceItem =>
                {
                    serviceOperations.Add($"Set-Service -Name '{serviceItem}' -StartupType Manual; Stop-Service '{serviceItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                serviceOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion
    }
}
