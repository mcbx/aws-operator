﻿using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSEc2Website
    {
        public static Task<Dictionary<string, JsonIIS>> GetIISWebsiteSnapshot(EAWSDomain domain, EAWSEnvironment env, string purposeIdTag)
        {
            return GetIISWebsiteSnapshot(domain, env, AWSEc2.GetEc2InstanceByPurposeId(purposeIdTag, domain, env).GetAwaiter().GetResult());
        }

        public static Task<Dictionary<string, JsonIIS>> GetIISWebsiteSnapshot(EAWSDomain domain, EAWSEnvironment env, List<string> ec2Targets)
        {
            return Task.Run(() =>
            {
                Dictionary<string, JsonIIS> returnValue = new Dictionary<string, JsonIIS>();

                Dictionary<string, CommandDetail> results = AWSRunCommand.RunPowershellCommand(domain, env, ec2Targets, new List<string>()
            { @"$overview = [pscustomobject]@{ IIS_State='Not Installed'; Websites=[System.Collections.ArrayList]@();}
if ((Get-WindowsFeature Web-Server).InstallState -eq ""Installed"")
{
     $overview.IIS_State = (Get-Service -Name ""W3SVC"" | Select -Expand Status).ToString()
     Foreach($Site in get-website)
     { 
        $entry =[pscustomobject]@{ Website =$Site.name;State=$Site.state; Bindings =[System.Collections.ArrayList]@(); ApplicationPool = ([pscustomobject]@{ Name=$Site.applicationPool; State = ''; Net_Clr_Version = ''})};
        Foreach($Bind in $Site.bindings.collection)
        {
            [void]$entry.Bindings.Add([pscustomobject]@{ Protocol=$Bind.Protocol; BindingAdres=$Bind.BindingInformation; })
        }

        $appPool = Get-IISAppPool -Name $entry.ApplicationPool.Name
        $entry.ApplicationPool.State = $appPool.State.ToString()
        $entry.ApplicationPool.Net_Clr_Version = $appPool.ManagedRuntimeVersion
        [void]$overview.Websites.add($entry)
    }
}

Write-host ($overview | ConvertTo-Json -depth 5 -Compress)
"
            }, string.Empty).GetAwaiter().GetResult();
                foreach (KeyValuePair<string, CommandDetail> item in results)
                {
                    if (!returnValue.ContainsKey(item.Key))
                        returnValue.Add(item.Key, null);

                    returnValue[item.Key] = AWSCommon.DeserializeIISWebsiteJson(item.Value.CommandResult);
                }

                return returnValue;
            });
        }

        #region IIS operations

        #region Start

        public static Task<CommandDetail> StartIIS(EAWSDomain domain, EAWSEnvironment env, string instanceId, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> iisOperations = new List<string>();
                iisOperations.Add("iisreset -start");

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                iisOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #region Stop

        public static Task<CommandDetail> StopIIS(EAWSDomain domain, EAWSEnvironment env, string instanceId, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> iisOperations = new List<string>();
                iisOperations.Add("iisreset -stop");

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                iisOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #region Restart

        public static Task<CommandDetail> RestartIIS(EAWSDomain domain, EAWSEnvironment env, string instanceId, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> iisOperations = new List<string>();
                iisOperations.Add("iisreset");

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                iisOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #endregion

        #region Website operations

        #region Start

        public static Task<CommandDetail> StartWebsiteOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string websiteName, string commandComment)
        {
            return StartWebsiteOnEC2(domain, env, instanceId, new List<string>() { websiteName }, commandComment);
        }

        public static Task<CommandDetail> StartWebsiteOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> websiteNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> websiteOperations = new List<string>();
                websiteNames.ForEach(websiteItem =>
                {
                    websiteOperations.Add($"Start-Website -Name '{websiteItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                websiteOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #region Stop

        public static Task<CommandDetail> StopWebsiteOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string websiteName, string commandComment)
        {
            return StopWebsiteOnEC2(domain, env, instanceId, new List<string>() { websiteName }, commandComment);
        }

        public static Task<CommandDetail> StopWebsiteOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> websiteNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> websiteOperations = new List<string>();
                websiteNames.ForEach(websiteItem =>
                {
                    websiteOperations.Add($"Stop-Website -Name '{websiteItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                websiteOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #region Restart

        public static Task<CommandDetail> RestartWebsiteOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string websiteName, string commandComment)
        {
            return RestartWebsiteOnEC2(domain, env, instanceId, new List<string>() { websiteName }, commandComment);
        }

        public static Task<CommandDetail> RestartWebsiteOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> websiteNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> websiteOperations = new List<string>();
                websiteNames.ForEach(websiteItem =>
                {
                    websiteOperations.Add($"Stop-Website -Name '{websiteItem}'; Start-Website -Name '{websiteItem}';");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                websiteOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #endregion

        #region AppPool operations

        #region Start

        public static Task<CommandDetail> StartAppPoolOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string appPoolName, string commandComment)
        {
            return StartAppPoolOnEC2(domain, env, instanceId, new List<string>() { appPoolName }, commandComment);
        }

        public static Task<CommandDetail> StartAppPoolOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> appPoolNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> appPoolOperations = new List<string>();
                appPoolNames.ForEach(appPoolItem =>
                {
                    appPoolOperations.Add($"Start-WebAppPool -Name '{appPoolItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                appPoolOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #region Stop

        public static Task<CommandDetail> StopAppPoolOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string appPoolName, string commandComment)
        {
            return StopAppPoolOnEC2(domain, env, instanceId, new List<string>() { appPoolName }, commandComment);
        }

        public static Task<CommandDetail> StopAppPoolOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> appPoolNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> appPoolOperations = new List<string>();
                appPoolNames.ForEach(appPoolItem =>
                {
                    appPoolOperations.Add($"Stop-WebAppPool -Name '{appPoolItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                appPoolOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #region Restart

        public static Task<CommandDetail> RestartAppPoolOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, string appPoolName, string commandComment)
        {
            return RestartAppPoolOnEC2(domain, env, instanceId, new List<string>() { appPoolName }, commandComment);
        }

        public static Task<CommandDetail> RestartAppPoolOnEC2(EAWSDomain domain, EAWSEnvironment env, string instanceId, List<string> appPoolNames, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> appPoolOperations = new List<string>();
                appPoolNames.ForEach(appPoolItem =>
                {
                    appPoolOperations.Add($"Restart-WebAppPool -Name '{appPoolItem}'");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
            {
                instanceId
            },
                appPoolOperations, commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }

        #endregion

        #endregion
    }
}
