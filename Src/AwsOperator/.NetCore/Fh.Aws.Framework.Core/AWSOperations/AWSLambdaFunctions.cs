﻿using Amazon.Lambda;
using Amazon.Lambda.Model;
using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSLambdaFunctions
    {
        public static Task<bool> RunLambdaFunctionEvent(EAWSDomain domain, EAWSEnvironment env, string lambdaFunction, string lambdaParameterJson)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return false;

                using (AmazonLambdaClient client = new AmazonLambdaClient(myCred))
                {
                    InvokeRequest command = new InvokeRequest();
                    command.FunctionName = lambdaFunction;
                    command.InvocationType = InvocationType.Event;
                    if (!string.IsNullOrEmpty(lambdaParameterJson))
                        command.Payload = lambdaParameterJson;

                    client.InvokeAsync(command).GetAwaiter().GetResult();
                }

                return true;
            });
        }
    }
}
