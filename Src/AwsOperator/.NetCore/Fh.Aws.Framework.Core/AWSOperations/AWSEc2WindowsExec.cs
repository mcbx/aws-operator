﻿using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSEc2WindowsExec
    {
        public static Task<Dictionary<string, List<JsonTaskItem>>> GetTaskSnapshot(EAWSDomain domain, EAWSEnvironment env, string purposeIdTag)
        {
            return GetTaskSnapshot(domain, env, AWSEc2.GetEc2InstanceByPurposeId(purposeIdTag, domain, env).GetAwaiter().GetResult());
        }

        public static Task<Dictionary<string, List<JsonTaskItem>>> GetTaskSnapshot(EAWSDomain domain, EAWSEnvironment env, List<string> ec2Targets)
        {
            return Task.Run(() =>
            {
                Dictionary<string, List<JsonTaskItem>> returnValue = new Dictionary<string, List<JsonTaskItem>>();
                Dictionary<string, CommandDetail> results = AWSRunCommand.RunPowershellCommand(domain, env, ec2Targets, new List<string>()
                {
                    "get-process \"fh.*\" -IncludeUserName | select Id, ProcessName, UserName | ConvertTo-Json -Compress"
                }
                , string.Empty).GetAwaiter().GetResult();

                foreach (KeyValuePair<string, CommandDetail> item in results)
                {
                    if (!returnValue.ContainsKey(item.Key))
                        returnValue.Add(item.Key, new List<JsonTaskItem>());

                    returnValue[item.Key].AddRange(AWSCommon.DeserializeTaskJson(item.Value.CommandResult));
                }

                return returnValue;
            });
        }

        public static Task<CommandDetail> KillWindowsTask(EAWSDomain domain, EAWSEnvironment env, string instanceId, string taskProcessId, string taskProcessName)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add(taskProcessId, taskProcessName);

            return KillWindowsTask(domain, env, instanceId, parameters, $"AwsEc2WindowsTask => {AWSCredentialKeeper.AWSOperatorUser}");
        }

        public static Task<CommandDetail> KillWindowsTask(EAWSDomain domain, EAWSEnvironment env, string instanceId, Dictionary<string, string> taskProcessItems, string commandComment)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return new CommandDetail() { CommandInstanceId = instanceId, CommandId = "-", CommandResult = "No valid role credentials" };

                List<string> taskKillOperations = new List<string>();
                taskProcessItems.ToList().ForEach(taskItem =>
                {
                    taskKillOperations.Add($"get-process -Id {taskItem.Key} -ErrorAction SilentlyContinue | where ProcessName -eq \"{taskItem.Value}\" | Stop-Process");
                });

                return AWSRunCommand.RunPowershellCommand(domain, env, new List<string>
                {
                    instanceId
                },
                taskKillOperations,
                commandComment).GetAwaiter().GetResult()[instanceId];
            });
        }
    }
}
