﻿using Amazon.Runtime;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSParameterStore
    {
        private const string Ec2CredPrefix = "rfh-aws-localdevadmin";

        #region Parameters 

        #region Get Parameter

        public static Task<ParameterEc2Credentials> GetEc2Credentials(EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                ParameterEc2Credentials returnValue = new ParameterEc2Credentials();
                var ec2CredName = GetParameter(Ec2CredPrefix, domain, env).GetAwaiter().GetResult();
                if (ec2CredName == null)
                    return returnValue;

                var ec2CredKey = GetParameter($"{Ec2CredPrefix}-password", domain, env).GetAwaiter().GetResult();
                if (ec2CredKey == null)
                    return returnValue;

                returnValue.Name = ec2CredName.Value;
                returnValue.Key = ec2CredKey.Value;

                return returnValue;
            });
        }

        public static Task<ParameterDetail> GetParameter(string parameterPath, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return null;

                using (AmazonSimpleSystemsManagementClient client = new AmazonSimpleSystemsManagementClient(myCred))
                {
                    GetParameterRequest req = new GetParameterRequest
                    {
                        Name = parameterPath,
                        WithDecryption = true
                    };

                    try
                    {
                        var res = client.GetParameterAsync(req).GetAwaiter().GetResult();
                        if (res.Parameter == null)
                            return null;

                        if (parameterPath.StartsWith(Ec2CredPrefix, StringComparison.CurrentCulture))
                            return new ParameterDetail() { Name = parameterPath, FullPath = parameterPath, Value = res.Parameter.Value, Secured = res.Parameter.Type == ParameterType.SecureString, Version = res.Parameter.Version };

                        var splitItems = res.Parameter.Name.TrimStart('/').Split('/');
                        if (splitItems.GetLength(0) > 3)
                            return new ParameterDetail() { Name = splitItems[splitItems.GetLength(0) - 1], FullPath = res.Parameter.Name, Value = res.Parameter.Value, Secured = res.Parameter.Type == ParameterType.SecureString, Version = res.Parameter.Version };
                    }
                    catch (ParameterNotFoundException)
                    {
                        return null;
                    }
                }

                return null;
            });
        }

        private static void ProcessGetParametersByPath(List<ParameterDetail> returnValue, GetParametersByPathRequest req, GetParametersByPathResponse res)
        {
            foreach (Parameter parameterItem in res.Parameters)
            {
                var splitItems = parameterItem.Name.TrimStart('/').Split('/');
                if (splitItems.GetLength(0) > 3)
                    returnValue.Add(new ParameterDetail() { Name = parameterItem.Name.Replace($"{req.Path}{splitItems[3]}", string.Empty).TrimStart('/'), FullPath = parameterItem.Name, Value = parameterItem.Value, Secured = parameterItem.Type == ParameterType.SecureString, Version = parameterItem.Version });
            }
        }

        public static Task<List<ParameterDetail>> GetParametersByPath(string parameterPath, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                List<ParameterDetail> returnValue = new List<ParameterDetail>();
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                using (AmazonSimpleSystemsManagementClient client = new AmazonSimpleSystemsManagementClient(myCred))
                {
                    GetParametersByPathRequest req = new GetParametersByPathRequest
                    {
                        Path = parameterPath,
                        Recursive = true,
                        WithDecryption = true
                    };

                    var res = client.GetParametersByPathAsync(req).GetAwaiter().GetResult();
                    ProcessGetParametersByPath(returnValue, req, res);

                    while (!string.IsNullOrEmpty(res.NextToken))
                    {
                        req.NextToken = res.NextToken;
                        res = client.GetParametersByPathAsync(req).GetAwaiter().GetResult();

                        ProcessGetParametersByPath(returnValue, req, res);
                    }
                }

                return returnValue;
            });
        }

        public static Task<Dictionary<string, List<ParameterDetail>>> GetParametersByApplicationCode(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                Dictionary<string, List<ParameterDetail>> returnValue = new Dictionary<string, List<ParameterDetail>>();

                var functionResult = GetParametersByPath(string.Format("/app/{0}/{1}/", appCode, AWSCommon.TranslateStage(env)), domain, env).GetAwaiter().GetResult();
                foreach (ParameterDetail parameterItem in functionResult)
                {
                    var splitItems = parameterItem.FullPath.TrimStart('/').Split('/');
                    if (splitItems.GetLength(0) > 3)
                    {
                        if (!returnValue.ContainsKey(splitItems[3]))
                            returnValue.Add(splitItems[3], new List<ParameterDetail>());

                        returnValue[splitItems[3]].Add(parameterItem);
                    }
                }

                return returnValue;
            });
        }

        public static Task<Dictionary<string, List<ParameterDetail>>> GetSharedParametersByApplicationCode(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                Dictionary<string, List<ParameterDetail>> returnValue = new Dictionary<string, List<ParameterDetail>>();

                var functionResult = GetParametersByPath(string.Format("/app/{0}/{1}/", appCode, AWSCommon.TranslateStage(env)), domain, env).GetAwaiter().GetResult();
                foreach (ParameterDetail parameterItem in functionResult)
                {
                    var splitItems = parameterItem.FullPath.TrimStart('/').Split('/');
                    if (splitItems.GetLength(0) > 3)
                    {
                        if (!returnValue.ContainsKey(splitItems[3]))
                            returnValue.Add(splitItems[3], new List<ParameterDetail>());

                        returnValue[splitItems[3]].Add(parameterItem);
                    }
                }

                return returnValue;
            });
        }

        #endregion

        #region Update Parameter

        public static Task<ParameterDetail> UpdateParameter(string fullParameterPath, string parameterValue, bool parameterIsSecured, long currentParameterVersion, EAWSDomain domain, EAWSEnvironment env)
        {
            return UpdateParameter(new ParameterDetail() { FullPath = fullParameterPath, Value = parameterValue, Secured = parameterIsSecured, Version = currentParameterVersion }, domain, env);
        }

        public static Task<ParameterDetail> UpdateParameter(ParameterDetail updateItem, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return null;

                using (AmazonSimpleSystemsManagementClient client = new AmazonSimpleSystemsManagementClient(myCred))
                {
                    PutParameterRequest updateReq = new PutParameterRequest
                    {
                        Name = updateItem.FullPath,
                        Value = updateItem.Value,
                        Type = updateItem.Secured ? ParameterType.SecureString : ParameterType.String,
                        Overwrite = true
                    };

                    var res = client.PutParameterAsync(updateReq).GetAwaiter().GetResult();
                    if (res.Version == updateItem.Version)
                        return null;

                    var updateResult = GetParameter(updateItem.FullPath, domain, env);
                    return updateResult;
                }
            });
        }

        #endregion

        #region Delete Parameter

        public static Task<bool> DeleteParameter(string fullParameterPath, EAWSDomain domain, EAWSEnvironment env)
        {
            return DeleteParameter(new ParameterDetail() { FullPath = fullParameterPath }, domain, env);
        }

        public static Task<bool> DeleteParameter(ParameterDetail parameterItem, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                if (myCred == null)
                    return false;

                try
                {
                    using (AmazonSimpleSystemsManagementClient client = new AmazonSimpleSystemsManagementClient(myCred))
                    {
                        DeleteParameterRequest dReq = new DeleteParameterRequest();
                        dReq.Name = parameterItem.FullPath;

                        var dRes = client.DeleteParameterAsync(dReq).GetAwaiter().GetResult();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return false;
                }
            });
        }

        #endregion

        #region Parameter History

        public static Task<SortedDictionary<long, ParameterHistoryDetail>> GetParameterHistory(ParameterDetail parameterItem, EAWSDomain domain, EAWSEnvironment env)
        {
            return Task.Run(() =>
            {
                SortedDictionary<long, ParameterHistoryDetail> returnValue = new SortedDictionary<long, ParameterHistoryDetail>();

                AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
                if (myCred == null)
                    return returnValue;

                try
                {
                    using (AmazonSimpleSystemsManagementClient client = new AmazonSimpleSystemsManagementClient(myCred))
                    {
                        GetParameterHistoryRequest req = new GetParameterHistoryRequest();
                        req.Name = parameterItem.FullPath;
                        req.WithDecryption = true;

                        var res = client.GetParameterHistoryAsync(req).GetAwaiter().GetResult();
                        foreach (ParameterHistory historyItem in res.Parameters)
                        {
                            returnValue.Add(historyItem.Version, new ParameterHistoryDetail()
                            {
                                FullPath = historyItem.Name,
                                Name = parameterItem.Name,
                                Value = historyItem.Value,
                                Version = historyItem.Version,
                                ModifiedBy = historyItem.LastModifiedUser,
                                ModifiedOn = historyItem.LastModifiedDate,
                                Secured = historyItem.Type == ParameterType.SecureString,
                                Labels = new List<string>(historyItem.Labels)
                            });
                        }

                        while (!string.IsNullOrEmpty(res.NextToken))
                        {
                            req.NextToken = res.NextToken;
                            res = client.GetParameterHistoryAsync(req).GetAwaiter().GetResult();

                            foreach (ParameterHistory historyItem in res.Parameters)
                            {
                                returnValue.Add(historyItem.Version, new ParameterHistoryDetail()
                                {
                                    FullPath = historyItem.Name,
                                    Name = parameterItem.Name,
                                    Value = historyItem.Value,
                                    Version = historyItem.Version,
                                    ModifiedBy = historyItem.LastModifiedUser,
                                    ModifiedOn = historyItem.LastModifiedDate,
                                    Secured = historyItem.Type == ParameterType.SecureString,
                                    Labels = new List<string>(historyItem.Labels)
                                });
                            }
                        }

                        return returnValue;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return new SortedDictionary<long, ParameterHistoryDetail>();
                }
            });
        }

        #endregion

        #endregion
    }
}
