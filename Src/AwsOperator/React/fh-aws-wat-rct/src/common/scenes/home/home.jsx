import React, {useState, useEffect} from 'react'
import {NavLink} from 'react-router-dom'

import {Button, Typography, Link} from '@material-ui/core'
import FilterNoneIcon from '@material-ui/icons/FilterNone'
import BackupIcon from '@material-ui/icons/Backup'
import { hasAccess } from 'WATApp/common/services/functions/user-role-utils'
import 'WATApp/common/scenes/home/home.scss'

export default function Home (props) {
    const [roles, setRoles] = useState( '' )

    useEffect( () => {
        setRoles( props.userRoles ? props.userRoles : '' )
      }, [props.userRoles])

    const createNavLink = (link, displayName) => {
        const navLink = React.forwardRef((props, ref) => (
            <NavLink to={link} innerRef={ref} exact {...props} />
        ))
        navLink.displayName = displayName
        return navLink
    }

    const Ec2ControllerLink = createNavLink('/ec2controller', 'EC2')
    const ViewController = createNavLink('/viewcontroller', 'VIEW')

    return (
      <div className='home-container' id='home-page'>
        <h1>Dashboard</h1>
        <div className='nav-buttons'>
          {roles && hasAccess(roles, 'serverRoles') && (
            <Link className='button-link' component={Ec2ControllerLink}>
              <Button className='nav-button large'>
                <FilterNoneIcon fontSize='inherit' />
                <Typography>EC2 Controller</Typography>
              </Button>
            </Link>
          )}
          {roles && hasAccess(roles, 'barRoles') && (
            <Link
              className='button-link'
              component={ViewController}
            >
              <Button className='nav-button large'>
                <BackupIcon fontSize='inherit' />
                <Typography>View Controller</Typography>
              </Button>
            </Link>
          )}
        </div>
      </div>
    )
}