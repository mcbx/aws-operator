import React from 'react'
import { Container, Button, Typography, makeStyles } from '@material-ui/core'
import { useHistory } from 'react-router-dom'

const initStyles = makeStyles({
  content: {
    flexGrow: 1,
    marginTop: 50,
  },
  root: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 100,
    overflow: 'hidden'
  }
})

function NotFound() {
  const classes = initStyles( {} )
  const history = useHistory()

  const gotoHome = () => {
    history.push('/')
  }

  return (
    <Container className={classes.root}>
      <Typography variant='h3' >Page not found!</Typography>
      <Button className={classes.content} onClick={gotoHome}>Go to homepage</Button>
    </Container>
  )
}

export default NotFound