import React, {useState, useEffect} from 'react'
import {Route, Switch} from 'react-router-dom'

// General
import Home from 'WATApp/common/scenes/home/home'

// Errors
import NotFound from 'WATApp/common/scenes/notfound/notfound'
import {hasAccess} from 'WATApp/common/services/functions/user-role-utils'

export default function Routes (props) {
  const [roles, setRoles] = useState('')

  useEffect( () => {
    setRoles( props.userInfo.profile ? props.userInfo.profile.roles : '' )
  }, [props.userInfo.profile])

  return (
    <Switch>
      <Route
        path='/'
        exact
        render={(props) => <Home {...props} userRoles={roles} />} 
      />
      {/* {roles && hasAccess(roles, 'serverRoles') && (
        <Route
          path='/ec2controller'
          render={props => <Ec2Controller {...props} props={userInfo} />}
        />
      )} */}
      <Route component={NotFound} />
    </Switch>
  )
}