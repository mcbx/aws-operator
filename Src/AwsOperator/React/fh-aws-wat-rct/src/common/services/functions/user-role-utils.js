import roles from 'WATApp/common/services/constants/user-roles'

export const hasAccess = (userRoles, roleType) => 
  userRoles.some(role => roles[roleType].access.includes(role))

export const hasAdminRights = (userRoles, roleType) => 
  userRoles.some(role => roles[roleType].adminRights.includes(role))

export const hasWriteAccess = (userRoles, roleType, env) =>
  hasAdminRights(userRoles, roleType) || (env !== 'production' && userRoles.some(role => roles[roleType].writeRights.includes(role)))