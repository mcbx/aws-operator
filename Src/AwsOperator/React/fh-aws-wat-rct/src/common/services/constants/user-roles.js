const roles = {
    serverRoles: {
      access: ['Admin', 'Starware', 'ReadOnly'],
      adminRights: ['Admin'],
      writeRights: ['Admin', 'Starware'],
    },
    barRoles: {
      access: ['Admin', 'BarAdmin', 'BarUser'],
      adminRights: ['Admin', 'BarAdmin'],
    },
    envRoles: {
      access: ['Admin', 'Starware', 'ReadOnly'],
      adminRights: ['Admin'],
      writeRights: ['Admin', 'Starware'],
    },
    performanceRoles: {
      access: ['Admin', 'ReadOnly', 'Starware'],
      adminRights: ['Admin'],
    },
  }
  
  export default roles  