import { AuthenticationContext } from 'react-adal'

const adalConfig = {
  tenant: process.env.TENANT,
  clientId: process.env.CLIENT_ID,
  redirectUri: process.env.REDIRECT_URI,
  cacheLocation: process.env.CACHE_LOCATION,
  endpoints: '' // Fill in API endpoints used | ! the adal library always needs a value for endpoints 
}

export default class AdalService {
  authContext = new AuthenticationContext(adalConfig)
  getAzureAdToken = async () => await this.authContext.getCachedToken(adalConfig.clientId)
  getCachedUser = async () => await this.authContext.getCachedUser()
}