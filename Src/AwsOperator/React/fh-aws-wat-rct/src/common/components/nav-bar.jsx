import * as React from 'react'
import { useHistory } from 'react-router-dom'

import { Paper, IconButton, makeStyles } from '@material-ui/core'
import ArrowBack from '@material-ui/icons/ArrowBack'
import ArrowForward from '@material-ui/icons/ArrowForward'
import Typography from "@material-ui/core/Typography"
import packageJson from '../../../package.json'

const initStyles = makeStyles({
    root: {
        display: 'flex',
        alignItems: 'center',
        height: '60px',
        padding: '25px',
    },
    buttonContainer: {
        marginLeft: 40,
    },
    version: {
        flex: 1,
    }
})

export default function NavBar(props) {
    const classes = initStyles({})
    const history = useHistory()
    
    return (
        <Paper className={classes.root}>
            <Typography variant="h5">AWS Management</Typography>
            <div className={classes.buttonContainer}>
                <IconButton color="primary" onClick={ () => history.goBack() }>
                    <ArrowBack/>
                </IconButton>
                <IconButton color="primary" onClick={ () => history.goForward() }>
                    <ArrowForward/>
                </IconButton>
            </div>
            <Typography className={classes.version} variant='subtitle2' color='inherit' align='right'>
                {`v${packageJson.version}`}
            </Typography>
        </Paper>
    )
}