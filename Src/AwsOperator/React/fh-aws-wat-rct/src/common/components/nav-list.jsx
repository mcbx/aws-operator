import React, {useState, useEffect} from 'react'
import {NavLink} from 'react-router-dom'
import {withRouter} from 'react-router'

import Link from '@material-ui/core/Link'
import {List, ListItem, ListItemIcon, Tooltip, Paper} from '@material-ui/core'
import FilterNoneIcon from '@material-ui/icons/FilterNone'
import HomeIcon from '@material-ui/icons/Home'
import BackupIcon from '@material-ui/icons/Backup'
import {hasAccess} from 'WATApp/common/services/functions/user-role-utils'
import 'WATApp/common/components/nav-list.scss'

function NavList ( props ) {
  const [roles, setRoles] = useState('')

  useEffect( () => {
    setRoles( props.userInfo.profile ? props.userInfo.profile.roles : '' )
  }, [props.userInfo.profile])

  const createNavLink = (link, displayName) => {
    const navLink = React.forwardRef((props, ref) => (
      <NavLink to={link} innerRef={ref} exact {...props} />
    ))
    navLink.displayName = displayName
    return navLink
  }
  const isSelected = path => path === props.location.pathname

  const HomeLink = createNavLink('/', 'Home')
  const Ec2ControllerLink = createNavLink('/ec2controller', 'EC2navlink')
  const ViewController = createNavLink('/viewcontroller', 'VIEWnavlink')

  return (
    <Paper square className='navListPaper'>
      <List className='navList'>
        <Link component={HomeLink}>
          <Tooltip title='Home' classes={{tooltip: 'nav-tooltip'}}>
            <ListItem button selected={isSelected('/')}>
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
            </ListItem>
          </Tooltip>
        </Link>
          <Link component={Ec2ControllerLink}>
            <Tooltip title='EC2 Controller' classes={{tooltip: 'nav-tooltip'}}>
              <ListItem button selected={isSelected('/ec2controller')}>
                <ListItemIcon>
                  <FilterNoneIcon />
                </ListItemIcon>
              </ListItem>
            </Tooltip>
          </Link>
        {roles && hasAccess(roles, 'serverRoles') && (
          <Link component={ViewController}>
            <Tooltip title='View Controller' classes={{tooltip: 'nav-tooltip'}}>
              <ListItem button selected={isSelected('/viewcontroller')}>
                <ListItemIcon>
                  <BackupIcon />
                </ListItemIcon>
              </ListItem>
            </Tooltip>
          </Link>
        )}
      </List>
    </Paper>
  )
}

const NavListWithRouter = withRouter(NavList)
export default NavListWithRouter