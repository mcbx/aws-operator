import React, { Suspense, useState, useEffect } from 'react'
import { HashRouter } from 'react-router-dom'

import { createStyles, WithStyles } from '@material-ui/styles'
import { createMuiTheme, MuiThemeProvider, CssBaseline, Theme, withStyles, makeStyles } from '@material-ui/core'
import red from '@material-ui/core/colors/red'
import blue from '@material-ui/core/colors/blue'
import deepPurple from '@material-ui/core/colors/deepPurple'

import AdalService from 'WATApp/common/services/adal-service'
import Routes from 'WATApp/common/scenes/routes/routes'
import NavBar from 'WATApp/common/components/nav-bar'
import NavList from 'WATApp/common/components/nav-list'

// const appStyles = (Theme) =>
//   createStyles({
//     content: {
//       flexGrow: 1,
//     },
//     root: {
//       display: 'flex',
//       flexGrow: 1,
//     },
// })

const initStyles = makeStyles({
    content: {
      flexGrow: 1,
    },
    root: {
      display: 'flex',
      flexGrow: 1,
    }
})

// Authentication
const adalService = new AdalService()

export default function App ( props ) {
  const [nightModus, setNightModus] = useState( JSON.parse(localStorage.getItem('nightModus')) )
  const [theme, setTheme] = useState( {} )
  const [user, setUser] = useState( {} )
  const classes = initStyles( {} )

  useEffect( () => {
    getUser()
  }, [] )

  useEffect( () => {
    setTheme( createTheme( nightModus ) ) 
  }, [nightModus])

  const getUser = async () => {
    setUser(await adalService.getCachedUser())
  }

  const createTheme = darkMode => {
    return createMuiTheme({
      palette: {
        type: darkMode ? 'dark' : 'light',
        primary: blue,
        secondary: deepPurple,
        error: red,
        contrastThreshold: 3,
        tonalOffset: 0.4,
      },
      typography: {
        useNextVariants: true,
      },
    })
  }

  return (
    <React.Fragment>
      <Suspense fallback={null}>
        <CssBaseline />
        <div className={classes.root}>
          <main className={classes.content}>
          <HashRouter>
            {/* <MuiThemeProvider theme={theme}> */}

              <NavBar />
              <NavList userInfo={user} />
              <Routes userInfo={user} />

            {/* </MuiThemeProvider> */}
          </HashRouter>
          </main>
        </div>
      </Suspense>
    </React.Fragment>
  )
}

// export default withStyles(appStyles)(App)