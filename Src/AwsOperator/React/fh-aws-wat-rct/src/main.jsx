import React from 'react'
import ReactDOM from 'react-dom'
import { runWithAdal } from 'react-adal'
import AdalService from 'WATApp/common/services/adal-service'
import App from 'WATApp/App'

// Developer account for sandbox environment
const devRole = ['rfh-sandbox:002779451522:DevRoleFromMain']
const DO_NOT_LOGIN = false
const adalService = new AdalService()

runWithAdal(
  adalService.authContext,
  () => {
    ReactDOM.render(<App role={ devRole } />, document.getElementById('app'))
  },
  DO_NOT_LOGIN
)