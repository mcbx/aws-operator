const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack')

const port = process.env.PORT || 3000

module.exports = {
  mode: 'development',
  entry: {
    polyfill: '@babel/polyfill',
    app: './src/main.jsx',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: 'bundle.js',
  },
  resolve: {
    alias: {
      WATApp: path.resolve(__dirname, 'src'),
    //   'react-dom': '@hot-loader/react-dom',
    },
    extensions: [
      '*',
      '.js',
      '.jsx',
      '.ts',
      '.tsx'
    ],
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        include: [
          path.resolve(__dirname, 'src')
        ],        
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              presets: [
                '@babel/env',
                '@babel/react',
              ],
              plugins: [
                'react-hot-loader/babel',
                '@babel/plugin-proposal-class-properties',
                '@babel/plugin-transform-modules-commonjs',
                '@babel/plugin-transform-runtime',
              ],
            },
          },
          {
            loader: 'eslint-loader',
          },
        ],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      favicon: 'public/favicon.ico',
      inject: false,
      hash: false,
    }),
    new webpack.HotModuleReplacementPlugin(),
    new Dotenv({
      path: './dev.env',
    }),
  ],
  devtool: 'source-map',
  devServer: {
    // https: true, // Local DevServer https 
    disableHostCheck: true, // temp workaround for Invalid Host/Origin header
    contentBase: path.join(__dirname, 'src'),
    index: 'index.html',
    compress: false,
    port: port,
    hot: true,
    historyApiFallback: true,
  },
}