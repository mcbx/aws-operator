﻿using Amazon.IdentityManagement;
using Amazon.IdentityManagement.Model;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using System;
using System.Collections.Generic;

namespace Fh.Aws.Framework.AWSLogin
{
    #region Enums

    public enum EAWSRole
    {
        Readonly,
        DevOps
    }

    public enum EAWSEnvironment
    {
        //ExperimentalBox,
        Dev,
        Sys,
        Acc,
        Prd
    }

    public enum EAWSDomain
    {
        //ExperimentalBox,
        Commerce,
        Operations,
        Finance,
        Support,
        Infra_Shared
    }

    public enum EAWSLoginStatus
    {
        Success,
        Failed,
        Expired,
        EasterEgg
    }

    #endregion

    public static class AWSCredentialKeeper
    {
        #region public fields/properties

        public static DateTime LoginDateTime { get; private set; }

        public static string AWSOperatorUser { get; set; }

        #endregion

        #region private fields/properties

        private static Dictionary<EAWSDomain, Dictionary<EAWSEnvironment, Dictionary<EAWSRole, string>>> _roles = new Dictionary<EAWSDomain, Dictionary<EAWSEnvironment, Dictionary<EAWSRole, string>>>();
        private static AWSCredentials cred = null;

        private static string ProfileName { get; set; }
        private static Dictionary<EAWSDomain, List<EAWSEnvironment>> _activeDomains { get; set; } = null;

        #endregion

        #region Getters/Setters

        public static Dictionary<EAWSDomain, List<EAWSEnvironment>> ActiveDomains
        {
            get
            {
                if (_activeDomains == null)
                    throw new InvalidOperationException("AWSCKeeper is not yet initialized.");

                return _activeDomains;
            }
        }

        #endregion

        #region Initialize

        public static void Initialize(Dictionary<string, string> configParameters)
        {
            Initialize(configParameters, string.Empty);
        }

        public static void Initialize(Dictionary<string, string> configParameters, string user)
        {
            AWSOperatorUser = user;
            _activeDomains = new Dictionary<EAWSDomain, List<EAWSEnvironment>>();

            foreach (EAWSDomain domainItem in Enum.GetValues(typeof(EAWSDomain)))
            {
                foreach (EAWSEnvironment environmentItem in Enum.GetValues(typeof(EAWSEnvironment)))
                {
                    foreach (EAWSRole roleItem in Enum.GetValues(typeof(EAWSRole)))
                    {
                        if (configParameters.ContainsKey(string.Format("{0}.{1}.{2}", domainItem.ToString(), environmentItem.ToString(), roleItem.ToString())))
                        {
                            if (!_roles.ContainsKey(domainItem))
                            {
                                _activeDomains.Add(domainItem, new List<EAWSEnvironment>());
                                _roles.Add(domainItem, new Dictionary<EAWSEnvironment, Dictionary<EAWSRole, string>>());
                            }

                            if (!_roles[domainItem].ContainsKey(environmentItem))
                            {
                                _activeDomains[domainItem].Add(environmentItem);
                                _roles[domainItem].Add(environmentItem, new Dictionary<EAWSRole, string>());
                            }

                            _roles[domainItem][environmentItem].Add(roleItem, configParameters[string.Format("{0}.{1}.{2}", domainItem.ToString(), environmentItem.ToString(), roleItem.ToString())]);
                        }
                    }
                }
            }
        }

        #endregion

        public static bool RegisterProfile(string profileName, string accessKey, string secretKey)
        {
            try
            {
                var options = new CredentialProfileOptions
                {
                    AccessKey = accessKey,
                    SecretKey = secretKey
                };

                var profile = new CredentialProfile(profileName, options);
                var netSDKFile = new NetSDKCredentialsFile();
                netSDKFile.RegisterProfile(profile);

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }
        }

        public static void ClearCredentials()
        {
            if (_activeDomains == null)
                throw new InvalidOperationException("AWSCKeeper is not yet initialized.");

            LoginDateTime = DateTime.MinValue;
            cred = null;
        }

        private static AWSCredentials GetProfileCredentials(string profileName)
        {
            var chain = new CredentialProfileStoreChain();
            AWSCredentials awsCredentials;
            if (!chain.TryGetAWSCredentials(profileName, out awsCredentials))
                return null;

            return awsCredentials;
        }

        public static AWSCredentials GetAssumeRoleAWSCredentials(EAWSDomain domain, EAWSEnvironment environment, EAWSRole role)
        {
            if (_activeDomains == null)
                throw new InvalidOperationException("AWSCKeeper is not yet initialized.");

            if (cred == null)
                return null;

            if (cred.GetType() == typeof(InstanceProfileAWSCredentials))
                return cred;

            if (!_roles.ContainsKey(domain))
                return null;

            if (!_roles[domain].ContainsKey(environment))
                return null;

            if (!_roles[domain][environment].ContainsKey(role))
                return null;

            if (ProfileName == "RFH-DEV-VEILEN")
                return cred;

            string sessionName = string.Join("-", new[] { "APP", "OPS", "VJC" });
            if (!string.IsNullOrEmpty(AWSOperatorUser))
                sessionName = $"{sessionName}-{AWSOperatorUser}";

            return new AssumeRoleAWSCredentials(cred, _roles[domain][environment][role], sessionName);
        }

        ///// <summary>
        ///// Not used or finished
        ///// </summary>
        ///// <param name="accessKey"></param>
        ///// <param name="secretKey"></param>
        ///// <param name="token"></param>
        ///// <param name="refresh"></param>
        ///// <returns></returns>
        //public static bool GetCredentialsWithToken(string accessKey, string secretKey, string token, bool refresh)
        //{
        //    if (_activeDomains == null)
        //        throw new InvalidOperationException("AWSCKeeper is not yet initialized.");

        //    if (cred != null && !refresh)
        //        return true;

        //    LoginDateTime = DateTime.MinValue;
        //    AWSCredentials myCred = new SessionAWSCredentials(accessKey, secretKey, token);

        //    return true;
        //}

        public static bool GetCredentialsWithToken(string profileName, string token, bool refresh)
        {
            ProfileName = string.Empty;

            if (_activeDomains == null)
                throw new InvalidOperationException("AWSCKeeper is not yet initialized.");

            if (cred != null && !refresh)
                return true;

            ClearCredentials();

            LoginDateTime = DateTime.MinValue;
            AWSCredentials myCred = GetProfileCredentials(profileName);
            if (myCred == null)
            {
                try
                {
                    cred = FallbackCredentialsFactory.GetCredentials();
                    LoginDateTime = DateTime.Now;
                    return true;
                }
                catch
                {
                    cred = null;
                    return false;
                }
            }

            ProfileName = profileName;

            SessionAWSCredentials returnValue = null;
            using (AmazonIdentityManagementServiceClient imc = new AmazonIdentityManagementServiceClient(myCred))
            {
                ListMFADevicesRequest mfaRequest = new ListMFADevicesRequest();
                ListMFADevicesResponse mfaResponse = imc.ListMFADevices(mfaRequest);

                //no mfadevice is connected to this account
                if (mfaResponse.MFADevices.Count == 0)
                {
                    cred = myCred;
                    LoginDateTime = DateTime.Now;
                    return true;
                }

                using (AmazonSecurityTokenServiceClient stsClient = new AmazonSecurityTokenServiceClient(myCred))
                {
                    GetSessionTokenRequest request = new GetSessionTokenRequest();
                    request.SerialNumber = mfaResponse.MFADevices[0].SerialNumber;
                    request.TokenCode = token;
                    request.DurationSeconds = 43500;

                    GetSessionTokenResponse getSessionTokenResponse = stsClient.GetSessionToken(request);
                    if (getSessionTokenResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
                        returnValue = new SessionAWSCredentials(getSessionTokenResponse.Credentials.AccessKeyId, getSessionTokenResponse.Credentials.SecretAccessKey, getSessionTokenResponse.Credentials.SessionToken);
                }
                cred = returnValue;
                LoginDateTime = DateTime.Now;
                return true;
            }
        }
    }
}