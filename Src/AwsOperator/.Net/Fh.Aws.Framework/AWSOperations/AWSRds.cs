﻿using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using System.Collections.Generic;
using Amazon.RDS;
using Amazon.RDS.Model;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System.Linq;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSRds
    {
        #region Rds Operations

        #region Start 

        public static Dictionary<string, RdsDetail> StartRdsInstance(EAWSDomain domain, EAWSEnvironment env, string rdsInstanceIds)
        {
            return StartRdsInstances(domain, env, new List<string>(new[] { rdsInstanceIds }));
        }

        public static Dictionary<string, RdsDetail> StartRdsInstances(EAWSDomain domain, EAWSEnvironment env, List<string> rdsInstanceIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return new Dictionary<string, RdsDetail>();

            var returnValue = new Dictionary<string, RdsDetail>();
            using (AmazonRDSClient client = new AmazonRDSClient(myCred))
            {
                foreach (string rdsItem in rdsInstanceIds)
                {
                    var req = new StartDBInstanceRequest()
                    {
                        DBInstanceIdentifier = rdsItem
                    };

                    var resp = client.StartDBInstance(req);
                    returnValue.Add(rdsItem, new RdsDetail()
                    {
                        Name = resp.DBInstance.DBInstanceIdentifier,
                        Endpoint = resp.DBInstance.Endpoint?.Address,
                        EndpointPort = resp.DBInstance.Endpoint?.Port,
                        Engine = resp.DBInstance.Engine,
                        RdsType = resp.DBInstance.StorageType,
                        Region = resp.DBInstance.AvailabilityZone,
                        Status = resp.DBInstance.DBInstanceStatus
                    });
                }
            }

            return returnValue;
        }

        #endregion

        #region Stop

        public static Dictionary<string, RdsDetail> StopRdsInstance(EAWSDomain domain, EAWSEnvironment env, string rdsInstanceIds)
        {
            return StopRdsInstances(domain, env, new List<string>(new[] { rdsInstanceIds }));
        }

        public static Dictionary<string, RdsDetail> StopRdsInstances(EAWSDomain domain, EAWSEnvironment env, List<string> rdsInstanceIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return new Dictionary<string, RdsDetail>();

            var returnValue = new Dictionary<string, RdsDetail>();
            using (AmazonRDSClient client = new AmazonRDSClient(myCred))
            {
                foreach (string rdsItem in rdsInstanceIds)
                {
                    var req = new StopDBInstanceRequest()
                    {
                        DBInstanceIdentifier = rdsItem
                    };

                    var resp = client.StopDBInstance(req);
                    returnValue.Add(rdsItem, new RdsDetail()
                    {
                        Name = resp.DBInstance.DBInstanceIdentifier,
                        Endpoint = resp.DBInstance.Endpoint?.Address,
                        EndpointPort = resp.DBInstance.Endpoint?.Port,
                        Engine = resp.DBInstance.Engine,
                        RdsType = resp.DBInstance.StorageType,
                        Region = resp.DBInstance.AvailabilityZone,
                        Status = resp.DBInstance.DBInstanceStatus
                    });
                }
            }

            return returnValue;
        }

        #endregion

        #region Reboot

        public static Dictionary<string, RdsDetail> RebootRdsInstance(EAWSDomain domain, EAWSEnvironment env, string rdsInstanceIds)
        {
            return StopRdsInstances(domain, env, new List<string>(new[] { rdsInstanceIds }));
        }

        public static Dictionary<string, RdsDetail> RebootRdsInstances(EAWSDomain domain, EAWSEnvironment env, List<string> rdsInstanceIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return new Dictionary<string, RdsDetail>();

            var returnValue = new Dictionary<string, RdsDetail>();
            using (AmazonRDSClient client = new AmazonRDSClient(myCred))
            {
                foreach (string rdsItem in rdsInstanceIds)
                {
                    var req = new RebootDBInstanceRequest()
                    {
                        DBInstanceIdentifier = rdsItem
                    };

                    var resp = client.RebootDBInstance(req);
                    returnValue.Add(rdsItem, new RdsDetail()
                    {
                        Name = resp.DBInstance.DBInstanceIdentifier,
                        Endpoint = resp.DBInstance.Endpoint?.Address,
                        EndpointPort = resp.DBInstance.Endpoint?.Port,
                        Engine = resp.DBInstance.Engine,
                        RdsType = resp.DBInstance.StorageType,
                        Region = resp.DBInstance.AvailabilityZone,
                        Status = resp.DBInstance.DBInstanceStatus
                    });
                }
            }

            return returnValue;
        }

        #endregion

        #endregion

        #region Rds Collections

        public static Dictionary<string, RdsDetail> GetRdsInstances(EAWSDomain domain, EAWSEnvironment env)
        {
            Dictionary<string, RdsDetail> returnValue = new Dictionary<string, RdsDetail>();

            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return returnValue;

            using (AmazonRDSClient client = new AmazonRDSClient(myCred))
            {
                DescribeDBInstancesRequest req = new DescribeDBInstancesRequest();
                //req.Filters.Add(new Filter() { Name = "db-instance-id", Values = new List<string>(new[] { $"*-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-*" }) });
    
                var resp = client.DescribeDBInstances(req);
                foreach (var res in resp.DBInstances.Where(item => item.DBInstanceIdentifier.Contains($"-{ AWSCommon.GetEnvironmentNotation(env)}{ AWSCommon.GetDomainNotation(domain)}-")))
                {
                    returnValue.Add(res.DBInstanceIdentifier, new RdsDetail() { Name = res.DBInstanceIdentifier, Endpoint = res.Endpoint?.Address, EndpointPort = res.Endpoint?.Port, Engine = res.Engine, RdsType = res.StorageType, Region = res.AvailabilityZone, Status = res.DBInstanceStatus });
                }

                while (!string.IsNullOrEmpty(resp.Marker))
                {
                    req.Marker = resp.Marker;
                    resp = client.DescribeDBInstances(req);

                    foreach (var res in resp.DBInstances.Where(item => item.DBInstanceIdentifier.Contains($"{ AWSCommon.GetEnvironmentNotation(env)}-{ AWSCommon.GetDomainNotation(domain)}-")))
                    {
                        returnValue.Add(res.DBInstanceIdentifier, new RdsDetail() { Name = res.DBInstanceIdentifier, Endpoint = res.Endpoint?.Address, EndpointPort = res.Endpoint?.Port, Engine = res.Engine, RdsType = res.StorageType, Region = res.AvailabilityZone, Status = res.DBInstanceStatus });
                    }
                }
            }

            return returnValue;
        }

        #endregion
    }
}
