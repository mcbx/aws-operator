﻿using Amazon.EC2.Model;
using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSEc2
    {
        #region EC2 machines operations

        public static Dictionary<string, string> StartInstance(EAWSDomain domain, EAWSEnvironment env, string instanceId)
        {
            return StartInstances(domain, env, new List<string>(new[] { instanceId }));
        }

        public static Dictionary<string ,string> StartInstances(EAWSDomain domain, EAWSEnvironment env, List<string> instanceIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return new Dictionary<string, string>();

            var returnValue = new Dictionary<string, string>();
            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                StartInstancesRequest req = new StartInstancesRequest();
                req.InstanceIds.AddRange(instanceIds);
                var resp = client.StartInstances(req);
                resp.StartingInstances.ForEach(item =>
                {
                    returnValue.Add(item.InstanceId, item.CurrentState.Name.ToString());
                });
            }

            return returnValue;
        }
        
        public static void RestartInstance(EAWSDomain domain, EAWSEnvironment env, string instanceId)
        {
            RestartInstances(domain, env, new List<string>(new[] { instanceId }));
        }

        public static void RestartInstances(EAWSDomain domain, EAWSEnvironment env, List<string> instanceIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return;

            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                RebootInstancesRequest req = new RebootInstancesRequest();
                req.InstanceIds.AddRange(instanceIds);

                var resp = client.RebootInstances(req);
            }
        }

        public static Dictionary<string, string> StopInstance(EAWSDomain domain, EAWSEnvironment env, string instanceId)
        {
            return StopInstances(domain, env, new List<string>(new[] { instanceId }));
        }

        public static Dictionary<string, string> StopInstances(EAWSDomain domain, EAWSEnvironment env, List<string> instanceIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return new Dictionary<string, string>();

            var returnValue = new Dictionary<string, string>();
            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                StopInstancesRequest req = new StopInstancesRequest();
                req.InstanceIds.AddRange(instanceIds);
                var resp = client.StopInstances(req);
                resp.StoppingInstances.ForEach(item =>
                {
                    returnValue.Add(item.InstanceId, item.CurrentState.Name.ToString());
                });
            }

            return returnValue;
        }

        public static Dictionary<string, string> TerminateInstance(EAWSDomain domain, EAWSEnvironment env, string instanceId)
        {
            return TerminateInstances(domain, env, new List<string>(new[] { instanceId }));
        }

        public static Dictionary<string, string> TerminateInstances(EAWSDomain domain, EAWSEnvironment env, List<string> instanceIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return new Dictionary<string, string>();

            var returnValue = new Dictionary<string, string>();
            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                TerminateInstancesRequest req = new TerminateInstancesRequest();
                req.InstanceIds.AddRange(instanceIds);
                var resp = client.TerminateInstances(req);
                resp.TerminatingInstances.ForEach(item =>
                {
                    returnValue.Add(item.InstanceId, item.CurrentState.Name.ToString());
                });
            }

            return returnValue;
        }

        #endregion

        #region Get EC2 machines

        public static Dictionary<string, AmiDetail> GetAmiDetails(EAWSDomain domain, EAWSEnvironment env, List<string> AmiIds)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new Dictionary<string, AmiDetail>();

            Dictionary<string, AmiDetail> returnValue = new Dictionary<string, AmiDetail>();
            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                DescribeImagesRequest req = new DescribeImagesRequest();
                req.ImageIds.AddRange(AmiIds);
                 
                DescribeImagesResponse resp = client.DescribeImages(req);
                resp.Images.ForEach(item =>
                {
                    DateTime convertDatetime;
                    returnValue.Add(item.ImageId, new AmiDetail()
                    {
                        AmiId = item.ImageId,
                        AmiName = item.Name,
                        CreationDateTime = string.IsNullOrEmpty(item.CreationDate) || !DateTime.TryParse(item.CreationDate, out convertDatetime) ? DateTime.MinValue : convertDatetime,
                        Description = item.Description,
                        Platform = item.Platform,
                        Owner = item.OwnerId,
                        Architecture = item.Architecture
                    });

                    returnValue[item.ImageId].SetImageState(item.State);
                });
            }

            return returnValue;
        }

        #region GetAmiDetailsFromLaunchConfiguration

        private static void ProcessAmiDetailsFromLaunchConfiguration(Dictionary<string, AmiDetail> returnValue, List<string> amiIds, Amazon.AutoScaling.Model.DescribeLaunchConfigurationsResponse resp)
        {
            resp.LaunchConfigurations.ForEach(item =>
            {
                returnValue.Add(item.LaunchConfigurationName, new AmiDetail() { AmiId = item.ImageId });
                if (!amiIds.Contains(item.ImageId))
                    amiIds.Add(item.ImageId);
            });
        }

        public static Dictionary<string, AmiDetail> GetAmiDetailsFromLaunchConfiguration(EAWSDomain domain, EAWSEnvironment env, bool disRegardEnvironment)
        {
            return GetAmiDetailsFromLaunchConfiguration(domain, env, new List<string>(), disRegardEnvironment);
        }

        public static Dictionary<string, AmiDetail> GetAmiDetailsFromLaunchConfiguration(EAWSDomain domain, EAWSEnvironment env, List<string> launchConfigurationIds, bool disRegardEnvironment)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new Dictionary<string, AmiDetail>();

            Dictionary<string, AmiDetail> returnValue = new Dictionary<string, AmiDetail>();
            List<string> amiIds = new List<string>();

            using (Amazon.AutoScaling.AmazonAutoScalingClient client = new Amazon.AutoScaling.AmazonAutoScalingClient(myCred))
            {
                var req = new Amazon.AutoScaling.Model.DescribeLaunchConfigurationsRequest();
                if (launchConfigurationIds.Count > 0)
                    req.LaunchConfigurationNames.AddRange(launchConfigurationIds);

                var resp = client.DescribeLaunchConfigurations(req);
                ProcessAmiDetailsFromLaunchConfiguration(returnValue, amiIds, resp);

                while (!string.IsNullOrEmpty(resp.NextToken))
                {
                    req.NextToken = resp.NextToken;
                    resp = client.DescribeLaunchConfigurations(req);

                    ProcessAmiDetailsFromLaunchConfiguration(returnValue, amiIds, resp);
                }
                if (!disRegardEnvironment)
                    returnValue = returnValue.Where(i => i.Key.Contains($"-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-")).ToDictionary(i => i.Key, i => i.Value);
            }

            var amiDetailResults = GetAmiDetails(domain, env, amiIds);
            returnValue.ToList().ForEach(returnItem =>
            {
                if (returnValue[returnItem.Key] != null)
                {
                    if (amiDetailResults.ContainsKey(returnValue[returnItem.Key].AmiId))
                        returnValue[returnItem.Key] = amiDetailResults[returnValue[returnItem.Key].AmiId];
                }
            });

            return returnValue.OrderBy(item => item.Key).ToDictionary(i => i.Key, i => i.Value);
        }

        #endregion

        public static Dictionary<string, InstanceDetail> GetEc2List(EAWSDomain domain, EAWSEnvironment env, bool withImageDetails, bool disRegardEnvironment)
        {
            return GetEc2List(domain, env, new List<string>(), withImageDetails, disRegardEnvironment);
        }

        public static Dictionary<string, InstanceDetail> GetEc2List(EAWSDomain domain, EAWSEnvironment env, List<string> instanceIds, bool withImageDetails, bool disRegardEnvironment)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new Dictionary<string, InstanceDetail>();

            Dictionary<string, InstanceDetail> returnValue = new Dictionary<string, InstanceDetail>();
            List<string> amiIds = new List<string>();

            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                DescribeInstancesRequest req = new DescribeInstancesRequest();
                if (instanceIds.Count > 0)
                    req.Filters.Add(new Filter("instance-id", instanceIds));
                else
                {
                    if (!disRegardEnvironment)
                        req.Filters.Add(new Filter("tag:Name", new List<string>(new[] { $"*-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-*" })));
                }

                DescribeInstancesResponse resp = client.DescribeInstances(req);
                ProcessGetEc2List(returnValue, amiIds, resp);

                while (!string.IsNullOrEmpty(resp.NextToken))
                {
                    req.NextToken = resp.NextToken;
                    resp = client.DescribeInstances(req);

                    ProcessGetEc2List(returnValue, amiIds, resp);
                }
            }

            if (withImageDetails)
            {
                var amiResults = GetAmiDetails(domain, env, amiIds);
                returnValue.Keys.ToList().ForEach(key =>
                {
                    if (amiResults.ContainsKey(returnValue[key].Ami))
                        returnValue[key].AmiDetail = amiResults[returnValue[key].Ami];
                });
            }

            return returnValue;
        }

        private static void ProcessGetEc2List(Dictionary<string, InstanceDetail> returnValue, List<string> amiIds, DescribeInstancesResponse resp)
        {
            resp.Reservations.ForEach(reservationItem =>
            {
                reservationItem.Instances.ForEach(instanceItem =>
                {
                    var nameTag = instanceItem.Tags.Where(tagItem => tagItem.Key.ToLower() == "name").FirstOrDefault();
                    returnValue.Add(instanceItem.InstanceId, new InstanceDetail()
                    {
                        InstanceId = instanceItem.InstanceId,
                        InstanceType = instanceItem.InstanceType.Value,
                        Ami = instanceItem.ImageId,
                        Name = nameTag == null ? string.Empty : nameTag?.Value,
                        NetworkAdres = string.IsNullOrEmpty(instanceItem.PublicIpAddress) ? (string.IsNullOrEmpty(instanceItem.PrivateIpAddress) ? string.Empty : instanceItem.PrivateIpAddress) : instanceItem.PublicIpAddress,
                        NetworkAdresType = string.IsNullOrEmpty(instanceItem.PublicIpAddress) ? (string.IsNullOrEmpty(instanceItem.PrivateIpAddress) ? IpAdresType.Unknown : IpAdresType.Private) : IpAdresType.Public,
                    });

                    returnValue[instanceItem.InstanceId].SetState(instanceItem.State.Name);
                    instanceItem.Tags.ForEach(item =>
                    {
                        returnValue[instanceItem.InstanceId].Tags.Add(item.Key, item.Value);
                    });

                    if (!amiIds.Contains(instanceItem.ImageId))
                        amiIds.Add(instanceItem.ImageId);
                });
            });
        }

        public static Dictionary<string, string> GetEc2AppList(EAWSDomain domain, EAWSEnvironment env, string appCode)
        {
            return GetEc2AppList(domain, env, appCode, new List<string>(new[] { "*" }));
        }

        public static Dictionary<string, string> GetEc2AppList(EAWSDomain domain, EAWSEnvironment env, string appCode, string nameSuffix)
        {
            return GetEc2AppList(domain, env, appCode, new List<string>(new[] { nameSuffix }));
        }

        public static Dictionary<string, string> GetEc2AppList(EAWSDomain domain, EAWSEnvironment env, string appCode, List<string> nameSuffix)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new Dictionary<string, string>();

            List<string> appNotations = new List<string>();
            nameSuffix.ForEach(suffixItem =>
            {
                appNotations.Add($"{appCode.ToLower()}-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-{suffixItem}");
            });

            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                // Executing request & fetching response
                DescribeInstancesResponse resp = client.DescribeInstances(
                    new DescribeInstancesRequest()
                    {
                        Filters = new List<Filter>()
                        {
                            new Filter("tag:Application", new List<string>(new []{ appCode })),
                            new Filter("tag:Name", appNotations),
                            new Filter("instance-state-name", new List<string>(new [] {"running" }))
                        }
                    });

                foreach (Reservation item in resp.Reservations)
                {
                    foreach (Instance instanceItem in item.Instances)
                    {
                        var nameTag = instanceItem.Tags.Where(tagItem => tagItem.Key.ToLower() == "name").FirstOrDefault();
                        returnValue.Add(instanceItem.InstanceId, nameTag == null ? string.Empty : nameTag.Value);
                    }
                }

                return returnValue;
            }
        }

        public static Dictionary<string, List<string>> GetEc2PurposeIdList(EAWSDomain domain, EAWSEnvironment env, string appCode)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new Dictionary<string, List<string>>();

            string appNotation = $"{appCode.ToLower()}-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-*";

            Dictionary<string, List<string>> returnValue = new Dictionary<string, List<string>>();
            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                // Executing request & fetching response
                DescribeInstancesResponse resp = client.DescribeInstances(
                    new DescribeInstancesRequest()
                    {
                        Filters = new List<Filter>()
                        {
                            new Filter("tag:PurposeId", new List<string>(new []{"*", string.Empty})),
                            new Filter("tag:Application", new List<string>(new []{ appCode })),
                            new Filter("tag:Name", new List<string>(new []{ appNotation })),
                            new Filter("instance-state-name", new List<string>(new [] {"running" }))
                        }
                    });

                foreach (Reservation item in resp.Reservations)
                {
                    foreach (Instance instanceItem in item.Instances)
                    {
                        foreach (Amazon.EC2.Model.Tag tagItem in instanceItem.Tags)
                        {
                            if (tagItem.Key.ToLower() != "purposeid")
                                continue;

                            if (!returnValue.ContainsKey(tagItem.Value))
                                returnValue.Add(tagItem.Value, new List<string>());

                            returnValue[tagItem.Value].Add(instanceItem.InstanceId);
                        }
                    }
                }

                return returnValue;
            }
        }

        public static string GetEc2PurposeIdByInstanceId(string appCode, EAWSDomain domain, EAWSEnvironment env, string instanceId)
        {
            return GetEc2PurposeIdByInstanceIds(appCode, domain, env, new List<string>(new[] { instanceId }))[instanceId];
        }

        public static Dictionary<string, string> GetEc2PurposeIdByInstanceIds(string appCode, EAWSDomain domain, EAWSEnvironment env, List<string> instanceIds)
        {
            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            instanceIds.ForEach(item => returnValue.Add(item, string.Empty));

            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return returnValue;

            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                var pageNumber = 1;
                var skip = 150 * (pageNumber - 1);
                do
                {

                    // Executing request & fetching response
                    DescribeInstancesResponse resp = client.DescribeInstances(
                    new DescribeInstancesRequest()
                    {
                        Filters = new List<Filter>()
                        {
                            new Filter("tag:Application", new List<string>(){appCode.ToLower()}),
                            new Filter("instance-id", new List<string>((instanceIds.Skip(skip).Take(150)).Select(item=>$"{item}*")))
                        }
                    });

                    List<Instance> instances = new List<Instance>();
                    instances.AddRange(resp.Reservations.SelectMany(x => x.Instances));

                    instanceIds.ForEach(item =>
                    {
                        var instanceItems = instances.Where(searchItem => searchItem.InstanceId.StartsWith(item)).ToList();
                        if (instanceItems.Count == 1)
                        {
                            Tag purposeId = instanceItems[0].Tags.FirstOrDefault(tagItem => tagItem.Key == "PurposeId");
                            returnValue[item] = purposeId == null ? string.Empty : purposeId.Value;
                        }
                    });
                    pageNumber++;
                    skip = 150 * (pageNumber - 1);
                }
                while (skip < instanceIds.Count);
            }
            return returnValue;
        }

        public static List<string> GetEc2InstanceByPurposeId(string tagValue, EAWSDomain domain, EAWSEnvironment env)
        {
            return GetEc2InstanceByPurposeId((new[] { tagValue }).ToList(), domain, env);
        }

        public static List<string> GetEc2InstanceByPurposeId(List<string> tagValue, EAWSDomain domain, EAWSEnvironment env)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new List<string>();

            List<string> returnValue = new List<string>();
            using (Amazon.EC2.AmazonEC2Client client = new Amazon.EC2.AmazonEC2Client(myCred))
            {
                // Executing request & fetching response
                DescribeInstancesResponse resp = client.DescribeInstances(
                    new DescribeInstancesRequest()
                    {
                        Filters = new List<Filter>()
                        {
                            new Filter("tag:PurposeId", tagValue),
                            new Filter("instance-state-name", new List<string>(new [] {"running" }))

                        }
                    });

                List<Instance> instances = new List<Instance>();
                instances.AddRange(resp.Reservations.SelectMany(x => x.Instances));
                instances.ForEach(item => returnValue.Add(item.InstanceId));
            }

            return returnValue;
        }

        #endregion
    }
}
