﻿using Amazon.CloudFormation;
using Amazon.CloudFormation.Model;
using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSCloudFormationStacks
    {
        public static List<CloudFormationStack> GetStacksByApplicationCode(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            List<CloudFormationStack> returnValues = new List<CloudFormationStack>();

            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return returnValues;

            using (AmazonCloudFormationClient client = new AmazonCloudFormationClient(myCred))
            {
                DescribeStacksRequest req = new DescribeStacksRequest();
                var foundStacks = client.DescribeStacks(req);
                ProcessGetStacksByApplicationCode(appCode, domain, env, returnValues, foundStacks);

                while (!string.IsNullOrEmpty(foundStacks.NextToken)) 
                {
                    req.NextToken = foundStacks.NextToken;
                    foundStacks = client.DescribeStacks(req);

                    ProcessGetStacksByApplicationCode(appCode, domain, env, returnValues, foundStacks);
                }
            }

            return returnValues;
        }

        private static void ProcessGetStacksByApplicationCode(string appCode, EAWSDomain domain, EAWSEnvironment env, List<CloudFormationStack> returnValues, DescribeStacksResponse foundStacks)
        {
            var regex = new Regex($@"(?:(?:^{appCode}(?:-|\.))|(?:(?:-|\.){appCode}(?:-|\.)))");
            var stackSpecial = foundStacks.Stacks.Where(item => regex.IsMatch(item.StackName.ToLower()) && (item.StackName.ToLower().Contains($"-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}")
                                || item.StackName.ToLower().Contains($".{env.ToString().ToLower()}.")));

            returnValues.AddRange(stackSpecial.Select(item => new CloudFormationStack()
            {
                StackName = item.StackName
            }));
        }

        public static List<CloudFormationStackDetailed> GetStackDetailedInfo(List<CloudFormationStack> stacks, EAWSDomain domain, EAWSEnvironment env)
        {
            List<CloudFormationStackDetailed> returnValues = new List<CloudFormationStackDetailed>();
            stacks.ForEach(item =>
            {
                returnValues.Add(GetStackDetailedInfo(item, domain, env));
            });

            return returnValues;
        }

        public static CloudFormationStackDetailed GetStackDetailedInfo(CloudFormationStack stack, EAWSDomain domain, EAWSEnvironment env)
        {
            CloudFormationStackDetailed returnValue = new CloudFormationStackDetailed(stack);


            //TODO: complement data

            return returnValue;
        }

    }
}
