﻿using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSCloudWatchAlarms
    {
        #region Cloudwatch Alarms

        #region GetAlarmsByApplicationCode

        private static List<AlarmDetail> ProcessAlarmsByApplicationCode(string appCode, EAWSEnvironment env, EAWSDomain domain, DescribeAlarmsResponse resp)
        {
            List<AlarmDetail> returnValue = new List<AlarmDetail>();

            if (appCode.ToLower() == "all")
            {
                returnValue.AddRange(resp.MetricAlarms.Select(item => new AlarmDetail()
                {
                    Name = item.AlarmName,
                    Description = item.AlarmDescription,
                    AlarmState = item.StateValue.Value,
                    AlarmStateChanged = item.StateUpdatedTimestamp,
                    TreatMissingData = AlarmDetail.ConvertTreatMissingData(item.TreatMissingData),
                    AlarmMetricNamespace = item.Namespace,
                    AlarmMetricName = item.MetricName
                }));
            }
            else
            {
                var regex = new Regex($@"(?:(?:^{appCode}(?:-|\.))|(?:(?:-|\.){appCode}(?:-|\.)))");
                var alarmsSpecial = resp.MetricAlarms.Where(item => regex.IsMatch(item.AlarmName.ToLower()) && (item.AlarmName.ToLower().Contains($"-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}")
                                    || item.AlarmName.ToLower().Contains($".{env.ToString().ToLower()}.")));

                returnValue.AddRange(alarmsSpecial.Select(item => new AlarmDetail()
                {
                    Name = item.AlarmName,
                    Description = item.AlarmDescription,
                    AlarmState = item.StateValue.Value,
                    AlarmStateChanged = item.StateUpdatedTimestamp,
                    TreatMissingData = AlarmDetail.ConvertTreatMissingData(item.TreatMissingData),
                    AlarmMetricNamespace = item.Namespace,
                    AlarmMetricName = item.MetricName
                }));
            }
            return returnValue;
        }

        public static List<AlarmDetail> GetAlarmsByApplicationCode(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            List<AlarmDetail> returnValue = new List<AlarmDetail>();
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return returnValue;

            using (AmazonCloudWatchClient client = new AmazonCloudWatchClient(myCred))
            {
                DescribeAlarmsRequest req = new DescribeAlarmsRequest();
                var foundAlarms = client.DescribeAlarms(req);
                returnValue.AddRange(ProcessAlarmsByApplicationCode(appCode, env, domain, foundAlarms));

                while (!string.IsNullOrEmpty(foundAlarms.NextToken))
                {
                    req.NextToken = foundAlarms.NextToken;
                    foundAlarms = client.DescribeAlarms(req);
                    returnValue.AddRange(ProcessAlarmsByApplicationCode(appCode, env, domain, foundAlarms));
                }
            }

            return returnValue;
        }

        #endregion

        #region Specific alarm history

        public static Dictionary<string, AlarmHistory> GetHistoryForAlarmNames(EAWSDomain domain, EAWSEnvironment env, string alarmItem)
        {
            return GetHistoryForAlarmNames(domain, env, new List<string>(new[] { alarmItem }));
        }

        public static Dictionary<string, AlarmHistory> GetHistoryForAlarmNames(EAWSDomain domain, EAWSEnvironment env, List<string> alarmItems)
        {
            Dictionary<string, AlarmHistory> returnValue = new Dictionary<string, AlarmHistory>();
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return returnValue;

            if (alarmItems.Count == 0)
                return returnValue;

            using (AmazonCloudWatchClient client = new AmazonCloudWatchClient(myCred))
            {
                foreach (string alarmItem in alarmItems)
                {
                    DescribeAlarmHistoryRequest req = new DescribeAlarmHistoryRequest();
                 //   var res = client.DescribeAlarmHistory(req);

                    


                   // res.AlarmHistoryItems[0]

                    //  req


                }
            }

            throw new System.NotImplementedException();
        }

        #endregion

        public static AlarmDetail GetAlarmByAlarmName(string alarmName, EAWSDomain domain, EAWSEnvironment env)
        {
            return GetAlarmByAlarmName(new List<string>() { alarmName }, domain, env).FirstOrDefault();
        }

        public static List<AlarmDetail> GetAlarmByAlarmName(List<string> alarmNames, EAWSDomain domain, EAWSEnvironment env)
        {
            List<AlarmDetail> returnValue = new List<AlarmDetail>();
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return returnValue;

            if (alarmNames.Count == 0)
                return returnValue;

            using (AmazonCloudWatchClient client = new AmazonCloudWatchClient(myCred))
            {
                var pageNumber = 1;
                var skip = 50 * (pageNumber - 1);
                do
                {
                    DescribeAlarmsRequest req = new DescribeAlarmsRequest();
                    req.AlarmNames = alarmNames.Skip(skip).Take(50).ToList();

                    var foundAlarms = client.DescribeAlarms(req);
                    returnValue.AddRange(foundAlarms.MetricAlarms.Select(item => new AlarmDetail()
                    {
                        Name = item.AlarmName,
                        Description = item.AlarmDescription,
                        AlarmState = item.StateValue.Value,
                        AlarmStateChanged = item.StateUpdatedTimestamp,
                        TreatMissingData = AlarmDetail.ConvertTreatMissingData(item.TreatMissingData),
                        AlarmMetricNamespace = item.Namespace,
                        AlarmMetricName = item.MetricName
                    }));

                    pageNumber++;
                    skip = 50 * (pageNumber - 1);
                }
                while (skip < alarmNames.Count);

            }
            return returnValue;
        }

        public static Task<bool> ResetAlarm(EAWSDomain domain, EAWSEnvironment env, AlarmDetail alarm)
        {
            return Task.Run(() =>
            {
                try
                {
                    alarm.ResetInProgress = true;

                    AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
                    if (myCred == null)
                        return false;

                    using (AmazonCloudWatchClient client = new AmazonCloudWatchClient(myCred))
                    {
                        DescribeAlarmsRequest req = new DescribeAlarmsRequest();
                        req.AlarmNames = new List<string>() { alarm.Name };

                        DescribeAlarmsResponse foundAlarms = client.DescribeAlarms(req);
                        if (foundAlarms.MetricAlarms == null || foundAlarms.MetricAlarms.Count == 0)
                            return false;

                        PutMetricAlarmRequest reqReset = new PutMetricAlarmRequest()
                        {
                            ActionsEnabled = foundAlarms.MetricAlarms[0].ActionsEnabled,
                            AlarmActions = foundAlarms.MetricAlarms[0].AlarmActions,
                            AlarmDescription = foundAlarms.MetricAlarms[0].AlarmDescription,
                            AlarmName = foundAlarms.MetricAlarms[0].AlarmName,
                            ComparisonOperator = foundAlarms.MetricAlarms[0].ComparisonOperator,
                            //  DatapointsToAlarm = foundAlarms.MetricAlarms[0].DatapointsToAlarm,
                            Dimensions = foundAlarms.MetricAlarms[0].Dimensions,
                            EvaluateLowSampleCountPercentile = foundAlarms.MetricAlarms[0].EvaluateLowSampleCountPercentile,
                            EvaluationPeriods = foundAlarms.MetricAlarms[0].EvaluationPeriods,
                            ExtendedStatistic = foundAlarms.MetricAlarms[0].ExtendedStatistic,
                            InsufficientDataActions = foundAlarms.MetricAlarms[0].InsufficientDataActions,
                            MetricName = foundAlarms.MetricAlarms[0].MetricName,
                            Namespace = foundAlarms.MetricAlarms[0].Namespace,
                            OKActions = foundAlarms.MetricAlarms[0].OKActions,
                            Period = foundAlarms.MetricAlarms[0].Period,
                            Statistic = foundAlarms.MetricAlarms[0].Statistic,
                            Threshold = foundAlarms.MetricAlarms[0].Threshold,
                            TreatMissingData = "notBreaching",
                            Unit = foundAlarms.MetricAlarms[0].Unit,
                        };

                        PutMetricAlarmRequest reqRestore = new PutMetricAlarmRequest()
                        {
                            ActionsEnabled = foundAlarms.MetricAlarms[0].ActionsEnabled,
                            AlarmActions = foundAlarms.MetricAlarms[0].AlarmActions,
                            AlarmDescription = foundAlarms.MetricAlarms[0].AlarmDescription,
                            AlarmName = foundAlarms.MetricAlarms[0].AlarmName,
                            ComparisonOperator = foundAlarms.MetricAlarms[0].ComparisonOperator,
                            // DatapointsToAlarm = foundAlarms.MetricAlarms[0].DatapointsToAlarm,
                            Dimensions = foundAlarms.MetricAlarms[0].Dimensions,
                            EvaluateLowSampleCountPercentile = foundAlarms.MetricAlarms[0].EvaluateLowSampleCountPercentile,
                            EvaluationPeriods = foundAlarms.MetricAlarms[0].EvaluationPeriods,
                            ExtendedStatistic = foundAlarms.MetricAlarms[0].ExtendedStatistic,
                            InsufficientDataActions = foundAlarms.MetricAlarms[0].InsufficientDataActions,
                            MetricName = foundAlarms.MetricAlarms[0].MetricName,
                            Namespace = foundAlarms.MetricAlarms[0].Namespace,
                            OKActions = foundAlarms.MetricAlarms[0].OKActions,
                            Period = foundAlarms.MetricAlarms[0].Period,
                            Statistic = foundAlarms.MetricAlarms[0].Statistic,
                            Threshold = foundAlarms.MetricAlarms[0].Threshold,
                            TreatMissingData = foundAlarms.MetricAlarms[0].TreatMissingData,
                            Unit = foundAlarms.MetricAlarms[0].Unit,
                        };

                        var respReset = client.PutMetricAlarm(reqReset);
                        Thread.Sleep((foundAlarms.MetricAlarms[0].Period * 1000)+30000);
                        var respRestore = client.PutMetricAlarm(reqRestore);

                        return true;
                    }
                }
                finally
                {
                    alarm.ResetInProgress = false;
                }
            });
        }

        #endregion
    }
}
