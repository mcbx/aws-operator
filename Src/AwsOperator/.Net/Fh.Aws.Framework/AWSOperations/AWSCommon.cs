﻿using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSCommon
    {
        #region General

        public static string FormatJson(string inputJson)
        {
            try
            {
                dynamic parsedJson = JsonConvert.DeserializeObject(inputJson);
                return JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
            }
            catch
            {
                return inputJson;
            }
        }

        public static IEnumerable<List<T>> SplitList<T>(List<T> locations, int nSize = 100)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }

        public static string TranslateStage(EAWSEnvironment env)
        {
            return TranslateStage(env, true);
        }

        public static string TranslateStage(EAWSEnvironment env, bool ignoreCase)
        {
            string awsStage;
            switch (env)
            {
                case EAWSEnvironment.Dev:
                    awsStage = "Dev";
                    break;
                case EAWSEnvironment.Sys:
                    awsStage = "Sys";
                    break;
                case EAWSEnvironment.Acc:
                    awsStage = "Acc";
                    break;
                case EAWSEnvironment.Prd:
                    awsStage = "Prd";
                    break;
                default:
                    awsStage = "UNKNOWN";
                    break;
            }

            return ignoreCase ? awsStage.ToLower() : awsStage;
        }

        public static string GetDomainNotation(EAWSDomain domain)
        {
            switch (domain)
            {
                //case EAWSDomain.ExperimentalBox:
                //    return "e";
                case EAWSDomain.Commerce:
                    return "c";
                case EAWSDomain.Finance:
                    return "f";
                case EAWSDomain.Support:
                    return "s";
                case EAWSDomain.Infra_Shared:
                    return "i";
                case EAWSDomain.Operations:
                    return "o";
                default:
                    throw new NotSupportedException();
            }
        }

        public static string GetEnvironmentNotation(EAWSEnvironment environment)
        {
            switch (environment)
            {
                //case EAWSEnvironment.ExperimentalBox:
                //    return "e";
                case EAWSEnvironment.Dev:
                    return "o";
                case EAWSEnvironment.Sys:
                    return "t";
                case EAWSEnvironment.Acc:
                    return "a";
                case EAWSEnvironment.Prd:
                    return "p";
                default:
                    throw new NotSupportedException();
            }
        }

        #endregion

        #region Deserialize functions

        public static List<string> DeserializeCommands(string commandsJson)
        {
            List<string> returnValue = new List<string>();

            try
            {
                if (string.IsNullOrEmpty(commandsJson))
                    return returnValue;

                returnValue.AddRange(new JavaScriptSerializer().Deserialize<JsonCommandRoot>(commandsJson).Commands);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                //todo: do nothing
            }

            return returnValue;
        }

        public static List<JsonAWSRunCommandTrigger> DeserializeCommandEvent(string runCommandEventJson)
        {
            List<JsonAWSRunCommandTrigger> returnValue = new List<JsonAWSRunCommandTrigger>();

            try
            {
                if (string.IsNullOrEmpty(runCommandEventJson))
                    return returnValue;

                if (!runCommandEventJson.StartsWith("["))
                    runCommandEventJson = $"[{runCommandEventJson}]";

                returnValue.AddRange(new JavaScriptSerializer().Deserialize<List<JsonAWSRunCommandTrigger>>(runCommandEventJson));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                //todo: do nothing
            }

            return returnValue;
        }

        public static JsonIIS DeserializeIISWebsiteJson(string iisWebsiteJson)
        {
            JsonIIS returnValue = new JsonIIS();
            try
            {
                if (string.IsNullOrEmpty(iisWebsiteJson))
                    return null;

                //if (!iisWebsiteJson.StartsWith("["))
                //    iisWebsiteJson = $"[{iisWebsiteJson}]";

                returnValue = new JavaScriptSerializer().Deserialize<JsonIIS>(iisWebsiteJson);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                //todo: do nothing
            }

            return returnValue;
        }

        public static List<JsonTaskItem> DeserializeTaskJson(string taskJson)
        {
            List<JsonTaskItem> returnValue = new List<JsonTaskItem>();
            try
            {
                if (string.IsNullOrEmpty(taskJson))
                    return returnValue;

                if (!taskJson.StartsWith("["))
                    taskJson = $"[{taskJson}]";

                returnValue.AddRange(new JavaScriptSerializer().Deserialize<List<JsonTaskItem>>(taskJson));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                //todo: do nothing
            }

            return returnValue;
        }

        public static List<JsonServiceItem> DeserializeServiceJson(string serviceJson)
        {
            List<JsonServiceItem> returnValue = new List<JsonServiceItem>();
            try
            {
                if (string.IsNullOrEmpty(serviceJson))
                    return returnValue;

                if (!serviceJson.StartsWith("["))
                    serviceJson = $"[{serviceJson}]";

                returnValue.AddRange(new JavaScriptSerializer().Deserialize<List<JsonServiceItem>>(serviceJson));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                //todo: do nothing
            }

            return returnValue;
        }

        #endregion
    }
}