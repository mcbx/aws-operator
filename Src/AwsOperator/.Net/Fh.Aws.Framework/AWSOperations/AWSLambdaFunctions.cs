﻿using Amazon.Lambda;
using Amazon.Lambda.Model;
using Amazon.Runtime;
using Fh.Aws.Framework.AWSLogin;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSLambdaFunctions
    {
        public static bool RunLambdaFunctionEvent(EAWSDomain domain, EAWSEnvironment env, string lambdaFunction, string lambdaParameterJson)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return false;

            using (AmazonLambdaClient client = new AmazonLambdaClient(myCred))
            {
                InvokeRequest command = new InvokeRequest();
                command.FunctionName = lambdaFunction;
                command.InvocationType = InvocationType.Event;
                if (!string.IsNullOrEmpty(lambdaParameterJson))
                    command.Payload = lambdaParameterJson;

                client.Invoke(command);
            }

            return true;
        }
    }
}
