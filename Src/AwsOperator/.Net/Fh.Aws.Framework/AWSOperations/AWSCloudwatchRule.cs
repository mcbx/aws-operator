﻿using Amazon.CloudWatchEvents;
using Amazon.CloudWatchEvents.Model;
using Amazon.CloudWatchLogs;
using Amazon.CloudWatchLogs.Model;
using Amazon.Runtime;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSCloudwatchRule
    {
        #region Cloudwatch Rules functions

        public static RuleDetail GetRuleInfo(string ruleName, EAWSDomain domain, EAWSEnvironment env)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new RuleDetail();

            using (AmazonCloudWatchEventsClient client = new AmazonCloudWatchEventsClient(myCred))
            {
                DescribeRuleRequest reqRule = new DescribeRuleRequest();
                reqRule.Name = ruleName;

                ListTargetsByRuleRequest reqTarget = new ListTargetsByRuleRequest();
                reqTarget.Rule = ruleName;

                try
                {
                    DescribeRuleResponse resRule = client.DescribeRule(reqRule);
                    if (resRule.HttpStatusCode != System.Net.HttpStatusCode.OK)
                        return new RuleDetail();

                    ListTargetsByRuleResponse resTarget = client.ListTargetsByRule(reqTarget);
                    if (resTarget.HttpStatusCode != System.Net.HttpStatusCode.OK)
                        return new RuleDetail();

                    RuleDetail item = new RuleDetail();
                    item.Id = resRule.Name;
                    item.Schedule = resRule.ScheduleExpression;
                    item.Enabled = resRule.State == RuleState.ENABLED;
                    item.RuleTargets = new List<RuleTargetDetail>();

                    //We only support rules with one target
                    if (resTarget.Targets.Count != 1)
                        new RuleDetail();

                    foreach (Amazon.CloudWatchEvents.Model.Target tg in resTarget.Targets)
                    {
                        //Only targets with runpowershelldocument are supported.
                        if (tg.Arn.ToLower().EndsWith("runpowershellscript"))
                        {
                            RuleTargetDetail ruleTargetItem = new RuleTargetDetail();
                            ruleTargetItem.Commands = AWSCommon.DeserializeCommands(tg.Input);
                            ruleTargetItem.MachineTargets = new Dictionary<string, List<string>>();
                            if (tg.RunCommandParameters != null)
                            {
                                foreach (RunCommandTarget target in tg.RunCommandParameters.RunCommandTargets)
                                {
                                    if (target.Key.ToLower() == "tag:purposeid")
                                    {
                                        foreach (string val in target.Values)
                                        {
                                            if (!ruleTargetItem.MachineTargets.ContainsKey(val))
                                                ruleTargetItem.MachineTargets.Add(val, new List<string>());
                                            ruleTargetItem.MachineTargets[val].AddRange(AWSEc2.GetEc2InstanceByPurposeId(val, domain, env));

                                            if (ruleTargetItem.MachineTargets[val].Count == 0)
                                                ruleTargetItem.MachineTargets[val].Add("NO EC2 MACHINE(S) FOUND!!!!");
                                        }
                                    }
                                }

                                item.RuleTargets.Add(ruleTargetItem);
                            }
                        }

                        if (tg.Arn.ToLower().StartsWith("arn:aws:lambda:"))
                        {
                            RuleLambdaDetail ruleTargetItem = new RuleLambdaDetail();
                            ruleTargetItem.LambdaParameterJson = tg.Input;
                            ruleTargetItem.LambdaFunction = tg.Arn.Split(':').Last();

                            item.RuleLambdaTargets.Add(ruleTargetItem);
                        }
                    }

                    return item;
                }
                catch
                {
                    return null;
                }
            }
        }

        public static List<string> GetRules(string appCode, EAWSDomain domain, EAWSEnvironment env)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new List<string>();

            List<string> returnValues = new List<string>();
            using (AmazonCloudWatchEventsClient client = new AmazonCloudWatchEventsClient(myCred))
            {
                ListRulesRequest req = new ListRulesRequest();
                req.NamePrefix = $"{appCode}-{AWSCommon.GetEnvironmentNotation(env)}{AWSCommon.GetDomainNotation(domain)}-";

                ListRulesResponse resp = client.ListRules(req);
                returnValues.AddRange(resp.Rules.Select(item => item.Name));

                while (!string.IsNullOrEmpty(resp.NextToken))
                {
                    req.NextToken = resp.NextToken;
                    resp = client.ListRules(req);

                    returnValues.AddRange(resp.Rules.Select(item => item.Name));
                }
            }

            return returnValues;
        }

        #region Get SSM Command by instance id & command

        public static Task<Dictionary<string, CommandDetail>> GetCommandsByTagAndCommand(EAWSDomain domain, EAWSEnvironment env, string purposeId, string executionCommand, DateTime invokedAfter, DateTime invokedBefore)
        {
            return Task.Run(() =>
            {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new Dictionary<string, CommandDetail>();

            List<string> instanceIds = AWSEc2.GetEc2InstanceByPurposeId(purposeId, domain, env);
            if (instanceIds.Count == 0)
                return new Dictionary<string, CommandDetail>();

            var returnValue = new Dictionary<string, CommandDetail>();
            using (var client = new AmazonCloudWatchLogsClient(myCred))
            {
                // get from cloudtrail logging
                var reqEv = new FilterLogEventsRequest();
                reqEv.LogGroupName = "rfh-cloudtrail";
                string filterExpression = $"{{ $.eventSource=\"ssm.amazonaws.com\" && $.eventName=\"SendCommand\" && $.requestParameters.targets[0].key = \"tag:PurposeId\" && $.requestParameters.targets[0].values[0]= \"{purposeId}\" && $.requestParameters.parameters.commands[0] =\"{(executionCommand.Replace("\\", "\\\\").Replace("\"", @"\""")).Substring(0, Math.Min(executionCommand.Length, 600)) }*\"}}";
                reqEv.FilterPattern = filterExpression;

                var resEv = client.FilterLogEvents(reqEv);
                ProcessFilterLogEventResponseCWRCommandRule(domain, env, purposeId, instanceIds, resEv, returnValue);

                while (!string.IsNullOrEmpty(resEv.NextToken))
                {
                    reqEv.NextToken = resEv.NextToken;
                    resEv = client.FilterLogEvents(reqEv);

                    ProcessFilterLogEventResponseCWRCommandRule(domain, env, purposeId, instanceIds, resEv, returnValue);
                }
            }

            //TODO: check if we get the right instances id's
            using (var client = new AmazonSimpleSystemsManagementClient(myCred))
            {
                //get from runcommands
                ListCommandsRequest req = new ListCommandsRequest();
                if (invokedAfter != DateTime.MinValue)
                    req.Filters.Add(new CommandFilter() { Key = "InvokedAfter", Value = invokedAfter.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'") });

                if (invokedBefore != DateTime.MinValue)
                    req.Filters.Add(new CommandFilter() { Key = "InvokedBefore", Value = invokedBefore.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'") });

                var resRc = client.ListCommands(req);
                if (resRc.Commands == null)
                    return new Dictionary<string, CommandDetail>();

                    foreach (Amazon.SimpleSystemsManagement.Model.Command item in resRc.Commands)
                    {
                        if (item.Parameters == null || item.Parameters.Count == 0 || !item.Parameters.ContainsKey("commands"))
                            continue;

                        if (!item.Parameters["commands"].Contains(executionCommand))
                            continue;

                        System.Diagnostics.Debug.WriteLine($"{item.CommandId}-{item.Status}-{item.StatusDetails}");

                        List<CommandDetail> targetInstanceIds = new List<CommandDetail>();
                        item.InstanceIds.ForEach(instanceItem =>
                        {
                            var command = new CommandDetail() { CommandId = item.CommandId, CommandInstanceId = instanceItem, CommandStatus = item.Status, CommandStatusDetailed = item.StatusDetails, CommandRunDateTime = item.RequestedDateTime };
                            command.CommandParameters.AddRange(item.Parameters["commands"]);
                            if (!returnValue.ContainsKey($"{item.CommandId}_{instanceItem}"))
                                returnValue.Add($"{item.CommandId}_{instanceItem}", command);
                            else
                                returnValue[$"{item.CommandId}_{instanceItem}"] = command;
                        });

                        if (targetInstanceIds.Count == 0)
                        {
                            AWSRunCommand.GetCommandInvocation(domain, env, item.CommandId).ForEach(cItem => 
                            {
                                cItem.CommandParameters.AddRange(item.Parameters["commands"]);
                                if (!returnValue.ContainsKey($"{item.CommandId}_{cItem.CommandInstanceId}"))
                                    returnValue.Add($"{item.CommandId}_{cItem.CommandInstanceId}", cItem);
                                else
                                    returnValue[$"{item.CommandId}_{cItem.CommandInstanceId}"] = cItem;
                            });
                        }
                    }

                    while (!string.IsNullOrEmpty(resRc.NextToken))
                    {
                        req.NextToken = resRc.NextToken;
                        resRc = client.ListCommands(req);

                        foreach (Amazon.SimpleSystemsManagement.Model.Command item in resRc.Commands)
                        {
                            if (item.Parameters == null || item.Parameters.Count == 0 || !item.Parameters.ContainsKey("commands"))
                                continue;

                            if (!item.Parameters["commands"].Contains(executionCommand))
                                continue;

                            System.Diagnostics.Debug.WriteLine($"{item.CommandId}-{item.Status}-{item.StatusDetails}");

                            List<CommandDetail> targetInstanceIds = new List<CommandDetail>();
                            item.InstanceIds.ForEach(instanceItem =>
                            {
                                var command = new CommandDetail() { CommandId = item.CommandId, CommandInstanceId = instanceItem, CommandStatus = item.Status, CommandStatusDetailed = item.StatusDetails, CommandRunDateTime = item.RequestedDateTime };
                                command.CommandParameters.AddRange(item.Parameters["commands"]);
                                if (!returnValue.ContainsKey($"{item.CommandId}_{instanceItem}"))
                                    returnValue.Add($"{item.CommandId}_{instanceItem}", command);
                                else
                                    returnValue[$"{item.CommandId}_{instanceItem}"] = command;
                            });

                            if (targetInstanceIds.Count == 0)
                            {
                                AWSRunCommand.GetCommandInvocation(domain, env, item.CommandId).ForEach(cItem =>
                                {
                                    cItem.CommandParameters.AddRange(item.Parameters["commands"]);
                                    if (!returnValue.ContainsKey($"{item.CommandId}_{cItem.CommandInstanceId}"))
                                        returnValue.Add($"{item.CommandId}_{cItem.CommandInstanceId}", cItem);
                                    else
                                        returnValue[$"{item.CommandId}_{cItem.CommandInstanceId}"] = cItem;
                                });
                            }
                        }
                    }
                }

                return returnValue;
            });
        }

        private static void ProcessFilterLogEventResponseCWRCommandRule(EAWSDomain domain, EAWSEnvironment env, string purposeId, List<string> instanceIds, FilterLogEventsResponse resEv, Dictionary<string, CommandDetail> returnValue)
        {

            resEv.Events.Select(item => item.Message).ToList().ForEach(item =>
            {
                var eventItem = AWSCommon.DeserializeCommandEvent(item);
                if (eventItem.Count == 1)
                {
                    if (eventItem[0].responseElements != null && eventItem[0].responseElements.command != null && !string.IsNullOrEmpty(eventItem[0].responseElements.command.commandId))
                    {
                        CommandDetail request = new CommandDetail()
                        {
                            CommandId = eventItem[0].responseElements.command.commandId
                        };
                        request.CommandParameters.AddRange(eventItem[0].requestParameters.parameters.commands);

                        var lookupCommands = AWSRunCommand.ListCommandInvocations(domain, env, request);
                        lookupCommands.ForEach(lookupItem =>
                        {
                            if (!returnValue.ContainsKey($"{lookupItem.CommandId}_{lookupItem.CommandInstanceId}"))
                                returnValue.Add($"{lookupItem.CommandId}_{lookupItem.CommandInstanceId}", lookupItem);
                            else
                                returnValue[$"{lookupItem.CommandId}_{lookupItem.CommandInstanceId}"] = lookupItem;
                        });
                    }
                }
            });
        }

        #endregion

        #endregion
    }
}

#region prototype code

///// <summary>
///// Get the event logs from the log group rfh-instance-logs-windows (Single instance id)
///// </summary>
///// <param name="instanceId"></param>
///// <param name="domain"></param>
///// <returns></returns>
//public static Dictionary<string, LogStreamItem> GetEventLogStreamsByInstanceId(string instanceId, EAWSDomain domain, EAWSEnvironment env)
//{
//    return GetEventLogStreamsByInstanceId(new List<string>() { instanceId }, domain, env);
//}

///// <summary>
///// Get the event logs from the log group rfh-instance-logs-windows (Multiple instance id's)
///// </summary>
///// <param name="instanceIds"></param>
///// <param name="domain"></param>
///// <returns></returns>
//public static Dictionary<string, LogStreamItem> GetEventLogStreamsByInstanceId(List<string> instanceIds, EAWSDomain domain, EAWSEnvironment env)
//{
//    Dictionary<string, LogStreamItem> returnValue = new Dictionary<string, LogStreamItem>();

//    AWSCredentials myCred = AWSCKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.ReadOnly);
//    if (myCred == null)
//        return returnValue;

//    using (AmazonCloudWatchLogsClient client = new AmazonCloudWatchLogsClient(myCred))
//    {
//        DescribeLogStreamsRequest req = new DescribeLogStreamsRequest();
//        req.LogGroupName = $"rfh-instance-logs-windows";

//        var res = client.DescribeLogStreams(req);
//        while (!string.IsNullOrEmpty(res.NextToken))
//        {
//            foreach (LogStream logStreamItem in res.LogStreams)
//            {
//                if (!instanceIds.Any(item => item.Contains($"{logStreamItem.LogStreamName}")))
//                    continue;

//                if (!returnValue.ContainsKey(logStreamItem.LogStreamName))
//                {
//                    LogStreamItem lItem = new LogStreamItem()
//                    {
//                        LogStreamName = logStreamItem.LogStreamName,
//                        LastEventTimestamp = logStreamItem.LastEventTimestamp,
//                        StoredBytes = logStreamItem.StoredBytes
//                    };
//                    returnValue.Add(logStreamItem.LogStreamName, lItem);
//                }
//            }

//            req.NextToken = res.NextToken;
//            res = client.DescribeLogStreams(req);
//        }

//        foreach (LogStream logStreamItem in res.LogStreams)
//        {
//            if (!instanceIds.Any(item => item.Contains($"{logStreamItem.LogStreamName}")))
//                continue;

//            if (!returnValue.ContainsKey(logStreamItem.LogStreamName))
//            {
//                LogStreamItem lItem = new LogStreamItem()
//                {
//                    LogStreamName = logStreamItem.LogStreamName,
//                    LastEventTimestamp = logStreamItem.LastEventTimestamp,
//                    StoredBytes = logStreamItem.StoredBytes
//                };
//                returnValue.Add(logStreamItem.LogStreamName, lItem);
//            }
//        }
//    }

//    return returnValue;
//}


#endregion

