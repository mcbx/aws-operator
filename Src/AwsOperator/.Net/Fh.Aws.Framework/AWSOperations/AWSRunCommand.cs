﻿using Amazon.Runtime;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Fh.Aws.Framework.AWSOperations
{
    public static class AWSRunCommand
    {
        #region Get SSM CommandInvocation

        public static List<CommandDetail> GetCommand(EAWSDomain domain, EAWSEnvironment env, string commandId)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new List<CommandDetail>();

            var returnValue = new List<CommandDetail>();
            using (var client = new AmazonSimpleSystemsManagementClient(myCred))
            {
                ListCommandsRequest req = new ListCommandsRequest();
                req.CommandId = commandId;

                ListCommandsResponse respCI = client.ListCommands(req);
                if (respCI.Commands == null)
                    return new List<CommandDetail>();

                returnValue.AddRange(ProcessListCommandResponse(respCI));

                while (!string.IsNullOrEmpty(respCI.NextToken))
                {
                    req.NextToken = respCI.NextToken;
                    respCI = client.ListCommands(req);

                    returnValue.AddRange(ProcessListCommandResponse(respCI));
                }

                return returnValue;
            }
        }

        public static List<CommandDetail> ListCommandInvocations(EAWSDomain domain, EAWSEnvironment env, string commandId)
        {
            return ListCommandInvocations(domain, env, new CommandDetail() { CommandId = commandId });
        }

        /// <summary>
        /// List the commandInvocations.  
        /// Commandinvocation does not have the parameters. 
        /// This function will add the parameters defined in the request to the result commanddetails
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="env"></param>
        /// <param name="commandItem"></param>
        /// <returns></returns>
        public static List<CommandDetail> ListCommandInvocations(EAWSDomain domain, EAWSEnvironment env, CommandDetail commandItem)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new List<CommandDetail>();

            var returnValue = new List<CommandDetail>();
            using (var client = new AmazonSimpleSystemsManagementClient(myCred))
            {
                ListCommandInvocationsRequest req = new ListCommandInvocationsRequest();
                req.CommandId = commandItem.CommandId;

                ListCommandInvocationsResponse respCI = client.ListCommandInvocations(req);
                if (respCI.CommandInvocations == null)
                    return new List<CommandDetail>();

                returnValue.AddRange(ProcessListCommandInvocationResponse(respCI, commandItem));

                while (!string.IsNullOrEmpty(respCI.NextToken))
                {
                    req.NextToken = respCI.NextToken;
                    respCI = client.ListCommandInvocations(req);

                    returnValue.AddRange(ProcessListCommandInvocationResponse(respCI, commandItem));
                }

                return returnValue;
            }
        }

        private static List<CommandDetail> ProcessListCommandInvocationResponse(ListCommandInvocationsResponse respCI, CommandDetail requestCommand)
        {
            List<CommandDetail> returnValues = new List<CommandDetail>();
            foreach (Amazon.SimpleSystemsManagement.Model.CommandInvocation item in respCI.CommandInvocations)
            {
                var command = new CommandDetail()
                {
                    CommandId = requestCommand.CommandId,
                    CommandInstanceId = item.InstanceId,
                    CommandStatus = item.Status,
                    CommandStatusDetailed = item.StatusDetails,
                    CommandRunDateTime = item.RequestedDateTime
                };

                command.CommandParameters.AddRange(requestCommand.CommandParameters);
                returnValues.Add(command);
            }
            return returnValues;
        }

        private static List<CommandDetail> ProcessListCommandResponse(ListCommandsResponse respCI)
        {
            List<CommandDetail> returnValues = new List<CommandDetail>();

            foreach (Amazon.SimpleSystemsManagement.Model.Command item in respCI.Commands)
            {
                if (item.Parameters == null || item.Parameters.Count != 1 || !item.Parameters.ContainsKey("commands"))
                    continue;

                Dictionary<TargetType, List<string>> targets = new Dictionary<TargetType, List<string>>();
                if (item.InstanceIds.Count != 0)
                {
                    targets.Add(TargetType.Instance, new List<string>());
                    targets[TargetType.Instance].AddRange(item.InstanceIds);
                }
                else
                {
                    if (item.Targets != null)
                    {
                        item.Targets.ForEach(targetItem =>
                        {
                            if (targetItem.Key == "tag:PurposeId")
                            {
                                if (!targets.ContainsKey(TargetType.TagPurposeId))
                                    targets.Add(TargetType.TagPurposeId, new List<string>());

                                targets[TargetType.TagPurposeId].AddRange(targetItem.Values);
                            }
                        });
                    }
                }

                //TODO: reduce calls
                foreach (KeyValuePair<TargetType, List<string>> targetItem in targets)
                {
                    targetItem.Value.ForEach(tItem =>
                    {
                        var command = new CommandDetail()
                        {
                            CommandId = item.CommandId,
                            CommandInstanceId = $"{(targetItem.Key == TargetType.Instance ? string.Empty : $"{targetItem.Key.ToString()}=")}{tItem}",
                            CommandStatus = item.Status,
                            CommandStatusDetailed = item.StatusDetails,
                            CommandRunDateTime = item.RequestedDateTime
                        };
                        command.CommandParameters.AddRange(item.Parameters["commands"]);
                        returnValues.Add(command);
                    });
                }
            }
            return returnValues;
        }

        /// <summary>
        /// Get all the command invocations of a runcommand id
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="env"></param>
        /// <param name="commandId"></param>
        /// <returns></returns>
        public static List<CommandDetail> GetCommandInvocation(EAWSDomain domain, EAWSEnvironment env, string commandId)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new List<CommandDetail>();

            List<CommandDetail> returnValue = new List<CommandDetail>();
            using (var client = new AmazonSimpleSystemsManagementClient(myCred))
            {
                ListCommandInvocationsRequest req = new ListCommandInvocationsRequest();
                req.CommandId = commandId;

                ListCommandInvocationsResponse respCI = client.ListCommandInvocations(req);
                respCI.CommandInvocations.ForEach(item =>
                {
                    returnValue.Add(new CommandDetail()
                    {
                        CommandId = item.CommandId,
                        CommandInstanceId = item.InstanceId,
                        CommandStatus = item.Status,
                        CommandStatusDetailed = item.StatusDetails,
                        CommandRunDateTime = item.RequestedDateTime
                    });
                });

                while (!string.IsNullOrEmpty(respCI.NextToken))
                {
                    req.NextToken = respCI.NextToken;
                    respCI = client.ListCommandInvocations(req);

                    respCI.CommandInvocations.ForEach(item =>
                    {
                        returnValue.Add(new CommandDetail()
                        {
                            CommandId = item.CommandId,
                            CommandInstanceId = item.InstanceId,
                            CommandStatus = item.Status,
                            CommandStatusDetailed = item.StatusDetails,
                            CommandRunDateTime = item.RequestedDateTime
                        });
                    });
                }
            }

            return returnValue;
        }

        public static CommandDetail GetCommandInvocation(EAWSDomain domain, EAWSEnvironment env, CommandDetail command)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.Readonly);
            if (myCred == null)
                return new CommandDetail() { CommandId = command.CommandId, CommandInstanceId = command.CommandInstanceId, CommandStatus = "No valid credentials" };

            CommandDetail returnValue = new CommandDetail() { CommandId = command.CommandId, CommandInstanceId = command.CommandInstanceId };
            using (var client = new AmazonSimpleSystemsManagementClient(myCred))
            {
                GetCommandInvocationRequest req = new GetCommandInvocationRequest();

                req.CommandId = command.CommandId;
                req.InstanceId = command.CommandInstanceId;

                GetCommandInvocationResponse respCI = client.GetCommandInvocation(req);
                returnValue.CommandStatus = respCI.Status;
                returnValue.CommandStatusDetailed = respCI.StatusDetails;
                returnValue.CommandRunDateTime = string.IsNullOrEmpty(respCI.ExecutionStartDateTime) ? DateTime.MinValue : DateTime.Parse(respCI.ExecutionStartDateTime);

                returnValue.CommandResult = string.IsNullOrEmpty(respCI.StandardOutputContent) && string.IsNullOrEmpty(respCI.StandardErrorContent) ? "No command output found." : string.Concat(respCI.StandardOutputContent, Environment.NewLine, respCI.StandardErrorContent); ;
            }

            return returnValue;
        }

        #endregion

        #region SSM RunCommand

        private static async Task<SendCommandResponse> CreateSSMCommand(AmazonSimpleSystemsManagementClient client, SendCommandRequest req, bool fireAndForget)
        {
            SendCommandResponse respC = await client.SendCommandAsync(req).ConfigureAwait(false);

            if (!fireAndForget)
                Thread.Sleep(3000);
            return respC;
        }

        public static Dictionary<string, CommandDetail> RunPowershellCommand(EAWSDomain domain, EAWSEnvironment env, string purposeIdTag, List<string> psCommands, string commandComment)
        {
            return RunPowershellCommand(domain, env, AWSEc2.GetEc2InstanceByPurposeId(purposeIdTag, domain, env), psCommands, commandComment);
        }

        public static Dictionary<string, CommandDetail> RunPowershellCommand(EAWSDomain domain, EAWSEnvironment env, List<string> ec2Targets, List<string> psCommands, string commandComment)
        {
            return RunPowershellCommand(domain, env, ec2Targets, psCommands, commandComment, false );
        }

        public static Dictionary<string, CommandDetail> RunPowershellCommand(EAWSDomain domain, EAWSEnvironment env, List<string> ec2Targets, List<string> psCommands, string commandComment, bool fireAndForget)
        {
            AWSCredentials myCred = AWSCredentialKeeper.GetAssumeRoleAWSCredentials(domain, env, EAWSRole.DevOps);
            if (myCred == null)
                return new Dictionary<string, CommandDetail>();

            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            using (var client = new AmazonSimpleSystemsManagementClient(myCred))
            {
                SendCommandRequest sReq = new SendCommandRequest();
                sReq.DocumentName = "AWS-RunPowerShellScript";
                sReq.InstanceIds.AddRange(ec2Targets);
                sReq.Parameters.Add("commands", psCommands);

                if (!string.IsNullOrEmpty(commandComment))
                    sReq.Comment = commandComment;

                Dictionary<string, CommandDetail> results = new Dictionary<string, CommandDetail>();
                SendCommandResponse respC = CreateSSMCommand(client, sReq, fireAndForget).Result;

                ec2Targets.ForEach(item => results.Add(item, new CommandDetail() { CommandInstanceId = item, CommandId = respC.Command.CommandId, CommandResult = string.Empty }));
                if (fireAndForget)
                    return results;

                Thread.Sleep(2000);
                foreach (string instanceItem in respC.Command.InstanceIds)
                {
                    CommandDetail cdItem = new CommandDetail
                    {
                        CommandType = CommandDetailType.RunCommand,
                        CommandId = respC.Command.CommandId,
                        CommandInstanceId = instanceItem,
                        CommandResult = string.Empty
                    };

                    GetCommandInvocationRequest req = new GetCommandInvocationRequest();
                    req.CommandId = respC.Command.CommandId;
                    req.InstanceId = instanceItem;

                    GetCommandInvocationResponse respCI = client.GetCommandInvocation(req);
                    while (respCI.Status == CommandInvocationStatus.Pending || respCI.Status == CommandInvocationStatus.InProgress)
                    {
                        Thread.Sleep(2000);
                        respCI = client.GetCommandInvocation(req);
                    }

                    cdItem.CommandRunDateTime = DateTime.Parse(respCI.ExecutionStartDateTime);
                    cdItem.CommandStatus = respCI.Status;
                    cdItem.CommandStatusDetailed = respCI.StatusDetails;
                    cdItem.CommandResult = string.Concat(respCI.StandardOutputContent, Environment.NewLine, Environment.NewLine, respCI.StandardErrorContent);
                    results[instanceItem] = cdItem;
                }
                return results;
            }
        }

        #endregion
    }
}
