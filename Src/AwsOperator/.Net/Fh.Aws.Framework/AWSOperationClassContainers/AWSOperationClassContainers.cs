﻿using Amazon.S3;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ServiceProcess;

namespace Fh.Aws.Framework.AWSOperationClassContainers
{
    #region Enums

    public enum CommandDetailType
    {
        Other,
        RunCommand
    }

    public enum ImageState
    {
        Unknown,
        Available,
        Deregistered,
        Error,
        Failed,
        Invalid,
        Pending,
        Transient
    }

    public enum InstanceState
    {
        Unknown,
        Pending,
        Running,
        ShuttingDown,
        Stopped,
        Stopping,
        Terminated
    }

    public enum IpAdresType
    {
        Unknown,
        Private,
        Public
    }

    public enum TargetType
    {
        Unknown,
        Instance,
        TagPurposeId
    }

    #endregion

    #region Class containers

    #region AWS Event for run commands

    public class JsonIIS
    {
        public string IIS_State { get; set; }
        public JsonIISWebsite[] Websites { get; set; }
    }

    public class JsonIISWebsite
    {
        public string Website { get; set; }
        public string State { get; set; }
        public Binding[] Bindings { get; set; }
        public Applicationpool ApplicationPool { get; set; }
    }

    public class Applicationpool
    {
        public string Name { get; set; }
        public string State { get; set; }
        public string Net_Clr_Version { get; set; }
    }

    public class Binding
    {
        public string Protocol { get; set; }
        public string BindingAdres { get; set; }

        public override string ToString()
        {
            return $"{Protocol.Trim()}  {BindingAdres.ToString()}";
        }
    }

    public class JsonAWSRunCommandTrigger
    {
        public string eventVersion { get; set; }
        public Useridentity userIdentity { get; set; }
        public DateTime eventTime { get; set; }
        public string eventSource { get; set; }
        public string eventName { get; set; }
        public string awsRegion { get; set; }
        public string sourceIPAddress { get; set; }
        public string userAgent { get; set; }
        public Requestparameters requestParameters { get; set; }
        public Responseelements responseElements { get; set; }
        public string requestID { get; set; }
        public string eventID { get; set; }
        public string eventType { get; set; }
        public string recipientAccountId { get; set; }
    }

    public class Useridentity
    {
        public string type { get; set; }
        public string principalId { get; set; }
        public string arn { get; set; }
        public string accountId { get; set; }
        public string accessKeyId { get; set; }
        public Sessioncontext sessionContext { get; set; }
        public string invokedBy { get; set; }
    }

    public class Sessioncontext
    {
        public Attributes attributes { get; set; }
        public Sessionissuer sessionIssuer { get; set; }
    }

    public class Attributes
    {
        public string mfaAuthenticated { get; set; }
        public DateTime creationDate { get; set; }
    }

    public class Sessionissuer
    {
        public string type { get; set; }
        public string principalId { get; set; }
        public string arn { get; set; }
        public string accountId { get; set; }
        public string userName { get; set; }
    }

    public class Requestparameters
    {
        public Parameters parameters { get; set; }
        public Target[] targets { get; set; }
        public string documentName { get; set; }
        public bool interactive { get; set; }
    }

    public class Parameters
    {
        public string[] commands { get; set; }
    }

    public class Target
    {
        public string key { get; set; }
        public string[] values { get; set; }
    }

    public class Responseelements
    {
        public Command command { get; set; }
    }

    public class Command
    {
        public string commandId { get; set; }
        public int targetCount { get; set; }
        public string expiresAfter { get; set; }
        public int errorCount { get; set; }
        public Target1[] targets { get; set; }
        public string status { get; set; }
        public Notificationconfig notificationConfig { get; set; }
        public string requestedDateTime { get; set; }
        public string maxErrors { get; set; }
        public bool interactive { get; set; }
        public string outputS3BucketName { get; set; }
        public string serviceRole { get; set; }
        public string documentVersion { get; set; }
        public Cloudwatchoutputconfig cloudWatchOutputConfig { get; set; }
        public string statusDetails { get; set; }
        public string maxConcurrency { get; set; }
        public int deliveryTimedOutCount { get; set; }
        public string comment { get; set; }
        public object[] instanceIds { get; set; }
        public string documentName { get; set; }
        public Parameters1 parameters { get; set; }
        public string outputS3KeyPrefix { get; set; }
        public int completedCount { get; set; }
    }

    public class Notificationconfig
    {
        public object[] notificationEvents { get; set; }
        public string notificationArn { get; set; }
        public string notificationType { get; set; }
    }

    public class Cloudwatchoutputconfig
    {
        public bool cloudWatchOutputEnabled { get; set; }
        public string cloudWatchLogGroupName { get; set; }
    }

    public class Parameters1
    {
        public string[] commands { get; set; }
    }

    public class Target1
    {
        public string key { get; set; }
        public string[] values { get; set; }
    }

    #endregion

    public class JsonCommandRoot
    {
        public string[] Commands { get; set; }
    }

    public class AlarmHistory
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<AlarmHistoryItem> StateHistory { get; set; } = new List<AlarmHistoryItem>();
    }

    public class AlarmHistoryItem
    {
        public DateTime ChangedDateTime { get; set; }
        public string NewState { get; set; }
        public string OldState { get; set; }
    }

    public class AlarmDetail
    {
        public enum AlarmTreatMissingDataSetting
        {
            breaching,
            notBreaching,
            ignore,
            missing,
            unknown
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string AlarmState { get; set; }
        public DateTime AlarmStateChanged { get; set; }
        public string AlarmMetricNamespace { get; set; }
        public string AlarmMetricName { get; set; }
        public AlarmTreatMissingDataSetting TreatMissingData { get; set; }

        public bool ResetInProgress { get; set; } = false;

        public static AlarmTreatMissingDataSetting ConvertTreatMissingData(string value)
        {
            AlarmTreatMissingDataSetting tryMode;
            if (!Enum.TryParse(value, true, out tryMode))
                return AlarmTreatMissingDataSetting.unknown;

            return tryMode;
        }
    }

    public class LogStreamItem
    {
        public string LogStreamName { get; set; }
        public DateTime LastEventTimestamp { get; set; }
        public long StoredBytes { get; set; }
    }

    public class RuleDetail
    {
        public string Id { get; set; }
        public string Schedule { get; set; }
        public bool Enabled { get; set; }
        public List<RuleTargetDetail> RuleTargets { get; set; }

        public List<RuleLambdaDetail> RuleLambdaTargets { get; set; } = new List<RuleLambdaDetail>();

        public RuleDetail()
        {
            RuleTargets = new List<RuleTargetDetail>();
        }
    }

    public class RuleLambdaDetail
    {
        public string LambdaFunction { get; set; }

        public string LambdaParameterJson { get; set; }

        public string LambdaParameterJsonFormatted()
        {
            dynamic parsedJson = JsonConvert.DeserializeObject(LambdaParameterJson);
            return JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
        }
    }

    public class RuleTargetDetail
    {
        public List<string> Commands { get; set; }
        public Dictionary<string, List<string>> MachineTargets { get; set; }

        public RuleTargetDetail()
        {
            Commands = new List<string>();
            MachineTargets = new Dictionary<string, List<string>>();
        }
    }

    public class CommandDetail
    {
        public CommandDetailType CommandType { get; set; }
        public string CommandId { get; set; }
        public string CommandInstanceId { get; set; }
        public List<string> CommandParameters { get; set; } = new List<string>();
        public string CommandResult { get; set; }
        public string CommandStatus { get; set; }
        public string CommandStatusDetailed { get; set; }
        public DateTime CommandRunDateTime { get; set; }
    }

    public class JsonServiceItem
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public ServiceControllerStatus Status { get; set; }
        public ServiceStartMode StartType { get; set; }
    }

    public class JsonTaskItem
    {
        public int Id { get; set; }
        public string ProcessName { get; set; }
        public string UserName { get; set; }
    }

    public class DeploymentDetail
    {
        public string DeploymentId { get; set; }
        public DateTime RunDate { get; set; }
        public string Status { get; set; }
        public string DeploymentErrorMessage { get; set; }
    }

    public class ParameterDetail
    {
        public string Name { get; set; }
        public string FullPath { get; set; }
        public string Value { get; set; }
        public bool Secured { get; set; }
        public long Version { get; set; }
    }

    public class ParameterHistoryDetail : ParameterDetail
    {
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public List<string> Labels { get; set; } = new List<string>();
    }

    public class ParameterEc2Credentials
    {
        public string Name { get; set; }

        public string Key { get; set; }
    }

    public class CloudFormationStackOutput
    {
        public string Key { get; set; }
        public string ExportName { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }

    public class CloudFormationStackResource
    {
        public string LogicalId { get; set; }
        public string PhysicalId { get; set; }
        public string ResourceType { get; set; }
        public string DriftStatus { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
    }

    public class CloudFormationStackTemplateParameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class CloudFormationStackTemplate
    {
        public string Template { get; set; }
        public List<CloudFormationStackTemplateParameter> TemplateParameters { get; set; }

        public CloudFormationStackTemplate()
        {
            TemplateParameters = new List<CloudFormationStackTemplateParameter>();
        }
    }

    public class CloudFormationStackLogEntry
    {
        public DateTime EventDateTime { get; set; }
        public string Resource { get; set; }
        public string LogicalId { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
    }

    public class CloudFormationStack
    {
        public string StackName { get; set; }
        public string StackDescription { get; set; }
        public string StackArn { get; set; }
        public string StackUrl { get; set; }
        public string StackStatus { get; set; }
        public string StatusReason { get; set; }
        public List<CloudFormationStackOutput> OutputItems { get; set; }
        public List<CloudFormationStackResource> ResourceItems { get; set; }

        public CloudFormationStack()
        {
            OutputItems = new List<CloudFormationStackOutput>();
            ResourceItems = new List<CloudFormationStackResource>();
        }
    }

    public class CloudFormationStackDetailed : CloudFormationStack
    {
        public CloudFormationStackTemplate Template { get; set; }
        public List<CloudFormationStackLogEntry> LogEntries { get; set; }

        public CloudFormationStackDetailed() : base()
        {
            LogEntries = new List<CloudFormationStackLogEntry>();
        }

        public CloudFormationStackDetailed(CloudFormationStack owner) : this()
        {
            StackName = owner.StackName;
            StackDescription = owner.StackDescription;
            StackArn = owner.StackArn;
            StackUrl = owner.StackUrl;
            StackStatus = owner.StackStatus;
            StatusReason = owner.StatusReason;
            OutputItems = owner.OutputItems;
            ResourceItems = owner.ResourceItems;
        }
    }

    public class AmiDetail
    {
        public string AmiId { get; set; }
        public string AmiName { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
        public DateTime CreationDateTime { get; set; }
        public string Platform { get; set; }
        public string Architecture { get; set; }
        public ImageState State { get; private set; }

        public void SetImageState(Amazon.EC2.ImageState amiImageState)
        {
            switch (amiImageState.Value.ToLowerInvariant())
            {
                case "available":
                    State = ImageState.Available;
                    break;
                case "deregistered":
                    State = ImageState.Deregistered;
                    break;
                case "invalid":
                    State = ImageState.Invalid;
                    break;
                case "transient":
                    State = ImageState.Transient;
                    break;
                default:
                    State = ImageState.Unknown;
                    break;
            }
        }
    }

    public class InstanceDetail
    {
        public string InstanceId { get; set; }
        public string Name { get; set; }
        public InstanceState State { get; private set; }
        public string InstanceType { get; set; }
        public Dictionary<string, string> Tags { get; set; } = new Dictionary<string, string>();
        public string Ami { get; set; }
        public AmiDetail AmiDetail { get; set; }
        public string NetworkAdres { get; set; }
        public IpAdresType NetworkAdresType { get; set; }

        public void SetState(Amazon.EC2.InstanceStateName instanceState)
        {
            switch (instanceState.Value.ToLowerInvariant())
            {
                case "pending":
                    State = InstanceState.Pending;
                    break;
                case "running":
                    State = InstanceState.Running;
                    break;
                case "shuttingdown":
                    State = InstanceState.ShuttingDown;
                    break;
                case "stopped":
                    State = InstanceState.Stopped;
                    break;
                case "stopping":
                    State = InstanceState.Stopping;
                    break;
                case "terminated":
                    State = InstanceState.Terminated;
                    break;
                default:
                    State = InstanceState.Unknown;
                    break;
            }
        }
    }

    #endregion

    #region RdsDetails

    public class RdsDetail
    {
        public string Name { get; set; }
        public string Endpoint { get; set; }
        public int? EndpointPort { get; set; }
        public string Engine { get; set; }
        public string Status { get; set; }
        public string RdsType { get; set; }
        public string Region { get; set; }
    }

    #endregion

    #region S3

    public class S3Bucket
    {

        public string BucketName { get; set; }

        public Dictionary<string, S3BucketItem> Items { get; set; } = new Dictionary<string, S3BucketItem>();
    }

    public class S3BucketItem
    {
        private string _filePath = string.Empty;

        public string Name { get; set; }

        public long Size { get; set; }

        public bool IsFolder { get { return Size == 0; } }

        public string FilePath
        {
            get { return IsFolder ? string.Empty : _filePath; }
            set { _filePath = value; }
        }

        public bool Downloadable { get; set; } = false;

        public DateTime LastModified { get; set; }

        public Dictionary<string, S3BucketItem> SubItems { get; set; } = new Dictionary<string, S3BucketItem>();
    }

    #endregion
}
