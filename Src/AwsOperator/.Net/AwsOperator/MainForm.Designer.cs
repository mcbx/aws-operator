﻿namespace AwsOperator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.gbAwsOperator = new System.Windows.Forms.GroupBox();
            this.btnGetEc2Credentials = new System.Windows.Forms.Button();
            this.btnPocOperator = new System.Windows.Forms.Button();
            this.btnGenerateAmiReport = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cboOperatorWindow = new System.Windows.Forms.ComboBox();
            this.btnOpenOperatorWindow = new System.Windows.Forms.Button();
            this.lblAppCode = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAppCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbDomain = new System.Windows.Forms.ComboBox();
            this.cbEnv = new System.Windows.Forms.ComboBox();
            this.gbAccount = new System.Windows.Forms.GroupBox();
            this.pbSpecial = new System.Windows.Forms.PictureBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblExpireText = new System.Windows.Forms.Label();
            this.lblExpireTimer = new System.Windows.Forms.Label();
            this.cboLoggedIn = new System.Windows.Forms.CheckBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboRefresh = new System.Windows.Forms.CheckBox();
            this.tmCountDown = new System.Windows.Forms.Timer(this.components);
            this.gbAwsOperator.SuspendLayout();
            this.gbAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpecial)).BeginInit();
            this.SuspendLayout();
            // 
            // gbAwsOperator
            // 
            this.gbAwsOperator.Controls.Add(this.btnGetEc2Credentials);
            this.gbAwsOperator.Controls.Add(this.btnPocOperator);
            this.gbAwsOperator.Controls.Add(this.btnGenerateAmiReport);
            this.gbAwsOperator.Controls.Add(this.label6);
            this.gbAwsOperator.Controls.Add(this.cboOperatorWindow);
            this.gbAwsOperator.Controls.Add(this.btnOpenOperatorWindow);
            this.gbAwsOperator.Controls.Add(this.lblAppCode);
            this.gbAwsOperator.Controls.Add(this.label4);
            this.gbAwsOperator.Controls.Add(this.txtAppCode);
            this.gbAwsOperator.Controls.Add(this.label3);
            this.gbAwsOperator.Controls.Add(this.cbDomain);
            this.gbAwsOperator.Controls.Add(this.cbEnv);
            this.gbAwsOperator.Enabled = false;
            this.gbAwsOperator.Location = new System.Drawing.Point(279, 12);
            this.gbAwsOperator.Name = "gbAwsOperator";
            this.gbAwsOperator.Size = new System.Drawing.Size(339, 230);
            this.gbAwsOperator.TabIndex = 17;
            this.gbAwsOperator.TabStop = false;
            this.gbAwsOperator.Text = "Applicatie";
            // 
            // btnGetEc2Credentials
            // 
            this.btnGetEc2Credentials.Location = new System.Drawing.Point(6, 201);
            this.btnGetEc2Credentials.Name = "btnGetEc2Credentials";
            this.btnGetEc2Credentials.Size = new System.Drawing.Size(80, 23);
            this.btnGetEc2Credentials.TabIndex = 23;
            this.btnGetEc2Credentials.Text = "Ec2 Creds";
            this.btnGetEc2Credentials.UseVisualStyleBackColor = true;
            this.btnGetEc2Credentials.Click += new System.EventHandler(this.btnGetEc2Credentials_Click);
            // 
            // btnPocOperator
            // 
            this.btnPocOperator.Location = new System.Drawing.Point(6, 113);
            this.btnPocOperator.Name = "btnPocOperator";
            this.btnPocOperator.Size = new System.Drawing.Size(80, 23);
            this.btnPocOperator.TabIndex = 22;
            this.btnPocOperator.Text = "POC";
            this.btnPocOperator.UseVisualStyleBackColor = true;
            this.btnPocOperator.Visible = false;
            //this.btnPocOperator.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnGenerateAmiReport
            // 
            this.btnGenerateAmiReport.Location = new System.Drawing.Point(112, 113);
            this.btnGenerateAmiReport.Name = "btnGenerateAmiReport";
            this.btnGenerateAmiReport.Size = new System.Drawing.Size(213, 23);
            this.btnGenerateAmiReport.TabIndex = 18;
            this.btnGenerateAmiReport.Text = "Generate Ami report";
            this.btnGenerateAmiReport.UseVisualStyleBackColor = true;
            this.btnGenerateAmiReport.Visible = false;
            this.btnGenerateAmiReport.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Operator:";
            // 
            // cboOperatorWindow
            // 
            this.cboOperatorWindow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperatorWindow.FormattingEnabled = true;
            this.cboOperatorWindow.Location = new System.Drawing.Point(112, 30);
            this.cboOperatorWindow.Name = "cboOperatorWindow";
            this.cboOperatorWindow.Size = new System.Drawing.Size(213, 21);
            this.cboOperatorWindow.TabIndex = 16;
            this.cboOperatorWindow.SelectedIndexChanged += new System.EventHandler(this.cboOperatorWindow_SelectedIndexChanged);
            // 
            // btnOpenOperatorWindow
            // 
            this.btnOpenOperatorWindow.Location = new System.Drawing.Point(112, 201);
            this.btnOpenOperatorWindow.Name = "btnOpenOperatorWindow";
            this.btnOpenOperatorWindow.Size = new System.Drawing.Size(213, 23);
            this.btnOpenOperatorWindow.TabIndex = 15;
            this.btnOpenOperatorWindow.Text = "&Open";
            this.btnOpenOperatorWindow.UseVisualStyleBackColor = true;
            this.btnOpenOperatorWindow.Click += new System.EventHandler(this.btnOpenOperatorWindow_Click);
            // 
            // lblAppCode
            // 
            this.lblAppCode.AutoSize = true;
            this.lblAppCode.Location = new System.Drawing.Point(6, 170);
            this.lblAppCode.Name = "lblAppCode";
            this.lblAppCode.Size = new System.Drawing.Size(83, 13);
            this.lblAppCode.TabIndex = 13;
            this.lblAppCode.Text = "Applicatie code:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Omgeving:";
            // 
            // txtAppCode
            // 
            this.txtAppCode.Location = new System.Drawing.Point(112, 167);
            this.txtAppCode.Name = "txtAppCode";
            this.txtAppCode.Size = new System.Drawing.Size(213, 20);
            this.txtAppCode.TabIndex = 14;
            this.txtAppCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtAppCode_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Domein:";
            // 
            // cbDomain
            // 
            this.cbDomain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDomain.FormattingEnabled = true;
            this.cbDomain.Location = new System.Drawing.Point(112, 58);
            this.cbDomain.Name = "cbDomain";
            this.cbDomain.Size = new System.Drawing.Size(213, 21);
            this.cbDomain.TabIndex = 7;
            this.cbDomain.TextChanged += new System.EventHandler(this.cbDomain_TextChanged);
            // 
            // cbEnv
            // 
            this.cbEnv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEnv.FormattingEnabled = true;
            this.cbEnv.Location = new System.Drawing.Point(112, 85);
            this.cbEnv.Name = "cbEnv";
            this.cbEnv.Size = new System.Drawing.Size(213, 21);
            this.cbEnv.TabIndex = 8;
            // 
            // gbAccount
            // 
            this.gbAccount.Controls.Add(this.pbSpecial);
            this.gbAccount.Controls.Add(this.btnImport);
            this.gbAccount.Controls.Add(this.label7);
            this.gbAccount.Controls.Add(this.lblExpireText);
            this.gbAccount.Controls.Add(this.lblExpireTimer);
            this.gbAccount.Controls.Add(this.cboLoggedIn);
            this.gbAccount.Controls.Add(this.btnLogin);
            this.gbAccount.Controls.Add(this.label1);
            this.gbAccount.Controls.Add(this.label16);
            this.gbAccount.Controls.Add(this.txtToken);
            this.gbAccount.Location = new System.Drawing.Point(12, 12);
            this.gbAccount.Name = "gbAccount";
            this.gbAccount.Size = new System.Drawing.Size(261, 230);
            this.gbAccount.TabIndex = 16;
            this.gbAccount.TabStop = false;
            this.gbAccount.Text = "Account ";
            // 
            // pbSpecial
            // 
            this.pbSpecial.ErrorImage = null;
            this.pbSpecial.InitialImage = null;
            this.pbSpecial.Location = new System.Drawing.Point(26, 51);
            this.pbSpecial.Name = "pbSpecial";
            this.pbSpecial.Size = new System.Drawing.Size(61, 85);
            this.pbSpecial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSpecial.TabIndex = 23;
            this.pbSpecial.TabStop = false;
            this.pbSpecial.Visible = false;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(125, 85);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(130, 42);
            this.btnImport.TabIndex = 22;
            this.btnImport.Text = "Profiel / AccessKey";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(126, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Gebruik niet het getal 666";
            // 
            // lblExpireText
            // 
            this.lblExpireText.AutoSize = true;
            this.lblExpireText.Location = new System.Drawing.Point(11, 174);
            this.lblExpireText.Name = "lblExpireText";
            this.lblExpireText.Size = new System.Drawing.Size(86, 13);
            this.lblExpireText.TabIndex = 18;
            this.lblExpireText.Text = "Login expires in: ";
            this.lblExpireText.Visible = false;
            // 
            // lblExpireTimer
            // 
            this.lblExpireTimer.AutoSize = true;
            this.lblExpireTimer.Location = new System.Drawing.Point(122, 170);
            this.lblExpireTimer.Name = "lblExpireTimer";
            this.lblExpireTimer.Size = new System.Drawing.Size(49, 13);
            this.lblExpireTimer.TabIndex = 18;
            this.lblExpireTimer.Text = "12:00:00";
            this.lblExpireTimer.Visible = false;
            // 
            // cboLoggedIn
            // 
            this.cboLoggedIn.AutoSize = true;
            this.cboLoggedIn.Enabled = false;
            this.cboLoggedIn.Location = new System.Drawing.Point(125, 147);
            this.cboLoggedIn.Name = "cboLoggedIn";
            this.cboLoggedIn.Size = new System.Drawing.Size(15, 14);
            this.cboLoggedIn.TabIndex = 21;
            this.cboLoggedIn.UseVisualStyleBackColor = true;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(6, 201);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(249, 23);
            this.btnLogin.TabIndex = 16;
            this.btnLogin.Text = "&Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Token:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 148);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Ingelogd:";
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(125, 19);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(128, 20);
            this.txtToken.TabIndex = 6;
            this.txtToken.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtToken_KeyDown);
            this.txtToken.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtToken_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 239);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Refresh credentials?";
            this.label2.Visible = false;
            // 
            // cboRefresh
            // 
            this.cboRefresh.AutoSize = true;
            this.cboRefresh.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cboRefresh.Location = new System.Drawing.Point(126, 238);
            this.cboRefresh.Name = "cboRefresh";
            this.cboRefresh.Size = new System.Drawing.Size(15, 14);
            this.cboRefresh.TabIndex = 10;
            this.cboRefresh.UseVisualStyleBackColor = true;
            this.cboRefresh.Visible = false;
            // 
            // tmCountDown
            // 
            this.tmCountDown.Interval = 1000;
            this.tmCountDown.Tick += new System.EventHandler(this.tmCountDown_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 254);
            this.Controls.Add(this.gbAccount);
            this.Controls.Add(this.gbAwsOperator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboRefresh);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "W. A. T. (v3.3.8)";
            this.gbAwsOperator.ResumeLayout(false);
            this.gbAwsOperator.PerformLayout();
            this.gbAccount.ResumeLayout(false);
            this.gbAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpecial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAwsOperator;
        private System.Windows.Forms.Label lblAppCode;
        private System.Windows.Forms.TextBox txtAppCode;
        private System.Windows.Forms.GroupBox gbAccount;
        private System.Windows.Forms.CheckBox cboLoggedIn;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cboRefresh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDomain;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.ComboBox cbEnv;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboOperatorWindow;
        private System.Windows.Forms.Button btnOpenOperatorWindow;
        private System.Windows.Forms.Timer tmCountDown;
        private System.Windows.Forms.Label lblExpireTimer;
        private System.Windows.Forms.Label lblExpireText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGenerateAmiReport;
        private System.Windows.Forms.Button btnPocOperator;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnGetEc2Credentials;
        private System.Windows.Forms.PictureBox pbSpecial;
    }
}

