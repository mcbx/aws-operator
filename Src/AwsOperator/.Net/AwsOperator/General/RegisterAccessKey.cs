﻿using Fh.Aws.Framework.AWSLogin;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator.General
{
    public partial class RegisterAccessKey : Form
    {
        private const string _awsProfileKey = "AWS_Profile";
        private MainForm _parent = null;

        public RegisterAccessKey(MainForm parent)
        {
            InitializeComponent();
            _parent = parent;
        }

        private void RegisterAccessKey_Load(object sender, EventArgs e)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(_awsProfileKey))
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings.Add(_awsProfileKey, "GeenNaam");

                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }

            txtProfile.Text = ConfigurationManager.AppSettings[_awsProfileKey];
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtProfile.Text))
            {
                MessageBox.Show(this, "Geen profiel naam opgegeven.", "Oops....", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return;
            }

            //TODO: Regex for special characters

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (config.AppSettings.Settings[_awsProfileKey].Value == txtProfile.Text)
                return;

            config.AppSettings.Settings[_awsProfileKey].Value = txtProfile.Text;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

            txtProfile.Text = ConfigurationManager.AppSettings[_awsProfileKey];
            if (_parent != null)
                _parent.OnChangedLoginStatus(EAWSLoginStatus.Expired);
            AWSCredentialKeeper.ClearCredentials();
        }

        private void btnImportAccessKey_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtProfile.Text) || string.IsNullOrEmpty(txtAccessFile.Text))
            {
                MessageBox.Show(this, "Geen bestand geselecteerd of geen profielnaam gekozen.", "Oops....", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return;
            }

            if (!File.Exists(txtAccessFile.Text))
            {
                MessageBox.Show(this, "Bestand bestaat niet.", "Oops....", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return;
            }

            btnUpdate_Click(this, e);

            string accessKey = string.Empty;
            string secretKey = string.Empty;

            var lines = File.ReadLines(txtAccessFile.Text);
            foreach (string line in lines)
            {
                if (!line.StartsWith("Access key ID", StringComparison.InvariantCultureIgnoreCase))
                {
                    var parts = line.Split(',');
                    if (parts.GetLength(0) != 2)
                        continue;

                    accessKey = parts[0];
                    secretKey = parts[1];
                    break;
                }
            }

            if (MessageBox.Show(this, $"Het profiel '{txtProfile.Text}' zal worden bijgewerkt met de volgende gegevens:{Environment.NewLine}{Environment.NewLine}Access key: {accessKey}{Environment.NewLine}Secret key: {secretKey}{Environment.NewLine}{Environment.NewLine}Weet u het zeker?", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                return;

            if (_parent != null)
                _parent.OnChangedLoginStatus(EAWSLoginStatus.Expired);
            AWSCredentialKeeper.ClearCredentials();

            if (!AWSCredentialKeeper.RegisterProfile(txtProfile.Text, accessKey, secretKey))
            {
                MessageBox.Show(this, "Update mislukt.", "Oops....", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            txtAccessFile.Text = string.Empty;

            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            if (!Path.GetFileName(openFileDialog.FileName).Equals("accesskeys.csv", StringComparison.InvariantCultureIgnoreCase))
            {
                MessageBox.Show(this, "Ongeldig bestand geselecteerd.", "Oops....", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return;
            }

            txtAccessFile.Text = openFileDialog.FileName;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
