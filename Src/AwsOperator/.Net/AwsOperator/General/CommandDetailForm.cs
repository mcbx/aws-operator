﻿using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace AwsOperator.General
{
    public partial class CommandDetailForm : FormTemplate
    {
        private CommandDetailControl _userControl;
        private Dictionary<string, CommandDetail> _commandItems = new Dictionary<string, CommandDetail>();

        public CommandDetailForm(string commandDetailTitle, EAWSDomain awsDomain, EAWSEnvironment awsEnvironment, CommandDetail commandItem, Form owner) : base(string.Empty, awsDomain, awsEnvironment, owner)
        {
            InitializeComponent();
            Text = $"RunCommand: {commandDetailTitle}";
            _commandItems.Add($"{commandItem.CommandId}_{commandItem.CommandInstanceId}", commandItem);
            _userControl = new CommandDetailControl(_domein, _environment, commandItem);
            _userControl.Left = 3;
            _userControl.Top = 10;

            gbRunCommandItems.Visible = false;
            gbCommandDetails.Controls.Add(_userControl);
            gbCommandDetails.Top = gbRunCommandItems.Top;
            gbCommandDetails.Left = gbRunCommandItems.Left;
            gbCommandDetails.Visible = true;
        }

        public Color DetermineRunningColor(CommandDetail commandItem)
        {
            switch (commandItem.CommandStatusDetailed)
            {
                case "Success":
                    return Color.Lime;
                case "Pending":
                case "In Progress":
                case "Delayed":
                    return Color.Aquamarine;
                case "Canceled":
                    return Color.LightGray;
                default:
                    return Color.LightSalmon;
            }
        }

        public CommandDetailForm(string commandDetailTitle, EAWSDomain awsDomain, EAWSEnvironment awsEnvironment, Dictionary<string, CommandDetail> commandItems, Form owner) : base(string.Empty, awsDomain, awsEnvironment, owner)
        {
            InitializeComponent();
            Text = $"RunCommand History: {commandDetailTitle}";

            foreach (KeyValuePair<string, CommandDetail> cItem in commandItems)
            {
                ListViewItem addItem = new ListViewItem() {  Name = cItem.Key, Text = cItem.Value.CommandId };
                addItem.SubItems.Add(new ListViewSubItem() { Name = cItem.Value.CommandInstanceId, Text = cItem.Value.CommandInstanceId });
                addItem.SubItems.Add(new ListViewSubItem() { Name = cItem.Value.CommandRunDateTime.ToString("yyyy-MM-dd HH:mm:ss"), Text = cItem.Value.CommandRunDateTime.ToString("yyyy-MM-dd HH:mm:ss") });

                addItem.BackColor = DetermineRunningColor(cItem.Value);
                lvRunCommands.Items.Add(addItem);
            }
            _commandItems = commandItems;
        }

        private void lvRunCommands_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (lvRunCommands.SelectedItems.Count != 1)
                return;
            if (!_commandItems.ContainsKey(lvRunCommands.SelectedItems[0].Name))
                return;

            CommandDetail selectedCommandItem = _commandItems[lvRunCommands.SelectedItems[0].Name];
            if (_userControl == null)
            {
                _userControl = new CommandDetailControl(_domein, _environment, selectedCommandItem);
                gbCommandDetails.Controls.Add(_userControl);
            }
            else
            {
                _userControl.LoadCommand(selectedCommandItem);
            }
            gbCommandDetails.Visible = true;
        }
    }
}
