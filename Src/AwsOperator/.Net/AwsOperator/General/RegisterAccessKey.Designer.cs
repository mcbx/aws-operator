﻿namespace AwsOperator.General
{
    partial class RegisterAccessKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImportAccessKey = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblSelectedAccessFile = new System.Windows.Forms.Label();
            this.txtAccessFile = new System.Windows.Forms.TextBox();
            this.txtProfile = new System.Windows.Forms.TextBox();
            this.lblProfile = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnImportAccessKey
            // 
            this.btnImportAccessKey.Location = new System.Drawing.Point(9, 76);
            this.btnImportAccessKey.Name = "btnImportAccessKey";
            this.btnImportAccessKey.Size = new System.Drawing.Size(292, 24);
            this.btnImportAccessKey.TabIndex = 0;
            this.btnImportAccessKey.Text = "Update";
            this.btnImportAccessKey.UseVisualStyleBackColor = true;
            this.btnImportAccessKey.Click += new System.EventHandler(this.btnImportAccessKey_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "accessKeys.csv";
            this.openFileDialog.Filter = "Aws accesskey bestand|accessKeys*.csv";
            // 
            // lblSelectedAccessFile
            // 
            this.lblSelectedAccessFile.AutoSize = true;
            this.lblSelectedAccessFile.Location = new System.Drawing.Point(6, 35);
            this.lblSelectedAccessFile.Name = "lblSelectedAccessFile";
            this.lblSelectedAccessFile.Size = new System.Drawing.Size(107, 13);
            this.lblSelectedAccessFile.TabIndex = 1;
            this.lblSelectedAccessFile.Text = "AccessKey bestand: ";
            // 
            // txtAccessFile
            // 
            this.txtAccessFile.Location = new System.Drawing.Point(9, 51);
            this.txtAccessFile.Name = "txtAccessFile";
            this.txtAccessFile.ReadOnly = true;
            this.txtAccessFile.Size = new System.Drawing.Size(253, 20);
            this.txtAccessFile.TabIndex = 2;
            // 
            // txtProfile
            // 
            this.txtProfile.Location = new System.Drawing.Point(9, 45);
            this.txtProfile.Name = "txtProfile";
            this.txtProfile.Size = new System.Drawing.Size(289, 20);
            this.txtProfile.TabIndex = 3;
            // 
            // lblProfile
            // 
            this.lblProfile.AutoSize = true;
            this.lblProfile.Location = new System.Drawing.Point(6, 29);
            this.lblProfile.Name = "lblProfile";
            this.lblProfile.Size = new System.Drawing.Size(68, 13);
            this.lblProfile.TabIndex = 4;
            this.lblProfile.Text = "Profiel naam:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(9, 71);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(289, 23);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "Schakel profiel";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblProfile);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.txtProfile);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 107);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Huidige profiel";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSelectFile);
            this.groupBox2.Controls.Add(this.lblSelectedAccessFile);
            this.groupBox2.Controls.Add(this.btnImportAccessKey);
            this.groupBox2.Controls.Add(this.txtAccessFile);
            this.groupBox2.Location = new System.Drawing.Point(12, 125);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(308, 112);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Update credentials";
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Location = new System.Drawing.Point(267, 51);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(34, 23);
            this.btnSelectFile.TabIndex = 3;
            this.btnSelectFile.Text = "...";
            this.btnSelectFile.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(12, 249);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(309, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Sluit scherm";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // RegisterAccessKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 284);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RegisterAccessKey";
            this.Text = "Profiel instellingen";
            this.Load += new System.EventHandler(this.RegisterAccessKey_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnImportAccessKey;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label lblSelectedAccessFile;
        private System.Windows.Forms.TextBox txtAccessFile;
        private System.Windows.Forms.TextBox txtProfile;
        private System.Windows.Forms.Label lblProfile;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.Button btnClose;
    }
}