﻿namespace AwsOperator.General
{
    partial class CommandDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbRunCommandItems = new System.Windows.Forms.GroupBox();
            this.lvRunCommands = new System.Windows.Forms.ListView();
            this.RunCommandId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InstanceId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RunDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbCommandDetails = new System.Windows.Forms.GroupBox();
            this.gbRunCommandItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbRunCommandItems
            // 
            this.gbRunCommandItems.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbRunCommandItems.Controls.Add(this.lvRunCommands);
            this.gbRunCommandItems.Location = new System.Drawing.Point(12, 12);
            this.gbRunCommandItems.Name = "gbRunCommandItems";
            this.gbRunCommandItems.Size = new System.Drawing.Size(473, 486);
            this.gbRunCommandItems.TabIndex = 0;
            this.gbRunCommandItems.TabStop = false;
            this.gbRunCommandItems.Text = "History runcommands:";
            // 
            // lvRunCommands
            // 
            this.lvRunCommands.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.RunCommandId,
            this.InstanceId,
            this.RunDate});
            this.lvRunCommands.Location = new System.Drawing.Point(9, 19);
            this.lvRunCommands.MultiSelect = false;
            this.lvRunCommands.Name = "lvRunCommands";
            this.lvRunCommands.ShowItemToolTips = true;
            this.lvRunCommands.Size = new System.Drawing.Size(458, 461);
            this.lvRunCommands.TabIndex = 1;
            this.lvRunCommands.UseCompatibleStateImageBehavior = false;
            this.lvRunCommands.View = System.Windows.Forms.View.Details;
            this.lvRunCommands.SelectedIndexChanged += new System.EventHandler(this.lvRunCommands_SelectedIndexChanged);
            // 
            // RunCommandId
            // 
            this.RunCommandId.Text = "CommandId";
            this.RunCommandId.Width = 204;
            // 
            // InstanceId
            // 
            this.InstanceId.Text = "InstanceId";
            this.InstanceId.Width = 176;
            // 
            // RunDate
            // 
            this.RunDate.Text = "RunDate";
            // 
            // gbCommandDetails
            // 
            this.gbCommandDetails.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbCommandDetails.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbCommandDetails.Location = new System.Drawing.Point(491, 12);
            this.gbCommandDetails.Name = "gbCommandDetails";
            this.gbCommandDetails.Size = new System.Drawing.Size(808, 486);
            this.gbCommandDetails.TabIndex = 1;
            this.gbCommandDetails.TabStop = false;
            this.gbCommandDetails.Text = " ";
            this.gbCommandDetails.Visible = false;
            // 
            // CommandDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1314, 510);
            this.Controls.Add(this.gbCommandDetails);
            this.Controls.Add(this.gbRunCommandItems);
            this.Name = "CommandDetailForm";
            this.Text = "CommandDetailForm";
            this.gbRunCommandItems.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbRunCommandItems;
        private System.Windows.Forms.ListView lvRunCommands;
        private System.Windows.Forms.GroupBox gbCommandDetails;
        private System.Windows.Forms.ColumnHeader RunCommandId;
        private System.Windows.Forms.ColumnHeader InstanceId;
        private System.Windows.Forms.ColumnHeader RunDate;
    }
}