﻿namespace AwsOperator.General
{
    partial class AWSAlarmTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvAlarms = new System.Windows.Forms.ListView();
            this.chAlarmName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAlarmStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmListviewItemContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.resetAlarmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckNewAlarmsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SetIntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec30ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec60ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec90ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec120ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec300ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmRefresher = new System.Windows.Forms.Timer(this.components);
            this.showInsuffecientDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmListviewItemContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvAlarms
            // 
            this.lvAlarms.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chAlarmName,
            this.chAlarmStatus});
            this.lvAlarms.ContextMenuStrip = this.cmListviewItemContextMenu;
            this.lvAlarms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvAlarms.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvAlarms.HideSelection = false;
            this.lvAlarms.Location = new System.Drawing.Point(0, 0);
            this.lvAlarms.MultiSelect = false;
            this.lvAlarms.Name = "lvAlarms";
            this.lvAlarms.OwnerDraw = true;
            this.lvAlarms.ShowItemToolTips = true;
            this.lvAlarms.Size = new System.Drawing.Size(855, 370);
            this.lvAlarms.TabIndex = 31;
            this.lvAlarms.UseCompatibleStateImageBehavior = false;
            this.lvAlarms.View = System.Windows.Forms.View.Tile;
            this.lvAlarms.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvAlarms_DrawItem);
            this.lvAlarms.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvAlarms_MouseClick);
            // 
            // chAlarmName
            // 
            this.chAlarmName.Text = "AlarmName";
            this.chAlarmName.Width = 226;
            // 
            // chAlarmStatus
            // 
            this.chAlarmStatus.Text = "AlarmStatus";
            this.chAlarmStatus.Width = 78;
            // 
            // cmListviewItemContextMenu
            // 
            this.cmListviewItemContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetAlarmToolStripMenuItem,
            this.CheckNewAlarmsToolStripMenuItem,
            this.SetIntervalToolStripMenuItem,
            this.showInsuffecientDataToolStripMenuItem});
            this.cmListviewItemContextMenu.Name = "cmListviewItemContextMenu";
            this.cmListviewItemContextMenu.Size = new System.Drawing.Size(205, 114);
            // 
            // resetAlarmToolStripMenuItem
            // 
            this.resetAlarmToolStripMenuItem.Name = "resetAlarmToolStripMenuItem";
            this.resetAlarmToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.resetAlarmToolStripMenuItem.Text = "Attempt: Suppress alarm";
            this.resetAlarmToolStripMenuItem.Click += new System.EventHandler(this.resetAlarmToolStripMenuItem_Click);
            // 
            // CheckNewAlarmsToolStripMenuItem
            // 
            this.CheckNewAlarmsToolStripMenuItem.Name = "CheckNewAlarmsToolStripMenuItem";
            this.CheckNewAlarmsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.CheckNewAlarmsToolStripMenuItem.Text = "Check for new alarms";
            this.CheckNewAlarmsToolStripMenuItem.Click += new System.EventHandler(this.CheckNewAlarmsToolStripMenuItem_Click);
            // 
            // SetIntervalToolStripMenuItem
            // 
            this.SetIntervalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sec10ToolStripMenuItem,
            this.sec30ToolStripMenuItem,
            this.sec60ToolStripMenuItem,
            this.sec90ToolStripMenuItem,
            this.sec120ToolStripMenuItem,
            this.sec300ToolStripMenuItem});
            this.SetIntervalToolStripMenuItem.Name = "SetIntervalToolStripMenuItem";
            this.SetIntervalToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.SetIntervalToolStripMenuItem.Text = "Set interval";
            // 
            // sec10ToolStripMenuItem
            // 
            this.sec10ToolStripMenuItem.CheckOnClick = true;
            this.sec10ToolStripMenuItem.Name = "sec10ToolStripMenuItem";
            this.sec10ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec10ToolStripMenuItem.Text = "10 sec";
            this.sec10ToolStripMenuItem.Click += new System.EventHandler(this.sec10ToolStripMenuItem_Click);
            // 
            // sec30ToolStripMenuItem
            // 
            this.sec30ToolStripMenuItem.Checked = true;
            this.sec30ToolStripMenuItem.CheckOnClick = true;
            this.sec30ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sec30ToolStripMenuItem.Name = "sec30ToolStripMenuItem";
            this.sec30ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec30ToolStripMenuItem.Text = "30 sec";
            this.sec30ToolStripMenuItem.Click += new System.EventHandler(this.sec30ToolStripMenuItem_Click);
            // 
            // sec60ToolStripMenuItem
            // 
            this.sec60ToolStripMenuItem.CheckOnClick = true;
            this.sec60ToolStripMenuItem.Name = "sec60ToolStripMenuItem";
            this.sec60ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec60ToolStripMenuItem.Text = "1 min";
            this.sec60ToolStripMenuItem.Click += new System.EventHandler(this.sec60ToolStripMenuItem_Click);
            // 
            // sec90ToolStripMenuItem
            // 
            this.sec90ToolStripMenuItem.CheckOnClick = true;
            this.sec90ToolStripMenuItem.Name = "sec90ToolStripMenuItem";
            this.sec90ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec90ToolStripMenuItem.Text = "1.5 min";
            this.sec90ToolStripMenuItem.Click += new System.EventHandler(this.sec90ToolStripMenuItem_Click);
            // 
            // sec120ToolStripMenuItem
            // 
            this.sec120ToolStripMenuItem.CheckOnClick = true;
            this.sec120ToolStripMenuItem.Name = "sec120ToolStripMenuItem";
            this.sec120ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec120ToolStripMenuItem.Text = "2 min";
            this.sec120ToolStripMenuItem.Click += new System.EventHandler(this.sec120ToolStripMenuItem_Click);
            // 
            // sec300ToolStripMenuItem
            // 
            this.sec300ToolStripMenuItem.CheckOnClick = true;
            this.sec300ToolStripMenuItem.Name = "sec300ToolStripMenuItem";
            this.sec300ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec300ToolStripMenuItem.Text = "5 min";
            this.sec300ToolStripMenuItem.Click += new System.EventHandler(this.sec300ToolStripMenuItem_Click);
            // 
            // tmRefresher
            // 
            this.tmRefresher.Interval = 30000;
            this.tmRefresher.Tick += new System.EventHandler(this.tmRefresher_Tick);
            // 
            // showInsuffecientDataToolStripMenuItem
            // 
            this.showInsuffecientDataToolStripMenuItem.Name = "showInsuffecientDataToolStripMenuItem";
            this.showInsuffecientDataToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.showInsuffecientDataToolStripMenuItem.Text = "Show \'Insuffecient Data\'";
            this.showInsuffecientDataToolStripMenuItem.Click += new System.EventHandler(this.showInsuffecientDataToolStripMenuItem_Click);
            // 
            // AWSAlarmTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvAlarms);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "AWSAlarmTab";
            this.Size = new System.Drawing.Size(855, 370);
            this.cmListviewItemContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvAlarms;
        private System.Windows.Forms.ColumnHeader chAlarmName;
        private System.Windows.Forms.ColumnHeader chAlarmStatus;
        private System.Windows.Forms.ContextMenuStrip cmListviewItemContextMenu;
        private System.Windows.Forms.ToolStripMenuItem resetAlarmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CheckNewAlarmsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SetIntervalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec10ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec30ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec60ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec90ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec120ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec300ToolStripMenuItem;
        public System.Windows.Forms.Timer tmRefresher;
        private System.Windows.Forms.ToolStripMenuItem showInsuffecientDataToolStripMenuItem;
    }
}
