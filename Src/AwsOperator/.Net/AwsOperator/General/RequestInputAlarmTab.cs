﻿using System;
using System.Windows.Forms;

namespace AwsOperator.General
{
    public partial class RequestInputAlarmTab : Form
    {
        public string AppCode { get; private set; }

        public RequestInputAlarmTab()
        {
            InitializeComponent();
        }

        public string GetRequestAppCode { get { return txtAppCode.Text.ToLower(); } }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            AppCode = txtAppCode.Text.ToLower();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            AppCode = string.Empty;
        }

        private void txtAppCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOk_Click(sender, e);
        }
    }
}
