﻿using AwsOperator.Properties;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace AwsOperator.General
{
    public partial class EasterEgg : Form
    {
        public EasterEgg(Form owner)
        {
            Owner = owner;
            InitializeComponent();
        }

        public static string CreateEgg(string a)
        {
            byte[] hash = new SHA1CryptoServiceProvider().ComputeHash(Encoding.ASCII.GetBytes(a));
            string str1 = "";
            for (int index = 0; index < hash.Length; ++index)
                str1 += hash[index].ToString("d");
            return str1;
        }

        private void EasterEgg_Load(object sender, EventArgs e)
        {
            BackgroundImage = Resources.EasterEgg;
        }

        private void EasterEgg_FormClosing(object sender, FormClosingEventArgs e)
        {
          //  e.Cancel = true;
        }

        private void EasterEgg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
              //  if (Owner != null && Owner.GetType() == typeof(MainForm))
              //      ((MainForm)Owner).OnChangedLoginStatus(EAWSLoginStatus.);

                Close();
            }
        }
    }
}
