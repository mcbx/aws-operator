﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace System.Windows.Forms
{
    public static class TreeNodeCollectionExtensions
    {
        public static IEnumerable<TreeNode> Descendants(this TreeNodeCollection c)
        {
            foreach (var node in c.OfType<TreeNode>())
            {
                yield return node;

                foreach (var child in node.Nodes.Descendants())
                {
                    yield return child;
                }
            }
        }
    }
}

namespace AwsOperator.General
{
    public class CTreeView : TreeView
    {
        #region DoubleBuffer

        protected override void OnHandleCreated(EventArgs e)
        {
            SendMessage(this.Handle, TVM_SETEXTENDEDSTYLE, (IntPtr)TVS_EX_DOUBLEBUFFER, (IntPtr)TVS_EX_DOUBLEBUFFER);
            base.OnHandleCreated(e);
        }

        // Pinvoke:
        private const int TVM_SETEXTENDEDSTYLE = 0x1100 + 44;
        private const int TVM_GETEXTENDEDSTYLE = 0x1100 + 45;
        private const int TVS_EX_DOUBLEBUFFER = 0x0004;
        [DllImport("user32.dll")]

        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        #endregion

        public CTreeView()
        {
        }

        public new TreeNodeCollection Nodes { get; } = (new TreeView()).Nodes;

        public TreeNodeCollection DisplayedNodes { get { return base.Nodes; } }

        private const int TVIF_STATE = 0x8;
        private const int TVIS_STATEIMAGEMASK = 0xF000;
        private const int TV_FIRST = 0x1100;
        private const int TVM_SETITEM = TV_FIRST + 63;

        [StructLayout(LayoutKind.Sequential, Pack = 8, CharSet = CharSet.Auto)]
        private struct TVITEM
        {
            public int mask;
            public IntPtr hItem;
            public int state;
            public int stateMask;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpszText;
            public int cchTextMax;
            public int iImage;
            public int iSelectedImage;
            public int cChildren;
            public IntPtr lParam;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam,
                                                 ref TVITEM lParam);

        ///// <summary>
        ///// Hides the checkbox for the specified node on a TreeView control.
        ///// </summary>
        //public static void HideCheckBox(this TreeNode node)
        //{
        //    TVITEM tvi = new TVITEM();
        //    tvi.hItem = node.Handle;
        //    tvi.mask = TVIF_STATE;
        //    tvi.stateMask = TVIS_STATEIMAGEMASK;
        //    tvi.state = 0;
        //    SendMessage(node.TreeView.Handle, TVM_SETITEM, IntPtr.Zero, ref tvi);
        //}

        protected override void WndProc(ref Message m)
        {
            // Filter WM_LBUTTONDBLCLK
            if (m.Msg != 0x203) base.WndProc(ref m);
        }

        public void Sync()
        {
            Clone(Nodes, base.Nodes);
        }

        #region Filter

        public void ResetFilter()
        {
            BeginUpdate();
            Clone(Nodes, base.Nodes);
            base.Nodes[0].Expand();
            EndUpdate();
            Refresh();
        }

        private void SearchTreeView(TreeNodeCollection nodes, string l)
        {
            TreeNode node = null;
            for (int ndx = nodes.Count; ndx > 0; ndx--)
            {
                node = nodes[ndx - 1];
                SearchTreeView(node.Nodes, l);
                if (node.Nodes.Count == 0 && !node.Name.Contains(l) && node.ImageIndex == 1)
                    nodes.Remove(node);
            }
        }

        void TrimTree(TreeNodeCollection nodes)
        {
            TreeNode node = null;
            for (int ndx = nodes.Count; ndx > 0; ndx--)
            {
                node = nodes[ndx - 1];
                TrimTree(node.Nodes);
                if (node.Nodes.Count == 0 && node.ImageIndex == 0)
                    nodes.Remove(node);
            }
        }

        public void FilterNodes(string filter)
        {
            BeginUpdate();
            Clone(Nodes, base.Nodes);
            SearchTreeView(base.Nodes[0].Nodes, filter);
            TrimTree(base.Nodes[0].Nodes);
            base.Nodes[0].ExpandAll();
            base.Nodes[0].EnsureVisible();
            EndUpdate();
            Refresh();
        }

        #endregion

        #region Clone

        public void Clone(TreeNodeCollection treeview1, TreeNodeCollection treeview2)
        {
            treeview2.Clear();

            TreeNode newTn;
            foreach (TreeNode tn in treeview1)
            {
                newTn = new TreeNode(tn.Text, tn.ImageIndex, tn.SelectedImageIndex) { Name = tn.Name, BackColor = tn.BackColor, Tag = tn.Tag, ForeColor = tn.ForeColor };
                CopyChildren(newTn, tn);
                treeview2.Add(newTn);
            }
        }
        public void CopyChildren(TreeNode parent, TreeNode original)
        {
            TreeNode newTn;
            foreach (TreeNode tn in original.Nodes)
            {
                newTn = new TreeNode(tn.Text, tn.ImageIndex, tn.SelectedImageIndex) { Name = tn.Name, BackColor = tn.BackColor, Tag = tn.Tag, ForeColor = tn.ForeColor };
                parent.Nodes.Add(newTn);
                CopyChildren(newTn, tn);
            }
        }

        #endregion
    }
}
