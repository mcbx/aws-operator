﻿namespace AwsOperator.General
{
    partial class CommandDetailControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCommandStatusDetailed = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCommandStatus = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCommandOutput = new System.Windows.Forms.TextBox();
            this.btnRefreshDetails = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLastUpdate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCommandInstance = new System.Windows.Forms.TextBox();
            this.txtCommandId = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCommandStatusDetailed);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtCommandStatus);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtCommandOutput);
            this.groupBox1.Location = new System.Drawing.Point(264, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(526, 426);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Details";
            // 
            // txtCommandStatusDetailed
            // 
            this.txtCommandStatusDetailed.Location = new System.Drawing.Point(179, 49);
            this.txtCommandStatusDetailed.Name = "txtCommandStatusDetailed";
            this.txtCommandStatusDetailed.ReadOnly = true;
            this.txtCommandStatusDetailed.Size = new System.Drawing.Size(193, 20);
            this.txtCommandStatusDetailed.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Command status (detailed):";
            // 
            // txtCommandStatus
            // 
            this.txtCommandStatus.Location = new System.Drawing.Point(179, 22);
            this.txtCommandStatus.Name = "txtCommandStatus";
            this.txtCommandStatus.ReadOnly = true;
            this.txtCommandStatus.Size = new System.Drawing.Size(193, 20);
            this.txtCommandStatus.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Command status:";
            // 
            // txtCommandOutput
            // 
            this.txtCommandOutput.Location = new System.Drawing.Point(6, 78);
            this.txtCommandOutput.Multiline = true;
            this.txtCommandOutput.Name = "txtCommandOutput";
            this.txtCommandOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCommandOutput.Size = new System.Drawing.Size(514, 342);
            this.txtCommandOutput.TabIndex = 7;
            // 
            // btnRefreshDetails
            // 
            this.btnRefreshDetails.Location = new System.Drawing.Point(14, 121);
            this.btnRefreshDetails.Name = "btnRefreshDetails";
            this.btnRefreshDetails.Size = new System.Drawing.Size(231, 23);
            this.btnRefreshDetails.TabIndex = 16;
            this.btnRefreshDetails.Text = "Refresh \'RunCommand\' info";
            this.btnRefreshDetails.UseVisualStyleBackColor = true;
            this.btnRefreshDetails.Click += new System.EventHandler(this.btnRefreshDetails_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Last update:";
            // 
            // txtLastUpdate
            // 
            this.txtLastUpdate.Location = new System.Drawing.Point(98, 12);
            this.txtLastUpdate.Name = "txtLastUpdate";
            this.txtLastUpdate.ReadOnly = true;
            this.txtLastUpdate.Size = new System.Drawing.Size(147, 20);
            this.txtLastUpdate.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Instance id:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Command id:";
            // 
            // txtCommandInstance
            // 
            this.txtCommandInstance.Location = new System.Drawing.Point(98, 84);
            this.txtCommandInstance.Name = "txtCommandInstance";
            this.txtCommandInstance.ReadOnly = true;
            this.txtCommandInstance.Size = new System.Drawing.Size(147, 20);
            this.txtCommandInstance.TabIndex = 19;
            // 
            // txtCommandId
            // 
            this.txtCommandId.Location = new System.Drawing.Point(98, 46);
            this.txtCommandId.Name = "txtCommandId";
            this.txtCommandId.ReadOnly = true;
            this.txtCommandId.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtCommandId.Size = new System.Drawing.Size(147, 20);
            this.txtCommandId.TabIndex = 18;
            // 
            // CommandDetailControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRefreshDetails);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLastUpdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCommandInstance);
            this.Controls.Add(this.txtCommandId);
            this.Name = "CommandDetailControl";
            this.Size = new System.Drawing.Size(800, 447);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCommandStatusDetailed;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCommandStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCommandOutput;
        private System.Windows.Forms.Button btnRefreshDetails;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLastUpdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCommandInstance;
        private System.Windows.Forms.TextBox txtCommandId;
    }
}
