﻿using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Windows.Forms;

namespace AwsOperator.General
{
    public partial class CommandDetailControl : UserControl
    {
        private bool invalidObject = false;
        internal EAWSDomain _domein;
        internal EAWSEnvironment _environment;

        internal CommandDetail _commandItem;

        public CommandDetailControl()
        {
            InitializeComponent();
            invalidObject = true;
        }

        public CommandDetailControl(EAWSDomain domein, EAWSEnvironment environment, CommandDetail commandItem)
        {
            InitializeComponent();
            _domein = domein;
            _environment = environment;
            LoadCommand(commandItem);
        }

        public void LoadCommand(CommandDetail commandItem)
        {
            if (invalidObject)
                return;

            _commandItem = commandItem;
            txtCommandId.Text = commandItem.CommandId;
            txtCommandInstance.Text = commandItem.CommandInstanceId;
            if (commandItem.CommandType == CommandDetailType.Other)
            {
                txtCommandStatus.Text = commandItem.CommandStatus;
                txtCommandStatusDetailed.Text = commandItem.CommandStatusDetailed;
                txtCommandOutput.Text = commandItem.CommandResult;

                txtLastUpdate.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            }
        }

        private void btnRefreshDetails_Click(object sender, EventArgs e)
        {
            if (invalidObject || _commandItem == null)
                return;

            CommandDetail result = AWSRunCommand.GetCommandInvocation(_domein, _environment, _commandItem);
            txtCommandStatus.Text = result.CommandStatus;
            txtCommandStatusDetailed.Text = result.CommandStatusDetailed;
            txtCommandOutput.Text = result.CommandResult;

            txtLastUpdate.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
        }
    }
}
