﻿using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AwsOperator.General
{
    public partial class AWSAlarmTab : UserControl, IDisposable
    {
        internal string _appCode;
        internal EAWSDomain _domein;
        internal EAWSEnvironment _environment;

        private bool _formOverride = false;

        private TabPage _parentTab;
        private TabControl _parentTabControl;

        public EAWSLoginStatus LoginStatus { get; set; } = EAWSLoginStatus.Success;

        private int _currentInterval = 30;
        private List<AlarmDetail> _currentAlarms = new List<AlarmDetail>();

        private int _currentAlarmCount = 0;

        public bool ShowTabAlert { get; set; }

        public AWSAlarmTab(string appCode, EAWSDomain domein, EAWSEnvironment environment, TabPage parentTab, TabControl parentTabControl, bool _vatMode)
        {
            _appCode = appCode;
            _domein = domein;
            _environment = environment;
            _parentTab = parentTab;
            _parentTabControl = parentTabControl;
            _formOverride = _vatMode;

            ImageList imgList = new ImageList();
            imgList.ImageSize = new Size(1, 60);

            InitializeComponent();
            lvAlarms.SmallImageList = imgList;

            lvAlarms.GetType()
                .GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(lvAlarms, true);

            Text = $"Operator: CWR Alarm, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}, Refresh interval: {tmRefresher.Interval / 1000}s";
            tmRefresher.Interval = _currentInterval * 1000;
            tmRefresher.Enabled = true;

            _currentAlarms = AWSCloudWatchAlarms.GetAlarmsByApplicationCode(_appCode, _domein, _environment);
            _currentAlarms.ForEach(item => UpdateListViewItem(item));

            _currentAlarmCount = _currentAlarms.Count(item => item.AlarmState.ToLower() == "alarm" && item.AlarmMetricNamespace.ToLowerInvariant() != "aws/ec2");
            _parentTab.Text = $"{appCode.ToUpperInvariant()} ({_currentAlarmCount}) ";
            System.Diagnostics.Debug.WriteLine($"{_currentAlarms.Count}-{lvAlarms.Items.Count}");
        }

        private void UpdateListViewItem(AlarmDetail alarmItem)
        {
            if (!string.IsNullOrEmpty(alarmItem.AlarmMetricNamespace) && alarmItem.AlarmMetricNamespace.ToLowerInvariant() == "aws/ec2")
                return;

                if (!lvAlarms.Items.ContainsKey(alarmItem.Name))
                {
                    ListViewItem lItem = new ListViewItem()
                    {
                        Name = alarmItem.Name,
                        Text = alarmItem.ResetInProgress ? $"{alarmItem.Name}\r\nReset in progress..." : alarmItem.Name,
                        UseItemStyleForSubItems = false
                    };

                    lvAlarms.Items.Add(lItem);
                }

                lvAlarms.Items[alarmItem.Name].ToolTipText = $"Alarm description: {alarmItem.Description}";

                Color activeColor;
                switch (alarmItem.AlarmState.ToLower())
                {
                    case "ok":
                        activeColor = Color.LightGreen;
                        break;
                    case "alarm":
                        lvAlarms.Items[alarmItem.Name].ToolTipText = $"{lvAlarms.Items[alarmItem.Name].ToolTipText}{Environment.NewLine}Alarm triggered: {alarmItem.AlarmStateChanged}{(alarmItem.AlarmMetricNamespace.ToLowerInvariant() != "logmetric" ? "" : $"{Environment.NewLine}{Environment.NewLine}Suppression unavailable")}";
                        if (alarmItem.ResetInProgress)
                        {
                            activeColor = Color.FromArgb(255, 99, 71);
                        }
                        else
                        {
                            activeColor = Color.FromArgb(255, 99, 71);
                        }
                        break;
                    default:
                        if (!showInsuffecientDataToolStripMenuItem.Checked)
                            activeColor = Color.LightGreen;
                        else
                            activeColor = Color.FromArgb(250, 194, 87);
                        break;
                }

                if (lvAlarms.Items[alarmItem.Name].BackColor != activeColor)
                    lvAlarms.Items[alarmItem.Name].BackColor = activeColor;
        }

        private void RefreshData()
        {
            if (LoginStatus != EAWSLoginStatus.Success)
                return;

            Task.Run(() =>
            {
                var oldAlarmCount = _currentAlarmCount;
                var previousState = lvAlarms.Enabled;
                lvAlarms.BeginInvoke(new MethodInvoker(() =>
                {
                    lvAlarms.Enabled = true;
                }));

                //*    var updatedAlarms = AWSCloudWatchAlarms.GetAlarmByAlarmName(_currentAlarms.Select(nameItem => nameItem.Name).ToList(), _domein, _environment)
                //        .OrderBy(item => item.AlarmMetricNamespace)
                //        .ThenBy(item => item.Name)
                //        .ToList();*/

                var updatedAlarms = AWSCloudWatchAlarms.GetAlarmsByApplicationCode(_appCode, _domein, _environment)
                        .OrderBy(item => item.AlarmMetricNamespace)
                        .ThenBy(item => item.Name)
                        .ToList();

                lvAlarms.BeginInvoke(new MethodInvoker(() =>
                {
                    List<string> deletedItems = new List<string>();
                    deletedItems.AddRange(_currentAlarms.Select(item => item.Name).Except(updatedAlarms.Select(item => item.Name)));
                    deletedItems.ForEach(delItem =>
                    {
                        if (lvAlarms.Items.ContainsKey(delItem))
                            lvAlarms.Items[delItem].Remove();
                    });

                    updatedAlarms.ForEach(item => UpdateListViewItem(item));
                }));

                _currentAlarms = updatedAlarms;
                _currentAlarmCount = _currentAlarms.Count(item => item.AlarmState.ToLower() == "alarm" && item.AlarmMetricNamespace.ToLowerInvariant() != "aws/ec2");
                System.Diagnostics.Debug.WriteLine($"{_currentAlarms.Count}-{lvAlarms.Items.Count}");

                if (_currentAlarmCount != oldAlarmCount)
                {
                    _parentTab.BeginInvoke(new MethodInvoker(() =>
                    {
                        _parentTab.Text = $"{_appCode.ToUpperInvariant()} ({_currentAlarmCount}) ";
                    }));
                }

                if (_currentAlarmCount > oldAlarmCount)
                    ShowTabAlert = true;

                if (LoginStatus == EAWSLoginStatus.Success)
                {
                    lvAlarms.BeginInvoke(new MethodInvoker(() =>
                    {
                        lvAlarms.Enabled = previousState;
                    }));
                }
            }).GetAwaiter().GetResult();
        }

        private void tmRefresher_Tick(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void lvAlarms_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawBackground();
            if (e.Item.Selected)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.Cyan), e.Bounds);
                e.DrawFocusRectangle();
            }
            e.Graphics.DrawString(e.Item.Text, new Font("Arial", 8), new SolidBrush(Color.Black), e.Bounds);
        }

        private async void resetAlarmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvAlarms.SelectedItems.Count == 0)
                return;

            var alarm = _currentAlarms.Where(item => item.Name == lvAlarms.SelectedItems[0].Name).FirstOrDefault();
            if (alarm == null)
                return;

            var selectedObject = lvAlarms.SelectedItems[0];
            string orgText = selectedObject.Text;
            lvAlarms.SelectedItems[0].Text += $"{Environment.NewLine}{{Reset attempt in progress}}";

            //UseWaitCursor = true;
            //Cursor = Cursors.WaitCursor;

            await AWSCloudWatchAlarms.ResetAlarm(_domein, _environment, alarm);
            RefreshData();

            selectedObject.Text = orgText;

            //UseWaitCursor = false;
            //Cursor = Cursors.Default;
        }

        private void lvAlarms_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;

            try
            {
                resetAlarmToolStripMenuItem.Visible = false;

                Point localPoint = lvAlarms.PointToClient(Cursor.Position);
                lvAlarms.FocusedItem = lvAlarms.GetItemAt(localPoint.X, localPoint.Y);

                var alarm = _currentAlarms.Where(item => item.Name == lvAlarms.FocusedItem.Name).FirstOrDefault();
                if (alarm == null)
                    return;

                if (alarm.AlarmMetricNamespace.ToLowerInvariant() != "logmetrics")
                    return;

                if (lvAlarms.FocusedItem.Bounds.Contains(localPoint) && alarm.AlarmState.ToLowerInvariant() == "alarm")
                    resetAlarmToolStripMenuItem.Visible = true;
            }
            finally
            {
                cmListviewItemContextMenu.Show(Cursor.Position);
            }
        }

        private void CheckNewAlarmsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var oldAlarmCount = _currentAlarmCount;

            _currentAlarms = AWSCloudWatchAlarms.GetAlarmsByApplicationCode(_appCode, _domein, _environment);
            _currentAlarms.ForEach(item => UpdateListViewItem(item));

            _currentAlarmCount = _currentAlarms.Count(item => item.AlarmState.ToLower() == "alarm" && item.AlarmMetricNamespace.ToLowerInvariant() != "aws/ec2");
            if (_currentAlarmCount != oldAlarmCount)
                _parentTab.Text = $"{_appCode.ToUpperInvariant()} ({_currentAlarmCount}) ";

            if (_currentAlarmCount > oldAlarmCount)
                ShowTabAlert = true;
        }

        private void ChangeRefreshInterval(int newInterval)
        {
            switch (_currentInterval)
            {
                case 10:
                    sec10ToolStripMenuItem.Checked = false;
                    break;
                case 30:
                    sec30ToolStripMenuItem.Checked = false;
                    break;
                case 60:
                    sec60ToolStripMenuItem.Checked = false;
                    break;
                case 90:
                    sec90ToolStripMenuItem.Checked = false;
                    break;
                case 120:
                    sec120ToolStripMenuItem.Checked = false;
                    break;
                case 300:
                    sec300ToolStripMenuItem.Checked = false;
                    break;
                default: break;
            }

            _currentInterval = newInterval;
            tmRefresher.Interval = _currentInterval * 1000;
        }

        private void sec10ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(10);
        }

        private void sec30ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(30);
        }

        private void sec60ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(60);
        }

        private void sec90ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(90);
        }

        private void sec120ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(120);
        }

        private void sec300ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(300);
        }

        private void showInsuffecientDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showInsuffecientDataToolStripMenuItem.Checked = !showInsuffecientDataToolStripMenuItem.Checked;
            RefreshData();
        }
    }
}
