﻿using Fh.Aws.Framework.AWSLogin;
using System.Drawing;
using System.Windows.Forms;

namespace AwsOperator.General
{
    public class FormTemplate : Form
    {
        internal string _appCode;
        internal EAWSDomain _domein;
        internal EAWSEnvironment _environment;
        internal Form _myOwner = null;
        internal bool _formOverride;
        
        public FormTemplate() : base()
        {
        }

        public FormTemplate(string appCode, EAWSDomain awsDomain, EAWSEnvironment awsEnvironment, Form owner) : this(appCode, awsDomain, awsEnvironment, owner, false)
        {
        }

        public FormTemplate(string appCode, EAWSDomain awsDomain, EAWSEnvironment awsEnvironment, Form owner, bool formOverride) : this()
        {
            _myOwner = owner;
            _appCode = appCode;
            _domein = awsDomain;
            _environment = awsEnvironment;
            _formOverride = formOverride;

            if (_myOwner != null && _myOwner.GetType() == typeof(MainForm))
                ((MainForm)_myOwner).ChangedLoginStatus += FormTemplate_ChangedLoginStatus;

            if (_myOwner != null)
                Icon = _myOwner.Icon;

            FormClosing += FormTemplate_FormClosing;
            SetEnviromentColor();
        }

        private void SetEnviromentColor()
        {
            switch (_environment)
            {
                case EAWSEnvironment.Dev:
                    BackColor = Color.FromArgb(221, 255, 170);
                    break;
                case EAWSEnvironment.Sys:
                    BackColor = Color.FromArgb(170, 170, 255);
                    break;
                case EAWSEnvironment.Acc:
                    BackColor = Color.FromArgb(255, 170, 255);
                    break;
                case EAWSEnvironment.Prd:
                    break;
                default:
                  //  BackColor = Color.Yellow;
                    break;
            }
        }

        private void FormTemplate_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_myOwner != null && _myOwner.GetType() == typeof(MainForm))
                ((MainForm)_myOwner).ChangedLoginStatus -= FormTemplate_ChangedLoginStatus;

            System.GC.Collect();
        }

        internal virtual void FormTemplate_ChangedLoginStatus(object sender, MainForm.LoginStatusEvent e)
        {
            foreach (Control c in Controls)
            {
                c.Enabled = e.LoginStatus == EAWSLoginStatus.Success;
            }
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTemplate));
            this.SuspendLayout();
            // 
            // FormTemplate
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
         //   this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTemplate";
            this.ResumeLayout(false);

            //if (_myOwner != null && _myOwner.GetType() == typeof(MainForm))
        }
    }
}