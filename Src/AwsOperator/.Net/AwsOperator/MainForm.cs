﻿using AwsOperator.General;
using AwsOperator.Operators;
using Fh.Aws.Framework.AWSLogin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class MainForm : Form
    {
        #region Attributes

        private bool _vatEnabled = false;
        private EAWSLoginStatus _loginStatus = EAWSLoginStatus.Expired;

        #endregion

        #region Enums

        private enum OperatorOptions
        {
            AWS_Log,
            AWS_CD_ScriptLog,
            AWS_EC2RDS_Status,
            AWS_EC2_WindowsService,
            AWS_EC2_WindowsTask,
            AWS_EC2_IIS_Website,
            AWS_EC2_EventLog,
            AWS_ParameterStoreByApp,
            AWS_CWR_RunCommand,
            AWS_CWR_AlarmStatus,
            AWS_S3_Bucket
        }

        #endregion

        #region Custom event

        public class LoginStatusEvent : EventArgs
        {
            public EAWSLoginStatus LoginStatus { get; set; }

            public LoginStatusEvent(EAWSLoginStatus status)
            {
                LoginStatus = status;
            }
        }

        public event EventHandler<LoginStatusEvent> ChangedLoginStatus;

        public void OnChangedLoginStatus(EAWSLoginStatus e)
        {
            ChangedLoginStatus?.Invoke(this, new LoginStatusEvent(e));
            _loginStatus = e;
        }

        #endregion

        #region Constructor

        public MainForm()
        {
            InitializeComponent();

            cboOperatorWindow.DataSource = Enum.GetValues(typeof(OperatorOptions));
            cbDomain.DataSource = AWSCredentialKeeper.ActiveDomains.Keys.ToArray();
        }

        #endregion


        private Form CreateOperator(OperatorOptions operatorItem)
        {
            Form returnValue;
            switch (operatorItem)
            {
                case OperatorOptions.AWS_EC2_WindowsService:
                    returnValue = new AwsEc2WindowsServiceOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_CWR_RunCommand:
                    returnValue = new AwsCwrRunCommandOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment) cbEnv.SelectedItem, this, _vatEnabled);
                    break;
                case OperatorOptions.AWS_Log:
                    returnValue = new AwsLogOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_CD_ScriptLog:
                    returnValue = new AwsCodeDeployScriptLogOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_EC2_EventLog:
                    returnValue = new AwsWindowsInstanceLogOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_ParameterStoreByApp:
                    returnValue = new AwsParameterOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_CWR_AlarmStatus:
                    returnValue = new AwsCwrAlarmStatusOperatorTabbed(string.Empty, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this, _vatEnabled);
                    break;
                case OperatorOptions.AWS_EC2_WindowsTask:
                    returnValue = new AwsEc2WindowsTaskOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_EC2_IIS_Website:
                    returnValue = new AwsEc2WebsiteOperator(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_S3_Bucket:
                    returnValue = new AwsS3Operator(string.Empty, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
                    break;
                case OperatorOptions.AWS_EC2RDS_Status:
                    returnValue = new AwsEc2RdsOperator(string.Empty, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this, _vatEnabled);
                    break;
                default:
                    returnValue = null;
                    break;
            }

            return returnValue;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            _vatEnabled = false;
            btnPocOperator.Visible = false;
            btnGenerateAmiReport.Visible = false;

            if (txtToken.Text == "666")
            {
                Form easter = new EasterEgg(this);
                easter.ShowDialog();
                return;
            }

            try
            {
                if (!AWSCredentialKeeper.GetCredentialsWithToken(ConfigurationManager.AppSettings["AWS_Profile"], txtToken.Text, cboRefresh.Checked))
                {
                    OnChangedLoginStatus(EAWSLoginStatus.Failed);
                    return;
                }

                txtToken.Text = string.Empty;

                cboLoggedIn.Checked = true;
                gbAwsOperator.Enabled = true;
                tmCountDown.Enabled = true;

                lblExpireTimer.Text = "12:00:00";
                lblExpireTimer.ForeColor = DefaultForeColor;
                lblExpireText.Visible = true;
                lblExpireTimer.Visible = true;
                OnChangedLoginStatus(EAWSLoginStatus.Success);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Oops......", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                cboLoggedIn.Checked = false;
                gbAwsOperator.Enabled = false;
                OnChangedLoginStatus(EAWSLoginStatus.Failed);
            }
        }

        private void btnOpenOperatorWindow_Click(object sender, EventArgs e)
        {
            if (!cboLoggedIn.Checked)
            {
                MessageBox.Show(this, "Niet ingelogd bij AWS", "Oops.......", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return;
            }

            if (txtAppCode.Visible && string.IsNullOrEmpty(txtAppCode.Text.Trim()))
            {
                MessageBox.Show(this, "Lege applicatiecode", "Oops", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                txtAppCode.Focus();
                return;
            }

            var vatKeys = new[] { "449140348185477411162939153874918275176215", "79133382531965515811298222218623913379244667052181" };
            if (txtToken.Text == "666")
            {
                if (EasterEgg.CreateEgg(txtAppCode.Text) == vatKeys[0])
                {
                    _vatEnabled = true;
                    pbSpecial.Image = Properties.Resources.vat;
                    pbSpecial.Visible = true;
                    txtAppCode.PasswordChar = '\0';
                    txtAppCode.Text = string.Empty;
                    txtToken.Text = string.Empty;
                }

                if (EasterEgg.CreateEgg(txtAppCode.Text) == vatKeys[0])
                {
                    txtAppCode.Text = string.Empty;
                    txtAppCode.PasswordChar = '\0';
                    pbSpecial.Visible = false;
                }

                if (EasterEgg.CreateEgg(txtAppCode.Text) == vatKeys[1])
                {
                    _vatEnabled = false;
                    pbSpecial.Visible = false;
                    txtAppCode.Text = string.Empty;
                    txtAppCode.PasswordChar = '\0';
                }
                return;
            }

            Form operatorItem = CreateOperator((OperatorOptions)cboOperatorWindow.SelectedItem);
            if (operatorItem == null)
                return;

            operatorItem.Show();
        }

        private void cbDomain_TextChanged(object sender, EventArgs e)
        {
            EAWSDomain domain = (EAWSDomain)Enum.Parse(typeof(EAWSDomain), cbDomain.Text);
            cbEnv.DataSource = AWSCredentialKeeper.ActiveDomains[domain];
        }

        private void tmCountDown_Tick(object sender, EventArgs e)
        {
            TimeSpan remainingTime = TimeSpan.FromHours(12) - (DateTime.Now - AWSCredentialKeeper.LoginDateTime);
         //   TimeSpan remainingTime = TimeSpan.FromSeconds(30) - (DateTime.Now - AWSCredentialKeeper.LoginDateTime);
            if (remainingTime.Ticks <= 0)
            {
                tmCountDown.Enabled = false;
                lblExpireTimer.Text = "Expired";
                lblExpireTimer.ForeColor = Color.Red;
                gbAwsOperator.Enabled = false;
                AWSCredentialKeeper.ClearCredentials();
                OnChangedLoginStatus(EAWSLoginStatus.Expired);
                return;
            }
            lblExpireTimer.Text = remainingTime.ToString("hh\\:mm\\:ss");
        }

        private void txtToken_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnLogin_Click(this, new EventArgs());
        }

        private void button1_Click(object sender, EventArgs e)
        {
           var returnValue = new AwsCwrAlarmStatusOperatorTabbed(txtAppCode.Text, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this, _vatEnabled);
           returnValue.Show();
        }

        private void cboOperatorWindow_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((OperatorOptions)cboOperatorWindow.SelectedItem)
            {
                case OperatorOptions.AWS_EC2_WindowsService:
                case OperatorOptions.AWS_CWR_RunCommand:
                case OperatorOptions.AWS_Log:
                case OperatorOptions.AWS_CD_ScriptLog:
                case OperatorOptions.AWS_EC2_EventLog:
                case OperatorOptions.AWS_ParameterStoreByApp:
                    txtAppCode.Visible = true;
                    lblAppCode.Visible = true;
                    break;
                case OperatorOptions.AWS_CWR_AlarmStatus:
                case OperatorOptions.AWS_S3_Bucket:
                case OperatorOptions.AWS_EC2RDS_Status:
                    txtAppCode.Visible = false;
                    lblAppCode.Visible = false;
                    break;
                default:
                    txtAppCode.Visible = true;
                    lblAppCode.Visible = true;
                    break;
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            var runDateTime = DateTime.Now.ToString("yyyyMMdd hhmm");

            //   var x = Fh.Aws.Framework.AWSOperations.AWSEc2.GetEc2List((EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, true);
            using (StreamWriter writer = new StreamWriter(Path.Combine(ConfigurationManager.AppSettings["LogFileOutputFolder"], $"LaunchConfiguration-AmiStatus-{runDateTime}.csv")))
            {
                writer.WriteLine($"Domain;Environment;Id;AmiId;Ami-Age (dagen)");
            }

            using (StreamWriter writer = new StreamWriter(Path.Combine(ConfigurationManager.AppSettings["LogFileOutputFolder"], $"EC2-AmiStatus-{runDateTime}.csv")))
            {
                writer.WriteLine("Domain;Environment;EC2-Name;EC2-Id;Ami-Id;Ami-Age (dagen)");
            }

            var launchConfigurationSandbox = Fh.Aws.Framework.AWSOperations.AWSEc2.GetAmiDetailsFromLaunchConfiguration(EAWSDomain.Commerce, EAWSEnvironment.Dev, true);
            var ec2Sandbox = Fh.Aws.Framework.AWSOperations.AWSEc2.GetEc2List(EAWSDomain.Commerce, EAWSEnvironment.Dev, true, false);

            using (StreamWriter writer = new StreamWriter(Path.Combine(ConfigurationManager.AppSettings["LogFileOutputFolder"], $"LaunchConfiguration-AmiStatus-{runDateTime}.csv"), true))
            {
                launchConfigurationSandbox.ToList().ForEach(item =>
                {
                    if (item.Value.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Unknown || item.Value.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Deregistered || item.Value.CreationDateTime == DateTime.MinValue)
                        writer.WriteLine($"Sandbox;{EAWSEnvironment.Dev.ToString()};{item.Key};;{item.Value.AmiId};EXPIRED");
                    else
                        writer.WriteLine($"Sandbox;{EAWSEnvironment.Dev.ToString()};{item.Key};;{item.Value.AmiId};{Math.Round((DateTime.Now - item.Value.CreationDateTime).TotalDays)}");
                });
            }

            using (StreamWriter writer = new StreamWriter(Path.Combine(ConfigurationManager.AppSettings["LogFileOutputFolder"], $"EC2-AmiStatus-{runDateTime}.csv"), true))
            {
                ec2Sandbox.ToList().ForEach(item =>
                {
                    if (item.Value.AmiDetail == null || item.Value.AmiDetail.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Unknown || item.Value.AmiDetail.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Deregistered || item.Value.AmiDetail.CreationDateTime == DateTime.MinValue)
                        writer.WriteLine($"Sandbox;{EAWSEnvironment.Dev.ToString()};{item.Value.Name};{item.Key};{item.Value.Ami};EXPIRED");
                    else
                        writer.WriteLine($"Sandbox;{EAWSEnvironment.Dev.ToString()};{item.Value.Name};{item.Key};{item.Value.Ami};{Math.Round((DateTime.Now - item.Value.AmiDetail.CreationDateTime).TotalDays)}");
                });
            }

            //voor elk geconfigureerde domein in the tool gaan de ami's info ophalen
            foreach (KeyValuePair<EAWSDomain, List<EAWSEnvironment>> activeDomain in AWSCredentialKeeper.ActiveDomains)
            {
                foreach (EAWSEnvironment activeEnviroment in activeDomain.Value)
                {
                    if (activeEnviroment == EAWSEnvironment.Dev)
                        continue;

                    if (activeEnviroment == EAWSEnvironment.Acc)
                        continue;

                    var launchConfiguration = Fh.Aws.Framework.AWSOperations.AWSEc2.GetAmiDetailsFromLaunchConfiguration(activeDomain.Key, activeEnviroment, true);
                    var ec2 = Fh.Aws.Framework.AWSOperations.AWSEc2.GetEc2List(activeDomain.Key, activeEnviroment, true, true);

                    string env = activeEnviroment.ToString();
                    if (activeEnviroment == EAWSEnvironment.Sys)
                        env = "Staging";

                    using (StreamWriter writer = new StreamWriter(Path.Combine(ConfigurationManager.AppSettings["LogFileOutputFolder"], $"LaunchConfiguration-AmiStatus-{runDateTime}.csv"), true))
                    {
                        launchConfiguration.ToList().ForEach(item =>
                        {
                            if (item.Value.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Unknown || item.Value.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Deregistered || item.Value.CreationDateTime == DateTime.MinValue)
                                writer.WriteLine($"{activeDomain.Key.ToString()};{env};{item.Key};;{item.Value.AmiId};EXPIRED");
                            else
                                writer.WriteLine($"{activeDomain.Key.ToString()};{env};{item.Key};;{item.Value.AmiId};{Math.Round((DateTime.Now - item.Value.CreationDateTime).TotalDays)}");
                        });
                    }

                    using (StreamWriter writer = new StreamWriter(Path.Combine(ConfigurationManager.AppSettings["LogFileOutputFolder"], $"EC2-AmiStatus-{runDateTime}.csv"), true))
                    {
                        ec2.ToList().ForEach(item =>
                        {
                            if (item.Value.AmiDetail == null || item.Value.AmiDetail.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Unknown || item.Value.AmiDetail.State == Fh.Aws.Framework.AWSOperationClassContainers.ImageState.Deregistered || item.Value.AmiDetail.CreationDateTime == DateTime.MinValue)
                                writer.WriteLine($"{activeDomain.Key.ToString()};{env};{item.Value.Name};{item.Key};{item.Value.Ami};EXPIRED");
                            else
                                writer.WriteLine($"{activeDomain.Key.ToString()};{env};{item.Value.Name};{item.Key};{item.Value.Ami};{Math.Round((DateTime.Now - item.Value.AmiDetail.CreationDateTime).TotalDays)}");
                        });
                    }
                }
            }
        }

        ////debug knop voor ontwikkelingen
        //private void button2_Click(object sender, EventArgs e)
        //{
        //    //var res = Fh.Aws.Framework.AWSOperations.AWSRunCommand.GetCommandInvocation((EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, new Fh.Aws.Framework.AWSOperationClassContainers.CommandDetail()
        //    //{
        //    //    CommandId = "cd10a189-5021-4ba1-9077-6782da3042d2",
        //    //    CommandInstanceId = "i-0885e0faec4eb8831"
        //    //});

        //    //System.Diagnostics.Debug.WriteLine(res.CommandResult);

        //    AWSEc2Operator frm = new AWSEc2Operator();
        //    frm.Show();

        //    //AwsEc2WebsiteOperator frm2 = new AwsEc2WebsiteOperator("aax", (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
        //    //frm2.Show();

        //    //    Fh.Aws.Framework.AWSOperations.AWSCloudWatchAlarms.GetHistoryForAlarmNames(EAWSDomain.Commerce, EAWSEnvironment.Dev, "aax-oc-vlr-lfa");
        //}

        private void BtnImport_Click(object sender, EventArgs e)
        {
            var frm = new RegisterAccessKey(this);
            frm.ShowDialog(this);
        }

        private void btnGetEc2Credentials_Click(object sender, EventArgs e)
        {
            AwsEc2CredsOperator frmCreds = new AwsEc2CredsOperator(string.Empty, (EAWSDomain)cbDomain.SelectedItem, (EAWSEnvironment)cbEnv.SelectedItem, this);
            frmCreds.Show();
        }

        private void txtToken_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                return;

            txtAppCode.PasswordChar = '\0';

            if (!_vatEnabled)
                pbSpecial.Visible = false;

            if (_vatEnabled)
                pbSpecial.Image = Properties.Resources.vat;

            if (txtToken.Text.Length != 3)
                return;

            if (txtToken.Text == "666" && string.IsNullOrEmpty(txtAppCode.Text) && _loginStatus == EAWSLoginStatus.Success)
            {
                txtAppCode.PasswordChar = '*';
                pbSpecial.Image = Properties.Resources.waiting;
                pbSpecial.Visible = true;
            }
        }

        private void txtAppCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOpenOperatorWindow_Click(this, null);
        }
    }
}