﻿using Fh.Aws.Framework.AWSLogin;
using System;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AWSCredentialKeeper.Initialize(ConfigurationManager.AppSettings.AllKeys.ToDictionary(item => item, item => ConfigurationManager.AppSettings[item]), Environment.UserName);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
