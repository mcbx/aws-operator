﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Windows.Forms;

namespace AwsOperator.Operators
{
    public partial class AwsEc2CredsOperator : FormTemplate
    {
        public AwsEc2CredsOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();
            Text = $"Ec2Creds, ({_domein.ToString()}, {_environment.ToString()})";

            FetchCredentials();
        }

        private void FetchCredentials()
        {
            var result = AWSParameterStore.GetEc2Credentials(_domein, _environment);

            txtec2CredName.Text = result.Name;
            txtEc2CredKey.Text = result.Key;

            btnCopyEc2CredName.Enabled = true;
            btnCopyEc2CredKey.Enabled = true;

            if (string.IsNullOrEmpty(result.Name))
            {
                txtec2CredName.Text = "Username not found!!!";
                btnCopyEc2CredName.Enabled = false;
            }

            if (string.IsNullOrEmpty(result.Key))
            {
                txtEc2CredKey.Text = "Key not found!!!";
                btnCopyEc2CredKey.Enabled = false;
            }
        }

        public AwsEc2CredsOperator()
        {
            InitializeComponent();
        }

        private void btnCopyEc2CredName_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtec2CredName.Text);
        }

        private void btnCopyEc2CredKey_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtEc2CredKey.Text);
        }

        private void btnRefreshCreds_Click(object sender, EventArgs e)
        {
            FetchCredentials();
        }
    }
}
