﻿namespace AwsOperator.Operators
{
    partial class AwsEc2CredsOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtec2CredName = new System.Windows.Forms.TextBox();
            this.btnCopyEc2CredName = new System.Windows.Forms.Button();
            this.btnCopyEc2CredKey = new System.Windows.Forms.Button();
            this.txtEc2CredKey = new System.Windows.Forms.TextBox();
            this.btnRefreshCreds = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Key";
            // 
            // txtec2CredName
            // 
            this.txtec2CredName.Location = new System.Drawing.Point(12, 25);
            this.txtec2CredName.Name = "txtec2CredName";
            this.txtec2CredName.ReadOnly = true;
            this.txtec2CredName.Size = new System.Drawing.Size(294, 20);
            this.txtec2CredName.TabIndex = 3;
            this.txtec2CredName.TabStop = false;
            // 
            // btnCopyEc2CredName
            // 
            this.btnCopyEc2CredName.Location = new System.Drawing.Point(312, 23);
            this.btnCopyEc2CredName.Name = "btnCopyEc2CredName";
            this.btnCopyEc2CredName.Size = new System.Drawing.Size(43, 23);
            this.btnCopyEc2CredName.TabIndex = 4;
            this.btnCopyEc2CredName.Text = "Copy";
            this.btnCopyEc2CredName.UseVisualStyleBackColor = true;
            this.btnCopyEc2CredName.Click += new System.EventHandler(this.btnCopyEc2CredName_Click);
            // 
            // btnCopyEc2CredKey
            // 
            this.btnCopyEc2CredKey.Location = new System.Drawing.Point(312, 87);
            this.btnCopyEc2CredKey.Name = "btnCopyEc2CredKey";
            this.btnCopyEc2CredKey.Size = new System.Drawing.Size(43, 23);
            this.btnCopyEc2CredKey.TabIndex = 6;
            this.btnCopyEc2CredKey.Text = "Copy";
            this.btnCopyEc2CredKey.UseVisualStyleBackColor = true;
            this.btnCopyEc2CredKey.Click += new System.EventHandler(this.btnCopyEc2CredKey_Click);
            // 
            // txtEc2CredKey
            // 
            this.txtEc2CredKey.Location = new System.Drawing.Point(12, 89);
            this.txtEc2CredKey.Name = "txtEc2CredKey";
            this.txtEc2CredKey.ReadOnly = true;
            this.txtEc2CredKey.Size = new System.Drawing.Size(294, 20);
            this.txtEc2CredKey.TabIndex = 5;
            this.txtEc2CredKey.TabStop = false;
            // 
            // btnRefreshCreds
            // 
            this.btnRefreshCreds.Location = new System.Drawing.Point(12, 115);
            this.btnRefreshCreds.Name = "btnRefreshCreds";
            this.btnRefreshCreds.Size = new System.Drawing.Size(343, 23);
            this.btnRefreshCreds.TabIndex = 7;
            this.btnRefreshCreds.Text = "Refresh";
            this.btnRefreshCreds.UseVisualStyleBackColor = true;
            this.btnRefreshCreds.Click += new System.EventHandler(this.btnRefreshCreds_Click);
            // 
            // Ec2Creds
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 144);
            this.Controls.Add(this.btnRefreshCreds);
            this.Controls.Add(this.btnCopyEc2CredKey);
            this.Controls.Add(this.txtEc2CredKey);
            this.Controls.Add(this.btnCopyEc2CredName);
            this.Controls.Add(this.txtec2CredName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "Ec2Creds";
            this.Text = "Ec2Creds";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtec2CredName;
        private System.Windows.Forms.Button btnCopyEc2CredName;
        private System.Windows.Forms.Button btnCopyEc2CredKey;
        private System.Windows.Forms.TextBox txtEc2CredKey;
        private System.Windows.Forms.Button btnRefreshCreds;
    }
}