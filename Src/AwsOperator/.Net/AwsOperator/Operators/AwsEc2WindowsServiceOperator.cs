﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class AwsEc2WindowsServiceOperator : FormTemplate
    {
        private Dictionary<string, List<string>> _activeEc2PurposeIdList = new Dictionary<string, List<string>>();
        private Dictionary<string, List<JsonServiceItem>> _activeEc2ServiceSnapshot = new Dictionary<string, List<JsonServiceItem>>();

        public AwsEc2WindowsServiceOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();

            Text = $"Operator: EC2 Windowsservice, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";
            btnRefreshTags_Click(this, null);
        }

        private void cbPurposeIdTag_TextChanged(object sender, EventArgs e)
        {
            lbTagInstanceId.Items.Clear();
            if (string.IsNullOrEmpty(cbPurposeIdTag.Text))
                return;

            if (!_activeEc2PurposeIdList.ContainsKey(cbPurposeIdTag.Text))
                return;

            lbTagInstanceId.Items.AddRange(_activeEc2PurposeIdList[cbPurposeIdTag.Text].ToArray());
        }

        private void btnRefreshTags_Click(object sender, EventArgs e)
        {
            var result = AWSEc2.GetEc2PurposeIdList(_domein, _environment, _appCode);
            if (result == null)
                return;

            _activeEc2PurposeIdList = result;
            cbPurposeIdTag.DataSource = result.Keys.ToList();
        }

        private void btnRefreshServiceSnapshot_Click(object sender, EventArgs e)
        {
            try
            {
                UseWaitCursor = true;

                cbSnapshotServiceEC2Ids.DataSource = null;
                txtSnapshotDateTime.Text = string.Empty;
                lvServicesOnInstance.Items.Clear();

                Application.DoEvents();
                if (string.IsNullOrEmpty(cbPurposeIdTag.Text))
                    return;

                if (!_activeEc2PurposeIdList.ContainsKey(cbPurposeIdTag.Text))
                    return;

                List<string> requestFromInstances = new List<string>();
                requestFromInstances.AddRange(lbTagInstanceId.Items.Cast<string>());

                var result = AWSEc2WindowsService.GetServiceSnapshot(_domein, _environment, requestFromInstances);
                if (result == null)
                    return;

                _activeEc2ServiceSnapshot = result;
                cbSnapshotServiceEC2Ids.DataSource = result.Keys.ToList();

                txtSnapshotDateTime.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            }
            finally
            {
                UseWaitCursor = false;
            }
        }

        private Color DetermineServiceStatusColor(JsonServiceItem serviceItem, Color defaultColor)
        {
            if (serviceItem.StartType.ToString().ToLower() == "automatic" && serviceItem.Status.ToString().ToLower() == "stopped")
                return Color.LightSalmon;

            if (serviceItem.StartType.ToString().ToLower() == "manual" && serviceItem.Status.ToString().ToLower() == "started")
                return Color.FromArgb(250, 194, 87);

            return defaultColor;
        }

        private void cbSnapshotServiceEC2Ids_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbSnapshotServiceEC2Ids.Text))
                return;

            if (!_activeEc2ServiceSnapshot.ContainsKey(cbSnapshotServiceEC2Ids.Text))
                return;

            lvServicesOnInstance.Items.Clear();
            foreach (JsonServiceItem serviceItem in _activeEc2ServiceSnapshot[cbSnapshotServiceEC2Ids.Text].OrderBy(item => item.DisplayName))
            {
                lvServicesOnInstance.Items.Add(serviceItem.Name, serviceItem.DisplayName, string.Empty);
                lvServicesOnInstance.Items[serviceItem.Name].UseItemStyleForSubItems = false;

                ListViewItem.ListViewSubItem serviceStartType = new ListViewItem.ListViewSubItem()
                {
                    Name = "StartType",
                    Text = serviceItem.StartType.ToString()
                };

                ListViewItem.ListViewSubItem serviceStatus = new ListViewItem.ListViewSubItem()
                {
                    Name = "Status",
                    Text = serviceItem.Status.ToString()
                };

                serviceStatus.BackColor = DetermineServiceStatusColor(serviceItem, lvServicesOnInstance.Items[serviceItem.Name].BackColor); 
                serviceStartType.BackColor = serviceStartType.Text.ToLower() == "manual" && serviceStatus.Text.ToLower() == "Started" ? Color.FromArgb(250, 194, 87) : lvServicesOnInstance.Items[serviceItem.Name].BackColor;

                lvServicesOnInstance.Items[serviceItem.Name].SubItems.Add(serviceStartType);
                lvServicesOnInstance.Items[serviceItem.Name].SubItems.Add(serviceStatus);
            }
        }

        private void btnStartServiceCK_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotServiceEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(this.lvServicesOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.Name));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende windows services wilt starten? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2WindowsService.StartWindowsServiceOnEC2(_domein, _environment, cbSnapshotServiceEC2Ids.Text, checkedItems, $"AwsEc2WindowsServiceOperator ({cbSnapshotServiceEC2Ids.Text}>Start service(s)) => {AWSCredentialKeeper.AWSOperatorUser}");
                btnRefreshServiceSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void btnStopServiceCK_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotServiceEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(this.lvServicesOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.Name));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende windows services wilt stoppen? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2WindowsService.StopWindowsServiceOnEC2(_domein, _environment, cbSnapshotServiceEC2Ids.Text, checkedItems, $"AwsEc2WindowsServiceOperator ({cbSnapshotServiceEC2Ids.Text}>Stop service(s)) => {AWSCredentialKeeper.AWSOperatorUser}");
                btnRefreshServiceSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void btnRestartServiceCK_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotServiceEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(lvServicesOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.Name));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende windows services wilt herstarten? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2WindowsService.RestartWindowsServiceOnEC2(_domein, _environment, cbSnapshotServiceEC2Ids.Text, checkedItems, $"AwsEc2WindowsServiceOperator ({cbSnapshotServiceEC2Ids.Text}>Restart service(s))=> {AWSCredentialKeeper.AWSOperatorUser}");
                btnRefreshServiceSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }
    }
}
