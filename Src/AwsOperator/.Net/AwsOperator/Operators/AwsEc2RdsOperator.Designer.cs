﻿namespace AwsOperator
{
    partial class AwsEc2RdsOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmRefresher = new System.Windows.Forms.Timer(this.components);
            this.lvEc2 = new System.Windows.Forms.ListView();
            this.chAlarmName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAlarmStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmsEc2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tmsAmiAge = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsIgnoreEnviroment = new System.Windows.Forms.ToolStripMenuItem();
            this.tss10 = new System.Windows.Forms.ToolStripSeparator();
            this.tmsEc2ClipboardActions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCopyInstanceId = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCopyIpAdres = new System.Windows.Forms.ToolStripMenuItem();
            this.tss1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmSwitch = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSMAmiAge = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSMEc2Operations = new System.Windows.Forms.ToolStripMenuItem();
            this.tss2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmEc2Operations = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRdpEc2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tssRdp1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmStartEc2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmStopEc2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tss4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmRestartEc2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tss5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmTerminateEc2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tss3 = new System.Windows.Forms.ToolStripSeparator();
            this.SetIntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec30ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec60ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec90ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec120ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec300ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sec600ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lvDatabases = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmsRds = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tmsRdsClipboardActions = new System.Windows.Forms.ToolStripMenuItem();
            this.copyEndpointAdresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsRdsC1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmRdsOperations = new System.Windows.Forms.ToolStripMenuItem();
            this.startRdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsrdso1 = new System.Windows.Forms.ToolStripSeparator();
            this.stopRdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsrdso2 = new System.Windows.Forms.ToolStripSeparator();
            this.restartRdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblEc2State = new System.Windows.Forms.Label();
            this.lblRdsState = new System.Windows.Forms.Label();
            this.lblRdpWarning = new System.Windows.Forms.Label();
            this.cmsEc2.SuspendLayout();
            this.cmsRds.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmRefresher
            // 
            this.tmRefresher.Interval = 30000;
            this.tmRefresher.Tick += new System.EventHandler(this.tmRefresher_Tick);
            // 
            // lvEc2
            // 
            this.lvEc2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvEc2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chAlarmName,
            this.chAlarmStatus});
            this.lvEc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.lvEc2.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvEc2.HideSelection = false;
            this.lvEc2.Location = new System.Drawing.Point(12, 32);
            this.lvEc2.MultiSelect = false;
            this.lvEc2.Name = "lvEc2";
            this.lvEc2.OwnerDraw = true;
            this.lvEc2.ShowItemToolTips = true;
            this.lvEc2.Size = new System.Drawing.Size(1216, 424);
            this.lvEc2.TabIndex = 30;
            this.lvEc2.UseCompatibleStateImageBehavior = false;
            this.lvEc2.View = System.Windows.Forms.View.Tile;
            this.lvEc2.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvAlarms_DrawItem);
            this.lvEc2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lvAlarms_MouseClick);
            // 
            // chAlarmName
            // 
            this.chAlarmName.Text = "AlarmName";
            this.chAlarmName.Width = 226;
            // 
            // chAlarmStatus
            // 
            this.chAlarmStatus.Text = "AlarmStatus";
            this.chAlarmStatus.Width = 78;
            // 
            // cmsEc2
            // 
            this.cmsEc2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmsAmiAge,
            this.tmsIgnoreEnviroment,
            this.tss10,
            this.tmsEc2ClipboardActions,
            this.tss1,
            this.tsmSwitch,
            this.tss2,
            this.tsmEc2Operations,
            this.tss3,
            this.SetIntervalToolStripMenuItem});
            this.cmsEc2.Name = "cmListviewItemContextMenu";
            this.cmsEc2.Size = new System.Drawing.Size(170, 160);
            // 
            // tmsAmiAge
            // 
            this.tmsAmiAge.Name = "tmsAmiAge";
            this.tmsAmiAge.Size = new System.Drawing.Size(169, 22);
            this.tmsAmiAge.Text = "Show AMI age";
            this.tmsAmiAge.Click += new System.EventHandler(this.tmsAmiAge_Click);
            // 
            // tmsIgnoreEnviroment
            // 
            this.tmsIgnoreEnviroment.Name = "tmsIgnoreEnviroment";
            this.tmsIgnoreEnviroment.Size = new System.Drawing.Size(169, 22);
            this.tmsIgnoreEnviroment.Text = "Ignore ENV";
            this.tmsIgnoreEnviroment.Click += new System.EventHandler(this.tmsIgnoreEnviroment_Click);
            // 
            // tss10
            // 
            this.tss10.Name = "tss10";
            this.tss10.Size = new System.Drawing.Size(166, 6);
            this.tss10.Visible = false;
            // 
            // tmsEc2ClipboardActions
            // 
            this.tmsEc2ClipboardActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCopyInstanceId,
            this.tsmCopyIpAdres});
            this.tmsEc2ClipboardActions.Name = "tmsEc2ClipboardActions";
            this.tmsEc2ClipboardActions.Size = new System.Drawing.Size(169, 22);
            this.tmsEc2ClipboardActions.Text = "Clipboard Actions";
            this.tmsEc2ClipboardActions.Visible = false;
            // 
            // tsmCopyInstanceId
            // 
            this.tsmCopyInstanceId.Name = "tsmCopyInstanceId";
            this.tsmCopyInstanceId.Size = new System.Drawing.Size(171, 22);
            this.tsmCopyInstanceId.Text = "Copy Instance id...";
            this.tsmCopyInstanceId.Click += new System.EventHandler(this.tsmCopyInstanceId_Click);
            // 
            // tsmCopyIpAdres
            // 
            this.tsmCopyIpAdres.Name = "tsmCopyIpAdres";
            this.tsmCopyIpAdres.Size = new System.Drawing.Size(171, 22);
            this.tsmCopyIpAdres.Text = "Copy Ip Adres...";
            this.tsmCopyIpAdres.Click += new System.EventHandler(this.tsmCopyIpAdres_Click);
            // 
            // tss1
            // 
            this.tss1.Name = "tss1";
            this.tss1.Size = new System.Drawing.Size(166, 6);
            this.tss1.Visible = false;
            // 
            // tsmSwitch
            // 
            this.tsmSwitch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmSMAmiAge,
            this.tsmSMEc2Operations});
            this.tsmSwitch.Name = "tsmSwitch";
            this.tsmSwitch.Size = new System.Drawing.Size(169, 22);
            this.tsmSwitch.Text = "Switch mode";
            this.tsmSwitch.Visible = false;
            // 
            // tsmSMAmiAge
            // 
            this.tsmSMAmiAge.Checked = true;
            this.tsmSMAmiAge.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmSMAmiAge.Name = "tsmSMAmiAge";
            this.tsmSMAmiAge.Size = new System.Drawing.Size(181, 22);
            this.tsmSMAmiAge.Text = "Ami Age";
            this.tsmSMAmiAge.Click += new System.EventHandler(this.tsmSMAmiAge_Click);
            // 
            // tsmSMEc2Operations
            // 
            this.tsmSMEc2Operations.Name = "tsmSMEc2Operations";
            this.tsmSMEc2Operations.Size = new System.Drawing.Size(181, 22);
            this.tsmSMEc2Operations.Text = "EC2/RDS Operations";
            this.tsmSMEc2Operations.Click += new System.EventHandler(this.tsmSMEc2Operations_Click);
            // 
            // tss2
            // 
            this.tss2.Name = "tss2";
            this.tss2.Size = new System.Drawing.Size(166, 6);
            this.tss2.Visible = false;
            // 
            // tsmEc2Operations
            // 
            this.tsmEc2Operations.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRdpEc2,
            this.tssRdp1,
            this.tsmStartEc2,
            this.tsmStopEc2,
            this.tss4,
            this.tsmRestartEc2,
            this.tss5,
            this.tsmTerminateEc2});
            this.tsmEc2Operations.Name = "tsmEc2Operations";
            this.tsmEc2Operations.Size = new System.Drawing.Size(169, 22);
            this.tsmEc2Operations.Text = "Ec2 Operations";
            this.tsmEc2Operations.Visible = false;
            // 
            // tsmRdpEc2
            // 
            this.tsmRdpEc2.Name = "tsmRdpEc2";
            this.tsmRdpEc2.Size = new System.Drawing.Size(208, 22);
            this.tsmRdpEc2.Text = "Attempt RDP Connection";
            this.tsmRdpEc2.ToolTipText = "Creates a RDP Connection and copies the known password to Clipboard.";
            this.tsmRdpEc2.Click += new System.EventHandler(this.tsmRdpEc2_Click);
            // 
            // tssRdp1
            // 
            this.tssRdp1.Name = "tssRdp1";
            this.tssRdp1.Size = new System.Drawing.Size(205, 6);
            // 
            // tsmStartEc2
            // 
            this.tsmStartEc2.Name = "tsmStartEc2";
            this.tsmStartEc2.Size = new System.Drawing.Size(208, 22);
            this.tsmStartEc2.Text = "Start Ec2";
            this.tsmStartEc2.Click += new System.EventHandler(this.tsmStartEc2_Click);
            // 
            // tsmStopEc2
            // 
            this.tsmStopEc2.Name = "tsmStopEc2";
            this.tsmStopEc2.Size = new System.Drawing.Size(208, 22);
            this.tsmStopEc2.Text = "Stop Ec2";
            this.tsmStopEc2.Visible = false;
            this.tsmStopEc2.Click += new System.EventHandler(this.tsmStopEc2_Click);
            // 
            // tss4
            // 
            this.tss4.Name = "tss4";
            this.tss4.Size = new System.Drawing.Size(205, 6);
            this.tss4.Visible = false;
            // 
            // tsmRestartEc2
            // 
            this.tsmRestartEc2.Name = "tsmRestartEc2";
            this.tsmRestartEc2.Size = new System.Drawing.Size(208, 22);
            this.tsmRestartEc2.Text = "Restart Ec2";
            this.tsmRestartEc2.Visible = false;
            this.tsmRestartEc2.Click += new System.EventHandler(this.tsmRestartEc2_Click);
            // 
            // tss5
            // 
            this.tss5.Name = "tss5";
            this.tss5.Size = new System.Drawing.Size(205, 6);
            this.tss5.Visible = false;
            // 
            // tsmTerminateEc2
            // 
            this.tsmTerminateEc2.Name = "tsmTerminateEc2";
            this.tsmTerminateEc2.Size = new System.Drawing.Size(208, 22);
            this.tsmTerminateEc2.Text = "Terminate Ec2";
            this.tsmTerminateEc2.Visible = false;
            this.tsmTerminateEc2.Click += new System.EventHandler(this.tsmTerminateEc2_Click);
            // 
            // tss3
            // 
            this.tss3.Name = "tss3";
            this.tss3.Size = new System.Drawing.Size(166, 6);
            this.tss3.Visible = false;
            // 
            // SetIntervalToolStripMenuItem
            // 
            this.SetIntervalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sec30ToolStripMenuItem,
            this.sec60ToolStripMenuItem,
            this.sec90ToolStripMenuItem,
            this.sec120ToolStripMenuItem,
            this.sec300ToolStripMenuItem,
            this.sec600ToolStripMenuItem});
            this.SetIntervalToolStripMenuItem.Name = "SetIntervalToolStripMenuItem";
            this.SetIntervalToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.SetIntervalToolStripMenuItem.Text = "Set interval";
            this.SetIntervalToolStripMenuItem.Visible = false;
            // 
            // sec30ToolStripMenuItem
            // 
            this.sec30ToolStripMenuItem.CheckOnClick = true;
            this.sec30ToolStripMenuItem.Name = "sec30ToolStripMenuItem";
            this.sec30ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec30ToolStripMenuItem.Text = "30 sec";
            this.sec30ToolStripMenuItem.Click += new System.EventHandler(this.sec30ToolStripMenuItem_Click);
            // 
            // sec60ToolStripMenuItem
            // 
            this.sec60ToolStripMenuItem.CheckOnClick = true;
            this.sec60ToolStripMenuItem.Name = "sec60ToolStripMenuItem";
            this.sec60ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec60ToolStripMenuItem.Text = "1 min";
            this.sec60ToolStripMenuItem.Click += new System.EventHandler(this.sec60ToolStripMenuItem_Click);
            // 
            // sec90ToolStripMenuItem
            // 
            this.sec90ToolStripMenuItem.CheckOnClick = true;
            this.sec90ToolStripMenuItem.Name = "sec90ToolStripMenuItem";
            this.sec90ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec90ToolStripMenuItem.Text = "1.5 min";
            this.sec90ToolStripMenuItem.Click += new System.EventHandler(this.sec90ToolStripMenuItem_Click);
            // 
            // sec120ToolStripMenuItem
            // 
            this.sec120ToolStripMenuItem.Checked = true;
            this.sec120ToolStripMenuItem.CheckOnClick = true;
            this.sec120ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sec120ToolStripMenuItem.Name = "sec120ToolStripMenuItem";
            this.sec120ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec120ToolStripMenuItem.Text = "2 min";
            this.sec120ToolStripMenuItem.Click += new System.EventHandler(this.sec120ToolStripMenuItem_Click);
            // 
            // sec300ToolStripMenuItem
            // 
            this.sec300ToolStripMenuItem.CheckOnClick = true;
            this.sec300ToolStripMenuItem.Name = "sec300ToolStripMenuItem";
            this.sec300ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec300ToolStripMenuItem.Text = "5 min";
            this.sec300ToolStripMenuItem.Click += new System.EventHandler(this.sec300ToolStripMenuItem_Click);
            // 
            // sec600ToolStripMenuItem
            // 
            this.sec600ToolStripMenuItem.CheckOnClick = true;
            this.sec600ToolStripMenuItem.Name = "sec600ToolStripMenuItem";
            this.sec600ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sec600ToolStripMenuItem.Text = "10 min";
            this.sec600ToolStripMenuItem.Click += new System.EventHandler(this.sec600ToolStripMenuItem_Click);
            // 
            // lvDatabases
            // 
            this.lvDatabases.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDatabases.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvDatabases.ContextMenuStrip = this.cmsRds;
            this.lvDatabases.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lvDatabases.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvDatabases.HideSelection = false;
            this.lvDatabases.Location = new System.Drawing.Point(983, 32);
            this.lvDatabases.MultiSelect = false;
            this.lvDatabases.Name = "lvDatabases";
            this.lvDatabases.OwnerDraw = true;
            this.lvDatabases.ShowItemToolTips = true;
            this.lvDatabases.Size = new System.Drawing.Size(245, 424);
            this.lvDatabases.TabIndex = 31;
            this.lvDatabases.UseCompatibleStateImageBehavior = false;
            this.lvDatabases.View = System.Windows.Forms.View.Tile;
            this.lvDatabases.Visible = false;
            this.lvDatabases.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvDatabases_DrawItem);
            this.lvDatabases.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lvDatabases_MouseDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "AlarmName";
            this.columnHeader1.Width = 226;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "AlarmStatus";
            this.columnHeader2.Width = 78;
            // 
            // cmsRds
            // 
            this.cmsRds.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmsRdsClipboardActions,
            this.tsRdsC1,
            this.tsmRdsOperations});
            this.cmsRds.Name = "cmsRds";
            this.cmsRds.Size = new System.Drawing.Size(170, 54);
            this.cmsRds.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.cmsRds_Closed);
            // 
            // tmsRdsClipboardActions
            // 
            this.tmsRdsClipboardActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyEndpointAdresToolStripMenuItem});
            this.tmsRdsClipboardActions.Name = "tmsRdsClipboardActions";
            this.tmsRdsClipboardActions.Size = new System.Drawing.Size(169, 22);
            this.tmsRdsClipboardActions.Text = "Clipboard Actions";
            // 
            // copyEndpointAdresToolStripMenuItem
            // 
            this.copyEndpointAdresToolStripMenuItem.Name = "copyEndpointAdresToolStripMenuItem";
            this.copyEndpointAdresToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.copyEndpointAdresToolStripMenuItem.Text = "Copy Endpoint Adres...";
            this.copyEndpointAdresToolStripMenuItem.Click += new System.EventHandler(this.copyEndpointAdresToolStripMenuItem_Click);
            // 
            // tsRdsC1
            // 
            this.tsRdsC1.Name = "tsRdsC1";
            this.tsRdsC1.Size = new System.Drawing.Size(166, 6);
            // 
            // tsmRdsOperations
            // 
            this.tsmRdsOperations.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startRdsToolStripMenuItem,
            this.tsrdso1,
            this.stopRdsToolStripMenuItem,
            this.tsrdso2,
            this.restartRdsToolStripMenuItem});
            this.tsmRdsOperations.Name = "tsmRdsOperations";
            this.tsmRdsOperations.Size = new System.Drawing.Size(169, 22);
            this.tsmRdsOperations.Text = "Rds Operations";
            // 
            // startRdsToolStripMenuItem
            // 
            this.startRdsToolStripMenuItem.Name = "startRdsToolStripMenuItem";
            this.startRdsToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.startRdsToolStripMenuItem.Text = "Start Rds";
            this.startRdsToolStripMenuItem.Click += new System.EventHandler(this.startRdsToolStripMenuItem_Click);
            // 
            // tsrdso1
            // 
            this.tsrdso1.Name = "tsrdso1";
            this.tsrdso1.Size = new System.Drawing.Size(129, 6);
            this.tsrdso1.Visible = false;
            // 
            // stopRdsToolStripMenuItem
            // 
            this.stopRdsToolStripMenuItem.Name = "stopRdsToolStripMenuItem";
            this.stopRdsToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.stopRdsToolStripMenuItem.Text = "Stop Rds";
            this.stopRdsToolStripMenuItem.Visible = false;
            this.stopRdsToolStripMenuItem.Click += new System.EventHandler(this.stopRdsToolStripMenuItem_Click);
            // 
            // tsrdso2
            // 
            this.tsrdso2.Name = "tsrdso2";
            this.tsrdso2.Size = new System.Drawing.Size(129, 6);
            this.tsrdso2.Visible = false;
            // 
            // restartRdsToolStripMenuItem
            // 
            this.restartRdsToolStripMenuItem.Name = "restartRdsToolStripMenuItem";
            this.restartRdsToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.restartRdsToolStripMenuItem.Text = "Restart Rds";
            this.restartRdsToolStripMenuItem.Visible = false;
            this.restartRdsToolStripMenuItem.Click += new System.EventHandler(this.restartRdsToolStripMenuItem_Click);
            // 
            // lblEc2State
            // 
            this.lblEc2State.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEc2State.AutoSize = true;
            this.lblEc2State.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEc2State.Location = new System.Drawing.Point(21, 9);
            this.lblEc2State.Name = "lblEc2State";
            this.lblEc2State.Size = new System.Drawing.Size(144, 17);
            this.lblEc2State.TabIndex = 32;
            this.lblEc2State.Text = "EC2 Ami Age State";
            // 
            // lblRdsState
            // 
            this.lblRdsState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRdsState.AutoSize = true;
            this.lblRdsState.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRdsState.Location = new System.Drawing.Point(1135, 9);
            this.lblRdsState.Name = "lblRdsState";
            this.lblRdsState.Size = new System.Drawing.Size(83, 17);
            this.lblRdsState.TabIndex = 33;
            this.lblRdsState.Text = "RDS State";
            this.lblRdsState.Visible = false;
            // 
            // lblRdpWarning
            // 
            this.lblRdpWarning.AutoSize = true;
            this.lblRdpWarning.Location = new System.Drawing.Point(276, 9);
            this.lblRdpWarning.Name = "lblRdpWarning";
            this.lblRdpWarning.Size = new System.Drawing.Size(278, 13);
            this.lblRdpWarning.TabIndex = 34;
            this.lblRdpWarning.Text = "Let op: RDP verbinding werkt alleen op de opstap server!";
            this.lblRdpWarning.Visible = false;
            // 
            // AwsEc2RdsOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 468);
            this.Controls.Add(this.lblRdpWarning);
            this.Controls.Add(this.lblRdsState);
            this.Controls.Add(this.lblEc2State);
            this.Controls.Add(this.lvDatabases);
            this.Controls.Add(this.lvEc2);
            this.Name = "AwsEc2RdsOperator";
            this.Text = "AwsCwrAlarmStatusOperator";
            this.cmsEc2.ResumeLayout(false);
            this.cmsRds.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmRefresher;
        private System.Windows.Forms.ListView lvEc2;
        private System.Windows.Forms.ColumnHeader chAlarmName;
        private System.Windows.Forms.ColumnHeader chAlarmStatus;
        private System.Windows.Forms.ContextMenuStrip cmsEc2;
        private System.Windows.Forms.ToolStripMenuItem tmsAmiAge;
        private System.Windows.Forms.ToolStripMenuItem tmsIgnoreEnviroment;
        private System.Windows.Forms.ToolStripSeparator tss1;
        private System.Windows.Forms.ToolStripMenuItem tsmSwitch;
        private System.Windows.Forms.ToolStripMenuItem tsmSMAmiAge;
        private System.Windows.Forms.ToolStripMenuItem tsmSMEc2Operations;
        private System.Windows.Forms.ToolStripSeparator tss2;
        private System.Windows.Forms.ToolStripMenuItem tsmEc2Operations;
        private System.Windows.Forms.ToolStripMenuItem tsmStartEc2;
        private System.Windows.Forms.ToolStripMenuItem tsmStopEc2;
        private System.Windows.Forms.ToolStripSeparator tss4;
        private System.Windows.Forms.ToolStripMenuItem tsmRestartEc2;
        private System.Windows.Forms.ToolStripSeparator tss5;
        private System.Windows.Forms.ToolStripMenuItem tsmTerminateEc2;
        private System.Windows.Forms.ToolStripSeparator tss3;
        private System.Windows.Forms.ToolStripMenuItem SetIntervalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec30ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec60ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec90ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec120ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sec300ToolStripMenuItem;
        private System.Windows.Forms.ListView lvDatabases;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ContextMenuStrip cmsRds;
        private System.Windows.Forms.ToolStripMenuItem tsmRdsOperations;
        private System.Windows.Forms.ToolStripMenuItem startRdsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator tsrdso1;
        private System.Windows.Forms.ToolStripMenuItem stopRdsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator tsrdso2;
        private System.Windows.Forms.ToolStripMenuItem restartRdsToolStripMenuItem;
        private System.Windows.Forms.Label lblEc2State;
        private System.Windows.Forms.Label lblRdsState;
        private System.Windows.Forms.ToolStripSeparator tss10;
        private System.Windows.Forms.ToolStripMenuItem tmsEc2ClipboardActions;
        private System.Windows.Forms.ToolStripMenuItem tsmCopyInstanceId;
        private System.Windows.Forms.ToolStripMenuItem tsmCopyIpAdres;
        private System.Windows.Forms.ToolStripMenuItem tmsRdsClipboardActions;
        private System.Windows.Forms.ToolStripMenuItem copyEndpointAdresToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator tsRdsC1;
        private System.Windows.Forms.ToolStripMenuItem tsmRdpEc2;
        private System.Windows.Forms.ToolStripSeparator tssRdp1;
        private System.Windows.Forms.ToolStripMenuItem sec600ToolStripMenuItem;
        private System.Windows.Forms.Label lblRdpWarning;
    }
}