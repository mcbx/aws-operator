﻿namespace AwsOperator
{
    partial class AwsEc2WindowsServiceOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbServicesOperations = new System.Windows.Forms.GroupBox();
            this.gbServiceSnapshotDetails = new System.Windows.Forms.GroupBox();
            this.btnRestartServiceCK = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.btnStopServiceCK = new System.Windows.Forms.Button();
            this.txtSnapshotDateTime = new System.Windows.Forms.TextBox();
            this.cbSnapshotServiceEC2Ids = new System.Windows.Forms.ComboBox();
            this.btnStartServiceCK = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lvServicesOnInstance = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbTagInstanceId = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbPurposeIdTag = new System.Windows.Forms.ComboBox();
            this.btnRefreshTags = new System.Windows.Forms.Button();
            this.btnRefreshServiceSnapshot = new System.Windows.Forms.Button();
            this.gbServicesOperations.SuspendLayout();
            this.gbServiceSnapshotDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbServicesOperations
            // 
            this.gbServicesOperations.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbServicesOperations.Controls.Add(this.gbServiceSnapshotDetails);
            this.gbServicesOperations.Controls.Add(this.lbTagInstanceId);
            this.gbServicesOperations.Controls.Add(this.label12);
            this.gbServicesOperations.Controls.Add(this.label11);
            this.gbServicesOperations.Controls.Add(this.cbPurposeIdTag);
            this.gbServicesOperations.Controls.Add(this.btnRefreshTags);
            this.gbServicesOperations.Controls.Add(this.btnRefreshServiceSnapshot);
            this.gbServicesOperations.Location = new System.Drawing.Point(12, 12);
            this.gbServicesOperations.Name = "gbServicesOperations";
            this.gbServicesOperations.Size = new System.Drawing.Size(622, 697);
            this.gbServicesOperations.TabIndex = 20;
            this.gbServicesOperations.TabStop = false;
            this.gbServicesOperations.Text = "Window Services";
            // 
            // gbServiceSnapshotDetails
            // 
            this.gbServiceSnapshotDetails.Controls.Add(this.btnRestartServiceCK);
            this.gbServiceSnapshotDetails.Controls.Add(this.label15);
            this.gbServiceSnapshotDetails.Controls.Add(this.btnStopServiceCK);
            this.gbServiceSnapshotDetails.Controls.Add(this.txtSnapshotDateTime);
            this.gbServiceSnapshotDetails.Controls.Add(this.cbSnapshotServiceEC2Ids);
            this.gbServiceSnapshotDetails.Controls.Add(this.btnStartServiceCK);
            this.gbServiceSnapshotDetails.Controls.Add(this.label13);
            this.gbServiceSnapshotDetails.Controls.Add(this.lvServicesOnInstance);
            this.gbServiceSnapshotDetails.Location = new System.Drawing.Point(6, 220);
            this.gbServiceSnapshotDetails.Name = "gbServiceSnapshotDetails";
            this.gbServiceSnapshotDetails.Size = new System.Drawing.Size(610, 471);
            this.gbServiceSnapshotDetails.TabIndex = 20;
            this.gbServiceSnapshotDetails.TabStop = false;
            this.gbServiceSnapshotDetails.Text = "EC2 Service snapshot details";
            // 
            // btnRestartServiceCK
            // 
            this.btnRestartServiceCK.Location = new System.Drawing.Point(6, 434);
            this.btnRestartServiceCK.Name = "btnRestartServiceCK";
            this.btnRestartServiceCK.Size = new System.Drawing.Size(152, 23);
            this.btnRestartServiceCK.TabIndex = 30;
            this.btnRestartServiceCK.Text = "Restart service(s)";
            this.btnRestartServiceCK.UseVisualStyleBackColor = true;
            this.btnRestartServiceCK.Click += new System.EventHandler(this.btnRestartServiceCK_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(117, 13);
            this.label15.TabIndex = 27;
            this.label15.Text = "Snapshot gemaakt op: ";
            // 
            // btnStopServiceCK
            // 
            this.btnStopServiceCK.Location = new System.Drawing.Point(6, 405);
            this.btnStopServiceCK.Name = "btnStopServiceCK";
            this.btnStopServiceCK.Size = new System.Drawing.Size(152, 23);
            this.btnStopServiceCK.TabIndex = 29;
            this.btnStopServiceCK.Text = "Stop service(s)";
            this.btnStopServiceCK.UseVisualStyleBackColor = true;
            this.btnStopServiceCK.Click += new System.EventHandler(this.btnStopServiceCK_Click);
            // 
            // txtSnapshotDateTime
            // 
            this.txtSnapshotDateTime.Location = new System.Drawing.Point(165, 41);
            this.txtSnapshotDateTime.Name = "txtSnapshotDateTime";
            this.txtSnapshotDateTime.ReadOnly = true;
            this.txtSnapshotDateTime.Size = new System.Drawing.Size(439, 20);
            this.txtSnapshotDateTime.TabIndex = 26;
            // 
            // cbSnapshotServiceEC2Ids
            // 
            this.cbSnapshotServiceEC2Ids.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSnapshotServiceEC2Ids.FormattingEnabled = true;
            this.cbSnapshotServiceEC2Ids.Location = new System.Drawing.Point(165, 85);
            this.cbSnapshotServiceEC2Ids.Name = "cbSnapshotServiceEC2Ids";
            this.cbSnapshotServiceEC2Ids.Size = new System.Drawing.Size(439, 21);
            this.cbSnapshotServiceEC2Ids.TabIndex = 20;
            this.cbSnapshotServiceEC2Ids.TextChanged += new System.EventHandler(this.cbSnapshotServiceEC2Ids_TextChanged);
            // 
            // btnStartServiceCK
            // 
            this.btnStartServiceCK.Location = new System.Drawing.Point(6, 376);
            this.btnStartServiceCK.Name = "btnStartServiceCK";
            this.btnStartServiceCK.Size = new System.Drawing.Size(153, 23);
            this.btnStartServiceCK.TabIndex = 28;
            this.btnStartServiceCK.Text = "Start service(s)";
            this.btnStartServiceCK.UseVisualStyleBackColor = true;
            this.btnStartServiceCK.Click += new System.EventHandler(this.btnStartServiceCK_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "EC2 instance:";
            // 
            // lvServicesOnInstance
            // 
            this.lvServicesOnInstance.CheckBoxes = true;
            this.lvServicesOnInstance.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvServicesOnInstance.FullRowSelect = true;
            this.lvServicesOnInstance.Location = new System.Drawing.Point(165, 115);
            this.lvServicesOnInstance.Name = "lvServicesOnInstance";
            this.lvServicesOnInstance.Size = new System.Drawing.Size(439, 350);
            this.lvServicesOnInstance.TabIndex = 27;
            this.lvServicesOnInstance.UseCompatibleStateImageBehavior = false;
            this.lvServicesOnInstance.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Service";
            this.columnHeader1.Width = 158;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "StartType";
            this.columnHeader2.Width = 160;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            this.columnHeader3.Width = 111;
            // 
            // lbTagInstanceId
            // 
            this.lbTagInstanceId.FormattingEnabled = true;
            this.lbTagInstanceId.Location = new System.Drawing.Point(171, 54);
            this.lbTagInstanceId.Name = "lbTagInstanceId";
            this.lbTagInstanceId.Size = new System.Drawing.Size(445, 95);
            this.lbTagInstanceId.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Instances:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "PurposeId Tag:";
            // 
            // cbPurposeIdTag
            // 
            this.cbPurposeIdTag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPurposeIdTag.FormattingEnabled = true;
            this.cbPurposeIdTag.Location = new System.Drawing.Point(171, 19);
            this.cbPurposeIdTag.Name = "cbPurposeIdTag";
            this.cbPurposeIdTag.Size = new System.Drawing.Size(445, 21);
            this.cbPurposeIdTag.TabIndex = 16;
            this.cbPurposeIdTag.TextChanged += new System.EventHandler(this.cbPurposeIdTag_TextChanged);
            // 
            // btnRefreshTags
            // 
            this.btnRefreshTags.Location = new System.Drawing.Point(171, 162);
            this.btnRefreshTags.Name = "btnRefreshTags";
            this.btnRefreshTags.Size = new System.Drawing.Size(445, 23);
            this.btnRefreshTags.TabIndex = 4;
            this.btnRefreshTags.Text = "Refresh available EC2";
            this.btnRefreshTags.UseVisualStyleBackColor = true;
            this.btnRefreshTags.Click += new System.EventHandler(this.btnRefreshTags_Click);
            // 
            // btnRefreshServiceSnapshot
            // 
            this.btnRefreshServiceSnapshot.Location = new System.Drawing.Point(171, 191);
            this.btnRefreshServiceSnapshot.Name = "btnRefreshServiceSnapshot";
            this.btnRefreshServiceSnapshot.Size = new System.Drawing.Size(445, 23);
            this.btnRefreshServiceSnapshot.TabIndex = 5;
            this.btnRefreshServiceSnapshot.Text = "Refresh Service Snapshot";
            this.btnRefreshServiceSnapshot.UseVisualStyleBackColor = true;
            this.btnRefreshServiceSnapshot.Click += new System.EventHandler(this.btnRefreshServiceSnapshot_Click);
            // 
            // AwsEc2WindowsServiceOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 723);
            this.Controls.Add(this.gbServicesOperations);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(663, 762);
            this.Name = "AwsEc2WindowsServiceOperator";
            this.Text = "AwsEc2WindowsServiceOperator";
            this.gbServicesOperations.ResumeLayout(false);
            this.gbServicesOperations.PerformLayout();
            this.gbServiceSnapshotDetails.ResumeLayout(false);
            this.gbServiceSnapshotDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbServicesOperations;
        private System.Windows.Forms.GroupBox gbServiceSnapshotDetails;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSnapshotDateTime;
        private System.Windows.Forms.ComboBox cbSnapshotServiceEC2Ids;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbPurposeIdTag;
        private System.Windows.Forms.ListBox lbTagInstanceId;
        private System.Windows.Forms.Button btnRefreshTags;
        private System.Windows.Forms.Button btnRefreshServiceSnapshot;
        private System.Windows.Forms.ListView lvServicesOnInstance;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnStartServiceCK;
        private System.Windows.Forms.Button btnStopServiceCK;
        private System.Windows.Forms.Button btnRestartServiceCK;
    }
}