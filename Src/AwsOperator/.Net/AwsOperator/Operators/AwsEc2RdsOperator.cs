﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class AwsEc2RdsOperator : FormTemplate
    {
        #region Attributes

        private int _currentInterval = 120;
        private List<InstanceDetail> _currentEc2Status = new List<InstanceDetail>();
        private List<InstanceDetail> _currenAsgStatus = new List<InstanceDetail>();
        private List<RdsDetail> _currentRdsStatus = new List<RdsDetail>();

        private EAWSLoginStatus LoginStatus { get; set; } = EAWSLoginStatus.Success;

        private bool rdpLoginKeyExists { get; set; } = true;

        #endregion

        #region Constructors

        public AwsEc2RdsOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner, bool vatMode) : base(appCode, domein, environment, owner, vatMode)
        {
            InitializeComponent();
            lvEc2.GetType()
                .GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(lvEc2, true);
            lvDatabases.GetType()
                .GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(lvDatabases, true);

            tmRefresher.Interval = 24 * 60 * 60 * 1000;
            Text = $"Operator: EC2/RDS, {(_formOverride ? "(V.A.T.)" : "")} AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}, Refresh interval: 24h";
            
            tmRefresher.Enabled = true;
            lvEc2.ContextMenuStrip = cmsEc2;
            lvDatabases.ContextMenuStrip = cmsRds;

            sec120ToolStripMenuItem.Checked = true;
            tss1.Visible = true;
            tsmSwitch.Visible = true;
            RefreshData();

            var rdpLogin = AWSParameterStore.GetEc2Credentials(_domein, _environment);
            if (string.IsNullOrEmpty(rdpLogin.Name) || string.IsNullOrEmpty(rdpLogin.Key))
            {
                rdpLoginKeyExists = false;
                lblRdpWarning.Visible = false;
            }
        }

        #endregion

        #region Events FormTemplate

        internal override void FormTemplate_ChangedLoginStatus(object sender, MainForm.LoginStatusEvent e)
        {
            if (e.LoginStatus == EAWSLoginStatus.Success)
                tmRefresher.Start();
            else
                tmRefresher.Stop();

            tmRefresher.Enabled = (e.LoginStatus == EAWSLoginStatus.Success);
            LoginStatus = e.LoginStatus;

            base.FormTemplate_ChangedLoginStatus(sender, e);
        }

        #endregion

        #region UpdateListViewItem

        private void UpdateListViewItemRds(RdsDetail rdsItem)
        {
            ListViewItem lItem = new ListViewItem()
            {
                Name = rdsItem.Name,
                UseItemStyleForSubItems = false
            };

            Color activeColor;
            if (!tsmSMEc2Operations.Checked)
                return;

            lItem.Text = rdsItem.Name;
            lItem.ToolTipText = $"Name:\t\t{rdsItem.Name}{Environment.NewLine}Type:\t\t{rdsItem.RdsType}{Environment.NewLine}Engine:\t\t{rdsItem.Engine}{Environment.NewLine}Region:\t\t{rdsItem.Region}{Environment.NewLine}Endpooint:\t{rdsItem.Endpoint}:{rdsItem.EndpointPort}{Environment.NewLine}{Environment.NewLine}STATUS:\t{rdsItem.Status.ToUpperInvariant()}";

            switch (rdsItem.Status.ToLower())
            {
                case "available":
                    activeColor = Color.LightGreen;
                    break;
                case "stopped":
                case "stopping":
                    activeColor = Color.FromArgb(255, 99, 71);
                    break;
                case "failed":
                    //text tooltip??
                    activeColor = Color.FromArgb(255, 99, 71);
                    break;
                case "deleting":
                    //text tooltip??
                    activeColor = Color.FromArgb(255, 99, 71);
                    break;
                case "rebooting":
                case "creating":
                    activeColor = Color.FromArgb(250, 194, 87);
                    break;
                case "starting":
                    activeColor = Color.LightBlue;
                    break;
                default:
                    activeColor = Color.White;
                    break;
            }

            lItem.BackColor = activeColor;
            lvDatabases.BeginInvoke(new MethodInvoker(() => {
                lvDatabases.Items.Add(lItem);
            }));
        }

        private void UpdateListViewItem(InstanceDetail ec2Item)
        {
            ListViewItem lItem = new ListViewItem()
            {
                Name = ec2Item.InstanceId,
                UseItemStyleForSubItems = false
            };

            Color activeColor;
            if (tsmSMEc2Operations.Checked)
            {
                #region Ec2 Operations

                if (ec2Item.InstanceType == "LaunchConfiguration")
                    return;

                lItem.Text = $"{(string.IsNullOrEmpty(ec2Item.Name) ? ec2Item.InstanceId : ec2Item.Name)}";
                lItem.ToolTipText = $"Instance:\t {ec2Item.InstanceId}{Environment.NewLine}Ip:\t\t{ec2Item.NetworkAdres}{Environment.NewLine}{Environment.NewLine}STATUS:\t{ec2Item.State.ToString().ToUpperInvariant()}";

                switch (ec2Item.State)
                {
                    case InstanceState.Running:
                        activeColor = Color.LightGreen;
                        break;
                    case InstanceState.Stopped:
                        activeColor = Color.FromArgb(255, 99, 71);
                        break;
                    case InstanceState.ShuttingDown:
                    case InstanceState.Stopping:
                        activeColor = Color.FromArgb(250, 194, 87);
                        break;
                    case InstanceState.Terminated:
                        activeColor = Color.LightGray;
                        break;
                    case InstanceState.Pending:
                        activeColor = Color.LightBlue;
                        break;
                    default:
                        activeColor = Color.White;
                        break;
                }

                #endregion
            }
            else
            {
                #region AMI Age refresh

                double amiAge = -1;
                if (ec2Item.AmiDetail != null && ec2Item.AmiDetail.CreationDateTime != DateTime.MinValue)
                    amiAge = Math.Round((DateTime.Now - ec2Item.AmiDetail.CreationDateTime).TotalDays);

                lItem.Text = $"{(string.IsNullOrEmpty(ec2Item.Name) ? ec2Item.InstanceId : ec2Item.Name)}{(tmsAmiAge.Checked ? $" ({amiAge})" : "")}";
                if (lItem.Text.Contains("-LaunchConfigurationEC2-"))
                    lItem.Text = $"{(lItem.Text.Split(new[] { "-LaunchConfigurationEC2-" }, StringSplitOptions.None)[0])} (LC){(tmsAmiAge.Checked ? $" ({amiAge})" : "")}";
                else
                    if (lItem.Text.Contains("-lc-"))
                        lItem.Text = $"{(lItem.Text.Split(new[] { "-lc-" }, StringSplitOptions.None)[0])} -lc (LC){(tmsAmiAge.Checked ? $" ({amiAge})" : "")}";

                switch (amiAge >= 0 && amiAge <= 30 ? "green" :
                    amiAge >= 30 && amiAge < 90 ? "orange" :
                    amiAge >= 90 ? "red" : "unknown")
                {
                    case "green":
                        activeColor = Color.LightGreen;
                        break;
                    case "red":
                        activeColor = Color.FromArgb(255, 99, 71);
                        break;
                    case "orange":
                        activeColor = Color.FromArgb(250, 194, 87);
                        break;
                    default:
                        activeColor = Color.LightGray;
                        lItem.Text = $"{(string.IsNullOrEmpty(ec2Item.Name) ? ec2Item.InstanceId : ec2Item.Name)}";

                        if (lItem.Text.Contains("-LaunchConfigurationEC2-"))
                            lItem.Text = $"{(lItem.Text.Split(new[] { "-LaunchConfigurationEC2-" }, StringSplitOptions.None)[0])} (LC)";

                        if (lItem.Text.Contains("-lc-"))
                            lItem.Text = $"{(lItem.Text.Split(new[] { "-lc-" }, StringSplitOptions.None)[0])} -lc (LC)";


                        break;
                }

                if (ec2Item.InstanceType != "LaunchConfiguration")
                    lItem.ToolTipText = $"Instance:\t {ec2Item.InstanceId}{Environment.NewLine}Ip:\t\t{ec2Item.NetworkAdres}{Environment.NewLine}AMI:\t\t{ec2Item.Ami}{Environment.NewLine}Age:\t\t{(amiAge == -1 ? "EXPIRED" : amiAge.ToString())}";
                else
                    lItem.ToolTipText = $"LaunchConfig:\t{ec2Item.Name}{Environment.NewLine}AMI:\t\t\t{ec2Item.Ami}{Environment.NewLine}Age:\t\t\t{(amiAge == -1 ? "EXPIRED" : amiAge.ToString())}";

                #endregion
            }

            lItem.BackColor = activeColor;
            lvEc2.BeginInvoke(new MethodInvoker(() => {
                lvEc2.Items.Add(lItem);
            }));
        }

        #endregion

        #region Refresh

        private void RefreshRdsData()
        {
            lvDatabases.Clear();
            Task.Run(() =>
            {
                _currentRdsStatus = AWSRds.GetRdsInstances(_domein, _environment).Values.OrderBy(item => item.Name).ToList();
                _currentRdsStatus.ForEach(item => UpdateListViewItemRds(item));
            });
        }

        private void RefreshEc2Data()
        {
            lvEc2.Clear();
            var previousState = lvEc2.Enabled;
            lvEc2.Enabled = true;

            Task.Run(() =>
            {
                _currentEc2Status = AWSEc2.GetEc2List(_domein, _environment, true, tmsIgnoreEnviroment.Checked).Values.ToList();
                _currentEc2Status.OrderBy(item => item.Name).ToList().ForEach(item => UpdateListViewItem(item));
                if (tsmSMAmiAge.Checked)
                {
                    _currenAsgStatus = AWSEc2.GetAmiDetailsFromLaunchConfiguration(_domein, _environment, tmsIgnoreEnviroment.Checked).Select(item => new InstanceDetail() { Name = item.Key, AmiDetail = item.Value, Ami = item.Value.AmiId, InstanceType = "LaunchConfiguration", InstanceId = "" }).ToList();
                    _currenAsgStatus.OrderBy(item => item.Name).ToList().ForEach(item => UpdateListViewItem(item));
                }
            });
        }

        private void RefreshData()
        {
            if (LoginStatus != EAWSLoginStatus.Success)
                return;

            if (tsmSMEc2Operations.Checked)
                RefreshRdsData();

            RefreshEc2Data();
        }

        private void tmRefresher_Tick(object sender, EventArgs e)
        {
            RefreshData();
        }

        #endregion

        #region Listview Draw

        private void lvAlarms_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawBackground();

            if (e.Item.Selected && lvEc2.Focused)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.Cyan), e.Bounds);
                e.DrawFocusRectangle();
            }

            e.Graphics.DrawString(e.Item.Text, new Font("Arial", 8), new SolidBrush(Color.Black), e.Bounds);
        }

        private void lvDatabases_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawBackground();
            
            if (e.Item.Selected && lvDatabases.Focused)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.Cyan), e.Bounds);
                e.DrawFocusRectangle();
            }

            e.Graphics.DrawString(e.Item.Text, new Font("Arial", 8), new SolidBrush(Color.Black), e.Bounds);
        }

        #endregion

        #region Interval Settings

        private void ChangeRefreshInterval(int newInterval)
        {
            switch (_currentInterval)
            {
                case 30:
                    sec30ToolStripMenuItem.Checked = false;
                    break;
                case 60:
                    sec60ToolStripMenuItem.Checked = false;
                    break;
                case 90:
                    sec90ToolStripMenuItem.Checked = false;
                    break;
                case 120:
                    sec120ToolStripMenuItem.Checked = false;
                    break;
                case 300:
                    sec300ToolStripMenuItem.Checked = false;
                    break;
                case 600:
                    sec300ToolStripMenuItem.Checked = false;
                    break;
                default: break;
            }

            _currentInterval = newInterval;
            tmRefresher.Interval = _currentInterval * 1000;
            
            Text = $"Operator: EC2/RDS, {(_formOverride ? "(V.A.T.)" : "")} AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}, Refresh interval: {_currentInterval}s";
        }

        private void sec30ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(30);
        }

        private void sec60ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(60);
        }

        private void sec90ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(90);
        }

        private void sec120ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(120);
        }

        private void sec300ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(300);
        }

        private void sec600ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeRefreshInterval(600);
        }

        #endregion

        #region ContextMenu logic

        private void lvAlarms_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;

            try
            {
                Point localPoint = lvEc2.PointToClient(Cursor.Position);
                if (lvEc2.GetItemAt(localPoint.X, localPoint.Y) == null)
                {
                    tss2.Visible = false;
                    tsmEc2Operations.Visible = false;

                    tss10.Visible = false;
                    tmsEc2ClipboardActions.Visible = false;
                    return;
                }

                if (!tsmSMEc2Operations.Checked)
                    return;

                if (!SetIntervalToolStripMenuItem.Visible)
                {
                    tss3.Visible = true;
                    SetIntervalToolStripMenuItem.Visible = true;
                }

                tss2.Visible = true;
                tsmEc2Operations.Visible = true;

                if (_formOverride)
                {
                    tsmStopEc2.Visible = true;
                    tsmRestartEc2.Visible = true;
                    tsmTerminateEc2.Visible = true;
                    tss4.Visible = true;
                    tss5.Visible = true;
                }

                lvEc2.FocusedItem = lvEc2.GetItemAt(localPoint.X, localPoint.Y);
                lvEc2.FocusedItem.Selected = true;
                if (lvEc2.FocusedItem.Text.Contains("(LC)"))
                {
                    tss10.Visible = false;
                    tmsEc2ClipboardActions.Visible = false;
                    return;
                }

                var selEc2Item = _currentEc2Status.Where(item => item.InstanceId == lvEc2.FocusedItem.Name).FirstOrDefault();
                if (
                    //(_formOverride || _environment == EAWSEnvironment.Dev) &&
                        selEc2Item != null 
                        && selEc2Item.State == InstanceState.Running 
                        && !string.IsNullOrEmpty(selEc2Item.NetworkAdres) 
                        && !lvEc2.FocusedItem.Text.Contains("(LC)")
                        && rdpLoginKeyExists)
                {
                    tsmRdpEc2.Visible = true;
                    tssRdp1.Visible = true;
                }
                else
                {
                    tsmRdpEc2.Visible = false;
                    tssRdp1.Visible = false;
                }

                tss10.Visible = true;
                tmsEc2ClipboardActions.Visible = true;

                //here we can check if we want to disable things based on the status
            }
            finally
            {
                cmsEc2.Show(Cursor.Position);
            }
        }

        private void lvDatabases_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;

            try
            {
                Point localPoint = lvDatabases.PointToClient(Cursor.Position);
                if (lvDatabases.GetItemAt(localPoint.X, localPoint.Y) == null)
                {
                    tss2.Visible = false;
                    tsmRdsOperations.Visible = false;

                    tmsRdsClipboardActions.Visible = false;
                    tsRdsC1.Visible = false;
                    return;
                }

                if (!SetIntervalToolStripMenuItem.Visible)
                {
                    tss3.Visible = true;
                    SetIntervalToolStripMenuItem.Visible = true;
                }

                tss2.Visible = true;
                tsmRdsOperations.Visible = true;
                tmsRdsClipboardActions.Visible = true;
                tsRdsC1.Visible =true;

                if (_formOverride)
                {
                    tsrdso1.Visible = true;
                    stopRdsToolStripMenuItem.Visible = true;
                    tsrdso2.Visible = true;
                    restartRdsToolStripMenuItem.Visible = true;
                }

                lvDatabases.FocusedItem = lvDatabases.GetItemAt(localPoint.X, localPoint.Y);
                lvDatabases.FocusedItem.Selected = true;

                //here we can check if we want to disable things based on the status
            }
            finally
            {
                cmsRds.Items.Insert(0, tmsIgnoreEnviroment);
                cmsRds.Items.Insert(1, tss1);
                cmsRds.Items.Insert(4, tsmSwitch);
                cmsRds.Items.Insert(5, tss2);
                cmsRds.Items.Insert(7, tss3);
                cmsRds.Items.Insert(8, SetIntervalToolStripMenuItem);
                cmsRds.Show(Cursor.Position);

                tss2.Visible = tsmRdsOperations.Visible;
            }
        }

        private void cmsRds_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            cmsEc2.Items.Insert(1, tmsIgnoreEnviroment);
            cmsEc2.Items.Insert(4, tss1);
            cmsEc2.Items.Insert(5, tsmSwitch);
            cmsEc2.Items.Insert(6, tss2);
            cmsEc2.Items.Insert(8, tss3);
            cmsEc2.Items.Insert(9, SetIntervalToolStripMenuItem);
        }

        #endregion

        private void tsmSMAmiAge_Click(object sender, EventArgs e)
        {
            if (tsmSMAmiAge.Checked)
                return;

            tmRefresher.Enabled = false;

            lvDatabases.Clear();
            lvDatabases.Visible = false;
            lvEc2.Width = lvEc2.Width + (245 + 12);

            tsmSMAmiAge.Checked = true;
            tsmSMEc2Operations.Checked = false;

            tmsAmiAge.Enabled = true;
            SetIntervalToolStripMenuItem.Visible = false;
            tss3.Visible = false;

            tsmEc2Operations.Enabled = false;
            tsmEc2Operations.Visible = false;
            lblEc2State.Text = "EC2 Ami Age State";
            lblRdsState.Visible = false;

            tmRefresher.Interval = 24 * 60 * 60 * 1000;
            RefreshEc2Data();
            tmRefresher.Enabled = true;
            lblRdpWarning.Visible = false;
            Text = $"Operator: EC2/RDS, {(_formOverride ? "(V.A.T.)" : "")} AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}, Refresh interval: 24h";
        }

        private void tsmSMEc2Operations_Click(object sender, EventArgs e)
        {
            if (tsmSMEc2Operations.Checked)
                return;

            tmRefresher.Enabled = false;

            lvDatabases.Clear();
            lvDatabases.Visible = true;
            lvEc2.Width = lvEc2.Width - (245 + 12);

            tsmSMEc2Operations.Checked = true;
            tsmSMAmiAge.Checked = false;

            tmsAmiAge.Enabled = false;
            SetIntervalToolStripMenuItem.Visible = true;
            tss3.Visible = true;

            tsmEc2Operations.Visible = true;
            tsmEc2Operations.Enabled = true;

            lblEc2State.Text = "EC2 State";
            lblRdsState.Visible = true;

            tmRefresher.Interval = _currentInterval * 1000;
            RefreshData();
            tmRefresher.Enabled = true;
            lblRdpWarning.Visible = (_environment != EAWSEnvironment.Dev && rdpLoginKeyExists);
            Text = $"Operator: EC2/RDS, {(_formOverride ? "(V.A.T.)" : "")} AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}, Refresh interval: {_currentInterval}s";
        }

        private void tmsAmiAge_Click(object sender, EventArgs e)
        {
            tmsAmiAge.Checked = !tmsAmiAge.Checked;

            lvEc2.Clear();
            _currentEc2Status.OrderBy(item => item.Name).ToList().ForEach(item => UpdateListViewItem(item));
            if (tsmSMAmiAge.Checked)
                _currenAsgStatus.OrderBy(item => item.Name).ToList().ForEach(item => UpdateListViewItem(item));
        }

        private void tmsIgnoreEnviroment_Click(object sender, EventArgs e)
        {
            tmsIgnoreEnviroment.Checked = !tmsIgnoreEnviroment.Checked;
            RefreshData();
        }

        #region EC2 Operations 

        private void InitiateRdpSession(string ipAddress)
        {
            const string fileName = "rdpConnection.rdp";
            const string rdpTemplate = @"auto connect:i:1
full address:s:{0}
username:s:{1}
promptcredentialonce:i:0";

            try
            {
                var result = AWSParameterStore.GetEc2Credentials(_domein, _environment);
                Clipboard.SetText(result.Key);

                using (StreamWriter fileWriter = new StreamWriter(fileName, false, Encoding.UTF8, 5120))
                {
                    fileWriter.WriteLine(string.Format(rdpTemplate, ipAddress, result.Name));
                }

                Process rdcProcess = new Process();
                rdcProcess.StartInfo.FileName = fileName;
                rdcProcess.Start();
                Thread.Sleep(2000);
            }
            finally
            {
                if (File.Exists(fileName))
                    File.Delete(fileName);
            }
        }

        private ListViewItem GetEc2SelectedItem()
        {
            if (lvEc2.SelectedItems.Count == 0)
                return null;

            var ec2 = _currentEc2Status.Where(item => item.InstanceId == lvEc2.SelectedItems[0].Name).FirstOrDefault();
            if (ec2 == null)
                return null;

            return lvEc2.SelectedItems[0];
        }

        private void tsmStartEc2_Click(object sender, EventArgs e)
        {
            if (GetEc2SelectedItem() == null)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to START Ec2 '{lvEc2.SelectedItems[0].Text}' ({lvEc2.SelectedItems[0].Name})?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            AWSEc2.StartInstance(_domein, _environment, lvEc2.SelectedItems[0].Name);
            RefreshEc2Data();
        }

        private void tsmStopEc2_Click(object sender, EventArgs e)
        {
            if (GetEc2SelectedItem() == null)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to STOP Ec2 '{lvEc2.SelectedItems[0].Text}' ({lvEc2.SelectedItems[0].Name})?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            AWSEc2.StopInstance(_domein, _environment, lvEc2.SelectedItems[0].Name);
            RefreshEc2Data();
        }

        private void tsmRestartEc2_Click(object sender, EventArgs e)
        {
            if (GetEc2SelectedItem() == null)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to RESTART Ec2 '{lvEc2.SelectedItems[0].Text}' ({lvEc2.SelectedItems[0].Name})?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            AWSEc2.RestartInstance(_domein, _environment, lvEc2.SelectedItems[0].Name);
            RefreshEc2Data();
        }

        private void tsmTerminateEc2_Click(object sender, EventArgs e)
        {
            if (GetEc2SelectedItem() == null)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to TERMINATE Ec2 '{lvEc2.SelectedItems[0].Text}' ({lvEc2.SelectedItems[0].Name})?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            AWSEc2.TerminateInstance(_domein, _environment, lvEc2.SelectedItems[0].Name);
            RefreshEc2Data();
        }

        #endregion

        #region Rds Operations 

        private ListViewItem GetRdsSelectedItem()
        {
            if (lvDatabases.SelectedItems.Count == 0)
                return null;

            var rds = _currentRdsStatus.Where(item => item.Name == lvDatabases.SelectedItems[0].Name).FirstOrDefault();
            if (rds == null)
                return null;

            return lvDatabases.SelectedItems[0];
        }

        private void startRdsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GetRdsSelectedItem() == null)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to START Rds '{lvDatabases.SelectedItems[0].Name}'?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            AWSRds.StartRdsInstance(_domein, _environment, lvDatabases.SelectedItems[0].Name);
            RefreshRdsData();
        }

        private void stopRdsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GetRdsSelectedItem() == null)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to STOP Rds '{lvDatabases.SelectedItems[0].Name}'?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            AWSRds.StopRdsInstance(_domein, _environment, lvDatabases.SelectedItems[0].Name);
            RefreshRdsData();
        }

        private void restartRdsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GetRdsSelectedItem() == null)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to RESTART Rds '{lvDatabases.SelectedItems[0].Name}'?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            AWSRds.RebootRdsInstance(_domein, _environment, lvDatabases.SelectedItems[0].Name);
            RefreshRdsData();
        }

        #endregion

        #region Copy to Clipboard

        private void tsmCopyInstanceId_Click(object sender, EventArgs e)
        {
            var selItem = GetEc2SelectedItem();
            if (selItem == null)
                return;

            var selEc2Item = _currentEc2Status.Where(item => item.InstanceId == lvEc2.SelectedItems[0].Name).FirstOrDefault();
            Clipboard.SetText(selEc2Item.InstanceId);
        }

        private void tsmCopyIpAdres_Click(object sender, EventArgs e)
        {
            var selItem = GetEc2SelectedItem();
            if (selItem == null)
                return;

            var selEc2Item = _currentEc2Status.Where(item => item.InstanceId == selItem.Name).FirstOrDefault();
            Clipboard.SetText(selEc2Item.NetworkAdres);
        }

        private void copyEndpointAdresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selItem = GetRdsSelectedItem();
            if (selItem == null)
                return;

            var selRdsItem = _currentRdsStatus.Where(item => item.Name == selItem.Name).FirstOrDefault();
            Clipboard.SetText($"{selRdsItem.Endpoint},{selRdsItem.EndpointPort}");
        }

        #endregion

        private void tsmRdpEc2_Click(object sender, EventArgs e)
        {
            if (GetEc2SelectedItem() == null)
                return;

            var selEc2Item = _currentEc2Status.Where(item => item.InstanceId == lvEc2.SelectedItems[0].Name).FirstOrDefault();
            if (selEc2Item == null || selEc2Item.State != InstanceState.Running || string.IsNullOrEmpty(selEc2Item.NetworkAdres))
                return;

            InitiateRdpSession(selEc2Item.NetworkAdres);
        }
    }
}
