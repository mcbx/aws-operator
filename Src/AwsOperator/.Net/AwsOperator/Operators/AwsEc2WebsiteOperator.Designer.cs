﻿namespace AwsOperator.Operators
{
    partial class AwsEc2WebsiteOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbEc2WebNames = new System.Windows.Forms.ComboBox();
            this.btnRefreshWebSnapshot = new System.Windows.Forms.Button();
            this.btnRestartWebsite = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.btnStopWebsiteCK = new System.Windows.Forms.Button();
            this.txtSnapshotDateTime = new System.Windows.Forms.TextBox();
            this.cbSnapshotWebItemEC2Ids = new System.Windows.Forms.ComboBox();
            this.btnStartWebsiteK = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lbInstanceId = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lvWebItemsOnInstance = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbServiceSnapshotDetails = new System.Windows.Forms.GroupBox();
            this.txtIISState = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbIIs = new System.Windows.Forms.GroupBox();
            this.btnStartIIS = new System.Windows.Forms.Button();
            this.btnStopIIS = new System.Windows.Forms.Button();
            this.btnIISRestart = new System.Windows.Forms.Button();
            this.gbWebsite = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRestartAppPool = new System.Windows.Forms.Button();
            this.btnStartAppPool = new System.Windows.Forms.Button();
            this.btnStopAppPool = new System.Windows.Forms.Button();
            this.btnRefreshEc2Websites = new System.Windows.Forms.Button();
            this.gbServicesOperations = new System.Windows.Forms.GroupBox();
            this.gbServiceSnapshotDetails.SuspendLayout();
            this.gbIIs.SuspendLayout();
            this.gbWebsite.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbServicesOperations.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbEc2WebNames
            // 
            this.cbEc2WebNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEc2WebNames.FormattingEnabled = true;
            this.cbEc2WebNames.Location = new System.Drawing.Point(186, 29);
            this.cbEc2WebNames.Name = "cbEc2WebNames";
            this.cbEc2WebNames.Size = new System.Drawing.Size(642, 21);
            this.cbEc2WebNames.TabIndex = 16;
            this.cbEc2WebNames.TextChanged += new System.EventHandler(this.CbEc2WebNames_TextChanged);
            // 
            // btnRefreshWebSnapshot
            // 
            this.btnRefreshWebSnapshot.Location = new System.Drawing.Point(186, 191);
            this.btnRefreshWebSnapshot.Name = "btnRefreshWebSnapshot";
            this.btnRefreshWebSnapshot.Size = new System.Drawing.Size(642, 23);
            this.btnRefreshWebSnapshot.TabIndex = 5;
            this.btnRefreshWebSnapshot.Text = "Refresh Website Snapshot";
            this.btnRefreshWebSnapshot.UseVisualStyleBackColor = true;
            this.btnRefreshWebSnapshot.Click += new System.EventHandler(this.BtnRefreshWebSnapshot_Click);
            // 
            // btnRestartWebsite
            // 
            this.btnRestartWebsite.Location = new System.Drawing.Point(6, 90);
            this.btnRestartWebsite.Name = "btnRestartWebsite";
            this.btnRestartWebsite.Size = new System.Drawing.Size(152, 23);
            this.btnRestartWebsite.TabIndex = 30;
            this.btnRestartWebsite.Text = "Restart website(s)";
            this.btnRestartWebsite.UseVisualStyleBackColor = true;
            this.btnRestartWebsite.Click += new System.EventHandler(this.btnRestartWebsite_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(117, 13);
            this.label15.TabIndex = 27;
            this.label15.Text = "Snapshot gemaakt op: ";
            // 
            // btnStopWebsiteCK
            // 
            this.btnStopWebsiteCK.Location = new System.Drawing.Point(6, 61);
            this.btnStopWebsiteCK.Name = "btnStopWebsiteCK";
            this.btnStopWebsiteCK.Size = new System.Drawing.Size(152, 23);
            this.btnStopWebsiteCK.TabIndex = 29;
            this.btnStopWebsiteCK.Text = "Stop website(s)";
            this.btnStopWebsiteCK.UseVisualStyleBackColor = true;
            this.btnStopWebsiteCK.Click += new System.EventHandler(this.BtnStopWebsiteCK_Click);
            // 
            // txtSnapshotDateTime
            // 
            this.txtSnapshotDateTime.Location = new System.Drawing.Point(180, 37);
            this.txtSnapshotDateTime.Name = "txtSnapshotDateTime";
            this.txtSnapshotDateTime.ReadOnly = true;
            this.txtSnapshotDateTime.Size = new System.Drawing.Size(642, 20);
            this.txtSnapshotDateTime.TabIndex = 26;
            // 
            // cbSnapshotWebItemEC2Ids
            // 
            this.cbSnapshotWebItemEC2Ids.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSnapshotWebItemEC2Ids.FormattingEnabled = true;
            this.cbSnapshotWebItemEC2Ids.Location = new System.Drawing.Point(180, 81);
            this.cbSnapshotWebItemEC2Ids.Name = "cbSnapshotWebItemEC2Ids";
            this.cbSnapshotWebItemEC2Ids.Size = new System.Drawing.Size(642, 21);
            this.cbSnapshotWebItemEC2Ids.TabIndex = 20;
            this.cbSnapshotWebItemEC2Ids.TextChanged += new System.EventHandler(this.CbSnapshotWebItemEC2Ids_TextChanged);
            // 
            // btnStartWebsiteK
            // 
            this.btnStartWebsiteK.Location = new System.Drawing.Point(6, 32);
            this.btnStartWebsiteK.Name = "btnStartWebsiteK";
            this.btnStartWebsiteK.Size = new System.Drawing.Size(153, 23);
            this.btnStartWebsiteK.TabIndex = 28;
            this.btnStartWebsiteK.Text = "Start website(s)";
            this.btnStartWebsiteK.UseVisualStyleBackColor = true;
            this.btnStartWebsiteK.Click += new System.EventHandler(this.BtnStartWebsiteK_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "EC2 instance:";
            // 
            // lbInstanceId
            // 
            this.lbInstanceId.FormattingEnabled = true;
            this.lbInstanceId.Location = new System.Drawing.Point(186, 67);
            this.lbInstanceId.Name = "lbInstanceId";
            this.lbInstanceId.Size = new System.Drawing.Size(642, 82);
            this.lbInstanceId.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Instance(s):";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "EC2 Name:";
            // 
            // lvWebItemsOnInstance
            // 
            this.lvWebItemsOnInstance.CheckBoxes = true;
            this.lvWebItemsOnInstance.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader5,
            this.columnHeader4});
            this.lvWebItemsOnInstance.FullRowSelect = true;
            this.lvWebItemsOnInstance.Location = new System.Drawing.Point(180, 162);
            this.lvWebItemsOnInstance.MultiSelect = false;
            this.lvWebItemsOnInstance.Name = "lvWebItemsOnInstance";
            this.lvWebItemsOnInstance.Size = new System.Drawing.Size(642, 420);
            this.lvWebItemsOnInstance.TabIndex = 27;
            this.lvWebItemsOnInstance.UseCompatibleStateImageBehavior = false;
            this.lvWebItemsOnInstance.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Website";
            this.columnHeader1.Width = 158;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Status";
            this.columnHeader2.Width = 131;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "AppPool";
            this.columnHeader3.Width = 154;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Net_Clr";
            this.columnHeader5.Width = 62;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "AppPool Status";
            this.columnHeader4.Width = 123;
            // 
            // gbServiceSnapshotDetails
            // 
            this.gbServiceSnapshotDetails.Controls.Add(this.txtIISState);
            this.gbServiceSnapshotDetails.Controls.Add(this.label1);
            this.gbServiceSnapshotDetails.Controls.Add(this.gbIIs);
            this.gbServiceSnapshotDetails.Controls.Add(this.gbWebsite);
            this.gbServiceSnapshotDetails.Controls.Add(this.groupBox1);
            this.gbServiceSnapshotDetails.Controls.Add(this.label15);
            this.gbServiceSnapshotDetails.Controls.Add(this.txtSnapshotDateTime);
            this.gbServiceSnapshotDetails.Controls.Add(this.cbSnapshotWebItemEC2Ids);
            this.gbServiceSnapshotDetails.Controls.Add(this.label13);
            this.gbServiceSnapshotDetails.Controls.Add(this.lvWebItemsOnInstance);
            this.gbServiceSnapshotDetails.Location = new System.Drawing.Point(6, 220);
            this.gbServiceSnapshotDetails.Name = "gbServiceSnapshotDetails";
            this.gbServiceSnapshotDetails.Size = new System.Drawing.Size(828, 588);
            this.gbServiceSnapshotDetails.TabIndex = 20;
            this.gbServiceSnapshotDetails.TabStop = false;
            this.gbServiceSnapshotDetails.Text = "EC2 Web snapshot details";
            // 
            // txtIISState
            // 
            this.txtIISState.Location = new System.Drawing.Point(180, 127);
            this.txtIISState.Name = "txtIISState";
            this.txtIISState.ReadOnly = true;
            this.txtIISState.Size = new System.Drawing.Size(642, 20);
            this.txtIISState.TabIndex = 34;
            this.txtIISState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "IIS status:";
            // 
            // gbIIs
            // 
            this.gbIIs.Controls.Add(this.btnStartIIS);
            this.gbIIs.Controls.Add(this.btnStopIIS);
            this.gbIIs.Controls.Add(this.btnIISRestart);
            this.gbIIs.Location = new System.Drawing.Point(6, 162);
            this.gbIIs.Name = "gbIIs";
            this.gbIIs.Size = new System.Drawing.Size(166, 127);
            this.gbIIs.TabIndex = 32;
            this.gbIIs.TabStop = false;
            this.gbIIs.Text = "IIS";
            // 
            // btnStartIIS
            // 
            this.btnStartIIS.Location = new System.Drawing.Point(6, 29);
            this.btnStartIIS.Name = "btnStartIIS";
            this.btnStartIIS.Size = new System.Drawing.Size(152, 23);
            this.btnStartIIS.TabIndex = 27;
            this.btnStartIIS.Text = "Start IIS";
            this.btnStartIIS.UseVisualStyleBackColor = true;
            this.btnStartIIS.Click += new System.EventHandler(this.BtnStartIIS_Click);
            // 
            // btnStopIIS
            // 
            this.btnStopIIS.Location = new System.Drawing.Point(7, 58);
            this.btnStopIIS.Name = "btnStopIIS";
            this.btnStopIIS.Size = new System.Drawing.Size(152, 23);
            this.btnStopIIS.TabIndex = 26;
            this.btnStopIIS.Text = "Stop IIS";
            this.btnStopIIS.UseVisualStyleBackColor = true;
            this.btnStopIIS.Click += new System.EventHandler(this.BtnStopIIS_Click);
            // 
            // btnIISRestart
            // 
            this.btnIISRestart.Location = new System.Drawing.Point(8, 87);
            this.btnIISRestart.Name = "btnIISRestart";
            this.btnIISRestart.Size = new System.Drawing.Size(152, 23);
            this.btnIISRestart.TabIndex = 25;
            this.btnIISRestart.Text = "Restart IIS";
            this.btnIISRestart.UseVisualStyleBackColor = true;
            this.btnIISRestart.Click += new System.EventHandler(this.BtnIISRestart_Click);
            // 
            // gbWebsite
            // 
            this.gbWebsite.Controls.Add(this.btnStartWebsiteK);
            this.gbWebsite.Controls.Add(this.btnStopWebsiteCK);
            this.gbWebsite.Controls.Add(this.btnRestartWebsite);
            this.gbWebsite.Location = new System.Drawing.Point(6, 454);
            this.gbWebsite.Name = "gbWebsite";
            this.gbWebsite.Size = new System.Drawing.Size(166, 128);
            this.gbWebsite.TabIndex = 31;
            this.gbWebsite.TabStop = false;
            this.gbWebsite.Text = "Website";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRestartAppPool);
            this.groupBox1.Controls.Add(this.btnStartAppPool);
            this.groupBox1.Controls.Add(this.btnStopAppPool);
            this.groupBox1.Location = new System.Drawing.Point(6, 307);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(166, 129);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "AppPool";
            // 
            // btnRestartAppPool
            // 
            this.btnRestartAppPool.Location = new System.Drawing.Point(6, 90);
            this.btnRestartAppPool.Name = "btnRestartAppPool";
            this.btnRestartAppPool.Size = new System.Drawing.Size(152, 23);
            this.btnRestartAppPool.TabIndex = 24;
            this.btnRestartAppPool.Text = "Restart apppool(s)";
            this.btnRestartAppPool.UseVisualStyleBackColor = true;
            this.btnRestartAppPool.Click += new System.EventHandler(this.BtnRestartAppPool_Click);
            // 
            // btnStartAppPool
            // 
            this.btnStartAppPool.Location = new System.Drawing.Point(6, 32);
            this.btnStartAppPool.Name = "btnStartAppPool";
            this.btnStartAppPool.Size = new System.Drawing.Size(152, 23);
            this.btnStartAppPool.TabIndex = 22;
            this.btnStartAppPool.Text = "Start apppool(s)";
            this.btnStartAppPool.UseVisualStyleBackColor = true;
            this.btnStartAppPool.Click += new System.EventHandler(this.BtnStartAppPool_Click);
            // 
            // btnStopAppPool
            // 
            this.btnStopAppPool.Location = new System.Drawing.Point(6, 61);
            this.btnStopAppPool.Name = "btnStopAppPool";
            this.btnStopAppPool.Size = new System.Drawing.Size(152, 23);
            this.btnStopAppPool.TabIndex = 23;
            this.btnStopAppPool.Text = "Stop apppool(s)";
            this.btnStopAppPool.UseVisualStyleBackColor = true;
            this.btnStopAppPool.Click += new System.EventHandler(this.BtnStopAppPool_Click);
            // 
            // btnRefreshEc2Websites
            // 
            this.btnRefreshEc2Websites.Location = new System.Drawing.Point(186, 162);
            this.btnRefreshEc2Websites.Name = "btnRefreshEc2Websites";
            this.btnRefreshEc2Websites.Size = new System.Drawing.Size(642, 23);
            this.btnRefreshEc2Websites.TabIndex = 4;
            this.btnRefreshEc2Websites.Text = "Refresh available EC2 IIS";
            this.btnRefreshEc2Websites.UseVisualStyleBackColor = true;
            this.btnRefreshEc2Websites.Click += new System.EventHandler(this.BtnRefreshEc2Websites_Click);
            // 
            // gbServicesOperations
            // 
            this.gbServicesOperations.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbServicesOperations.Controls.Add(this.gbServiceSnapshotDetails);
            this.gbServicesOperations.Controls.Add(this.lbInstanceId);
            this.gbServicesOperations.Controls.Add(this.label12);
            this.gbServicesOperations.Controls.Add(this.label11);
            this.gbServicesOperations.Controls.Add(this.cbEc2WebNames);
            this.gbServicesOperations.Controls.Add(this.btnRefreshEc2Websites);
            this.gbServicesOperations.Controls.Add(this.btnRefreshWebSnapshot);
            this.gbServicesOperations.Location = new System.Drawing.Point(12, 12);
            this.gbServicesOperations.Name = "gbServicesOperations";
            this.gbServicesOperations.Size = new System.Drawing.Size(842, 814);
            this.gbServicesOperations.TabIndex = 21;
            this.gbServicesOperations.TabStop = false;
            this.gbServicesOperations.Text = "Web";
            // 
            // AwsEc2WebsiteOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 838);
            this.Controls.Add(this.gbServicesOperations);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AwsEc2WebsiteOperator";
            this.Text = "AwsEc2WebsiteOperator";
            this.gbServiceSnapshotDetails.ResumeLayout(false);
            this.gbServiceSnapshotDetails.PerformLayout();
            this.gbIIs.ResumeLayout(false);
            this.gbWebsite.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.gbServicesOperations.ResumeLayout(false);
            this.gbServicesOperations.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbEc2WebNames;
        private System.Windows.Forms.Button btnRefreshWebSnapshot;
        private System.Windows.Forms.Button btnRestartWebsite;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnStopWebsiteCK;
        private System.Windows.Forms.TextBox txtSnapshotDateTime;
        private System.Windows.Forms.ComboBox cbSnapshotWebItemEC2Ids;
        private System.Windows.Forms.Button btnStartWebsiteK;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListBox lbInstanceId;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListView lvWebItemsOnInstance;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.GroupBox gbServiceSnapshotDetails;
        private System.Windows.Forms.Button btnRefreshEc2Websites;
        private System.Windows.Forms.GroupBox gbServicesOperations;
        private System.Windows.Forms.Button btnStartAppPool;
        private System.Windows.Forms.Button btnStopAppPool;
        private System.Windows.Forms.Button btnRestartAppPool;
        private System.Windows.Forms.GroupBox gbWebsite;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbIIs;
        private System.Windows.Forms.Button btnIISRestart;
        private System.Windows.Forms.TextBox txtIISState;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnStartIIS;
        private System.Windows.Forms.Button btnStopIIS;
    }
}