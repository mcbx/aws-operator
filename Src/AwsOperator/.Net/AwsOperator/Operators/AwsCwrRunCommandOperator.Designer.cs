﻿namespace AwsOperator
{
    partial class AwsCwrRunCommandOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCWR = new System.Windows.Forms.GroupBox();
            this.cbRules = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnExecuteRule = new System.Windows.Forms.Button();
            this.gbCWRDetails = new System.Windows.Forms.GroupBox();
            this.btnViewHistoryResults = new System.Windows.Forms.Button();
            this.lblFetchHistoryStatus = new System.Windows.Forms.Label();
            this.btnGetExecutedCommands = new System.Windows.Forms.Button();
            this.lblCommandOverridden = new System.Windows.Forms.Label();
            this.btnRefreshAvailableRules = new System.Windows.Forms.Button();
            this.txtRuleStatus = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbRuleTags = new System.Windows.Forms.ComboBox();
            this.lblPurposeIdTag = new System.Windows.Forms.Label();
            this.lbRuleCommands = new System.Windows.Forms.ListBox();
            this.lbRuleTargets = new System.Windows.Forms.ListBox();
            this.lblCommandParameter = new System.Windows.Forms.Label();
            this.lblTargetFunction = new System.Windows.Forms.Label();
            this.txtRuleSchedule = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gbOverrideCommand = new System.Windows.Forms.GroupBox();
            this.txtOverrideCommand = new System.Windows.Forms.TextBox();
            this.btnOverrideCommand = new System.Windows.Forms.Button();
            this.gbCWR.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbCWRDetails.SuspendLayout();
            this.gbOverrideCommand.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCWR
            // 
            this.gbCWR.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbCWR.Controls.Add(this.cbRules);
            this.gbCWR.Controls.Add(this.groupBox3);
            this.gbCWR.Controls.Add(this.gbCWRDetails);
            this.gbCWR.Location = new System.Drawing.Point(12, 12);
            this.gbCWR.Name = "gbCWR";
            this.gbCWR.Size = new System.Drawing.Size(528, 690);
            this.gbCWR.TabIndex = 13;
            this.gbCWR.TabStop = false;
            this.gbCWR.Text = "Rules";
            // 
            // cbRules
            // 
            this.cbRules.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRules.FormattingEnabled = true;
            this.cbRules.Location = new System.Drawing.Point(6, 19);
            this.cbRules.Name = "cbRules";
            this.cbRules.Size = new System.Drawing.Size(511, 21);
            this.cbRules.TabIndex = 2;
            this.cbRules.TextChanged += new System.EventHandler(this.cbRules_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnExecuteRule);
            this.groupBox3.Location = new System.Drawing.Point(7, 625);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(515, 59);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Operations";
            // 
            // btnExecuteRule
            // 
            this.btnExecuteRule.Location = new System.Drawing.Point(7, 19);
            this.btnExecuteRule.Name = "btnExecuteRule";
            this.btnExecuteRule.Size = new System.Drawing.Size(504, 33);
            this.btnExecuteRule.TabIndex = 16;
            this.btnExecuteRule.Text = "Run Command ";
            this.btnExecuteRule.UseVisualStyleBackColor = true;
            this.btnExecuteRule.Click += new System.EventHandler(this.btnExecuteRule_Click);
            // 
            // gbCWRDetails
            // 
            this.gbCWRDetails.Controls.Add(this.btnViewHistoryResults);
            this.gbCWRDetails.Controls.Add(this.lblFetchHistoryStatus);
            this.gbCWRDetails.Controls.Add(this.btnGetExecutedCommands);
            this.gbCWRDetails.Controls.Add(this.lblCommandOverridden);
            this.gbCWRDetails.Controls.Add(this.btnRefreshAvailableRules);
            this.gbCWRDetails.Controls.Add(this.txtRuleStatus);
            this.gbCWRDetails.Controls.Add(this.label1);
            this.gbCWRDetails.Controls.Add(this.cbRuleTags);
            this.gbCWRDetails.Controls.Add(this.lblPurposeIdTag);
            this.gbCWRDetails.Controls.Add(this.lbRuleCommands);
            this.gbCWRDetails.Controls.Add(this.lbRuleTargets);
            this.gbCWRDetails.Controls.Add(this.lblCommandParameter);
            this.gbCWRDetails.Controls.Add(this.lblTargetFunction);
            this.gbCWRDetails.Controls.Add(this.txtRuleSchedule);
            this.gbCWRDetails.Controls.Add(this.label6);
            this.gbCWRDetails.Location = new System.Drawing.Point(7, 46);
            this.gbCWRDetails.Name = "gbCWRDetails";
            this.gbCWRDetails.Size = new System.Drawing.Size(515, 573);
            this.gbCWRDetails.TabIndex = 10;
            this.gbCWRDetails.TabStop = false;
            this.gbCWRDetails.Text = "Details";
            // 
            // btnViewHistoryResults
            // 
            this.btnViewHistoryResults.Location = new System.Drawing.Point(7, 525);
            this.btnViewHistoryResults.Name = "btnViewHistoryResults";
            this.btnViewHistoryResults.Size = new System.Drawing.Size(503, 37);
            this.btnViewHistoryResults.TabIndex = 18;
            this.btnViewHistoryResults.Text = "View command history results";
            this.btnViewHistoryResults.UseVisualStyleBackColor = true;
            this.btnViewHistoryResults.Click += new System.EventHandler(this.btnViewHistoryResults_Click);
            // 
            // lblFetchHistoryStatus
            // 
            this.lblFetchHistoryStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFetchHistoryStatus.ForeColor = System.Drawing.Color.Red;
            this.lblFetchHistoryStatus.Location = new System.Drawing.Point(7, 461);
            this.lblFetchHistoryStatus.Name = "lblFetchHistoryStatus";
            this.lblFetchHistoryStatus.Size = new System.Drawing.Size(502, 19);
            this.lblFetchHistoryStatus.TabIndex = 17;
            this.lblFetchHistoryStatus.Text = "Searching.....";
            this.lblFetchHistoryStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFetchHistoryStatus.Visible = false;
            // 
            // btnGetExecutedCommands
            // 
            this.btnGetExecutedCommands.Location = new System.Drawing.Point(7, 483);
            this.btnGetExecutedCommands.Name = "btnGetExecutedCommands";
            this.btnGetExecutedCommands.Size = new System.Drawing.Size(503, 37);
            this.btnGetExecutedCommands.TabIndex = 16;
            this.btnGetExecutedCommands.Text = "Search command history (7 days)";
            this.btnGetExecutedCommands.UseVisualStyleBackColor = true;
            this.btnGetExecutedCommands.Click += new System.EventHandler(this.btnGetExecutedCommands_Click);
            // 
            // lblCommandOverridden
            // 
            this.lblCommandOverridden.AutoSize = true;
            this.lblCommandOverridden.Location = new System.Drawing.Point(7, 405);
            this.lblCommandOverridden.Name = "lblCommandOverridden";
            this.lblCommandOverridden.Size = new System.Drawing.Size(0, 13);
            this.lblCommandOverridden.TabIndex = 15;
            // 
            // btnRefreshAvailableRules
            // 
            this.btnRefreshAvailableRules.Location = new System.Drawing.Point(7, 365);
            this.btnRefreshAvailableRules.Name = "btnRefreshAvailableRules";
            this.btnRefreshAvailableRules.Size = new System.Drawing.Size(503, 37);
            this.btnRefreshAvailableRules.TabIndex = 0;
            this.btnRefreshAvailableRules.Text = "Refresh rule list";
            this.btnRefreshAvailableRules.UseVisualStyleBackColor = true;
            this.btnRefreshAvailableRules.Click += new System.EventHandler(this.btnRefreshAvailableRules_Click);
            // 
            // txtRuleStatus
            // 
            this.txtRuleStatus.Location = new System.Drawing.Point(105, 27);
            this.txtRuleStatus.Name = "txtRuleStatus";
            this.txtRuleStatus.ReadOnly = true;
            this.txtRuleStatus.Size = new System.Drawing.Size(404, 20);
            this.txtRuleStatus.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Rule status:";
            // 
            // cbRuleTags
            // 
            this.cbRuleTags.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRuleTags.FormattingEnabled = true;
            this.cbRuleTags.Location = new System.Drawing.Point(105, 105);
            this.cbRuleTags.Name = "cbRuleTags";
            this.cbRuleTags.Size = new System.Drawing.Size(404, 21);
            this.cbRuleTags.TabIndex = 12;
            this.cbRuleTags.TextChanged += new System.EventHandler(this.cbRuleTags_TextChanged);
            // 
            // lblPurposeIdTag
            // 
            this.lblPurposeIdTag.AutoSize = true;
            this.lblPurposeIdTag.Location = new System.Drawing.Point(6, 106);
            this.lblPurposeIdTag.Name = "lblPurposeIdTag";
            this.lblPurposeIdTag.Size = new System.Drawing.Size(80, 13);
            this.lblPurposeIdTag.TabIndex = 11;
            this.lblPurposeIdTag.Text = "PurposeId Tag:";
            // 
            // lbRuleCommands
            // 
            this.lbRuleCommands.FormattingEnabled = true;
            this.lbRuleCommands.HorizontalScrollbar = true;
            this.lbRuleCommands.Location = new System.Drawing.Point(106, 246);
            this.lbRuleCommands.Name = "lbRuleCommands";
            this.lbRuleCommands.Size = new System.Drawing.Size(403, 108);
            this.lbRuleCommands.TabIndex = 9;
            this.lbRuleCommands.DoubleClick += new System.EventHandler(this.lbRuleCommands_DoubleClick);
            // 
            // lbRuleTargets
            // 
            this.lbRuleTargets.FormattingEnabled = true;
            this.lbRuleTargets.HorizontalScrollbar = true;
            this.lbRuleTargets.Location = new System.Drawing.Point(105, 145);
            this.lbRuleTargets.Name = "lbRuleTargets";
            this.lbRuleTargets.Size = new System.Drawing.Size(404, 95);
            this.lbRuleTargets.TabIndex = 8;
            // 
            // lblCommandParameter
            // 
            this.lblCommandParameter.AutoSize = true;
            this.lblCommandParameter.Location = new System.Drawing.Point(6, 246);
            this.lblCommandParameter.Name = "lblCommandParameter";
            this.lblCommandParameter.Size = new System.Drawing.Size(70, 13);
            this.lblCommandParameter.TabIndex = 6;
            this.lblCommandParameter.Text = "Commando\'s:";
            // 
            // lblTargetFunction
            // 
            this.lblTargetFunction.AutoSize = true;
            this.lblTargetFunction.Location = new System.Drawing.Point(6, 145);
            this.lblTargetFunction.Name = "lblTargetFunction";
            this.lblTargetFunction.Size = new System.Drawing.Size(46, 13);
            this.lblTargetFunction.TabIndex = 4;
            this.lblTargetFunction.Text = "Targets:";
            // 
            // txtRuleSchedule
            // 
            this.txtRuleSchedule.Location = new System.Drawing.Point(105, 68);
            this.txtRuleSchedule.Name = "txtRuleSchedule";
            this.txtRuleSchedule.ReadOnly = true;
            this.txtRuleSchedule.Size = new System.Drawing.Size(404, 20);
            this.txtRuleSchedule.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Schedule:";
            // 
            // gbOverrideCommand
            // 
            this.gbOverrideCommand.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbOverrideCommand.Controls.Add(this.txtOverrideCommand);
            this.gbOverrideCommand.Controls.Add(this.btnOverrideCommand);
            this.gbOverrideCommand.Location = new System.Drawing.Point(558, 12);
            this.gbOverrideCommand.Name = "gbOverrideCommand";
            this.gbOverrideCommand.Size = new System.Drawing.Size(476, 690);
            this.gbOverrideCommand.TabIndex = 14;
            this.gbOverrideCommand.TabStop = false;
            this.gbOverrideCommand.Text = "Override command (session)";
            // 
            // txtOverrideCommand
            // 
            this.txtOverrideCommand.Location = new System.Drawing.Point(6, 19);
            this.txtOverrideCommand.Multiline = true;
            this.txtOverrideCommand.Name = "txtOverrideCommand";
            this.txtOverrideCommand.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOverrideCommand.Size = new System.Drawing.Size(464, 615);
            this.txtOverrideCommand.TabIndex = 1;
            // 
            // btnOverrideCommand
            // 
            this.btnOverrideCommand.Location = new System.Drawing.Point(6, 644);
            this.btnOverrideCommand.Name = "btnOverrideCommand";
            this.btnOverrideCommand.Size = new System.Drawing.Size(464, 33);
            this.btnOverrideCommand.TabIndex = 0;
            this.btnOverrideCommand.Text = "Override command";
            this.btnOverrideCommand.UseVisualStyleBackColor = true;
            this.btnOverrideCommand.Click += new System.EventHandler(this.btnSaveCommand_Click);
            // 
            // AwsCwrRunCommandOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1037, 715);
            this.Controls.Add(this.gbOverrideCommand);
            this.Controls.Add(this.gbCWR);
            this.MinimumSize = new System.Drawing.Size(566, 754);
            this.Name = "AwsCwrRunCommandOperator";
            this.Text = "AwsCwrOperator";
            this.gbCWR.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.gbCWRDetails.ResumeLayout(false);
            this.gbCWRDetails.PerformLayout();
            this.gbOverrideCommand.ResumeLayout(false);
            this.gbOverrideCommand.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCWR;
        private System.Windows.Forms.ComboBox cbRules;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnExecuteRule;
        private System.Windows.Forms.GroupBox gbCWRDetails;
        private System.Windows.Forms.ComboBox cbRuleTags;
        private System.Windows.Forms.Button btnRefreshAvailableRules;
        private System.Windows.Forms.Label lblPurposeIdTag;
        private System.Windows.Forms.ListBox lbRuleCommands;
        private System.Windows.Forms.ListBox lbRuleTargets;
        private System.Windows.Forms.Label lblCommandParameter;
        private System.Windows.Forms.Label lblTargetFunction;
        private System.Windows.Forms.TextBox txtRuleSchedule;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRuleStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbOverrideCommand;
        private System.Windows.Forms.TextBox txtOverrideCommand;
        private System.Windows.Forms.Button btnOverrideCommand;
        private System.Windows.Forms.Label lblCommandOverridden;
        private System.Windows.Forms.Button btnGetExecutedCommands;
        private System.Windows.Forms.Label lblFetchHistoryStatus;
        private System.Windows.Forms.Button btnViewHistoryResults;
    }
}