﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace AwsOperator.Operators
{
    public partial class AwsEc2WebsiteOperator : FormTemplate
    {
        private Dictionary<string, List<string>> _activeEc2NameList = new Dictionary<string, List<string>>();
        private Dictionary<string, JsonIIS> _activeEc2ServiceSnapshot = new Dictionary<string, JsonIIS>();

        public AwsEc2WebsiteOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();

            lvWebItemsOnInstance.GetType()
                .GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(lvWebItemsOnInstance, true);

            Text = $"Operator: EC2 WebItems, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";
            BtnRefreshEc2Websites_Click(this, null);
        }

        private void BtnRefreshEc2Websites_Click(object sender, EventArgs e)
        {
            List<string> searchSuffixes = new List<string>();
            searchSuffixes.Add("*asg");
            searchSuffixes.Add("*wb-*");

            var result = AWSEc2.GetEc2AppList(_domein, _environment, _appCode, searchSuffixes);
               if (result == null)
                   return;

            _activeEc2NameList = result.GroupBy(x => x.Value, x => x.Key).ToDictionary(g => g.Key, g => g.ToList());
            cbEc2WebNames.DataSource = _activeEc2NameList.Keys.ToList();

            lvWebItemsOnInstance.ShowItemToolTips = true;
        }

        private void CbEc2WebNames_TextChanged(object sender, EventArgs e)
        {
            lbInstanceId.Items.Clear();
            if (string.IsNullOrEmpty(cbEc2WebNames.Text))
                return;

            if (!_activeEc2NameList.ContainsKey(cbEc2WebNames.Text))
                return;

            lbInstanceId.Items.AddRange(_activeEc2NameList[cbEc2WebNames.Text].ToArray());
        }

        private void BtnRefreshWebSnapshot_Click(object sender, EventArgs e)
        {
            try
            {
                UseWaitCursor = true;

                cbSnapshotWebItemEC2Ids.DataSource = null;
                txtSnapshotDateTime.Text = string.Empty;
                lvWebItemsOnInstance.Items.Clear();
                txtIISState.Text = string.Empty;
                txtIISState.BackColor = txtSnapshotDateTime.BackColor;

                Application.DoEvents();
                if (string.IsNullOrEmpty(cbEc2WebNames.Text))
                    return;

                if (!_activeEc2NameList.ContainsKey(cbEc2WebNames.Text))
                    return;

                List<string> requestFromInstances = new List<string>();
                requestFromInstances.AddRange(lbInstanceId.Items.Cast<string>());

                var result = AWSEc2Website.GetIISWebsiteSnapshot(_domein, _environment, requestFromInstances);
                if (result == null)
                    return;

                _activeEc2ServiceSnapshot = result;
                cbSnapshotWebItemEC2Ids.DataSource = result.Keys.ToList();

                txtSnapshotDateTime.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            }
            finally
            {
                UseWaitCursor = false;
            }
        }

        private Color DetermineIISStateColor(string state)
        {
            switch (state.ToLower())
            {
                case "running":
                    return Color.Lime;
                case "stopped":
                    return Color.LightSalmon;
                default:
                    return Color.FromArgb(250, 194, 87);
            }
        }

        private void CbSnapshotWebItemEC2Ids_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                return;

            if (!_activeEc2ServiceSnapshot.ContainsKey(cbSnapshotWebItemEC2Ids.Text))
                return;

            txtIISState.Text = _activeEc2ServiceSnapshot[cbSnapshotWebItemEC2Ids.Text].IIS_State;
            txtIISState.BackColor = DetermineIISStateColor(_activeEc2ServiceSnapshot[cbSnapshotWebItemEC2Ids.Text].IIS_State);

            lvWebItemsOnInstance.Items.Clear();
            foreach (JsonIISWebsite serviceItem in _activeEc2ServiceSnapshot[cbSnapshotWebItemEC2Ids.Text].Websites.OrderBy(item => item.Website))
            {
                lvWebItemsOnInstance.Items.Add(serviceItem.Website, serviceItem.Website, string.Empty);
                lvWebItemsOnInstance.Items[serviceItem.Website].ToolTipText = $"Bindings:{Environment.NewLine}{ string.Join(Environment.NewLine, serviceItem.Bindings.Select(item => item.ToString()))}";
                lvWebItemsOnInstance.Items[serviceItem.Website].UseItemStyleForSubItems = false;

                ListViewSubItem websiteState = new ListViewItem.ListViewSubItem()
                {
                    Name = "WebsiteState",
                    Text = serviceItem.State ?? "Stopped"
                };
                websiteState.BackColor = websiteState.Text.ToLower() == "started" ? lvWebItemsOnInstance.Items[serviceItem.Website].BackColor : Color.LightSalmon;

                lvWebItemsOnInstance.Items[serviceItem.Website].SubItems.Add(websiteState);
                lvWebItemsOnInstance.Items[serviceItem.Website].SubItems.Add(serviceItem.ApplicationPool.Name);
                lvWebItemsOnInstance.Items[serviceItem.Website].SubItems.Add(serviceItem.ApplicationPool.Net_Clr_Version);

                ListViewSubItem appPoolState = new ListViewItem.ListViewSubItem()
                {
                    Name = "AppPoolState",
                    Text = serviceItem.ApplicationPool.State ?? "Stopped"
                };
                appPoolState.BackColor = appPoolState.Text.ToLower() == "started" ? lvWebItemsOnInstance.Items[serviceItem.Website].BackColor : Color.LightSalmon;
                lvWebItemsOnInstance.Items[serviceItem.Website].SubItems.Add(appPoolState);
            }
        }

        #region AppPool operations 

        private void BtnStartAppPool_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(lvWebItemsOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.SubItems[2].Text));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende application pools wilt starten? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.StartAppPoolOnEC2(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, checkedItems, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Start AppPool) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void BtnStopAppPool_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(lvWebItemsOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.SubItems[2].Text));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende application pools wilt stoppen? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.StopAppPoolOnEC2(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, checkedItems, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Stop AppPool) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void BtnRestartAppPool_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(lvWebItemsOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.SubItems[2].Text));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende application pools wilt herstarten? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.RestartAppPoolOnEC2(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, checkedItems, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Restart AppPool) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        #endregion

        #region Website operations

        private void BtnStartWebsiteK_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(lvWebItemsOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.Name));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende websites wilt starten? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.StartWebsiteOnEC2(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, checkedItems, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Start website(s)) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void BtnStopWebsiteCK_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(lvWebItemsOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.Name));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende websites wilt stoppen? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.StopWebsiteOnEC2(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, checkedItems, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Stop website(s)) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void btnRestartWebsite_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                List<string> checkedItems = new List<string>();
                checkedItems.AddRange(lvWebItemsOnInstance.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.Name));

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende websites wilt herstarten? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.RestartWebsiteOnEC2(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, checkedItems, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Restart website(s)) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        #endregion

        #region IIS operations

        private void BtnStartIIS_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                if (MessageBox.Show(this, "Weet u zeker dat u IIS wilt starten?", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.StartIIS(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Start IIS) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void BtnStopIIS_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                if (MessageBox.Show(this, "Weet u zeker dat u IIS wilt stoppen?", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.StopIIS(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Stop IIS) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void BtnIISRestart_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotWebItemEC2Ids.Text))
                    return;

                if (MessageBox.Show(this, "Weet u zeker dat u IIS wilt herstarten?", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2Website.RestartIIS(_domein, _environment, cbSnapshotWebItemEC2Ids.Text, $"AwsEc2WebsiteOperator ({cbSnapshotWebItemEC2Ids.Text}>Restart IIS) => {AWSCredentialKeeper.AWSOperatorUser}");
                BtnRefreshWebSnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        #endregion
    }
}
