﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class AwsCwrRunCommandOperator : FormTemplate
    {
        private RuleDetail _activeRule;
        Dictionary<string, CommandDetail> _historySearchResults = new Dictionary<string, CommandDetail>();

        public AwsCwrRunCommandOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner, bool formOverride) : base(appCode, domein, environment, owner, formOverride)
        {
            InitializeComponent();
            Text = $"Operator: CloudWatch Rules (VAT), AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";

            lblCommandOverridden.Visible = false;
            lblCommandOverridden.Text = $"Warning: CWR command/parameter is overridden.{Environment.NewLine}The command will be reset if another rule is chosen.";
            lblCommandOverridden.ForeColor = Color.Red;

            gbOverrideCommand.Visible = false;
            btnRefreshAvailableRules_Click(this, null);
        }

        public AwsCwrRunCommandOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();
            Text = $"Operator: CloudWatch Rules, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";

            lblCommandOverridden.Visible = false;
            lblCommandOverridden.Text = $"Warning: CWR command/parameter is overridden.{Environment.NewLine}The command will be reset if another rule is chosen.";
            lblCommandOverridden.ForeColor = Color.Red;

            gbOverrideCommand.Visible = false;
            btnRefreshAvailableRules_Click(this, null);
        }

        private void ResetAllFields()
        {
            _historySearchResults.Clear();
            btnViewHistoryResults.Enabled = false;
            lbRuleCommands.Items.Clear();
            lbRuleTargets.Items.Clear();
            txtRuleStatus.Text = string.Empty;
            cbRuleTags.DataSource = null;
            txtRuleSchedule.Text = string.Empty;
            lblCommandOverridden.Visible = false;
            gbOverrideCommand.Visible = false;
        }

        private void btnRefreshAvailableRules_Click(object sender, EventArgs e)
        {
            cbRules.Items.Clear();
            ResetAllFields();
            List<string> rules = new List<string>();
            rules.AddRange(AWSCloudwatchRule.GetRules(_appCode, _domein, _environment).Where(item => !item.EndsWith("-rule")));
            cbRules.Items.AddRange(rules.ToArray());
        }

        private void cbRules_TextChanged(object sender, EventArgs e)
        {
            ResetAllFields();
            var result = AWSCloudwatchRule.GetRuleInfo(cbRules.Text, _domein, _environment);
            if (result == null)
                return;

            _activeRule = result;

            txtRuleStatus.Text = result.Enabled ? "ENABLED" : "DISABLED";
            txtRuleSchedule.Text = result.Schedule;

            if (result.RuleTargets.Count == 1)
            {
                lblTargetFunction.Text = "Targets";
                lblCommandParameter.Text = @"Commando's:";

                lblPurposeIdTag.Visible = true;
                cbRuleTags.Visible = true;
                cbRuleTags.DataSource = result.RuleTargets[0].MachineTargets.Keys.ToList();

                btnGetExecutedCommands.Enabled = true;
            }

            if (result.RuleLambdaTargets.Count == 1)
            {
                lblTargetFunction.Text = "Lambda function:";
                lblCommandParameter.Text = @"Parameter (JSON):";

                lblPurposeIdTag.Visible = false;
                cbRuleTags.Visible = false;
                lbRuleTargets.Items.Add(result.RuleLambdaTargets[0].LambdaFunction);
                lbRuleCommands.Items.Add(string.IsNullOrEmpty(result.RuleLambdaTargets[0].LambdaParameterJson) ? string.Empty : result.RuleLambdaTargets[0].LambdaParameterJson);

                btnGetExecutedCommands.Enabled = false;
                btnViewHistoryResults.Enabled = false;
            }
        }

        private void ExecuteRunCommand()
        {
            if (string.IsNullOrEmpty(cbRuleTags.Text))
                return;

            if (lbRuleCommands.Items.Count == 0)
            {
                //message???
                return;
            }

            if (MessageBox.Show(this, $"Are you sure you want to execute the commands of the rule on the displayed machines?{Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, lbRuleCommands.Items.Cast<string>().ToArray())}", "Confirm....", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                return;

            List<string> machineIds = new List<string>();
            List<string> commands = new List<string>();

            machineIds.AddRange(lbRuleTargets.Items.Cast<string>().ToArray());
            commands.AddRange(lbRuleCommands.Items.Cast<string>().ToArray());

            Dictionary<string, CommandDetail> results = AWSRunCommand.RunPowershellCommand(_domein, _environment, machineIds, commands, $"AwsCwrRunCommandOperator ({cbRules.Text}-{_domein.ToString()}>Command: {(lblCommandOverridden.Visible ? "Overridden" : "Normal")}) => {AWSCredentialKeeper.AWSOperatorUser}", true);
            results.Keys.ToList().ForEach(item =>
            {
                CommandDetailForm cdfItem = new CommandDetailForm($"CWR: {cbRules.Text}, InstanceId: {item}, Executed on: {DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")}",
                    _domein, _environment, results[item], Owner);
                cdfItem.Show(this);
            });
        }

        private void ExecuteLambdaFunction()
        {
            if (lbRuleTargets.Items.Count != 1 || lbRuleCommands.Items.Count != 1)
                return;

            if (MessageBox.Show(this, $"Are you sure you want to execute the Lambda function: '{(string)lbRuleTargets.Items[0]}'?{Environment.NewLine}{Environment.NewLine}Parameter:{Environment.NewLine}{string.Join(Environment.NewLine, lbRuleCommands.Items.Cast<string>().ToArray())}", "Confirm....", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                return;

            if (!AWSLambdaFunctions.RunLambdaFunctionEvent(_domein, _environment, (string)lbRuleTargets.Items[0], (string)lbRuleCommands.Items[0]))
            {
                MessageBox.Show(this, "Request could not be executed.");
                return;
            }
            MessageBox.Show(this, "Request has been processed.");
        }

        private void btnExecuteRule_Click(object sender, EventArgs e)
        {
            if (cbRuleTags.Visible)
            {
                ExecuteRunCommand();
                return;
            }

            ExecuteLambdaFunction();
        }

        private void cbRuleTags_TextChanged(object sender, EventArgs e)
        {
            lbRuleCommands.Items.Clear();
            lbRuleTargets.Items.Clear();
            lbRuleCommands.Items.Clear();
            gbOverrideCommand.Visible = false;
            lblCommandOverridden.Visible = false;

            if (string.IsNullOrEmpty(cbRuleTags.Text))
            {
                btnGetExecutedCommands.Enabled = false;
                btnViewHistoryResults.Enabled = false;
                lblFetchHistoryStatus.Visible = false;
                return;
            }

            if (_activeRule.RuleTargets.Count == 1 && _activeRule.RuleTargets[0].MachineTargets.ContainsKey(cbRuleTags.Text))
            {
                _activeRule.RuleTargets[0].MachineTargets[cbRuleTags.Text].ForEach(ec2 =>
                {
                    lbRuleTargets.Items.Add(ec2);
                });

                _activeRule.RuleTargets[0].Commands.ForEach(cItem =>
                {
                    lbRuleCommands.Items.Add(cItem);
                });

                btnGetExecutedCommands.Enabled = true;
            }
        }

        private void btnSaveCommand_Click(object sender, EventArgs e)
        {
            lblCommandOverridden.Visible = true;
            gbOverrideCommand.Visible = false;

            lbRuleCommands.Items.Clear();
            lbRuleCommands.Items.Add(txtOverrideCommand.Text);
        }

        private void lbRuleCommands_DoubleClick(object sender, EventArgs e)
        {
            txtOverrideCommand.Text = string.Empty;

            if (cbRuleTags.Visible && !_formOverride)
            {
                MessageBox.Show("Sorry.. Overriding a CWR command is disabled.", "Sorry..", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }

            if (lbRuleCommands.SelectedIndex == -1)
                return;

            if (lbRuleCommands.SelectedItem == null)
                return;

            if (lblCommandParameter.Text == @"Parameter (JSON):")
                txtOverrideCommand.Text = AWSCommon.FormatJson((string)lbRuleCommands.SelectedItem);
            else
                txtOverrideCommand.Text = (string)lbRuleCommands.SelectedItem;

            gbOverrideCommand.Visible = true;
        }

        private async void btnGetExecutedCommands_Click(object sender, EventArgs e)
        {
            Stopwatch searchElapsed = new Stopwatch();
            _historySearchResults.Clear();
            try
            {
                searchElapsed.Start();
                UseWaitCursor = true;
                Cursor = Cursors.WaitCursor;

                cbRules.Enabled = false;
                cbRuleTags.Enabled = false;
                btnExecuteRule.Enabled = false;
                btnOverrideCommand.Enabled = false;
                btnRefreshAvailableRules.Enabled = false;

                btnGetExecutedCommands.Enabled = false;
                lblFetchHistoryStatus.Text = "Searching.....";
                lblFetchHistoryStatus.ForeColor = Color.Red;
                lblFetchHistoryStatus.Visible = true;

                if (string.IsNullOrEmpty(cbRuleTags.Text))
                    return;

                if (lbRuleCommands.Items.Count == 0)
                    return;

                foreach (var ruleCommandItem in lbRuleCommands.Items.OfType<string>())
                {
                   _historySearchResults = await AWSCloudwatchRule.GetCommandsByTagAndCommand(_domein, _environment, cbRuleTags.Text, ruleCommandItem, DateTime.Now.AddMinutes(-21), DateTime.Now.AddMinutes(-1));
                }
            }
            finally
            {
                searchElapsed.Stop();

                UseWaitCursor = false;
                Cursor = Cursors.Default;
                btnGetExecutedCommands.Enabled = true;

                lblFetchHistoryStatus.Text = string.Empty;
                lblFetchHistoryStatus.ForeColor = Color.Green;
                lblFetchHistoryStatus.Text = $"Completed in {Math.Round(searchElapsed.Elapsed.TotalSeconds)}s. Found '{_historySearchResults.Count}' results.";
                btnViewHistoryResults.Enabled = _historySearchResults.Count > 0;

                cbRules.Enabled = true;
                cbRuleTags.Enabled = true;
                btnExecuteRule.Enabled = true;
                btnOverrideCommand.Enabled = true;
                btnRefreshAvailableRules.Enabled = true;
            }
        }

        private void btnViewHistoryResults_Click(object sender, EventArgs e)
        {
            CommandDetailForm frm = new CommandDetailForm($"rule => {cbRules.Text}", _domein, _environment, _historySearchResults, Owner);
            frm.Show();

            foreach (KeyValuePair<string, CommandDetail> item in _historySearchResults)
            {
                Debug.WriteLine($"{item.Value.CommandId}, {item.Value.CommandInstanceId}, {item.Value.CommandStatus}");
            }
        }
    }
}
