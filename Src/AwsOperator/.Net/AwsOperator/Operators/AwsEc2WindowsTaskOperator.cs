﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class AwsEc2WindowsTaskOperator : FormTemplate
    {
        private Dictionary<string, List<string>> _activeEc2PurposeIdList = new Dictionary<string, List<string>>();
        private Dictionary<string, List<JsonTaskItem>> _activeEc2TaskSnapshot = new Dictionary<string, List<JsonTaskItem>>();

        public AwsEc2WindowsTaskOperator (string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();

            Text = $"Operator: EC2 Windowsservice, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";
            btnRefreshTags_Click(this, null);
        }

        private void cbPurposeIdTag_TextChanged(object sender, EventArgs e)
        {
            lbTagInstanceId.Items.Clear();
            if (string.IsNullOrEmpty(cbPurposeIdTag.Text))
                return;

            if (!_activeEc2PurposeIdList.ContainsKey(cbPurposeIdTag.Text))
                return;

            lbTagInstanceId.Items.AddRange(_activeEc2PurposeIdList[cbPurposeIdTag.Text].ToArray());
        }

        private void btnRefreshTags_Click(object sender, EventArgs e)
        {
            var result = AWSEc2.GetEc2PurposeIdList(_domein, _environment, _appCode);
            if (result == null)
                return;

            _activeEc2PurposeIdList = result;
            cbPurposeIdTag.DataSource = result.Keys.ToList();
        }

        private void cbSnapshotServiceEC2Ids_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbSnapshotTaskEC2Ids.Text))
                return;

            if (!_activeEc2TaskSnapshot.ContainsKey(cbSnapshotTaskEC2Ids.Text))
                return;

            lvTasksOnInstance.Items.Clear();
            foreach (JsonTaskItem taskItem in _activeEc2TaskSnapshot[cbSnapshotTaskEC2Ids.Text].OrderBy(item => item.ProcessName))
            {
                lvTasksOnInstance.Items.Add(taskItem.Id.ToString(), taskItem.ProcessName, string.Empty);
                lvTasksOnInstance.Items[taskItem.Id.ToString()].SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "TaskUsername", Text = taskItem.UserName });
                lvTasksOnInstance.Items[taskItem.Id.ToString()].SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "TaskProcessId", Text = taskItem.Id.ToString() });
            }
        }

        private void btnStartServiceCK_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                if (string.IsNullOrEmpty(cbSnapshotTaskEC2Ids.Text))
                    return;

                Dictionary<string, string> checkedItems = new Dictionary<string, string>();
                lvTasksOnInstance.CheckedItems.Cast<ListViewItem>().ToList().ForEach(item =>
                {
                    checkedItems.Add(item.Name, item.Text);
                });

                if (checkedItems.Count == 0)
                    return;

                if (MessageBox.Show(this, $"Weet u zeker dat u de volgende windows processen wilt stoppen? {Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, checkedItems.Values)}", "Zeker weten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    return;

                AWSEc2WindowsExec.KillWindowsTask(_domein, _environment, cbSnapshotTaskEC2Ids.Text, checkedItems, $"AwsEc2WindowsTaskOperator ({cbSnapshotTaskEC2Ids.Text}>Kill process(s))=> {AWSCredentialKeeper.AWSOperatorUser}");
                btnRefreshTasksnapshot_Click(sender, e);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void btnRefreshTasksnapshot_Click(object sender, EventArgs e)
        {
            try
            {
                UseWaitCursor = true;

                cbSnapshotTaskEC2Ids.DataSource = null;
                txtSnapshotDateTime.Text = string.Empty;
                lvTasksOnInstance.Items.Clear();

                Application.DoEvents();
                if (string.IsNullOrEmpty(cbPurposeIdTag.Text))
                    return;

                if (!_activeEc2PurposeIdList.ContainsKey(cbPurposeIdTag.Text))
                    return;

                List<string> requestFromInstances = new List<string>();
                requestFromInstances.AddRange(lbTagInstanceId.Items.Cast<string>());

                var result = AWSEc2WindowsExec.GetTaskSnapshot(_domein, _environment, requestFromInstances);
                if (result == null)
                    return;

                _activeEc2TaskSnapshot = result;
                cbSnapshotTaskEC2Ids.DataSource = result.Keys.ToList();

                txtSnapshotDateTime.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            }
            finally
            {
                UseWaitCursor = false;
            }
        }
    }
}
