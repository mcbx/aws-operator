﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AwsOperator.Operators
{
    public partial class AwsS3Operator : FormTemplate
    {
        Dictionary<string, S3Bucket> _currentBuckets = new Dictionary<string, S3Bucket>();
        bool _filterActive = false;

        public AwsS3Operator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();

            Text = $"Operator: S3, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";
            _currentBuckets = AwsS3Operations.GetS3Buckets(_domein, _environment);
            cbS3Buckets.DataSource = _currentBuckets.Keys.ToList();
        }

        #region Check items

        private bool AnyChildChecked(TreeNode currentNode)
        {
            bool res = false;
            foreach (TreeNode node in currentNode.Nodes)
            {
                res = node.Checked;
                if (res) break;

                res = this.AnyChildChecked(node);
                if (res) break;
            }

            return res;
        }

        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        private void TvBucketStructure_AfterCheck(object sender, TreeViewEventArgs e)
        {
            UseWaitCursor = true;
            Cursor = Cursors.WaitCursor;

            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    /* Calls the CheckAllChildNodes method, passing in the current 
                    Checked value of the TreeNode whose checked state changed. */
                    this.CheckAllChildNodes(e.Node, e.Node.Checked);
                }
            }

            if (e.Node.Parent != null)
                e.Node.Parent.Checked = AnyChildChecked(e.Node.Parent);

            UseWaitCursor = false;
            Cursor = Cursors.Default;
        }

        #endregion

        private void CbS3Buckets_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UseWaitCursor = true;
                Cursor = Cursors.WaitCursor;

                tvBucketStructure.Nodes.Clear();
                tvBucketStructure.DisplayedNodes.Clear();

                if (!_currentBuckets.ContainsKey(cbS3Buckets.Text))
                    return;

                AwsS3Operations.ListItemsFromS3Bucket(_currentBuckets[cbS3Buckets.Text], _domein, _environment);

                foreach (KeyValuePair<string, S3BucketItem> item in _currentBuckets[cbS3Buckets.Text].Items)
                {
                    TreeNode node = item.Value.ToTreeNode(ckShowNotDownloadable.Checked);
                    if (node != null)
                        tvBucketStructure.Nodes.Add(node);
                }

                tvBucketStructure.Sync();
                tvBucketStructure.DisplayedNodes[0].Expand();

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, $"Sorry it seems we cant access the S3 bucket called '{cbS3Buckets.Text}'", "Oops..", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void BtnRefreshS3Buckets_Click(object sender, EventArgs e)
        {
            _currentBuckets = AwsS3Operations.GetS3Buckets(_domein, _environment);
            cbS3Buckets.Text = string.Empty;
            cbS3Buckets.DataSource = _currentBuckets.Keys.ToList();
        }

        //private void TxtFilterText_TextChanged(object sender, EventArgs e)
        //{
        //    UseWaitCursor = true;
        //    Cursor = Cursors.WaitCursor;

        //    if (txtFilterText.Text.Length == 0)
        //    {
        //        tvBucketStructure.ResetFilter();
        //    }

        //    if (txtFilterText.Text.Length > 2)
        //    {
        //        tvBucketStructure.FilterNodes(txtFilterText.Text);
        //    }

        //    UseWaitCursor = false;
        //    Cursor = Cursors.Default;
        //}

        private void BtnDownloadSelected_Click(object sender, EventArgs e)
        {
            lblDownLoadStatus.ForeColor = SystemColors.ControlText;
            lblDownLoadStatus.Text = string.Empty;

            if (!_currentBuckets.ContainsKey(cbS3Buckets.Text))
                return;

            if (string.IsNullOrEmpty(txtBaseSavePath.Text))
                return;

            if (!System.IO.Directory.Exists(txtBaseSavePath.Text))
                return;

            var selecteFilesCount = tvBucketStructure.DisplayedNodes.Descendants()
                    .Where(n => n.Checked && n.Tag is S3BucketItem && !((S3BucketItem)n.Tag).IsFolder && !string.IsNullOrEmpty(((S3BucketItem)n.Tag).FilePath) && n.ForeColor != Color.LightSalmon)
                    .Count();

            if (selecteFilesCount == 0)
            {
                MessageBox.Show(this, $"Sorry maar er is geen enkele downloadbaar bestand geselecteerd.", "Oops...", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            if (MessageBox.Show(this, $"Weet u zeker dat u '{selecteFilesCount}' bestanden wilt downloaden?{Environment.NewLine}(Bestanden waarvan de tekst rood is, worden NIET gedownload!){Environment.NewLine}{Environment.NewLine} Als de taak eenmaal gestart is kan deze niet worden gestopt!", "Confirm....", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;

            var bucket = _currentBuckets[cbS3Buckets.Text];
            btnDownloadSelected.Enabled = false;
            Task.Run(() =>
            {
                try
                {
                    var selectedFiles = tvBucketStructure.DisplayedNodes.Descendants()
                                        .Where(n => n.Checked && n.Tag is S3BucketItem && !((S3BucketItem)n.Tag).IsFolder && !string.IsNullOrEmpty(((S3BucketItem)n.Tag).FilePath) && n.ForeColor != Color.LightSalmon)
                                        .Select(n => (S3BucketItem)n.Tag)
                                        .ToList();

                    selectedFiles.ForEach(itm => System.Diagnostics.Debug.WriteLine(itm));

                    int filePosCounter = 1;
                    foreach (S3BucketItem sItem in selectedFiles)
                    {
                        lblDownLoadStatus.BeginInvoke(new MethodInvoker(() =>
                        {
                            lblDownLoadStatus.Text = $"DOWNLOADING FILE ({filePosCounter} of {selectedFiles.Count}) => {sItem.Name}....";
                        }));

                        AwsS3Operations.SaveS3ItemsToDisk(bucket, sItem, txtBaseSavePath.Text, _domein, _environment);
                        filePosCounter++;
                    }

                    lblDownLoadStatus.BeginInvoke(new MethodInvoker(() =>
                    {
                        lblDownLoadStatus.Text = $"DOWNLOADING COMPLETE....";
                    }));
                }
                catch (Exception ex)
                {
                    lblDownLoadStatus.BeginInvoke(new MethodInvoker(() =>
                    {
                        lblDownLoadStatus.Text = $"DOWNLOADING FAILED!";
                    }));
                }
                finally
                {
                    btnDownloadSelected.BeginInvoke(new MethodInvoker(() =>
                    {
                        btnDownloadSelected.Enabled = true;
                    }));
                }
            });
        }

        private void BtnSelectBaseSaveFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.SelectedPath = string.Empty;
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK)
                return;

            txtBaseSavePath.Text = folderBrowserDialog.SelectedPath;
        }

        private void ckShowNotDownloadable_CheckedChanged(object sender, EventArgs e)
        {
            CbS3Buckets_TextChanged(this, null);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtFilterText.Text = string.Empty;

            if (!_filterActive)
                return;

            tvBucketStructure.FilterNodes(string.Empty);
            _filterActive = false;
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (txtFilterText.Text.Length < 3)
            {
                MessageBox.Show(this, "Sorry. A filter submission should have at least 3 characters.", "Oops....", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            UseWaitCursor = true;
            Cursor = Cursors.WaitCursor;

            tvBucketStructure.FilterNodes(txtFilterText.Text);
            _filterActive = true;

            UseWaitCursor = false;
            Cursor = Cursors.Default;
        }

        private void txtFilterText_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFilter_Click(sender, null);
        }

        private void btnRefreshS3items_Click(object sender, EventArgs e)
        {
            CbS3Buckets_TextChanged(this, null);
        }
    }
}

namespace Fh.Aws.Framework.AWSOperationClassContainers
{
    public static class S3BucketItemExtension
    {
        public static TreeNode ToTreeNode(this S3BucketItem itm, bool showNotDownloadable)
        {
            TreeNode node = new TreeNode(itm.Name, itm.IsFolder ? 0 : 1, itm.IsFolder ? 0 : 1) { Name = $"{itm.Name}{(!itm.IsFolder && itm.LastModified != null && itm.LastModified != DateTime.MinValue ? $"({ itm.LastModified.ToString("dd-MM-yyyy HH:mm") })": "" )}", Tag = itm, Text = $"{itm.Name}{(!itm.IsFolder && itm.LastModified != null && itm.LastModified != DateTime.MinValue ? $" ({itm.LastModified.ToString("dd-MM-yyyy HH:mm")})": "" )}" };
            if (showNotDownloadable && !itm.Downloadable && !itm.IsFolder)
                node.ForeColor = Color.LightSalmon;

            if (!showNotDownloadable && !itm.Downloadable && !itm.IsFolder)
                return null;

            if (itm.Size == -1)
            {
                node.ImageIndex = 2;
                node.SelectedImageIndex = 2;
            }

            foreach (KeyValuePair<string, S3BucketItem> subItem in itm.SubItems.OrderByDescending(item => item.Value.IsFolder).ThenBy(item => item.Value.Name))
            {
                TreeNode subNode = subItem.Value.ToTreeNode(showNotDownloadable);
                if (subNode != null)
                    node.Nodes.Add(subNode);
            }
            return node;
        }
    }
}
