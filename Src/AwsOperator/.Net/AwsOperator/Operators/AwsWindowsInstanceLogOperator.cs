﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class AwsWindowsInstanceLogOperator : FormTemplate
    {
        private const string logOwner = "AwsWindowsInstanceLogOperator";

        private Dictionary<string, string> _currentEc2List = new Dictionary<string, string>();
        private Dictionary<string, string> _activeLogFiles = new Dictionary<string, string>();

        private string _outputDir = string.Empty;

        public AwsWindowsInstanceLogOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            _outputDir = ConfigurationManager.AppSettings["LogFileOutputFolder"];

            if (!Directory.Exists(_outputDir))
                Directory.CreateDirectory(_outputDir);

            InitializeComponent();
            Text = $"Operator: AWS WindowsEvent Log, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";

            _currentEc2List = AWSEc2.GetEc2AppList(_domein, _environment, _appCode);
            cboEc2Instance.DataSource = _currentEc2List.Values.Distinct().ToArray();
        }

        private void cbFetchFromAlls_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvItem in lvEventLogStreams.Items)
            {
                lvItem.Checked = cbFetchFromAll.Checked;
            }
        }

        private void dtFromTime_ValueChanged(object sender, EventArgs e)
        {
            if (dtToTime.Value <= dtFromTime.Value)
                dtToTime.Value = dtFromTime.Value.AddMinutes(1);
        }

        private void dtToTime_ValueChanged(object sender, EventArgs e)
        {
            if (dtFromTime.Value >= dtToTime.Value)
                dtFromTime.Value = dtToTime.Value.AddMinutes(-1);
        }

        private async void btnGetLog_Click(object sender, EventArgs e)
        {
            UseWaitCursor = false;
            Cursor = Cursors.WaitCursor;

            try
            {
                btnGetLog.Enabled = false;
                btnDeleteLogFiles.Enabled = false;
                cbChooseLogStream.DataSource = null;
                DateTime filterDateTimeFrom = DateTime.MinValue;
                DateTime filterDateTimeTo = DateTime.MinValue;

                if (ckFilterPreviousMinutes.Checked)
                {
                    var filterDateTime = DateTime.Now;
                    filterDateTimeFrom = filterDateTime.AddMinutes(-((int)nudLastMinutes.Value)).AddSeconds(-filterDateTime.Second);
                    filterDateTimeTo = filterDateTime.AddSeconds(-filterDateTime.Second);
                }
                else
                {

                    if (ckFilterOnDay.Checked)
                    {
                        filterDateTimeFrom = dtDate.Value.Date;
                        filterDateTimeTo = dtDate.Value.Date.AddDays(1).AddSeconds(-1);
                    }

                    if (ckFilterOnTime.Checked)
                    {
                        if (ckFilterOnDay.Checked)
                        {
                            filterDateTimeFrom = filterDateTimeFrom.Date.AddHours(dtFromTime.Value.Hour).AddMinutes(dtFromTime.Value.Minute);
                            filterDateTimeTo = filterDateTimeTo.Date.AddHours(dtToTime.Value.Hour).AddMinutes(dtToTime.Value.Minute);
                        }
                        else
                        {
                            DateTime now = DateTime.Now;
                            filterDateTimeFrom = now.Date.AddHours(dtFromTime.Value.Hour).AddMinutes(dtFromTime.Value.Minute);
                            filterDateTimeTo = now.Date.AddHours(dtToTime.Value.Hour).AddMinutes(dtToTime.Value.Minute);
                        }

                        filterDateTimeFrom = filterDateTimeFrom.ToUniversalTime();
                        filterDateTimeTo = filterDateTimeTo.ToUniversalTime();
                    }
                }

                List<string> checkedLogStreams = new List<string>();
                checkedLogStreams.AddRange(lvEventLogStreams.CheckedItems
                                     .Cast<ListViewItem>()
                                     .Select(x => x.Name));

                Dictionary<string, string> result = await AWSCloudwatchLog.SaveLogEntriesToFileByInstanceEventLog(_domein, _environment,
                    filterDateTimeFrom, filterDateTimeTo, checkedLogStreams, _outputDir, logOwner);

                _activeLogFiles = result;
                cbChooseLogStream.DataSource = result.Keys.ToList();
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
                btnGetLog.Enabled = true;
                btnDeleteLogFiles.Enabled = true;
            }
        }

        private void cboEc2Instance_TextChanged(object sender, EventArgs e)
        {
            lvEventLogStreams.Items.Clear();
            _currentEc2List.Where(item => item.Value == cboEc2Instance.Text).ToList().ForEach(item =>
            {
                var result = AWSCloudwatchLog.GetWindowsLogStreamsByInstanceId(item.Key, _domein, _environment);
                foreach (KeyValuePair<string, LogStreamItem> logStreamItem in result.OrderBy(logStreamItem => logStreamItem.Value.LogStreamName).ThenByDescending(logStreamItem => logStreamItem.Value.LastEventTimestamp))
                {
                    var splitItems = logStreamItem.Key.Split('/');
                    if (splitItems.GetLength(0) < 2 || !splitItems[0].ToLower().StartsWith("i-"))
                        continue;

                    lvEventLogStreams.Items.Add(logStreamItem.Key, splitItems[0], string.Empty);

                    string prefix = $"{splitItems[0]}/";
                    if (splitItems.GetLength(0) > 1)
                        lvEventLogStreams.Items[logStreamItem.Key].SubItems.Add(logStreamItem.Key.Replace(prefix, string.Empty));

                    lvEventLogStreams.Items[logStreamItem.Key].SubItems.Add(logStreamItem.Value.LastEventTimestamp.ToString());
                }
            });
        }

        private void cbChooseLogStream_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UseWaitCursor = false;
                Cursor = Cursors.WaitCursor;

                logText.Text = string.Empty;
                if (_activeLogFiles.ContainsKey(cbChooseLogStream.Text))
                {
                    using (StreamReader reader = new StreamReader(_activeLogFiles[cbChooseLogStream.Text]))
                    {
                        logText.Text = reader.ReadToEnd();
                    }
                }
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void ckFilterPreviousMinutes_Click(object sender, EventArgs e)
        {
            ckFilterOnTime.Checked = false;
            ckFilterOnDay.Checked = false;
        }

        private void ckFilterOnDay_Click(object sender, EventArgs e)
        {
            ckFilterPreviousMinutes.Checked = false;
        }

        private void ckFilterOnTime_Click(object sender, EventArgs e)
        {
            ckFilterPreviousMinutes.Checked = false;
        }

        private void BtnDeleteLogFiles_Click(object sender, EventArgs e)
        {
            var logFiles = Directory.GetFiles(_outputDir, $"{logOwner}-*.log", SearchOption.TopDirectoryOnly);
            if (logFiles.GetLength(0) == 0)
                return;

            if (MessageBox.Show(this, $"Are you sure you want delete all ({logFiles.GetLength(0)}) the temporary file(s) for this operator? ", "Are you sure....?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                return;

            foreach (string logFileItem in logFiles)
            {
                System.Diagnostics.Debug.WriteLine(logFileItem);
                File.Delete(logFileItem);
            }
        }
    }
}
