﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class AwsParameterOperator : FormTemplate
    {
        private Dictionary<string, List<ParameterDetail>> _currentParameters = new Dictionary<string, List<ParameterDetail>>();

        public AwsParameterOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();
            Text = $"Operator: AWS Parameter, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";

            _currentParameters = AWSParameterStore.GetParametersByApplicationCode(_appCode, _domein, _environment);
            cbApplication.DataSource = _currentParameters.Keys.ToArray();
        }

        private void cbApplication_TextChanged(object sender, EventArgs e)
        {
            lvParameterHistory.Items.Clear();
            gbUpdateParameter.Visible = false;
            gbParameterHistory.Visible = false;

            lvParameters.Items.Clear();
            if (!_currentParameters.ContainsKey(cbApplication.Text))
            {
                cbApplication.Text = string.Empty;
                return;
            }

            if (string.IsNullOrEmpty(cbApplication.Text))
                return;

            foreach (ParameterDetail item in _currentParameters[cbApplication.Text].OrderBy(item => item.Name))
            {
                ListViewItem lvItem = new ListViewItem() { Name = item.FullPath, Text = item.Name };
                lvItem.SubItems.Add(item.Value);
                lvItem.SubItems.Add(item.Secured.ToString());
                lvItem.SubItems.Add(item.Version.ToString());

                lvParameters.Items.Add(lvItem);
            }
        }

        private void btnRefreshParameterList_Click(object sender, EventArgs e)
        {
            _currentParameters = AWSParameterStore.GetParametersByApplicationCode(_appCode, _domein, _environment);
            cbApplication.DataSource = _currentParameters.Keys.ToArray();
            lvParameters.Items.Clear();
            lvParameterHistory.Items.Clear();
            gbUpdateParameter.Visible = false;
            gbParameterHistory.Visible = false;
        }

        private void btnUpdateParameter_Click(object sender, EventArgs e)
        {
            var selectedApp = cbApplication.Text;
            ParameterDetail updateResult = AWSParameterStore.UpdateParameter(new ParameterDetail()
            {
                FullPath = txtParameterName.Text,
                Name = txtHiddenParameterName.Text,
                Value = txtParameterValue.Text,
                Secured = ckParameterEncrypted.Checked,
                Version = long.Parse(txtParameterVersion.Text)
            }, _domein, _environment);

            if (updateResult == null)
            {
                MessageBox.Show(this, "Update mislukt.....", "Oops......", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            _currentParameters = AWSParameterStore.GetParametersByApplicationCode(_appCode, _domein, _environment);
            cbApplication.DataSource = _currentParameters.Keys.ToArray();
            cbApplication_TextChanged(this, null);
            gbUpdateParameter.Visible = false;
            gbParameterHistory.Visible = false;
            lvParameterHistory.Items.Clear();

            cbApplication.Text = selectedApp;
        }

        private void lvParameters_DoubleClick(object sender, EventArgs e)
        {
            lvParameterHistory.Items.Clear();
            if (lvParameters.SelectedItems.Count != 1)
            {
                gbParameterHistory.Visible = false;
                return;
            }

            ParameterDetail historyRequest = new ParameterDetail()
            {
                FullPath = lvParameters.SelectedItems[0].Name,
                Name = lvParameters.SelectedItems[0].Text
            };

            var historyResult = AWSParameterStore.GetParameterHistory(historyRequest, _domein, _environment);
            if (historyResult.Count == 0)
            {
                gbParameterHistory.Visible = false;
                return;
            }

            foreach (KeyValuePair<long, ParameterHistoryDetail> historyItem in historyResult.OrderByDescending(item => item.Key))
            {
                lvParameterHistory.Items.Add($"{historyItem.Value.FullPath}_{historyItem.Value.Version}", historyItem.Value.Version.ToString(), string.Empty);
                lvParameterHistory.Items[$"{historyItem.Value.FullPath}_{historyItem.Value.Version}"].SubItems.Add(historyItem.Value.Value);
                lvParameterHistory.Items[$"{historyItem.Value.FullPath}_{historyItem.Value.Version}"].SubItems.Add(historyItem.Value.Secured.ToString());
                lvParameterHistory.Items[$"{historyItem.Value.FullPath}_{historyItem.Value.Version}"].SubItems.Add(historyItem.Value.Labels.Count == 0 ? string.Empty : string.Join(",", historyItem.Value.Labels));
                lvParameterHistory.Items[$"{historyItem.Value.FullPath}_{historyItem.Value.Version}"].SubItems.Add(historyItem.Value.ModifiedBy.ToString());
                lvParameterHistory.Items[$"{historyItem.Value.FullPath}_{historyItem.Value.Version}"].SubItems.Add(historyItem.Value.ModifiedOn.ToString());
                lvParameterHistory.Items[$"{historyItem.Value.FullPath}_{historyItem.Value.Version}"].SubItems.Add(historyItem.Value.FullPath);
                lvParameterHistory.Items[$"{historyItem.Value.FullPath}_{historyItem.Value.Version}"].SubItems.Add(historyItem.Value.Name);
            }
            gbParameterHistory.Visible = true;
        }

        private void btnRollBackParameter_Click(object sender, EventArgs e)
        {
            if (lvParameterHistory.SelectedItems.Count != 1)
                return;

            if (MessageBox.Show(this, $"Weet u zeker dat u de parameter waarden wilt terugrollen naar versie '{lvParameterHistory.SelectedItems[0].Text}'?", "Zeker weten???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var selectedApp = cbApplication.Text;

                ParameterDetail updateResult = AWSParameterStore.UpdateParameter(new ParameterDetail()
                {
                    FullPath = lvParameterHistory.SelectedItems[0].SubItems[6].Text,
                    Name = lvParameterHistory.SelectedItems[0].SubItems[7].Text,
                    Value = lvParameterHistory.SelectedItems[0].SubItems[1].Text,
                    Secured = bool.Parse(lvParameterHistory.SelectedItems[0].SubItems[2].Text),
                    Version = long.Parse(lvParameterHistory.SelectedItems[0].Text)
                }, _domein, _environment);

                if (updateResult == null)
                {
                    MessageBox.Show(this, "Update mislukt.....", "Oops......", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }

                _currentParameters = AWSParameterStore.GetParametersByApplicationCode(_appCode, _domein, _environment);
                cbApplication.DataSource = _currentParameters.Keys.ToArray();
                cbApplication_TextChanged(this, null);
                gbUpdateParameter.Visible = false;
                gbParameterHistory.Visible = false;
                lvParameterHistory.Items.Clear();

                cbApplication.Text = selectedApp;
            }
        }

        private void btnDeleteParameter_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, $"Weet u zeker dat u de volgende parameter '{txtParameterName.Text}' wilt verwijderen?{Environment.NewLine}Dit kan niet ongedaan gemaakt worden!", "Zeker weten???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var selectedApp = cbApplication.Text;

                if (!AWSParameterStore.DeleteParameter(txtParameterName.Text, _domein, _environment))
                {
                    MessageBox.Show(this, "Verwijderen mislukt.....", "Oops......", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    return;
                }

                _currentParameters = AWSParameterStore.GetParametersByApplicationCode(_appCode, _domein, _environment);
                cbApplication.DataSource = _currentParameters.Keys.ToArray();
                cbApplication_TextChanged(this, null);
                gbUpdateParameter.Visible = false;
                gbParameterHistory.Visible = false;
                lvParameterHistory.Items.Clear();

                if (cbApplication.Items.Contains(selectedApp))
                    cbApplication.Text = selectedApp;
            }
        }

        private bool DeleteParameter(string parameterPath)
        {
            if (!AWSParameterStore.DeleteParameter(parameterPath, _domein, _environment))
            {
                MessageBox.Show(this, "Verwijderen mislukt.....", "Oops......", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }

            return true;
        }

        private void deleteSelectedParametersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (lvParameters.SelectedItems.Count == 0)
                return;

            var parameters = lvParameters.SelectedItems.OfType<ListViewItem>().Select(item => item.Name);
            if (MessageBox.Show(this, $"Weet u zeker dat u de geselecteerde parameter(s) wilt verwijderen?{Environment.NewLine}Dit kan niet ongedaan gemaakt worden!{Environment.NewLine}{Environment.NewLine}{string.Join(Environment.NewLine, parameters)}", "Zeker weten???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var selectedApp = cbApplication.Text;
                foreach (string parameterPath in parameters)
                {
                    System.Diagnostics.Debug.WriteLine(parameterPath);
                    DeleteParameter(parameterPath);
                }

                _currentParameters = AWSParameterStore.GetParametersByApplicationCode(_appCode, _domein, _environment);
                cbApplication.DataSource = _currentParameters.Keys.ToArray();
                cbApplication_TextChanged(this, null);
                gbUpdateParameter.Visible = false;
                gbParameterHistory.Visible = false;
                lvParameterHistory.Items.Clear();

                if (cbApplication.Items.Contains(selectedApp))
                    cbApplication.Text = selectedApp;
            }
        }

        private void LvParameters_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (!e.IsSelected && lvParameters.SelectedItems.Count > 1)
                return;

            if (lvParameters.SelectedItems.Count == 0)
            {
                lvParameterHistory.Items.Clear();
                gbUpdateParameter.Visible = false;
                gbParameterHistory.Visible = false;
                return;
            }

            if (lvParameters.SelectedItems.Count != 1)
            {
                lvParameterHistory.Items.Clear();
                gbUpdateParameter.Visible = false;
                gbParameterHistory.Visible = false;
                return;
            }

            int index = lvParameters.SelectedItems[0].Index;
            txtHiddenParameterName.Text = lvParameters.Items[index].Text;
            txtParameterName.Text = lvParameters.Items[index].Name;
            txtParameterVersion.Text = lvParameters.Items[index].SubItems[3].Text;
            txtParameterValue.Text = lvParameters.Items[index].SubItems[1].Text;
            ckParameterEncrypted.Checked = bool.Parse(lvParameters.Items[index].SubItems[2].Text);

            gbUpdateParameter.Visible = true;
        }

        private void CmParameterOperations_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            deleteSelectedParametersToolStripMenuItem1.Enabled = lvParameters.SelectedItems.Count > 0;
        }

        private void deleteallParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvParameters.Items.Count == 0)
                return;

            if (MessageBox.Show(this, $"Weet u zeker dat u de alle parameter(s) van de applicatie wilt verwijderen?{Environment.NewLine}Dit kan niet ongedaan gemaakt worden!", "Zeker weten???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var parameters = lvParameters.Items.OfType<ListViewItem>().Select(item => item.Name);
                foreach (string parameterPath in parameters)
                {
                    DeleteParameter(parameterPath);
                }

                _currentParameters = AWSParameterStore.GetParametersByApplicationCode(_appCode, _domein, _environment);
                cbApplication.DataSource = _currentParameters.Keys.ToArray();
                cbApplication_TextChanged(this, null);
                gbUpdateParameter.Visible = false;
                gbParameterHistory.Visible = false;
                lvParameterHistory.Items.Clear();
            }
        }
    }
}
