﻿using AwsOperator.General;

namespace AwsOperator.Operators
{
    partial class AwsS3Operator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AwsS3Operator));
            this.cbS3Buckets = new System.Windows.Forms.ComboBox();
            this.btnRefreshS3Buckets = new System.Windows.Forms.Button();
            this.tvBucketStructure = new AwsOperator.General.CTreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtFilterText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDownloadSelected = new System.Windows.Forms.Button();
            this.btnSelectBaseSaveFolder = new System.Windows.Forms.Button();
            this.txtBaseSavePath = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.lblDownLoadStatus = new System.Windows.Forms.Label();
            this.ckShowNotDownloadable = new System.Windows.Forms.CheckBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRefreshS3items = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbS3Buckets
            // 
            this.cbS3Buckets.FormattingEnabled = true;
            this.cbS3Buckets.Location = new System.Drawing.Point(12, 22);
            this.cbS3Buckets.Name = "cbS3Buckets";
            this.cbS3Buckets.Size = new System.Drawing.Size(299, 21);
            this.cbS3Buckets.TabIndex = 0;
            this.cbS3Buckets.TextChanged += new System.EventHandler(this.CbS3Buckets_TextChanged);
            // 
            // btnRefreshS3Buckets
            // 
            this.btnRefreshS3Buckets.Location = new System.Drawing.Point(12, 49);
            this.btnRefreshS3Buckets.Name = "btnRefreshS3Buckets";
            this.btnRefreshS3Buckets.Size = new System.Drawing.Size(299, 23);
            this.btnRefreshS3Buckets.TabIndex = 1;
            this.btnRefreshS3Buckets.Text = "Refresh S3Bucket list";
            this.btnRefreshS3Buckets.UseVisualStyleBackColor = true;
            this.btnRefreshS3Buckets.Click += new System.EventHandler(this.BtnRefreshS3Buckets_Click);
            // 
            // tvBucketStructure
            // 
            this.tvBucketStructure.CheckBoxes = true;
            this.tvBucketStructure.ImageIndex = 0;
            this.tvBucketStructure.ImageList = this.imageList1;
            this.tvBucketStructure.Location = new System.Drawing.Point(12, 188);
            this.tvBucketStructure.Name = "tvBucketStructure";
            this.tvBucketStructure.SelectedImageIndex = 0;
            this.tvBucketStructure.Size = new System.Drawing.Size(752, 364);
            this.tvBucketStructure.TabIndex = 2;
            this.tvBucketStructure.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.TvBucketStructure_AfterCheck);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Folder");
            this.imageList1.Images.SetKeyName(1, "File");
            this.imageList1.Images.SetKeyName(2, "Close.png");
            // 
            // txtFilterText
            // 
            this.txtFilterText.Location = new System.Drawing.Point(12, 98);
            this.txtFilterText.Name = "txtFilterText";
            this.txtFilterText.Size = new System.Drawing.Size(563, 20);
            this.txtFilterText.TabIndex = 4;
            this.txtFilterText.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilterText_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Filter:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "S3 Items";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "S3Bucket:";
            // 
            // btnDownloadSelected
            // 
            this.btnDownloadSelected.Location = new System.Drawing.Point(397, 50);
            this.btnDownloadSelected.Name = "btnDownloadSelected";
            this.btnDownloadSelected.Size = new System.Drawing.Size(365, 23);
            this.btnDownloadSelected.TabIndex = 8;
            this.btnDownloadSelected.Text = "Save checked items to disk";
            this.btnDownloadSelected.UseVisualStyleBackColor = true;
            this.btnDownloadSelected.Click += new System.EventHandler(this.BtnDownloadSelected_Click);
            // 
            // btnSelectBaseSaveFolder
            // 
            this.btnSelectBaseSaveFolder.Location = new System.Drawing.Point(727, 23);
            this.btnSelectBaseSaveFolder.Name = "btnSelectBaseSaveFolder";
            this.btnSelectBaseSaveFolder.Size = new System.Drawing.Size(35, 21);
            this.btnSelectBaseSaveFolder.TabIndex = 9;
            this.btnSelectBaseSaveFolder.Text = "...";
            this.btnSelectBaseSaveFolder.UseVisualStyleBackColor = true;
            this.btnSelectBaseSaveFolder.Click += new System.EventHandler(this.BtnSelectBaseSaveFolder_Click);
            // 
            // txtBaseSavePath
            // 
            this.txtBaseSavePath.Location = new System.Drawing.Point(397, 23);
            this.txtBaseSavePath.Name = "txtBaseSavePath";
            this.txtBaseSavePath.Size = new System.Drawing.Size(324, 20);
            this.txtBaseSavePath.TabIndex = 10;
            // 
            // lblDownLoadStatus
            // 
            this.lblDownLoadStatus.AutoSize = true;
            this.lblDownLoadStatus.Location = new System.Drawing.Point(183, 131);
            this.lblDownLoadStatus.Name = "lblDownLoadStatus";
            this.lblDownLoadStatus.Size = new System.Drawing.Size(0, 13);
            this.lblDownLoadStatus.TabIndex = 11;
            // 
            // ckShowNotDownloadable
            // 
            this.ckShowNotDownloadable.AutoSize = true;
            this.ckShowNotDownloadable.Location = new System.Drawing.Point(566, 165);
            this.ckShowNotDownloadable.Name = "ckShowNotDownloadable";
            this.ckShowNotDownloadable.Size = new System.Drawing.Size(198, 17);
            this.ckShowNotDownloadable.TabIndex = 12;
            this.ckShowNotDownloadable.Text = "Show Not Downloadable Files (slow)";
            this.ckShowNotDownloadable.UseVisualStyleBackColor = true;
            this.ckShowNotDownloadable.CheckedChanged += new System.EventHandler(this.ckShowNotDownloadable_CheckedChanged);
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(633, 98);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(129, 20);
            this.btnFilter.TabIndex = 13;
            this.btnFilter.Text = "Filter items";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = global::AwsOperator.Properties.Resources.Close;
            this.btnClear.Location = new System.Drawing.Point(602, 98);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(25, 20);
            this.btnClear.TabIndex = 14;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRefreshS3items
            // 
            this.btnRefreshS3items.Location = new System.Drawing.Point(63, 165);
            this.btnRefreshS3items.Name = "btnRefreshS3items";
            this.btnRefreshS3items.Size = new System.Drawing.Size(97, 23);
            this.btnRefreshS3items.TabIndex = 15;
            this.btnRefreshS3items.Text = "Refresh";
            this.btnRefreshS3items.UseVisualStyleBackColor = true;
            this.btnRefreshS3items.Click += new System.EventHandler(this.btnRefreshS3items_Click);
            // 
            // AwsS3Operator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 564);
            this.Controls.Add(this.btnRefreshS3items);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.ckShowNotDownloadable);
            this.Controls.Add(this.lblDownLoadStatus);
            this.Controls.Add(this.txtBaseSavePath);
            this.Controls.Add(this.btnSelectBaseSaveFolder);
            this.Controls.Add(this.btnDownloadSelected);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFilterText);
            this.Controls.Add(this.tvBucketStructure);
            this.Controls.Add(this.btnRefreshS3Buckets);
            this.Controls.Add(this.cbS3Buckets);
            this.Name = "AwsS3Operator";
            this.Text = "AwsS3Operator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbS3Buckets;
        private System.Windows.Forms.Button btnRefreshS3Buckets;
        private CTreeView tvBucketStructure;
        private System.Windows.Forms.TextBox txtFilterText;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDownloadSelected;
        private System.Windows.Forms.Button btnSelectBaseSaveFolder;
        private System.Windows.Forms.TextBox txtBaseSavePath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label lblDownLoadStatus;
        private System.Windows.Forms.CheckBox ckShowNotDownloadable;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnRefreshS3items;
    }
}