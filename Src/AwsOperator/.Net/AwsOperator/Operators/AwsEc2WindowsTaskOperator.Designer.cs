﻿namespace AwsOperator
{
    public partial class AwsEc2WindowsTaskOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbTasksOperations = new System.Windows.Forms.GroupBox();
            this.gbTaskSnapshotDetails = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSnapshotDateTime = new System.Windows.Forms.TextBox();
            this.cbSnapshotTaskEC2Ids = new System.Windows.Forms.ComboBox();
            this.btnStartServiceCK = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lvTasksOnInstance = new System.Windows.Forms.ListView();
            this.Process = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbTagInstanceId = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbPurposeIdTag = new System.Windows.Forms.ComboBox();
            this.btnRefreshTags = new System.Windows.Forms.Button();
            this.btnRefreshTasksnapshot = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbTasksOperations.SuspendLayout();
            this.gbTaskSnapshotDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbTasksOperations
            // 
            this.gbTasksOperations.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbTasksOperations.Controls.Add(this.label1);
            this.gbTasksOperations.Controls.Add(this.gbTaskSnapshotDetails);
            this.gbTasksOperations.Controls.Add(this.lbTagInstanceId);
            this.gbTasksOperations.Controls.Add(this.label12);
            this.gbTasksOperations.Controls.Add(this.label11);
            this.gbTasksOperations.Controls.Add(this.cbPurposeIdTag);
            this.gbTasksOperations.Controls.Add(this.btnRefreshTags);
            this.gbTasksOperations.Controls.Add(this.btnRefreshTasksnapshot);
            this.gbTasksOperations.Location = new System.Drawing.Point(12, 12);
            this.gbTasksOperations.Name = "gbTasksOperations";
            this.gbTasksOperations.Size = new System.Drawing.Size(629, 697);
            this.gbTasksOperations.TabIndex = 20;
            this.gbTasksOperations.TabStop = false;
            this.gbTasksOperations.Text = "Windows Processes";
            // 
            // gbTaskSnapshotDetails
            // 
            this.gbTaskSnapshotDetails.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbTaskSnapshotDetails.Controls.Add(this.label15);
            this.gbTaskSnapshotDetails.Controls.Add(this.txtSnapshotDateTime);
            this.gbTaskSnapshotDetails.Controls.Add(this.cbSnapshotTaskEC2Ids);
            this.gbTaskSnapshotDetails.Controls.Add(this.btnStartServiceCK);
            this.gbTaskSnapshotDetails.Controls.Add(this.label13);
            this.gbTaskSnapshotDetails.Controls.Add(this.lvTasksOnInstance);
            this.gbTaskSnapshotDetails.Location = new System.Drawing.Point(6, 220);
            this.gbTaskSnapshotDetails.Name = "gbTaskSnapshotDetails";
            this.gbTaskSnapshotDetails.Size = new System.Drawing.Size(610, 471);
            this.gbTaskSnapshotDetails.TabIndex = 20;
            this.gbTaskSnapshotDetails.TabStop = false;
            this.gbTaskSnapshotDetails.Text = "EC2 Service snapshot details";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(117, 13);
            this.label15.TabIndex = 27;
            this.label15.Text = "Snapshot gemaakt op: ";
            // 
            // txtSnapshotDateTime
            // 
            this.txtSnapshotDateTime.Location = new System.Drawing.Point(165, 41);
            this.txtSnapshotDateTime.Name = "txtSnapshotDateTime";
            this.txtSnapshotDateTime.ReadOnly = true;
            this.txtSnapshotDateTime.Size = new System.Drawing.Size(439, 20);
            this.txtSnapshotDateTime.TabIndex = 26;
            // 
            // cbSnapshotTaskEC2Ids
            // 
            this.cbSnapshotTaskEC2Ids.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSnapshotTaskEC2Ids.FormattingEnabled = true;
            this.cbSnapshotTaskEC2Ids.Location = new System.Drawing.Point(165, 85);
            this.cbSnapshotTaskEC2Ids.Name = "cbSnapshotTaskEC2Ids";
            this.cbSnapshotTaskEC2Ids.Size = new System.Drawing.Size(439, 21);
            this.cbSnapshotTaskEC2Ids.TabIndex = 20;
            this.cbSnapshotTaskEC2Ids.TextChanged += new System.EventHandler(this.cbSnapshotServiceEC2Ids_TextChanged);
            // 
            // btnStartServiceCK
            // 
            this.btnStartServiceCK.Location = new System.Drawing.Point(6, 442);
            this.btnStartServiceCK.Name = "btnStartServiceCK";
            this.btnStartServiceCK.Size = new System.Drawing.Size(153, 23);
            this.btnStartServiceCK.TabIndex = 28;
            this.btnStartServiceCK.Text = "Terminate process(es)";
            this.btnStartServiceCK.UseVisualStyleBackColor = true;
            this.btnStartServiceCK.Click += new System.EventHandler(this.btnStartServiceCK_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "EC2 instance:";
            // 
            // lvTasksOnInstance
            // 
            this.lvTasksOnInstance.CheckBoxes = true;
            this.lvTasksOnInstance.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Process,
            this.columnHeader2,
            this.columnHeader3});
            this.lvTasksOnInstance.FullRowSelect = true;
            this.lvTasksOnInstance.Location = new System.Drawing.Point(165, 115);
            this.lvTasksOnInstance.Name = "lvTasksOnInstance";
            this.lvTasksOnInstance.Size = new System.Drawing.Size(439, 350);
            this.lvTasksOnInstance.TabIndex = 27;
            this.lvTasksOnInstance.UseCompatibleStateImageBehavior = false;
            this.lvTasksOnInstance.View = System.Windows.Forms.View.Details;
            // 
            // Process
            // 
            this.Process.Text = "Process";
            this.Process.Width = 158;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "User";
            this.columnHeader2.Width = 160;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ProcessId";
            this.columnHeader3.Width = 111;
            // 
            // lbTagInstanceId
            // 
            this.lbTagInstanceId.FormattingEnabled = true;
            this.lbTagInstanceId.Location = new System.Drawing.Point(171, 67);
            this.lbTagInstanceId.Name = "lbTagInstanceId";
            this.lbTagInstanceId.Size = new System.Drawing.Size(445, 82);
            this.lbTagInstanceId.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Instances:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "PurposeId Tag:";
            // 
            // cbPurposeIdTag
            // 
            this.cbPurposeIdTag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPurposeIdTag.FormattingEnabled = true;
            this.cbPurposeIdTag.Location = new System.Drawing.Point(171, 39);
            this.cbPurposeIdTag.Name = "cbPurposeIdTag";
            this.cbPurposeIdTag.Size = new System.Drawing.Size(445, 21);
            this.cbPurposeIdTag.TabIndex = 16;
            this.cbPurposeIdTag.TextChanged += new System.EventHandler(this.cbPurposeIdTag_TextChanged);
            // 
            // btnRefreshTags
            // 
            this.btnRefreshTags.Location = new System.Drawing.Point(171, 162);
            this.btnRefreshTags.Name = "btnRefreshTags";
            this.btnRefreshTags.Size = new System.Drawing.Size(445, 23);
            this.btnRefreshTags.TabIndex = 4;
            this.btnRefreshTags.Text = "Refresh available EC2";
            this.btnRefreshTags.UseVisualStyleBackColor = true;
            this.btnRefreshTags.Click += new System.EventHandler(this.btnRefreshTags_Click);
            // 
            // btnRefreshTasksnapshot
            // 
            this.btnRefreshTasksnapshot.Location = new System.Drawing.Point(171, 191);
            this.btnRefreshTasksnapshot.Name = "btnRefreshTasksnapshot";
            this.btnRefreshTasksnapshot.Size = new System.Drawing.Size(445, 23);
            this.btnRefreshTasksnapshot.TabIndex = 5;
            this.btnRefreshTasksnapshot.Text = "Refresh process snapshot";
            this.btnRefreshTasksnapshot.UseVisualStyleBackColor = true;
            this.btnRefreshTasksnapshot.Click += new System.EventHandler(this.btnRefreshTasksnapshot_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(326, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Let op: Toont alleen processen welke begint met:     \" Fh.* \"";
            // 
            // AwsEc2WindowsTaskOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 723);
            this.Controls.Add(this.gbTasksOperations);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(672, 762);
            this.Name = "AwsEc2WindowsTaskOperator";
            this.Text = "AwsEc2WindowsTaskOperator";
            this.gbTasksOperations.ResumeLayout(false);
            this.gbTasksOperations.PerformLayout();
            this.gbTaskSnapshotDetails.ResumeLayout(false);
            this.gbTaskSnapshotDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbTasksOperations;
        private System.Windows.Forms.GroupBox gbTaskSnapshotDetails;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSnapshotDateTime;
        private System.Windows.Forms.ComboBox cbSnapshotTaskEC2Ids;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbPurposeIdTag;
        private System.Windows.Forms.ListBox lbTagInstanceId;
        private System.Windows.Forms.Button btnRefreshTags;
        private System.Windows.Forms.Button btnRefreshTasksnapshot;
        private System.Windows.Forms.ListView lvTasksOnInstance;
        private System.Windows.Forms.ColumnHeader Process;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnStartServiceCK;
        private System.Windows.Forms.Label label1;
    }
}