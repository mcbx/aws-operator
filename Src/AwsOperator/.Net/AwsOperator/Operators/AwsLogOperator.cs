﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AwsOperator
{
    public partial class AwsLogOperator : FormTemplate
    {
        private const string logOwner = "AwsLogOperator";

        private ColumnHeader SortingColumn = null;
        private Dictionary<string, string> _activeLambdaLogGroups = new Dictionary<string, string>();
        private Dictionary<string, string> _activeLogFiles = new Dictionary<string, string>();

        private string _outputDir = string.Empty;

        public AwsLogOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            _outputDir = ConfigurationManager.AppSettings["LogFileOutputFolder"];

            if (!Directory.Exists(_outputDir))
                Directory.CreateDirectory(_outputDir);

            InitializeComponent();
            Text = $"Operator: Cloudwatch application log, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";

            List<string> applicationDataSource = new List<string>();
            try
            {
                _activeLambdaLogGroups = AWSCloudwatchLog.GetLambdaLogGroupsByAppCode(_appCode, _domein, _environment);
                applicationDataSource.AddRange(_activeLambdaLogGroups.Keys.ToArray());
            }
            catch (Exception)
            {

            }

            try
            {
                applicationDataSource.AddRange(AWSCloudwatchLog.GetApplicationLogNamesByAppCode(_appCode, _domein, _environment));
            }
            catch (Exception)
            {
            }

            cboApplication.DataSource = applicationDataSource.OrderBy(item => item).ToList();

        }

        private async void btnGetLog_Click(object sender, EventArgs e)
        {
            UseWaitCursor = true;
            Cursor = Cursors.WaitCursor;

            try
            {
                btnGetLog.Enabled = false;
                btnDeleteLogFiles.Enabled = false;
                cbChooseLogStream.DataSource = null;
                DateTime filterDateTimeFrom = DateTime.MinValue;
                DateTime filterDateTimeTo = DateTime.MinValue;

                if (ckFilterPreviousMinutes.Checked)
                {
                    var filterDateTime = DateTime.Now;
                    filterDateTimeFrom = filterDateTime.AddMinutes(-((int)nudLastMinutes.Value)).AddSeconds(-filterDateTime.Second);
                    filterDateTimeTo = filterDateTime.AddSeconds(-filterDateTime.Second);
                }
                else
                {
                    if (ckFilterOnDay.Checked)
                    {
                        filterDateTimeFrom = dateTimePicker1.Value.Date;
                        filterDateTimeTo = dateTimePicker1.Value.Date.AddDays(1).AddSeconds(-1);
                    }

                    if (ckFilterOnTime.Checked)
                    {
                        if (ckFilterOnDay.Checked)
                        {
                            filterDateTimeFrom = filterDateTimeFrom.Date.AddHours(dtFromTime.Value.Hour).AddMinutes(dtFromTime.Value.Minute);
                            filterDateTimeTo = filterDateTimeTo.Date.AddHours(dtToTime.Value.Hour).AddMinutes(dtToTime.Value.Minute);
                        }
                        else
                        {
                            DateTime now = DateTime.Now;
                            filterDateTimeFrom = now.Date.AddHours(dtFromTime.Value.Hour).AddMinutes(dtFromTime.Value.Minute);
                            filterDateTimeTo = now.Date.AddHours(dtToTime.Value.Hour).AddMinutes(dtToTime.Value.Minute);
                        }

                        filterDateTimeFrom = filterDateTimeFrom.ToUniversalTime();
                        filterDateTimeTo = filterDateTimeTo.ToUniversalTime();
                    }
                }

                if (_activeLambdaLogGroups.ContainsKey(cboApplication.Text))
                {
                    Dictionary<string, string> resultLambda = AWSCloudwatchLog.SaveLogEntriesToFileByLambdaFunction(_domein, _environment, filterDateTimeFrom, filterDateTimeTo, _activeLambdaLogGroups[cboApplication.Text], cboApplication.Text, _outputDir, logOwner);
                    _activeLogFiles = resultLambda;
                    cbChooseLogStream.DataSource = resultLambda.Keys.ToList();
                }
                else
                {
                    List<string> checkedLogStreams = new List<string>();
                    checkedLogStreams.AddRange(lvAppLogStreams.CheckedItems
                                         .Cast<ListViewItem>()
                                         .Select(x => x.Name));

                    Dictionary<string, string> result = await AWSCloudwatchLog.SaveLogEntriesToFileByAppCode(_appCode, _domein, _environment,
                        filterDateTimeFrom, filterDateTimeTo, checkedLogStreams, _outputDir, false, logOwner);

                    _activeLogFiles = result;
                    cbChooseLogStream.DataSource = result.Keys.ToList();
                }
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
                btnGetLog.Enabled = true;
                btnDeleteLogFiles.Enabled = true;
            }
        }

        private void cboApplication_TextChanged(object sender, EventArgs e)
        {
            lvAppLogStreams.Items.Clear();
            cbFetchFromAllInstances.Checked = false;

            if (_activeLambdaLogGroups.ContainsKey(cboApplication.Text))
            {
                lvAppLogStreams.Visible = false;
                cbFetchFromAllInstances.Visible = false;
                return;
            }

            lvAppLogStreams.Visible = true;
            cbFetchFromAllInstances.Visible = true;

            List<string> ec2Ids = new List<string>();
            Dictionary<string, LogStreamItem> appLogStreams = AWSCloudwatchLog.GetApplicationLogStreamsByApplication(_appCode, cboApplication.Text, _domein, _environment);

            foreach (KeyValuePair<string, LogStreamItem> logStreamItem in appLogStreams)
            {
                var splitItems = logStreamItem.Key.Split('/');
                if (splitItems.GetLength(0) < 3 /*|| !splitItems[2].ToLower().StartsWith("i-")*/)
                    continue;

                if (!ec2Ids.Contains(splitItems[2]))
                    ec2Ids.Add(splitItems[2]);
            }

            var purposeIds = AWSEc2.GetEc2PurposeIdByInstanceIds(_appCode, _domein, _environment, ec2Ids);
            foreach (KeyValuePair<string, LogStreamItem> logStreamItem in appLogStreams.OrderBy(item => item.Value.LogStreamName).ThenByDescending(item => item.Value.LastEventTimestamp))
            {
                var splitItems = logStreamItem.Key.Split('/');
                if (splitItems.GetLength(0) < 3 /*|| !splitItems[2].ToLower().StartsWith("i-")*/)
                    continue;

                lvAppLogStreams.Items.Add(logStreamItem.Key, splitItems[2], string.Empty);

                string purposeIdItem = purposeIds.ContainsKey(splitItems[2]) ?  purposeIds[splitItems[2]] : string.Empty;
                lvAppLogStreams.Items[logStreamItem.Key].SubItems.Add(purposeIdItem);

                string prefix = $"{splitItems[0]}/{splitItems[1]}/{splitItems[2]}/";
                if (splitItems.GetLength(0) > 3)
                    lvAppLogStreams.Items[logStreamItem.Key].SubItems.Add(logStreamItem.Key.Replace(prefix, string.Empty));

                lvAppLogStreams.Items[logStreamItem.Key].SubItems.Add(logStreamItem.Value.LastEventTimestamp.ToString());
            }
        }

        private void cbFetchFromAllInstances_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvItem in lvAppLogStreams.Items)
            {
                lvItem.Checked = cbFetchFromAllInstances.Checked;
            }
        }

        private void dtFromTime_ValueChanged(object sender, EventArgs e)
        {
            if (dtToTime.Value <= dtFromTime.Value)
                dtToTime.Value = dtFromTime.Value.AddMinutes(1);
        }

        private void dtToTime_ValueChanged(object sender, EventArgs e)
        {
            if (dtFromTime.Value >= dtToTime.Value)
                dtFromTime.Value = dtToTime.Value.AddMinutes(-1);
        }

        private void lvAppLogStreams_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Get the new sorting column.
            ColumnHeader new_sorting_column = lvAppLogStreams.Columns[e.Column];

            // Figure out the new sorting order.
            System.Windows.Forms.SortOrder sort_order;
            if (SortingColumn == null)
            {
                // New column. Sort ascending.
                sort_order = SortOrder.Ascending;
            }
            else
            {
                // See if this is the same column.
                if (new_sorting_column == SortingColumn)
                {
                    // Same column. Switch the sort order.
                    if (SortingColumn.Text.StartsWith("> "))
                    {
                        sort_order = SortOrder.Descending;
                    }
                    else
                    {
                        sort_order = SortOrder.Ascending;
                    }
                }
                else
                {
                    // New column. Sort ascending.
                    sort_order = SortOrder.Ascending;
                }

                // Remove the old sort indicator.
                SortingColumn.Text = SortingColumn.Text.Substring(2);
            }

            // Display the new sort order.
            SortingColumn = new_sorting_column;
            if (sort_order == SortOrder.Ascending)
            {
                SortingColumn.Text = "> " + SortingColumn.Text;
            }
            else
            {
                SortingColumn.Text = "< " + SortingColumn.Text;
            }

            // Create a comparer.
            lvAppLogStreams.ListViewItemSorter =
                new ListViewComparer(e.Column, sort_order);

            lvAppLogStreams.Sort();
        }

        private void cbChooseLogStream_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UseWaitCursor = false;
                Cursor = Cursors.WaitCursor;

                logText.Text = string.Empty;
                if (_activeLogFiles.ContainsKey(cbChooseLogStream.Text))
                {
                    using (StreamReader reader = new StreamReader(_activeLogFiles[cbChooseLogStream.Text]))
                    {
                        logText.Text = reader.ReadToEnd();
                    }
                }
            }
            finally
            {
                UseWaitCursor = false;
                Cursor = Cursors.Default;
            }
        }

        private void btnResetOrder_Click(object sender, EventArgs e)
        {
            lvAppLogStreams.Sorting = SortOrder.None;
            lvAppLogStreams.ListViewItemSorter = null;
        }

        private void ckFilterPreviousMinutes_Click(object sender, EventArgs e)
        {
            ckFilterOnTime.Checked = false;
            ckFilterOnDay.Checked = false;
        }

        private void ckFilterOnTime_Click(object sender, EventArgs e)
        {
            ckFilterPreviousMinutes.Checked = false;
        }

        private void ckFilterOnDay_Click(object sender, EventArgs e)
        {
            ckFilterPreviousMinutes.Checked = false;
        }

        private void BtnDeleteLogFiles_Click(object sender, EventArgs e)
        {
            var logFiles = Directory.GetFiles(_outputDir, $"{logOwner}-*.log", SearchOption.TopDirectoryOnly);
            if (logFiles.GetLength(0) == 0)
                return;

            if (MessageBox.Show(this, $"Are you sure you want delete all ({logFiles.GetLength(0)}) the temporary file(s) for this operator? ", "Are you sure....?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                return;

            foreach (string logFileItem in logFiles)
            {
                System.Diagnostics.Debug.WriteLine(logFileItem);
                File.Delete(logFileItem);
            }
        }
    }
}
