﻿namespace AwsOperator
{
    partial class AwsCodeDeployScriptLogOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCodeDeployApplication = new System.Windows.Forms.ComboBox();
            this.cbCodeDeployGroup = new System.Windows.Forms.ComboBox();
            this.lvDeployments = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvDeployInstanceTargets = new System.Windows.Forms.ListView();
            this.chInstanceId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnCollectLog = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbCodeDeployApplication
            // 
            this.cbCodeDeployApplication.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCodeDeployApplication.FormattingEnabled = true;
            this.cbCodeDeployApplication.Location = new System.Drawing.Point(226, 22);
            this.cbCodeDeployApplication.Name = "cbCodeDeployApplication";
            this.cbCodeDeployApplication.Size = new System.Drawing.Size(282, 21);
            this.cbCodeDeployApplication.TabIndex = 17;
            this.cbCodeDeployApplication.TextChanged += new System.EventHandler(this.cbCodeDeployApplication_TextChanged);
            // 
            // cbCodeDeployGroup
            // 
            this.cbCodeDeployGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCodeDeployGroup.FormattingEnabled = true;
            this.cbCodeDeployGroup.Location = new System.Drawing.Point(226, 49);
            this.cbCodeDeployGroup.Name = "cbCodeDeployGroup";
            this.cbCodeDeployGroup.Size = new System.Drawing.Size(282, 21);
            this.cbCodeDeployGroup.TabIndex = 18;
            this.cbCodeDeployGroup.TextChanged += new System.EventHandler(this.cbCodeDeployGroup_TextChanged);
            // 
            // lvDeployments
            // 
            this.lvDeployments.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvDeployments.FullRowSelect = true;
            this.lvDeployments.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvDeployments.HideSelection = false;
            this.lvDeployments.Location = new System.Drawing.Point(20, 114);
            this.lvDeployments.MultiSelect = false;
            this.lvDeployments.Name = "lvDeployments";
            this.lvDeployments.Size = new System.Drawing.Size(488, 182);
            this.lvDeployments.TabIndex = 28;
            this.lvDeployments.UseCompatibleStateImageBehavior = false;
            this.lvDeployments.View = System.Windows.Forms.View.Details;
            this.lvDeployments.SelectedIndexChanged += new System.EventHandler(this.lvDeployments_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "DeploymentId";
            this.columnHeader1.Width = 209;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Start Time";
            this.columnHeader2.Width = 160;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            this.columnHeader3.Width = 111;
            // 
            // lvDeployInstanceTargets
            // 
            this.lvDeployInstanceTargets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chInstanceId,
            this.columnHeader4});
            this.lvDeployInstanceTargets.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvDeployInstanceTargets.HideSelection = false;
            this.lvDeployInstanceTargets.Location = new System.Drawing.Point(20, 316);
            this.lvDeployInstanceTargets.MultiSelect = false;
            this.lvDeployInstanceTargets.Name = "lvDeployInstanceTargets";
            this.lvDeployInstanceTargets.Size = new System.Drawing.Size(488, 182);
            this.lvDeployInstanceTargets.TabIndex = 29;
            this.lvDeployInstanceTargets.UseCompatibleStateImageBehavior = false;
            this.lvDeployInstanceTargets.View = System.Windows.Forms.View.Details;
            // 
            // chInstanceId
            // 
            this.chInstanceId.Text = "InstanceId";
            this.chInstanceId.Width = 422;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Actief";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Codedeploy Application:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Codedeployment Group:";
            // 
            // BtnCollectLog
            // 
            this.BtnCollectLog.Location = new System.Drawing.Point(20, 504);
            this.BtnCollectLog.Name = "BtnCollectLog";
            this.BtnCollectLog.Size = new System.Drawing.Size(488, 37);
            this.BtnCollectLog.TabIndex = 33;
            this.BtnCollectLog.Text = "Haal script log op";
            this.BtnCollectLog.UseVisualStyleBackColor = true;
            this.BtnCollectLog.Click += new System.EventHandler(this.BtnCollectLog_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.BtnCollectLog);
            this.groupBox1.Controls.Add(this.cbCodeDeployApplication);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbCodeDeployGroup);
            this.groupBox1.Controls.Add(this.lvDeployments);
            this.groupBox1.Controls.Add(this.lvDeployInstanceTargets);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 555);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            // 
            // AwsCodeDeployScriptLogOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 579);
            this.Controls.Add(this.groupBox1);
            this.Name = "AwsCodeDeployScriptLogOperator";
            this.Text = "AwsCodeDeployScriptLog";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCodeDeployApplication;
        private System.Windows.Forms.ComboBox cbCodeDeployGroup;
        private System.Windows.Forms.ListView lvDeployments;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ListView lvDeployInstanceTargets;
        private System.Windows.Forms.ColumnHeader chInstanceId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnCollectLog;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}