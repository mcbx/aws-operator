﻿namespace AwsOperator
{
    partial class AwsWindowsInstanceLogOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AwsWindowsInstanceLogOperator));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.logText = new FastColoredTextBoxNS.FastColoredTextBox();
            this.cbChooseLogStream = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudLastMinutes = new System.Windows.Forms.NumericUpDown();
            this.ckFilterPreviousMinutes = new System.Windows.Forms.CheckBox();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.ckFilterOnDay = new System.Windows.Forms.CheckBox();
            this.ckFilterOnTime = new System.Windows.Forms.CheckBox();
            this.dtFromTime = new System.Windows.Forms.DateTimePicker();
            this.dtToTime = new System.Windows.Forms.DateTimePicker();
            this.btnResetOrder = new System.Windows.Forms.Button();
            this.cboEc2Instance = new System.Windows.Forms.ComboBox();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbFetchFromAll = new System.Windows.Forms.CheckBox();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvEventLogStreams = new System.Windows.Forms.ListView();
            this.btnGetLog = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDeleteLogFiles = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logText)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLastMinutes)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox3.Controls.Add(this.logText);
            this.groupBox3.Controls.Add(this.cbChooseLogStream);
            this.groupBox3.Location = new System.Drawing.Point(597, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(755, 685);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Event log per machine";
            // 
            // logText
            // 
            this.logText.AllowMacroRecording = false;
            this.logText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logText.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.logText.AutoIndent = false;
            this.logText.AutoIndentChars = false;
            this.logText.AutoIndentExistingLines = false;
            this.logText.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.logText.BackBrush = null;
            this.logText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logText.CharHeight = 14;
            this.logText.CharWidth = 8;
            this.logText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.logText.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.logText.HighlightFoldingIndicator = false;
            this.logText.IsReplaceMode = false;
            this.logText.Location = new System.Drawing.Point(6, 46);
            this.logText.Name = "logText";
            this.logText.Paddings = new System.Windows.Forms.Padding(0);
            this.logText.ReadOnly = true;
            this.logText.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.logText.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("logText.ServiceColors")));
            this.logText.Size = new System.Drawing.Size(743, 633);
            this.logText.TabIndex = 29;
            this.logText.Tag = "";
            this.logText.WordWrapAutoIndent = false;
            this.logText.Zoom = 100;
            // 
            // cbChooseLogStream
            // 
            this.cbChooseLogStream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChooseLogStream.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseLogStream.FormattingEnabled = true;
            this.cbChooseLogStream.Location = new System.Drawing.Point(6, 19);
            this.cbChooseLogStream.Name = "cbChooseLogStream";
            this.cbChooseLogStream.Size = new System.Drawing.Size(743, 21);
            this.cbChooseLogStream.TabIndex = 27;
            this.cbChooseLogStream.TextChanged += new System.EventHandler(this.cbChooseLogStream_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(89, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 20);
            this.label1.TabIndex = 23;
            this.label1.Text = "t/m";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.nudLastMinutes);
            this.groupBox1.Controls.Add(this.ckFilterPreviousMinutes);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtDate);
            this.groupBox1.Controls.Add(this.ckFilterOnDay);
            this.groupBox1.Controls.Add(this.ckFilterOnTime);
            this.groupBox1.Controls.Add(this.dtFromTime);
            this.groupBox1.Controls.Add(this.dtToTime);
            this.groupBox1.Location = new System.Drawing.Point(12, 393);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(579, 202);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter settings";
            // 
            // nudLastMinutes
            // 
            this.nudLastMinutes.Location = new System.Drawing.Point(6, 52);
            this.nudLastMinutes.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.nudLastMinutes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudLastMinutes.Name = "nudLastMinutes";
            this.nudLastMinutes.Size = new System.Drawing.Size(200, 20);
            this.nudLastMinutes.TabIndex = 27;
            this.nudLastMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudLastMinutes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ckFilterPreviousMinutes
            // 
            this.ckFilterPreviousMinutes.AutoSize = true;
            this.ckFilterPreviousMinutes.Location = new System.Drawing.Point(6, 29);
            this.ckFilterPreviousMinutes.Name = "ckFilterPreviousMinutes";
            this.ckFilterPreviousMinutes.Size = new System.Drawing.Size(169, 17);
            this.ckFilterPreviousMinutes.TabIndex = 26;
            this.ckFilterPreviousMinutes.Text = "Filter op laatste aantal minuten";
            this.ckFilterPreviousMinutes.UseVisualStyleBackColor = true;
            this.ckFilterPreviousMinutes.Click += new System.EventHandler(this.ckFilterPreviousMinutes_Click);
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(6, 110);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(200, 20);
            this.dtDate.TabIndex = 14;
            // 
            // ckFilterOnDay
            // 
            this.ckFilterOnDay.AutoSize = true;
            this.ckFilterOnDay.Location = new System.Drawing.Point(6, 87);
            this.ckFilterOnDay.Name = "ckFilterOnDay";
            this.ckFilterOnDay.Size = new System.Drawing.Size(84, 17);
            this.ckFilterOnDay.TabIndex = 18;
            this.ckFilterOnDay.Text = "Filter op dag";
            this.ckFilterOnDay.UseVisualStyleBackColor = true;
            this.ckFilterOnDay.Click += new System.EventHandler(this.ckFilterOnDay_Click);
            // 
            // ckFilterOnTime
            // 
            this.ckFilterOnTime.AutoSize = true;
            this.ckFilterOnTime.Location = new System.Drawing.Point(6, 147);
            this.ckFilterOnTime.Name = "ckFilterOnTime";
            this.ckFilterOnTime.Size = new System.Drawing.Size(79, 17);
            this.ckFilterOnTime.TabIndex = 20;
            this.ckFilterOnTime.Text = "Filter op tijd";
            this.ckFilterOnTime.UseVisualStyleBackColor = true;
            this.ckFilterOnTime.Click += new System.EventHandler(this.ckFilterOnTime_Click);
            // 
            // dtFromTime
            // 
            this.dtFromTime.CustomFormat = "HH:mm";
            this.dtFromTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromTime.Location = new System.Drawing.Point(6, 170);
            this.dtFromTime.Name = "dtFromTime";
            this.dtFromTime.ShowUpDown = true;
            this.dtFromTime.Size = new System.Drawing.Size(68, 20);
            this.dtFromTime.TabIndex = 21;
            this.dtFromTime.ValueChanged += new System.EventHandler(this.dtFromTime_ValueChanged);
            // 
            // dtToTime
            // 
            this.dtToTime.CustomFormat = "HH:mm";
            this.dtToTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToTime.Location = new System.Drawing.Point(138, 170);
            this.dtToTime.Name = "dtToTime";
            this.dtToTime.ShowUpDown = true;
            this.dtToTime.Size = new System.Drawing.Size(68, 20);
            this.dtToTime.TabIndex = 22;
            this.dtToTime.ValueChanged += new System.EventHandler(this.dtToTime_ValueChanged);
            // 
            // btnResetOrder
            // 
            this.btnResetOrder.Location = new System.Drawing.Point(6, 381);
            this.btnResetOrder.Name = "btnResetOrder";
            this.btnResetOrder.Size = new System.Drawing.Size(406, 23);
            this.btnResetOrder.TabIndex = 28;
            this.btnResetOrder.Text = "Reset sort order";
            this.btnResetOrder.UseVisualStyleBackColor = true;
            this.btnResetOrder.Visible = false;
            // 
            // cboEc2Instance
            // 
            this.cboEc2Instance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEc2Instance.FormattingEnabled = true;
            this.cboEc2Instance.Location = new System.Drawing.Point(6, 19);
            this.cboEc2Instance.Name = "cboEc2Instance";
            this.cboEc2Instance.Size = new System.Drawing.Size(567, 21);
            this.cboEc2Instance.TabIndex = 15;
            this.cboEc2Instance.TextChanged += new System.EventHandler(this.cboEc2Instance_TextChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Last event";
            this.columnHeader3.Width = 111;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "LogType";
            this.columnHeader2.Width = 304;
            // 
            // cbFetchFromAll
            // 
            this.cbFetchFromAll.AutoSize = true;
            this.cbFetchFromAll.Location = new System.Drawing.Point(6, 358);
            this.cbFetchFromAll.Name = "cbFetchFromAll";
            this.cbFetchFromAll.Size = new System.Drawing.Size(75, 17);
            this.cbFetchFromAll.TabIndex = 25;
            this.cbFetchFromAll.Text = "Select all?";
            this.cbFetchFromAll.UseVisualStyleBackColor = true;
            this.cbFetchFromAll.CheckedChanged += new System.EventHandler(this.cbFetchFromAlls_CheckedChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Instance";
            this.columnHeader1.Width = 143;
            // 
            // lvEventLogStreams
            // 
            this.lvEventLogStreams.CheckBoxes = true;
            this.lvEventLogStreams.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvEventLogStreams.Location = new System.Drawing.Point(6, 46);
            this.lvEventLogStreams.Name = "lvEventLogStreams";
            this.lvEventLogStreams.Size = new System.Drawing.Size(567, 306);
            this.lvEventLogStreams.TabIndex = 26;
            this.lvEventLogStreams.UseCompatibleStateImageBehavior = false;
            this.lvEventLogStreams.View = System.Windows.Forms.View.Details;
            // 
            // btnGetLog
            // 
            this.btnGetLog.Location = new System.Drawing.Point(6, 19);
            this.btnGetLog.Name = "btnGetLog";
            this.btnGetLog.Size = new System.Drawing.Size(567, 23);
            this.btnGetLog.TabIndex = 32;
            this.btnGetLog.Text = "Fetch event log";
            this.btnGetLog.UseVisualStyleBackColor = true;
            this.btnGetLog.Click += new System.EventHandler(this.btnGetLog_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Controls.Add(this.cboEc2Instance);
            this.groupBox2.Controls.Add(this.cbFetchFromAll);
            this.groupBox2.Controls.Add(this.btnResetOrder);
            this.groupBox2.Controls.Add(this.lvEventLogStreams);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(579, 379);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Log selectie";
            // 
            // btnDeleteLogFiles
            // 
            this.btnDeleteLogFiles.Location = new System.Drawing.Point(6, 61);
            this.btnDeleteLogFiles.Name = "btnDeleteLogFiles";
            this.btnDeleteLogFiles.Size = new System.Drawing.Size(567, 23);
            this.btnDeleteLogFiles.TabIndex = 36;
            this.btnDeleteLogFiles.Text = "Delete all operator log files";
            this.btnDeleteLogFiles.UseVisualStyleBackColor = true;
            this.btnDeleteLogFiles.Click += new System.EventHandler(this.BtnDeleteLogFiles_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox4.Controls.Add(this.btnGetLog);
            this.groupBox4.Controls.Add(this.btnDeleteLogFiles);
            this.groupBox4.Location = new System.Drawing.Point(12, 601);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(579, 96);
            this.groupBox4.TabIndex = 37;
            this.groupBox4.TabStop = false;
            // 
            // AwsWindowsInstanceLogOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 709);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.MinimumSize = new System.Drawing.Size(1380, 748);
            this.Name = "AwsWindowsInstanceLogOperator";
            this.Text = "AwsWindowsEventLogOperator";
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logText)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLastMinutes)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbChooseLogStream;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.CheckBox ckFilterOnDay;
        private System.Windows.Forms.CheckBox ckFilterOnTime;
        private System.Windows.Forms.DateTimePicker dtFromTime;
        private System.Windows.Forms.DateTimePicker dtToTime;
        private System.Windows.Forms.Button btnResetOrder;
        private System.Windows.Forms.ComboBox cboEc2Instance;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.CheckBox cbFetchFromAll;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView lvEventLogStreams;
        private System.Windows.Forms.Button btnGetLog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown nudLastMinutes;
        private System.Windows.Forms.CheckBox ckFilterPreviousMinutes;
        private System.Windows.Forms.Button btnDeleteLogFiles;
        private FastColoredTextBoxNS.FastColoredTextBox logText;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}