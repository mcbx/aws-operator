﻿namespace AwsOperator
{
    partial class AwsParameterOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmParameterOperations = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteSelectedParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedParametersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteallParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbParameterHistory = new System.Windows.Forms.GroupBox();
            this.btnRollBackParameter = new System.Windows.Forms.Button();
            this.lvParameterHistory = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbUpdateParameter = new System.Windows.Forms.GroupBox();
            this.txtHiddenParameterName = new System.Windows.Forms.TextBox();
            this.btnDeleteParameter = new System.Windows.Forms.Button();
            this.txtParameterVersion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtParameterValue = new System.Windows.Forms.TextBox();
            this.txtParameterName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdateParameter = new System.Windows.Forms.Button();
            this.ckParameterEncrypted = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbApplication = new System.Windows.Forms.ComboBox();
            this.btnRefreshParameterList = new System.Windows.Forms.Button();
            this.lvParameters = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmParameterOperations.SuspendLayout();
            this.gbParameterHistory.SuspendLayout();
            this.gbUpdateParameter.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmParameterOperations
            // 
            this.cmParameterOperations.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSelectedParametersToolStripMenuItem});
            this.cmParameterOperations.Name = "cmParameterOperations";
            this.cmParameterOperations.Size = new System.Drawing.Size(126, 26);
            this.cmParameterOperations.Opening += new System.ComponentModel.CancelEventHandler(this.CmParameterOperations_Opening);
            // 
            // deleteSelectedParametersToolStripMenuItem
            // 
            this.deleteSelectedParametersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSelectedParametersToolStripMenuItem1,
            this.deleteallParametersToolStripMenuItem});
            this.deleteSelectedParametersToolStripMenuItem.Name = "deleteSelectedParametersToolStripMenuItem";
            this.deleteSelectedParametersToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.deleteSelectedParametersToolStripMenuItem.Text = "&Delete......";
            // 
            // deleteSelectedParametersToolStripMenuItem1
            // 
            this.deleteSelectedParametersToolStripMenuItem1.Name = "deleteSelectedParametersToolStripMenuItem1";
            this.deleteSelectedParametersToolStripMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.deleteSelectedParametersToolStripMenuItem1.Text = "Delete &selected parameters";
            this.deleteSelectedParametersToolStripMenuItem1.Click += new System.EventHandler(this.deleteSelectedParametersToolStripMenuItem1_Click);
            // 
            // deleteallParametersToolStripMenuItem
            // 
            this.deleteallParametersToolStripMenuItem.Name = "deleteallParametersToolStripMenuItem";
            this.deleteallParametersToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.deleteallParametersToolStripMenuItem.Text = "Delete &all parameters";
            this.deleteallParametersToolStripMenuItem.Click += new System.EventHandler(this.deleteallParametersToolStripMenuItem_Click);
            // 
            // gbParameterHistory
            // 
            this.gbParameterHistory.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbParameterHistory.Controls.Add(this.btnRollBackParameter);
            this.gbParameterHistory.Controls.Add(this.lvParameterHistory);
            this.gbParameterHistory.Location = new System.Drawing.Point(12, 441);
            this.gbParameterHistory.Name = "gbParameterHistory";
            this.gbParameterHistory.Size = new System.Drawing.Size(1379, 356);
            this.gbParameterHistory.TabIndex = 5;
            this.gbParameterHistory.TabStop = false;
            this.gbParameterHistory.Text = "Parameter History";
            this.gbParameterHistory.Visible = false;
            // 
            // btnRollBackParameter
            // 
            this.btnRollBackParameter.Location = new System.Drawing.Point(9, 309);
            this.btnRollBackParameter.Name = "btnRollBackParameter";
            this.btnRollBackParameter.Size = new System.Drawing.Size(1364, 41);
            this.btnRollBackParameter.TabIndex = 4;
            this.btnRollBackParameter.Text = "Roll back naar versie";
            this.btnRollBackParameter.UseVisualStyleBackColor = true;
            this.btnRollBackParameter.Click += new System.EventHandler(this.btnRollBackParameter_Click);
            // 
            // lvParameterHistory
            // 
            this.lvParameterHistory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.lvParameterHistory.Cursor = System.Windows.Forms.Cursors.Default;
            this.lvParameterHistory.FullRowSelect = true;
            this.lvParameterHistory.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvParameterHistory.Location = new System.Drawing.Point(12, 19);
            this.lvParameterHistory.MultiSelect = false;
            this.lvParameterHistory.Name = "lvParameterHistory";
            this.lvParameterHistory.ShowItemToolTips = true;
            this.lvParameterHistory.Size = new System.Drawing.Size(1361, 281);
            this.lvParameterHistory.TabIndex = 4;
            this.lvParameterHistory.UseCompatibleStateImageBehavior = false;
            this.lvParameterHistory.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Parameter versie";
            this.columnHeader5.Width = 157;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Value";
            this.columnHeader6.Width = 699;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Secured";
            this.columnHeader7.Width = 84;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Labels";
            this.columnHeader8.Width = 193;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Mutatie user";
            this.columnHeader9.Width = 102;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Mutatie datum";
            this.columnHeader10.Width = 106;
            // 
            // gbUpdateParameter
            // 
            this.gbUpdateParameter.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbUpdateParameter.Controls.Add(this.txtHiddenParameterName);
            this.gbUpdateParameter.Controls.Add(this.btnDeleteParameter);
            this.gbUpdateParameter.Controls.Add(this.txtParameterVersion);
            this.gbUpdateParameter.Controls.Add(this.label5);
            this.gbUpdateParameter.Controls.Add(this.label4);
            this.gbUpdateParameter.Controls.Add(this.txtParameterValue);
            this.gbUpdateParameter.Controls.Add(this.txtParameterName);
            this.gbUpdateParameter.Controls.Add(this.label3);
            this.gbUpdateParameter.Controls.Add(this.label2);
            this.gbUpdateParameter.Controls.Add(this.btnUpdateParameter);
            this.gbUpdateParameter.Controls.Add(this.ckParameterEncrypted);
            this.gbUpdateParameter.Location = new System.Drawing.Point(865, 12);
            this.gbUpdateParameter.Name = "gbUpdateParameter";
            this.gbUpdateParameter.Size = new System.Drawing.Size(526, 403);
            this.gbUpdateParameter.TabIndex = 4;
            this.gbUpdateParameter.TabStop = false;
            this.gbUpdateParameter.Text = "Parameter";
            this.gbUpdateParameter.Visible = false;
            // 
            // txtHiddenParameterName
            // 
            this.txtHiddenParameterName.Location = new System.Drawing.Point(6, 343);
            this.txtHiddenParameterName.Name = "txtHiddenParameterName";
            this.txtHiddenParameterName.Size = new System.Drawing.Size(100, 20);
            this.txtHiddenParameterName.TabIndex = 10;
            this.txtHiddenParameterName.Visible = false;
            // 
            // btnDeleteParameter
            // 
            this.btnDeleteParameter.Location = new System.Drawing.Point(6, 304);
            this.btnDeleteParameter.Name = "btnDeleteParameter";
            this.btnDeleteParameter.Size = new System.Drawing.Size(514, 43);
            this.btnDeleteParameter.TabIndex = 9;
            this.btnDeleteParameter.Text = "Delete parameter";
            this.btnDeleteParameter.UseVisualStyleBackColor = true;
            this.btnDeleteParameter.Click += new System.EventHandler(this.btnDeleteParameter_Click);
            // 
            // txtParameterVersion
            // 
            this.txtParameterVersion.Location = new System.Drawing.Point(167, 26);
            this.txtParameterVersion.Name = "txtParameterVersion";
            this.txtParameterVersion.ReadOnly = true;
            this.txtParameterVersion.Size = new System.Drawing.Size(346, 20);
            this.txtParameterVersion.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Versie:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Encrypted:";
            // 
            // txtParameterValue
            // 
            this.txtParameterValue.Location = new System.Drawing.Point(167, 96);
            this.txtParameterValue.Multiline = true;
            this.txtParameterValue.Name = "txtParameterValue";
            this.txtParameterValue.Size = new System.Drawing.Size(346, 82);
            this.txtParameterValue.TabIndex = 5;
            // 
            // txtParameterName
            // 
            this.txtParameterName.Location = new System.Drawing.Point(167, 63);
            this.txtParameterName.Name = "txtParameterName";
            this.txtParameterName.ReadOnly = true;
            this.txtParameterName.Size = new System.Drawing.Size(346, 20);
            this.txtParameterName.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Waarde:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Parameter:";
            // 
            // btnUpdateParameter
            // 
            this.btnUpdateParameter.Location = new System.Drawing.Point(6, 353);
            this.btnUpdateParameter.Name = "btnUpdateParameter";
            this.btnUpdateParameter.Size = new System.Drawing.Size(514, 43);
            this.btnUpdateParameter.TabIndex = 1;
            this.btnUpdateParameter.Text = "Update parameter";
            this.btnUpdateParameter.UseVisualStyleBackColor = true;
            this.btnUpdateParameter.Click += new System.EventHandler(this.btnUpdateParameter_Click);
            // 
            // ckParameterEncrypted
            // 
            this.ckParameterEncrypted.AutoSize = true;
            this.ckParameterEncrypted.Location = new System.Drawing.Point(167, 184);
            this.ckParameterEncrypted.Name = "ckParameterEncrypted";
            this.ckParameterEncrypted.Size = new System.Drawing.Size(15, 14);
            this.ckParameterEncrypted.TabIndex = 0;
            this.ckParameterEncrypted.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbApplication);
            this.groupBox1.Controls.Add(this.btnRefreshParameterList);
            this.groupBox1.Controls.Add(this.lvParameters);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(833, 403);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Overview";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Applicatie";
            // 
            // cbApplication
            // 
            this.cbApplication.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbApplication.FormattingEnabled = true;
            this.cbApplication.Items.AddRange(new object[] {
            "test"});
            this.cbApplication.Location = new System.Drawing.Point(123, 31);
            this.cbApplication.Name = "cbApplication";
            this.cbApplication.Size = new System.Drawing.Size(704, 21);
            this.cbApplication.TabIndex = 1;
            this.cbApplication.TextChanged += new System.EventHandler(this.cbApplication_TextChanged);
            // 
            // btnRefreshParameterList
            // 
            this.btnRefreshParameterList.Location = new System.Drawing.Point(6, 354);
            this.btnRefreshParameterList.Name = "btnRefreshParameterList";
            this.btnRefreshParameterList.Size = new System.Drawing.Size(821, 43);
            this.btnRefreshParameterList.TabIndex = 0;
            this.btnRefreshParameterList.Text = "Refresh snapshot";
            this.btnRefreshParameterList.UseVisualStyleBackColor = true;
            this.btnRefreshParameterList.Click += new System.EventHandler(this.btnRefreshParameterList_Click);
            // 
            // lvParameters
            // 
            this.lvParameters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvParameters.ContextMenuStrip = this.cmParameterOperations;
            this.lvParameters.Cursor = System.Windows.Forms.Cursors.Default;
            this.lvParameters.FullRowSelect = true;
            this.lvParameters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvParameters.Location = new System.Drawing.Point(6, 58);
            this.lvParameters.Name = "lvParameters";
            this.lvParameters.ShowItemToolTips = true;
            this.lvParameters.Size = new System.Drawing.Size(821, 290);
            this.lvParameters.TabIndex = 2;
            this.lvParameters.UseCompatibleStateImageBehavior = false;
            this.lvParameters.View = System.Windows.Forms.View.Details;
            this.lvParameters.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvParameters_ItemSelectionChanged);
            this.lvParameters.DoubleClick += new System.EventHandler(this.lvParameters_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Parameter";
            this.columnHeader1.Width = 157;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 511;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Secured";
            this.columnHeader3.Width = 84;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Versie";
            // 
            // AwsParameterOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1398, 809);
            this.Controls.Add(this.gbParameterHistory);
            this.Controls.Add(this.gbUpdateParameter);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(875, 470);
            this.Name = "AwsParameterOperator";
            this.Text = "AwsParameterOperator";
            this.cmParameterOperations.ResumeLayout(false);
            this.gbParameterHistory.ResumeLayout(false);
            this.gbUpdateParameter.ResumeLayout(false);
            this.gbUpdateParameter.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRefreshParameterList;
        private System.Windows.Forms.ComboBox cbApplication;
        private System.Windows.Forms.ListView lvParameters;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.GroupBox gbUpdateParameter;
        private System.Windows.Forms.TextBox txtParameterVersion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtParameterValue;
        private System.Windows.Forms.TextBox txtParameterName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdateParameter;
        private System.Windows.Forms.CheckBox ckParameterEncrypted;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnDeleteParameter;
        private System.Windows.Forms.TextBox txtHiddenParameterName;
        private System.Windows.Forms.GroupBox gbParameterHistory;
        private System.Windows.Forms.Button btnRollBackParameter;
        private System.Windows.Forms.ListView lvParameterHistory;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ContextMenuStrip cmParameterOperations;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectedParametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectedParametersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteallParametersToolStripMenuItem;
    }
}