﻿using Fh.Aws.Framework.AWSLogin;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Linq;
using AwsOperator.General;

namespace AwsOperator
{
    public partial class AwsCwrAlarmStatusOperatorTabbed : FormTemplate
    {
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
        private const int TCM_SETMINTABWIDTH = 0x1300 + 149;

        private Color _alertColor = Color.Pink;

        public AwsCwrAlarmStatusOperatorTabbed(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner, bool vatMode) : base(appCode, domein, environment, owner, vatMode)
        {
            InitializeComponent();
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true);

            tabControl1.TabPages.Add(new TabPage());
            tabControl1.TabPages[tabControl1.TabCount - 1].Text = "";
            tabControl1.Padding = new Point(12, 4);
            tabControl1.DrawMode = TabDrawMode.OwnerDrawFixed;

            tabControl1.DrawItem += tabControl1_DrawItem;
            tabControl1.MouseDown += tabControl1_MouseDown;
            tabControl1.Selecting += tabControl1_Selecting;
            tabControl1.HandleCreated += tabControl1_HandleCreated;

            Text = $"Operator: CWR Alarm {(_formOverride ? "(VAT)" : "")  }, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";
        }

        private TabPage AddNewAlarmPage(string appCode)
        {
            if (!Enabled)
            {
                MessageBox.Show(this, "Operator is disabled. Login is expired.", "Sorry...", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return null;
            }

            TabPage returnValue = new TabPage($"   {appCode.ToUpper()}   ");
            AWSAlarmTab tb1 = new AWSAlarmTab(appCode, _domein, _environment, returnValue, tabControl1, _formOverride);
            returnValue.Controls.Add(tb1);
            tb1.Dock = DockStyle.Fill;

            return returnValue;
        }

        private void tabControl1_HandleCreated(object sender, EventArgs e)
        {
            SendMessage(tabControl1.Handle, TCM_SETMINTABWIDTH, IntPtr.Zero, (IntPtr)16);
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPageIndex == tabControl1.TabCount - 1)
                e.Cancel = true;

            if (e.TabPage.Controls.Count == 1 && e.TabPage.Controls[0] is AWSAlarmTab)
            {
                var alarmControl = e.TabPage.Controls[0] as AWSAlarmTab;
                alarmControl.ShowTabAlert = false;
            }
        }

        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            var lastIndex = tabControl1.TabCount - 1;
            if (tabControl1.GetTabRect(lastIndex).Contains(e.Location))
            {
                using (RequestInputAlarmTab req = new RequestInputAlarmTab())
                {
                    if (req.ShowDialog() == DialogResult.OK)
                    {
                        req.AppCode.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(item =>
                        {
                            TabPage newTab = AddNewAlarmPage(item.Trim());
                            if (newTab != null)
                            {
                                tabControl1.TabPages.Insert(lastIndex, newTab);
                                tabControl1.SelectedIndex = lastIndex;
                                tabControl1.TabPages[lastIndex].UseVisualStyleBackColor = true;
                            }
                        });
                    }
                }
                return;
            }

            for (var i = 0; i < tabControl1.TabPages.Count; i++)
            {
                var tabRect = tabControl1.GetTabRect(i);
                tabRect.Inflate(-2, -2);

                var closeImage = Properties.Resources.Close;
                var imageRect = new Rectangle(
                    (tabRect.Right - closeImage.Width),
                    tabRect.Top + (tabRect.Height - closeImage.Height) / 2,
                    closeImage.Width,
                    closeImage.Height);

                if (imageRect.Contains(e.Location))
                {
                    if (tabControl1.TabPages[i].Controls.Count == 1 && tabControl1.TabPages[i].Controls[0] is AWSAlarmTab)
                    {
                        var alarmControl = tabControl1.TabPages[i].Controls[0] as AWSAlarmTab;
                        alarmControl.tmRefresher.Enabled = false;
                        alarmControl.Dispose();
                    }
                    tabControl1.TabPages.RemoveAt(i);
                    break;
                }
            }
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index > tabControl1.TabPages.Count - 1)
                    return;

                var tabPage = tabControl1.TabPages[e.Index];
                var tabRect = tabControl1.GetTabRect(e.Index);

                tabRect.Inflate(-2, -2);

                if (e.Index == tabControl1.TabCount - 1)
                {
                    var addImage = Properties.Resources.Add;

                    e.Graphics.DrawImage(addImage,
                        tabRect.Left + (tabRect.Width - addImage.Width) / 2,
                        tabRect.Top + (tabRect.Height - addImage.Height) / 2);
                }
                else
                {
                    if (tabPage.Controls.Count == 1 && tabPage.Controls[0] is AWSAlarmTab)
                    {
                        var alarmControl = tabPage.Controls[0] as AWSAlarmTab;
                        if (alarmControl.ShowTabAlert)
                        {
                            if (tabControl1.SelectedIndex != e.Index)
                            {
                                if (!tabPage.Text.Contains("! "))
                                    tabPage.Text = $"! {tabPage.Text}";

                                e.Graphics.FillRectangle(new SolidBrush(_alertColor), e.Bounds);
                            }
                            else
                            {
                                if (!tabPage.Text.Contains("! "))
                                    tabPage.Text = $"! {tabPage.Text}";

                                alarmControl.ShowTabAlert = false;
                            }
                        }
                        else
                        {
                            if (tabPage.Text.Contains("! "))
                                tabPage.Text = tabPage.Text.Replace("! ", "");

                            e.Graphics.FillRectangle(new SolidBrush(BackColor), e.Bounds);
                        }
                    }

                    var closeImage = Properties.Resources.Close;

                    e.Graphics.DrawImage(closeImage,
                        (tabRect.Right - closeImage.Width),
                        tabRect.Top + (tabRect.Height - closeImage.Height) / 2);

                    TextRenderer.DrawText(e.Graphics, tabPage.Text, tabPage.Font,
                        tabRect, tabPage.ForeColor, TextFormatFlags.Left);
                }
            }
            catch { }
        }

        internal override void FormTemplate_ChangedLoginStatus(object sender, MainForm.LoginStatusEvent e)
        {
            foreach (TabPage tabItem in tabControl1.TabPages)
            {
                if (tabItem.Controls.Count == 1 && tabItem.Controls[0] is AWSAlarmTab)
                {
                    var alarmControl = tabItem.Controls[0] as AWSAlarmTab;
                    alarmControl.tmRefresher.Enabled = e.LoginStatus == EAWSLoginStatus.Success;

                    if (e.LoginStatus == EAWSLoginStatus.Success)
                        alarmControl.tmRefresher.Start();
                    else
                        alarmControl.tmRefresher.Stop();

                    alarmControl.Enabled = e.LoginStatus == EAWSLoginStatus.Success;
                    alarmControl.LoginStatus = e.LoginStatus;
                }
            }
            Text = $"Operator: CWR Alarm, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()} {(e.LoginStatus == EAWSLoginStatus.Success ? string.Empty : "(DISABLED)") }";

            base.FormTemplate_ChangedLoginStatus(sender, e);
        }
    }
}
