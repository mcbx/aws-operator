﻿using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace AwsOperator
{
    public partial class AwsCodeDeployScriptLogOperator : FormTemplate
    {
        private Dictionary<string, string> _currentCodeDeployDeploymentGroups = new Dictionary<string, string>();
        private Dictionary<string, DeploymentDetail> _currentCodeDeployDeployments = new Dictionary<string, DeploymentDetail>();

        public AwsCodeDeployScriptLogOperator(string appCode, EAWSDomain domein, EAWSEnvironment environment, Form owner) : base(appCode, domein, environment, owner)
        {
            InitializeComponent();

            Text = $"Operator: AWS Codedeploy log, AppCode: {_appCode}, Domein: {_domein.ToString()}, Omgeving: {_environment.ToString()}";
            cbCodeDeployApplication.DataSource = AWSCodeDeployDebug.ListCodeDeployApplications(_appCode, _domein, _environment).ToArray();
        }

        private void cbCodeDeployApplication_TextChanged(object sender, EventArgs e)
        {
            lvDeployInstanceTargets.Items.Clear();
            lvDeployments.Items.Clear();

            _currentCodeDeployDeploymentGroups = AWSCodeDeployDebug.ListCodeDeploymentGroups(cbCodeDeployApplication.Text, _domein, _environment);
            cbCodeDeployGroup.DataSource = _currentCodeDeployDeploymentGroups.Keys.ToArray();
        }

        private void cbCodeDeployGroup_TextChanged(object sender, EventArgs e)
        {
            lvDeployments.Items.Clear();
            Dictionary<string, DeploymentDetail> res = AWSCodeDeployDebug.ListCodeDeployments(cbCodeDeployApplication.Text, cbCodeDeployGroup.Text, _domein, _environment);
            _currentCodeDeployDeployments = res;

            foreach (DeploymentDetail deploymentItem in _currentCodeDeployDeployments.Values.ToArray())
            {
                lvDeployments.Items.Add(deploymentItem.DeploymentId, deploymentItem.DeploymentId, string.Empty);
                lvDeployments.Items[deploymentItem.DeploymentId].SubItems.Add(deploymentItem.RunDate.ToString());
                lvDeployments.Items[deploymentItem.DeploymentId].SubItems.Add(deploymentItem.Status);
            }
        }

        private void lvDeployments_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvDeployments.SelectedItems.Count == 0)
                return;

            List<string> activeEc2 = AWSEc2.GetEc2AppList(_domein, _environment, _appCode).Keys.ToList();
            List<string> deploymentTargets = AWSCodeDeployDebug.GetDeploymentInstanceTargets(lvDeployments.SelectedItems[0].Name, _domein, _environment);

            var intersectItems = activeEc2.Intersect(deploymentTargets);
            lvDeployInstanceTargets.Items.Clear();

            foreach (string target in deploymentTargets)
            {
                var activeColor = intersectItems.Contains(target) ? Color.Green : Color.Red;
                ListViewItem lItem = new ListViewItem()
                {
                    Text = target,
                    UseItemStyleForSubItems = false
                };

                ListViewSubItem subLItem = new ListViewSubItem()
                {
                    Text = "       ",
                    BackColor = activeColor
                };
                lItem.SubItems.Add(subLItem);

                lvDeployInstanceTargets.Items.Add(lItem);
            }           
        }

        private void BtnCollectLog_Click(object sender, EventArgs e)
        {
            if (lvDeployInstanceTargets.SelectedItems.Count == 0)
                return;

            if (lvDeployments.SelectedItems.Count == 0)
                return;

            CommandDetailForm cdfItem;

            var cw_SearchResult = AWSCloudwatchLog.SearchLogEntries(_domein, _environment, DateTime.MinValue, DateTime.MinValue, $"rfh-instance-logs-windows", new List<string>(new[] { $"{lvDeployInstanceTargets.SelectedItems[0].Text}/codedeploy-deployments" }), $"\"[{lvDeployments.SelectedItems[0].Name}]\"", false);

            if (cw_SearchResult.ContainsKey($"{lvDeployInstanceTargets.SelectedItems[0].Text}/codedeploy-deployments") &&
                (cw_SearchResult[$"{lvDeployInstanceTargets.SelectedItems[0].Text}/codedeploy-deployments"] is StringBuilder)
                && ((StringBuilder)cw_SearchResult[$"{lvDeployInstanceTargets.SelectedItems[0].Text}/codedeploy-deployments"]).Length > 0)
            {
                cdfItem = new CommandDetailForm($"CDS: {lvDeployments.SelectedItems[0].Name}, InstanceId: {lvDeployInstanceTargets.SelectedItems[0].Text}, Executed on: {DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")}",
                    _domein, _environment,
                    new CommandDetail()
                    {
                        CommandId = "",
                        CommandInstanceId = lvDeployInstanceTargets.SelectedItems[0].Text,
                        CommandRunDateTime = DateTime.Now,
                        CommandStatus = "Completed (Log)",
                        CommandStatusDetailed = "Completed (Log)",
                        CommandType = CommandDetailType.Other,
                        CommandResult = ((StringBuilder)cw_SearchResult[$"{lvDeployInstanceTargets.SelectedItems[0].Text}/codedeploy-deployments"]).ToString()
                    }
                    , Owner);
                cdfItem.Show(this);
                return;
            }

            if (lvDeployInstanceTargets.SelectedItems[0].SubItems[1].BackColor == Color.Red)
            {
                var deploymentResult = AWSCodeDeployDebug.FetchScriptLogFromDeployment(_domein, _environment, lvDeployInstanceTargets.SelectedItems[0].Text, lvDeployments.SelectedItems[0].Name);
                if (!string.IsNullOrEmpty(_currentCodeDeployDeployments[lvDeployments.SelectedItems[0].Name].DeploymentErrorMessage))
                {
                    deploymentResult.CommandResult = $"Deployment error message: {_currentCodeDeployDeployments[lvDeployments.SelectedItems[0].Name].DeploymentErrorMessage}{Environment.NewLine}{deploymentResult.CommandResult}{Environment.NewLine}{Environment.NewLine}";
                }

                cdfItem = new CommandDetailForm($"CDS: {lvDeployments.SelectedItems[0].Name}, InstanceId: {lvDeployInstanceTargets.SelectedItems[0].Text}, Executed on: {DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")}",
                    _domein, _environment, deploymentResult, Owner);
            }
            else
            {
                var result = AWSCodeDeployDebug.FetchScriptLogFromInstance(_domein, _environment, lvDeployInstanceTargets.SelectedItems[0].Text, _currentCodeDeployDeploymentGroups[cbCodeDeployGroup.Text],
                  lvDeployments.SelectedItems[0].Name);

                if (!string.IsNullOrEmpty(_currentCodeDeployDeployments[lvDeployments.SelectedItems[0].Name].DeploymentErrorMessage))
                {
                    result.CommandResult = $"Deployment error message: {_currentCodeDeployDeployments[lvDeployments.SelectedItems[0].Name].DeploymentErrorMessage}{Environment.NewLine}{result.CommandResult}{Environment.NewLine}{Environment.NewLine}";
                }

                cdfItem = new CommandDetailForm($"CDS: {lvDeployments.SelectedItems[0].Name}, InstanceId: {lvDeployInstanceTargets.SelectedItems[0].Text}, Executed on: {DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")}",
                    _domein, _environment, result, Owner);
            }
            
            cdfItem.Show(this);
        }
    }
}
