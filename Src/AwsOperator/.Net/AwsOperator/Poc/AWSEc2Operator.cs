﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using AwsOperator.General;
using Fh.Aws.Framework.AWSLogin;
using Fh.Aws.Framework.AWSOperationClassContainers;
using Fh.Aws.Framework.AWSOperations;
using static System.Windows.Forms.ListViewItem;

namespace AwsOperator
{
    public partial class AWSEc2Operator : FormTemplate
    {
        private enum ItemType
        {
            EC2,
            LaunchConfiguration,
            RDS
        }

        private Dictionary<string, InstanceDetail> _ec2Machines = new Dictionary<string, InstanceDetail>();
        private Dictionary<string, AmiDetail> _lgMachines = new Dictionary<string, AmiDetail>();

        private ColumnHeader SortingColumn = null;

        public AWSEc2Operator() : base("aax", EAWSDomain.Commerce, EAWSEnvironment.Dev, null)
        {
            InitializeComponent();
            cbItemType.DataSource = Enum.GetValues(typeof(ItemType));

            listView1.GetType()
.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic)
.SetValue(listView1, true);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            _ec2Machines = Fh.Aws.Framework.AWSOperations.AWSEc2.GetEc2List(EAWSDomain.Commerce, EAWSEnvironment.Dev, true, true);
          //  _lgMachines = Fh.Aws.Framework.AWSOperations.AWSEc2.GetAmiDetailsFromLaunchConfiguration(EAWSDomain.Commerce, EAWSEnvironment.Prd);

            if (cbItemType.Text == "EC2")
            {
                _ec2Machines.ToList().ForEach(item =>
                {
                    ListViewItem item2 = new ListViewItem() { Name = item.Key, Text = item.Key };
                    item2.UseItemStyleForSubItems = false;
                    item2.SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "InstanceName", Text = item.Value.Name });

                    ListViewSubItem runningState = new ListViewItem.ListViewSubItem()
                    {
                        Name = "InstanceState",
                        Text = item.Value.State.ToString()
                    };

                    DetermineRunningColor(item.Value.State, runningState);

                    item2.SubItems.Add(runningState);
                    item2.SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "AmiId", Text = item.Value.Ami });

                    ListViewSubItem state = new ListViewItem.ListViewSubItem()
                    {
                        Name = "AmiState",
                        //   Text = item.Value.AmiDetail != null ? $"{item.Value.AmiDetail.State.ToString()} ({Math.Round((DateTime.Now - item.Value.AmiDetail.CreationDateTime).TotalDays).ToString()})" : "EXPIRED"
                        Text = item.Value.AmiDetail != null ? $"{Math.Round((DateTime.Now - item.Value.AmiDetail.CreationDateTime).TotalDays).ToString()} dagen)" : "EXPIRED"

                    };

                    state.BackColor = item.Value.AmiDetail == null || item.Value.AmiDetail.State == ImageState.Deregistered || item.Value.AmiDetail.State == ImageState.Invalid
                    ? Color.PaleVioletRed : Color.LightGreen;
                    item2.SubItems.Add(state);
                    if (item.Value.AmiDetail != null)
                        item2.SubItems.Add(new ListViewSubItem() { Name = "AmiAge",
                            Text = item.Value != null ? $"{Math.Round((DateTime.Now - item.Value.AmiDetail.CreationDateTime).TotalDays).ToString()} dagen)" : "EXPIRED"
                        });

                    listView1.Items.Add(item2);
                });
                tmRefresh.Enabled = true;
            }
            else
            {
                _lgMachines.ToList().ForEach(item =>
                {
                    ListViewItem item2 = new ListViewItem() { Name = item.Key, Text = item.Key };
                    item2.UseItemStyleForSubItems = false;
                    item2.SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "Lg", Text = item.Key });

                        //ListViewSubItem runningState = new ListViewItem.ListViewSubItem()
                        //{
                        //    Name = "LG",
                        //    Text = item.Value.State.ToString()
                        //};

                        //  DetermineRunningColor(item.Value.State, runningState);

                        //  item2.SubItems.Add(runningState);
                        item2.SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "AmiId", Text = item.Value.AmiId });

                    ListViewSubItem state = new ListViewItem.ListViewSubItem()
                    {
                        Name = "AmiState",
                            //   Text = item.Value.AmiDetail != null ? $"{item.Value.AmiDetail.State.ToString()} ({Math.Round((DateTime.Now - item.Value.AmiDetail.CreationDateTime).TotalDays).ToString()})" : "EXPIRED"
                            Text = item.Value != null ? $"{Math.Round((DateTime.Now - item.Value.CreationDateTime).TotalDays).ToString()} dagen)" : "EXPIRED"

                    };

                    state.BackColor = item.Value == null || item.Value.State == ImageState.Deregistered || item.Value.State == ImageState.Invalid
                    ? Color.PaleVioletRed : Color.LightGreen;
                    item2.SubItems.Add(state);
                    listView1.Items.Add(item2);

                });
            }
        }

        private static void DetermineRunningColor(InstanceState state, ListViewSubItem runningState)
        {
            runningState.ForeColor = Color.Black;

            switch (state)
            {
                case InstanceState.Running:
                    runningState.BackColor = Color.LightGreen;
                    break;
                case InstanceState.ShuttingDown:
                    runningState.BackColor = Color.Orange;
                    break;
                case InstanceState.Stopped:
                    runningState.BackColor = Color.PaleVioletRed;
                    break;
                case InstanceState.Stopping:
                    runningState.BackColor = Color.Orange;
                    break;
                case InstanceState.Terminated:
                    runningState.BackColor = Color.Black;
                    runningState.ForeColor = Color.White;
                    break;
                case InstanceState.Pending:
                    runningState.BackColor = Color.LightYellow;
                    break;
                default:
                    runningState.BackColor = Color.White;
                    break;
            }
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
                // Get the new sorting column.
                ColumnHeader new_sorting_column = listView1.Columns[e.Column];

                // Figure out the new sorting order.
                System.Windows.Forms.SortOrder sort_order;
                if (SortingColumn == null)
                {
                    // New column. Sort ascending.
                    sort_order = SortOrder.Ascending;
                }
                else
                {
                    // See if this is the same column.
                    if (new_sorting_column == SortingColumn)
                    {
                        // Same column. Switch the sort order.
                        if (SortingColumn.Text.StartsWith("> "))
                        {
                            sort_order = SortOrder.Descending;
                        }
                        else
                        {
                            sort_order = SortOrder.Ascending;
                        }
                    }
                    else
                    {
                        // New column. Sort ascending.
                        sort_order = SortOrder.Ascending;
                    }

                    // Remove the old sort indicator.
                    SortingColumn.Text = SortingColumn.Text.Substring(2);
                }

                // Display the new sort order.
                SortingColumn = new_sorting_column;
                if (sort_order == SortOrder.Ascending)
                {
                    SortingColumn.Text = "> " + SortingColumn.Text;
                }
                else
                {
                    SortingColumn.Text = "< " + SortingColumn.Text;
                }

                // Create a comparer.
                listView1.ListViewItemSorter =
                    new ListViewComparer(e.Column, sort_order);

            listView1.Sort();
            }

        private void tmRefresh_Tick(object sender, EventArgs e)
        {
            _ec2Machines = Fh.Aws.Framework.AWSOperations.AWSEc2.GetEc2List(EAWSDomain.Commerce, EAWSEnvironment.Dev, true, true);

            _ec2Machines.ToList().ForEach(item =>
            {
                if (!listView1.Items.ContainsKey(item.Key))
                {
                    ListViewItem item2 = new ListViewItem() { Name = item.Key, Text = item.Key };
                    item2.UseItemStyleForSubItems = false;
                    item2.SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "InstanceName", Text = item.Value.Name });

                    ListViewSubItem runningState = new ListViewItem.ListViewSubItem()
                    {
                        Name = "InstanceState",
                        Text = item.Value.State.ToString()
                    };

                    DetermineRunningColor(item.Value.State, runningState);

                    item2.SubItems.Add(runningState);
                    item2.SubItems.Add(new ListViewItem.ListViewSubItem() { Name = "InstanceAmiId", Text = item.Value.Ami });

                    ListViewSubItem state = new ListViewItem.ListViewSubItem()
                    {
                        Name = "AmiState",
                        Text = item.Value.AmiDetail != null ? item.Value.AmiDetail.State.ToString() : "EXPIRED"
                    };

                    state.BackColor = item.Value.AmiDetail == null || item.Value.AmiDetail.State == ImageState.Deregistered || item.Value.AmiDetail.State == ImageState.Invalid
                    ? Color.PaleVioletRed : Color.LightGreen;
                    item2.SubItems.Add(state);
                    listView1.Items.Add(item2);
                }
                else
                {
                    ListViewItem updateItem = listView1.Items[item.Key];
                    DetermineRunningColor(item.Value.State, updateItem.SubItems["InstanceState"]);
                    updateItem.SubItems["InstanceState"].Text = item.Value.State.ToString();

                    updateItem.SubItems["AmiState"].Text = item.Value.AmiDetail != null ? item.Value.AmiDetail.State.ToString() : "EXPIRED";
                    updateItem.SubItems["AmiState"].BackColor = item.Value.AmiDetail == null || item.Value.AmiDetail.State == ImageState.Deregistered || item.Value.AmiDetail.State == ImageState.Invalid
? Color.PaleVioletRed : Color.LightGreen;
                }

            });
        }

        private void button2_Click(object sender, EventArgs e)
        {
           // var res = Fh.Aws.Framework.AWSOperations.AWSEc2.StopInstance(EAWSDomain.Commerce, EAWSEnvironment.Dev, "i-078e00b1b63837514");
        }

        private void button3_Click(object sender, EventArgs e)
        {
          //  var res = Fh.Aws.Framework.AWSOperations.AWSEc2.StartInstance(EAWSDomain.Commerce, EAWSEnvironment.Dev, "i-078e00b1b63837514");

        }

        private void cbItemType_TextChanged(object sender, EventArgs e)
        {
            switch ((ItemType)cbItemType.SelectedItem)
            {
                case ItemType.EC2:
                    MessageBox.Show("EC2");

                    break;
                case ItemType.LaunchConfiguration:
                    MessageBox.Show("LC");

                    break;
                case ItemType.RDS:
                    MessageBox.Show("RDS");
                    break;
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            List<string> ii = new List<string>();
            ii.Add("i-0d06a91c036a2df9f");
            var x = AWSEc2Website.GetIISWebsiteSnapshot(EAWSDomain.Commerce, EAWSEnvironment.Dev, ii);
        }
    }
}
